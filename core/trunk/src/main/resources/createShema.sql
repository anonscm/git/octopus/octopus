CREATE TABLE octopustask (
    pk_octopustask numeric (12) NOT NULL, 
    process_id varchar(100) NOT NULL,
    request_id varchar(100) NOT NULL, 
    module_name varchar(50) NOT NULL,
    task_name varchar(50) NOT NULL,     
    request_data varchar(4000) NOT NULL,
    call_back_uri varchar(400),
    status numeric (1) NOT NULL,
    creation_date timestamp NOT NULL,
    update_date timestamp NOT NULL,
    PRIMARY KEY (pk_octopustask),
    UNIQUE (process_id)    
    );

