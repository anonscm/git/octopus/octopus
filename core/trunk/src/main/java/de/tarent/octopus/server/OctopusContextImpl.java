/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.octopus.server;

import java.io.File;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import de.tarent.octopus.config.CommonConfig;
import de.tarent.octopus.config.ModuleConfig;
import de.tarent.octopus.content.OctopusContent;
import de.tarent.octopus.request.OctopusRequest;
import de.tarent.octopus.response.ResponseCreator;

import java.util.ArrayList;
import de.tarent.octopus.client.OctopusTask;

/**
 * Kontainer zur Speicherung von OctopusContent, OctopusRequest und
 * OctopusConfig.
 */
public class OctopusContextImpl implements OctopusContext {
	private final OctopusRequest octopusRequest;
	private final OctopusContent octopusContent;
	private final PersonalConfig personalConfig;
	private final CommonConfig commonConfig;
	private final ModuleConfig moduleConfig;
	
	/**
	 * Erzeugt ein OctopusContextImpl Object.
	 * 
	 * @param octopusRequest
	 * @param octopusContent
	 * @param personalConfig
	 * @param commonConfig
	 * @param moduleConfig
	 */
	public OctopusContextImpl(OctopusRequest octopusRequest, OctopusContent octopusContent, PersonalConfig personalConfig, CommonConfig commonConfig, ModuleConfig moduleConfig) {
		this.octopusRequest = octopusRequest;
		this.octopusContent = octopusContent;
		this.personalConfig = personalConfig;
		this.commonConfig = commonConfig;
		this.moduleConfig = moduleConfig;
	}
	
	public Object getContextField(String fieldName) {
		// Aus dem OctopusContent
		if (fieldName.startsWith(CONTENT_FIELD_PREFIX)) {
			return contentAsObject(fieldName.substring(CONTENT_FIELD_PREFIX.length()));
		}

		// Aus dem Request
		else if (fieldName.startsWith(REQUEST_FIELD_PREFIX)) {
			return requestAsObject(fieldName.substring(REQUEST_FIELD_PREFIX.length()));
		}

		// Aus der Session
		else if (fieldName.startsWith(SESSION_FIELD_PREFIX)) {
			return sessionAsObject(fieldName.substring(SESSION_FIELD_PREFIX.length()));
		}

		// Aus der Config
		else if (fieldName.startsWith(CONFIG_FIELD_PREFIX)) {
			return moduleConfig().getParamAsObject(fieldName.substring(CONFIG_FIELD_PREFIX.length()));
		}
		
		// DEFAULT, ohne Prefix:
		// Wenn vorhanden aus dem OctopusContent, sonst aus dem Request oder der
		// Session.
		if (null != contentAsObject(fieldName))
			return contentAsObject(fieldName);
		else if (null != requestAsObject(fieldName))
			return requestAsObject(fieldName);
		else
			return sessionAsObject(fieldName);
	}
	
	public void setContextField(String fieldName, Object value) {
		// In den OctopusContent
		if (fieldName.startsWith(CONTENT_FIELD_PREFIX)) {
			setContent(fieldName.substring(CONTENT_FIELD_PREFIX.length()), value);
		}

		// In den Request - IST NICHT ERLAUBT
		else if (fieldName.startsWith(REQUEST_FIELD_PREFIX)) {
			throw new RuntimeException("Anfragefehler: Setzen von Parametern im Request ist nicht erlaubt.");
		}

		// In die Session
		else if (fieldName.startsWith(SESSION_FIELD_PREFIX)) {
			setSession(fieldName.substring(SESSION_FIELD_PREFIX.length()), value);
		}

		// In die Config - IST NICHT ERLAUBT
		else if (fieldName.startsWith(CONFIG_FIELD_PREFIX)) {
			throw new RuntimeException("Anfragefehler: Setzen von Parametern der Config ist nicht erlaubt.");
		}

		// DEFAULT, ohne Prefix:
		// In den OctopusContent
		else {
			setContent(fieldName, value);
		}
	}
	
	public OctopusTask getTask(String taskName) {
		return octopusRequest.getTask(taskName);
	}
	
	public void addCleanupCode(Closeable closeable) {
		addCleanupCodeInternal(closeable);
	}
	
	public void addCleanupCode(Runnable runnable) {
		addCleanupCodeInternal(runnable);
	}
	
	protected void addCleanupCodeInternal(Object cleanable) {
		List cleanableList = (List) contentAsObject(CLEANUP_OBJECT_LIST);
		if (cleanableList == null) {
			cleanableList = new ArrayList();
			setContent(CLEANUP_OBJECT_LIST, cleanableList);
		}
		cleanableList.add(cleanable);
	}
	
	/**
	 * @return OctopusContent-Object
	 */
	public OctopusContent getContentObject() {
		return octopusContent;
	}
	
	/**
	 * @return OctopusRequest-Object
	 */
	public OctopusRequest getRequestObject() {
		return octopusRequest;
	}
	
	// ===> C O N T E N T <===//
	
	public boolean contentContains(String key) {
		return octopusContent.getAsObject(key) != null;
	}
	
	public Iterator contentKeys() {
		return octopusContent.getKeys();
	}
	
	public String contentAsString(String key) {
		return octopusContent.getAsString(key);
	}
	
	public Object contentAsObject(String key) {
		return octopusContent.getAsObject(key);
	}
	
	public void setContent(String key, Integer value) {
		octopusContent.setField(key, value);
	}
	
	public void setContent(String key, List value) {
		octopusContent.setField(key, value);
	}
	
	public void setContent(String key, Map value) {
		octopusContent.setField(key, value);
	}
	
	public void setContent(String key, String value) {
		octopusContent.setField(key, value);
	}
	
	public void setContent(String key, Object value) {
		octopusContent.setField(key, value);
	}
	
	public void setContentError(Exception e) {
		octopusContent.setError(e);
	}
	
	public void setContentError(String message) {
		octopusContent.setError(message);
	}
	
	public void setContentStatus(String status) {
		octopusContent.setStatus(status);
	}
	
	// ===> R E Q U E S T <===//
	
	public int requestType() {
		return octopusRequest.getRequestType();
	}
	
	public String requestTypeName() {
		return OctopusRequest.getRequestTypeName(octopusRequest.getRequestType());
	}
	
	public boolean requestContains(String key) {
		return octopusRequest.containsParam(key);
	}
	
	public Object requestAsObject(String key) {
		return octopusRequest.getParam(key);
	}
	
	public String requestAsString(String key) {
		return octopusRequest.get(key);
	}
	
	public Integer requestAsInteger(String key) {
		return new Integer(octopusRequest.getParamAsInt(key));
	}
	
	public Boolean requestAsBoolean(String key) {
		return Boolean.valueOf(octopusRequest.getParameterAsBoolean(key));
	}
	
	// ===> R E S P O N S E <===//
	
	public ResponseCreator getResponseCreator() {
		return moduleConfig.getResponseCreator();
	}
	
	public String getRequestedResponseType() {
		return octopusRequest.getResponse();
	}
	
	// ===> C O N F I G <===//
	
	public CommonConfig commonConfig() {
		return commonConfig;
	}
	
	public PersonalConfig configImpl() {
		return personalConfig;
	}
	
	public ModuleConfig moduleConfig() {
		return moduleConfig;
	}
	
	public File moduleRootPath() {
		return moduleConfig.getRealPath();
	}
	
	public String getModuleName() {
		return octopusRequest.getModule();
	}
	
	public String getTaskName() {
		return octopusRequest.getTask();
	}
	
	// ===> S E S S I O N <===//
	
	public Object sessionAsObject(String key) {
		if (personalConfig == null)
			return null;
		return personalConfig.getSessionValue(key);
	}
	
	public String sessionAsString(String key) {
		Object value = sessionAsObject(key);
		if (value == null)
			return null;
		return value.toString();
	}
	
	public void setSession(String key, Object value) {
		if (personalConfig != null)
			personalConfig.setSessionValue(key, value);
	}
	
	// ===> S T A T U S <===//
	
	public void setStatus(String status) {
		octopusContent.setStatus(status);
	}
	
	public String getStatus() {
		return octopusContent.getStatus();
	}
	
	public void setError(Exception e) {
		octopusContent.setError(e);
	}
	
	public void setError(String message) {
		octopusContent.setError(message);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public OctopusContext cloneContext() {
		return cloneContext(true, true);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public OctopusContext cloneContext(boolean newRequest, boolean newContent) {
		OctopusRequest octopusRequest;
		OctopusContent octopusContent;
		if (newRequest) {
			octopusRequest = new OctopusRequest();
			octopusRequest.setModule(getModuleName());
			octopusRequest.setRequestParameters(new LinkedHashMap());
			octopusContent = new OctopusContent();
		} else {
			octopusRequest = getRequestObject();
			octopusRequest.setModule(getModuleName());
			octopusRequest.setRequestParameters(new LinkedHashMap(octopusRequest.getRequestParameters()));
			octopusContent = getContentObject();
		}
		return new OctopusContextImpl(octopusRequest, octopusContent, personalConfig, commonConfig, moduleConfig);
	}
}
