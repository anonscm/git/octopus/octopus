/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.octopus.worker;

import java.util.HashMap;
import java.util.Map;

import de.tarent.octopus.config.CommonConfig;
import de.tarent.octopus.config.ModuleConfig;
import de.tarent.octopus.content.ContentProzessException;
import de.tarent.octopus.content.ContentWorker;
import de.tarent.octopus.content.MessageDefinition;
import de.tarent.octopus.content.OperationDefinition;
import de.tarent.octopus.content.PortDefinition;
import de.tarent.octopus.server.OctopusContext;

/**
 * Worker, der die Felder des Requests in den OctopusContent schieben kann
 * 
 * @author <a href="mailto:mancke@mancke-software.de">Sebastian Mancke</a>,
 *         <b>tarent GmbH</b>
 */
public class PutParams implements ContentWorker {
	public static String CONTENT_PREFIX = "request";
	
	/**
	 * Diese Methode wird nach Erzeugung des Workers aufgerufen, so dass dieser
	 * sich im Kontext seines Moduls konfigurieren kann.
	 * 
	 * @param config
	 *            Modulkonfiguration.
	 * @see de.tarent.octopus.content.ContentWorker#init(de.tarent.octopus.config.ModuleConfig)
	 */
	public void init(ModuleConfig config) {
	}
	
	public String doAction(String actionName, OctopusContext octopusContext) throws ContentProzessException {
		
		String returnStatus = RESULT_error;
		
		if ("putMinimal".equals(actionName)) {
			returnStatus = putMinimal(octopusContext);
		} else if ("putAll".equals(actionName)) {
			returnStatus = putAll(octopusContext);
		} else {
			throw new ContentProzessException("Nicht unterstützte action im Worker 'PutParams': " + actionName);
		}
		return returnStatus;
	}
	
	public String putMinimal(OctopusContext octopusContext) {
		CommonConfig commonConfig = octopusContext.commonConfig();
		
		octopusContext.setContent("url", octopusContext.getRequestObject().get("encodedUrl"));
		octopusContext.setContent("jsessionid", octopusContext.getRequestObject().get("jsessionid"));
		
		Map paths = new HashMap();
		paths.put("templatesRelative", "");
		paths.put("staticWeb", octopusContext.moduleConfig().getRelativeWebRootPath());
		if (commonConfig.getDefaultModuleName() != null)
			paths.put("defaultStaticWeb", commonConfig.getRelativeWebRootPath(commonConfig.getDefaultModuleName()));
		octopusContext.setContent("paths", paths);
		
		return RESULT_ok;
	}
	
	public String putAll(OctopusContext octopusContext) {
		octopusContext.setContent(CONTENT_PREFIX, octopusContext.getRequestObject().getRequestParameters());
		return RESULT_ok;
	}
	
	public PortDefinition getWorkerDefinition() {
		PortDefinition port = new PortDefinition("de.tarent.octopus.content.PutRequestParams", "Worker, der die Felder des Requests in den OctopusContent schieben kann.");
		
		OperationDefinition putMinimal = port.addOperation("putMinimal", "Setzen weniger notwendiger Felder.");
		
		putMinimal.setInputMessage();
		putMinimal.setOutputMessage().addPart("url", MessageDefinition.TYPE_SCALAR, "Url des Systems mit Sessioninformationen.").addPart("jsessionid", MessageDefinition.TYPE_SCALAR, "Die Session Id.").addPart("paths", MessageDefinition.TYPE_STRUCT,
				"Pfade z.B. zu den Templateverzeichnissen.");
		
		OperationDefinition putAll = port.addOperation("putAll", "Setzen aller Felder des OctopusRequest Objektes.");
		
		putAll.setInputMessage();
		putAll.setOutputMessage().addPart(CONTENT_PREFIX, MessageDefinition.TYPE_STRUCT, "Parameter des Request.");
		
		return port;
	}
}
