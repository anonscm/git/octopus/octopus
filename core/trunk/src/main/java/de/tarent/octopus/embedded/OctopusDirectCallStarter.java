/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */
package de.tarent.octopus.embedded;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import org.apache.commons.logging.Log;

import de.tarent.octopus.config.OctopusConfigException;
import de.tarent.octopus.config.OctopusEnvironment;
import de.tarent.octopus.logging.LogFactory;
import de.tarent.octopus.request.Octopus;
import de.tarent.octopus.request.OctopusRequest;
import de.tarent.octopus.request.OctopusSession;
import de.tarent.octopus.resource.Resources;

/**
 * Ermöglicht das einfache Starten des Octopus aus einer Anwendung heraus, oder
 * als neuen Prozess.
 * 
 * @author <a href="mailto:mancke@mancke-software.de">Sebastian Mancke</a>,
 *         <b>tarent GmbH</b>
 */
public class OctopusDirectCallStarter implements OctopusStarter {
	private static final Log logger = LogFactory.getLog(OctopusStarter.class);
	
	private Octopus octopus = null;
	private OctopusEnvironment environment = null;
	
	private static Logger baseLogger = null;
	
	private File logFile = null;
	private Handler logHandler = null;
	
	// Derzeit hat ein OctopusStarter genau eine Session
	OctopusSession octopusSession = new DirectCallSession();
	
	/**
	 * Startet den Octopus in der Komandozeile
	 */
	public static void main(String[] args) {
		try {
			if (args.length > 0 && ("-h".equals(args[0]) || "--help".equals(args[0])))
				dieError();
			
			Map cfg = new HashMap();
			int i = 0;
			while (args.length > i && args[i].startsWith("-P")) {
				int pos = args[i].indexOf("=");
				if (pos == -1)
					dieError();
				
				cfg.put(args[i].substring(2, pos), args[i].substring(pos + 1));
				i++;
			}
			OctopusDirectCallStarter starter = new OctopusDirectCallStarter(cfg);
			
			Map params = new HashMap();
			for (; i + 1 < args.length; i += 2) {
				params.put(args[i], args[i + 1]);
			}
			starter.request(params);
		} catch (DirectCallException re) {
			Throwable rc = re.getRootCause();
			System.out.println("Fehler während der Octopus-Anfragebearbeitung: " + rc);
			rc.printStackTrace();
		} catch (Exception e) {
			System.out.println("Fehler während der Octopus-Anfragebearbeitung: " + e);
			e.printStackTrace();
		}
	}
	
	protected static void dieError() {
		System.out.println("Usage: octopusStarter [-Ppropname=value ..] [key value ..]");
		System.exit(1);
	}
	
	
	public OctopusDirectCallStarter() {
	}
	
	/**
	 * Inititalisiert die Komponenten, die für alle Aufrufe gleich sind.
	 * 
	 * @param configParams
	 *            Parameter, die die Konfigurationen überschreiben, darf null
	 *            sein
	 */
	public OctopusDirectCallStarter(Map configParams) {
		boolean loggerWasNull = false;
		try {
			// Statische Loggerinstanz erstellen
			if (baseLogger == null) {
				loggerWasNull = true;
				baseLogger = Logger.getLogger("de.tarent");
				baseLogger.setLevel(Level.ALL);
				
				baseLogger.getHandlers();
				
				// Keinen Consolen-Logger wenn der Octopus direkt gestartet
				// wird.
				baseLogger.setUseParentHandlers(false);
			}
			
			environment = createEnvironmentObject(configParams);
			
			// TODO: Warum wird das als System Property gesetzt?
			System.setProperty(OctopusEnvironment.KEY_PATHS_ROOT, environment.getValueAsString(OctopusEnvironment.KEY_PATHS_ROOT));
			
			// Konfigurieren der Logging Api, wenn der
			// statische looger nicht schon existierte
			if (loggerWasNull) {
				File logDir = new File(System.getProperty("user.home") + "/.tarent/octopuslog/");
				if (!logDir.exists())
					logDir.mkdirs();
				logFile = new File(logDir, "current.log");
				// //Verzeichnis anlegen, wenn es nicht bereits existiert
				// (new
				// File(environment.getValueAsString(OctopusEnvironment.KEY_PATHS_ROOT)
				// + "log")).mkdirs();
				logHandler = new FileHandler(logFile.getAbsolutePath());
				logHandler.setFormatter(new SimpleFormatter());
				baseLogger.addHandler(logHandler);
				baseLogger.info(Resources.getInstance().get("REQUESTPROXY_LOG_START_LOGGING"));
				String logLevel = environment.getValueAsString(OctopusEnvironment.KEY_LOGGING_LEVEL);
				if (logLevel != null && logLevel.length() > 0)
					try {
						Level level = Level.parse(logLevel);
						baseLogger.config(Resources.getInstance().get("REQUESTPROXY_LOG_NEW_LOG_LEVEL", level));
						baseLogger.setLevel(level);
					} catch (IllegalArgumentException iae) {
						baseLogger.log(Level.WARNING, Resources.getInstance().get("REQUESTPROXY_LOG_INVALID_LOG_LEVEL", logLevel), iae);
					}
			}
			
			octopus = new Octopus();
			octopus.init(environment, new DirectCallModuleLookup(this));
		} catch (Exception e) {
			logger.error(Resources.getInstance().get("REQUESTPROXY_LOG_INIT_EXCEPTION"), e);
			throw new RuntimeException("Fehler beim Initialisieren des lokalen Octopus", e);
		}
		
		logger.trace("RequextProxy init");
	}
	
	public void destroy() {
		if (octopus != null)
			try {
				octopus.deInit();
			} catch (Exception e) {
				logger.warn(Resources.getInstance().get("REQUESTPROXY_LOG_CLEANUP_EXCEPTION"), e);
			}
		
		// TODO: Sollte der logHandler wirklich geschlossen werden?
		// Der baseLogger list ja Statisch und somit in
		// anderen Instanzen evt. noch aktiv
		baseLogger.removeHandler(logHandler);
		logHandler.close();
	}
	
	
	/**
	 * Startet die Abarbeitung einer Anfrage
	 */
	public OctopusDirectCallResult request(Map requestParams) throws DirectCallException {
		
		logger.debug(Resources.getInstance().get("REQUESTPROXY_LOG_REQUEST_PROCESSING_START"));
		
		try {
			DirectCallResponse response = new DirectCallResponse();
			logger.trace(Resources.getInstance().get("REQUESTPROXY_LOG_RESPONSE_OBJECT_CREATED"));
			logger.trace(Resources.getInstance().get("REQUESTPROXY_LOG_SESSION_OBJECT_CREATED"));
			
			OctopusRequest request = new OctopusRequest();
			request.setRequestParameters(requestParams);
			request.setParam(OctopusRequest.PARAM_SESSION_ID, octopusSession.getId());
			logger.debug(Resources.getInstance().get("REQUESTPROXY_LOG_REQUEST_OBJECT_CREATED"));
			
			octopus.dispatch(request, response, octopusSession);
			
			response.flush();
			return new OctopusDirectCallResult(response);
		} catch (Exception e) {
			logger.error(Resources.getInstance().get("REQUESTPROXY_LOG_PROCESSING_EXCEPTION"), e);
			throw new DirectCallException(Resources.getInstance().get("REQUESTPROXY_LOG_PROCESSING_EXCEPTION"), e);
		}
		
	}
	
	
	/**
	 * Erstellt eine default-Konfiguration im OctopusEnvironment Anschließend
	 * werden diese Werte durch die im Knoten /de/tarent/octopus/overrides der
	 * Java-System-Prefferences überschrieben.
	 * 
	 * @param overrideSettings
	 *            Parameter, die die Konfigurationen überschreiben, darf null
	 *            sein
	 * @throws OctopusConfigException
	 */
	protected OctopusEnvironment createEnvironmentObject(Map overrideSettings) throws OctopusConfigException {
		// Env Objekt mit Umgebungsvariablen und Einstellungsparametern
		OctopusEnvironment environment = new OctopusEnvironment();
		
		// environment.setValue(OctopusEnvironment.KEY_PATHS_ROOT, "." +
		// System.getProperty("file.separator"));
		environment.setValue(OctopusEnvironment.KEY_PATHS_ROOT, System.getProperty("user.dir") + "/OCTOPUS/");
		environment.setValue(OctopusEnvironment.KEY_LOGGING_LEVEL, "CONFIG");
		environment.setValue(OctopusEnvironment.KEY_PATHS_CONFIG_ROOT, "config/");
		environment.setValue(OctopusEnvironment.KEY_PATHS_CONFIG_FILE, "config.xml");
		// environment.setValue(OctopusEnvironment.loggerConfigFile,
		// "logger.conf");
		// environment.setValue(OctopusEnvironment.paths.pageDescriptionRoot, );
		environment.setValue(OctopusEnvironment.KEY_PATHS_TEMPLATE_ROOT, "templates/");
		environment.overrideValues("base", "/de/tarent/octopus/overrides");
		
		if (null != overrideSettings)
			for (Iterator iter = overrideSettings.keySet().iterator(); iter.hasNext();) {
				String key = "" + iter.next();
				environment.setValue(key, "" + overrideSettings.get(key));
			}
		
		return environment;
	}
	
	public OctopusEnvironment getEnvironment() {
		return environment;
	}
}
