package de.tarent.octopus.response;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import de.tarent.octopus.request.OctopusResponse;
import de.tarent.octopus.server.OctopusContext;

public abstract class AbstractResponseEngine implements ResponseEngine {
	public final String PARAM_ENCODING_DEFAULT;
	public final String PARAM_MIMETYPE_DEFAULT;
	public final String PARAM_OUTPUT_DEFAULT;
	
	public static final String PARAM_ENCODING_FALLBACK = "encoding";
	public static final String PARAM_MIMETYPE_FALLBACK = "mimetype";
	public static final String PARAM_OUTPUT_FALLBACK = "output";
	
	public AbstractResponseEngine() {
		PARAM_ENCODING_DEFAULT = getDefaultName() + "." + PARAM_ENCODING_FALLBACK;
		PARAM_MIMETYPE_DEFAULT = getDefaultName() + "." + PARAM_MIMETYPE_FALLBACK;
		PARAM_OUTPUT_DEFAULT = getDefaultName() + "." + PARAM_OUTPUT_FALLBACK;
	}
	
	public String getEncodingParameter() {
		return PARAM_ENCODING_DEFAULT;
	}
	
	public String getContentTypeParameter() {
		return PARAM_MIMETYPE_DEFAULT;
	}
	
	public String getOutputParameter() {
		return PARAM_OUTPUT_DEFAULT;
	}
	
	public void performEncoding(ResponseDescription desc, OctopusContext octopusContext, OctopusResponse octopusResponse) {
		String encoding = (String) desc.getParameter(getEncodingParameter());
		if (encoding == null)
			encoding = (String) desc.getParameter(PARAM_ENCODING_FALLBACK);
		//octopusResponse.setContentType(contentType);
	}
	
	public void performContentType(ResponseDescription desc, OctopusContext octopusContext, OctopusResponse octopusResponse) {
		String contentType = (String) desc.getParameter(getEncodingParameter());
		if (contentType == null)
			contentType = (String) desc.getParameter(PARAM_MIMETYPE_FALLBACK);
		if (contentType == null)
			contentType = getDefaultContentType();
		octopusResponse.setContentType(contentType);
	}
	
	public Map getOutputFields(ResponseDescription desc, OctopusContext octopusContext, OctopusResponse octopusResponse) {
		Object outputFields = desc.getParameter(getOutputParameter());
		if (outputFields == null)
			outputFields = desc.getParameter(PARAM_OUTPUT_FALLBACK);
		if (outputFields == null)
			return null;
		return getOutputFields(outputFields, octopusContext);
	}
	
	public static Map getOutputFields(Object outputFields, OctopusContext octopusContext) {
		if (outputFields == null) {
			return Collections.EMPTY_MAP;
		} else if (outputFields instanceof Map) {
			Map result = new LinkedHashMap();
			for (Iterator it = ((Map) outputFields).entrySet().iterator(); it.hasNext(); ) {
				Map.Entry entry = (Map.Entry) it.next();
				result.put(entry.getKey(), octopusContext.contentAsObject((String) entry.getValue()));
			}
			return result;
		} else if (outputFields instanceof Collection) {
			Map result = new LinkedHashMap();
			for (Iterator it = ((Collection) outputFields).iterator(); it.hasNext(); ) {
				Object key = it.next();
				result.put(key, octopusContext.contentAsObject((String) key));
			}
			return result;
		} else if (outputFields instanceof Object[]) {
			Map result = new LinkedHashMap();
			for (Iterator it = Arrays.asList(((Object[]) outputFields)).iterator(); it.hasNext(); ) {
				Object key = it.next();
				result.put(key, octopusContext.contentAsObject((String) key));
			}
			return result;
		} else if (outputFields instanceof String) {
			Map result = new LinkedHashMap();
			result.put( outputFields, octopusContext.contentAsObject((String) outputFields));
			return result;
		} else {
			throw new IllegalArgumentException(
					"Unsupported data type " +
					outputFields.getClass().getName() +
					" for getting output fields.");
		}
	}
	
	public static Map getOutputParameters(Object outputFields) {
		if (outputFields == null) {
			return Collections.EMPTY_MAP;
		} else if (outputFields instanceof Map) {
			return (Map) outputFields;
		} else if (outputFields instanceof Collection) {
			Map result = new LinkedHashMap();
			for (Iterator it = ((Collection) outputFields).iterator(); it.hasNext(); ) {
				Object key = it.next();
				result.put(key, key);
			}
			return result;
		} else if (outputFields instanceof Object[]) {
			Map result = new LinkedHashMap();
			for (Iterator it = Arrays.asList(((Object[]) outputFields)).iterator(); it.hasNext(); ) {
				Object key = it.next();
				result.put(key, key);
			}
			return result;
		} else if (outputFields instanceof String) {
			Map result = new LinkedHashMap();
			result.put(outputFields, outputFields);
			return result;
		} else {
			throw new IllegalArgumentException(
					"Unsupported data type " +
					outputFields.getClass().getName() +
					" for getting output parameters.");
		}
	}
}
