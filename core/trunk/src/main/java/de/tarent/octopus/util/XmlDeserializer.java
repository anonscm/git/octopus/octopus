/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */
package de.tarent.octopus.util;

import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;

import com.thoughtworks.xstream.XStream;

/**
 * @author Alex Maier, tarent GmbH
 * 
 */
public class XmlDeserializer {
	
	public static final String CHAR_SET_ISO_8859_1 = "ISO-8859-1";
	public static final String CHAR_SET_UTF8 = "UTF-8";
	
	/**
	 * Deserialize the given XML representation <code>xml</code> of an
	 * serialized Object to Object. By converting of the string to a byte array
	 * will be used the default charset encoding {@link #CHAR_SET_UTF8}.
	 * 
	 * @param xml
	 *            The XML representation of an serialized Object.
	 * @return The Object which was deserialized from given xml string.
	 * @throws UnsupportedEncodingException
	 */
	public static Object xmlToObject(String xml) throws UnsupportedEncodingException {
		return xmlToObject(xml, CHAR_SET_UTF8);
	}
	
	/**
	 * By converting of the string will be used the given charset encoding
	 * <code>charset</code>
	 * 
	 * @param xml
	 *            The XML representation of an serialized Object.
	 * @param charset
	 *            Charset encoding, which should be used by converting the
	 *            string to a byte array.
	 * @return The Object which was deserialized from given xml string.
	 * @throws UnsupportedEncodingException
	 */
	public static Object xmlToObject(String xml, String charset) throws UnsupportedEncodingException {
		ByteArrayInputStream bais = new ByteArrayInputStream(xml.getBytes(charset));
		XStream xstream = new XStream();
		return xstream.fromXML(bais);
	}
}
