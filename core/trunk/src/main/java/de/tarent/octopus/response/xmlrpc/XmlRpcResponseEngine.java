/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */
package de.tarent.octopus.response.xmlrpc;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Map;

import org.apache.commons.logging.Log;

import de.tarent.octopus.config.ModuleConfig;
import de.tarent.octopus.logging.LogFactory;
import de.tarent.octopus.request.OctopusResponse;
import de.tarent.octopus.response.AbstractResponseEngine;
import de.tarent.octopus.response.ResponseDescription;
import de.tarent.octopus.response.ResponseEngine;
import de.tarent.octopus.response.ResponseProcessingException;
import de.tarent.octopus.server.OctopusContext;

/**
 * Diese Klasse dient der Ausgabe von XML-RPC-Rückgaben.
 * 
 * @author Michael Klink, tarent GmbH
 */
public class XmlRpcResponseEngine extends AbstractResponseEngine implements ResponseEngine {
	//
	// Membervariablen
	//
	private static Log logger = LogFactory.getLog(XmlRpcResponseEngine.class);
	
	public String getDefaultName() {
		return "xmlRpc";
	}

	public String getDefaultContentType() {
		return "application/xml+rpc";
	}

	public void init(ModuleConfig moduleConfig) {
	}

	public void sendEmptyResponse(ResponseDescription desc, OctopusContext octopusContext, OctopusResponse octopusResponse) throws ResponseProcessingException {
		try {
			logger.debug("Gebe XML-RPC-Message aus. Antwort auf Methode:" + octopusResponse.getTaskName());
			XmlRpcBuffer buffer = new XmlRpcBuffer();
			buffer.appendResponse(null);
			
			ByteBuffer byteBuffer = buffer.toByteBuffer();
			byte[] bytes = new byte[byteBuffer.remaining()];
			byteBuffer.get(bytes);
			octopusResponse.getOutputStream().write(bytes);
			
			octopusResponse.getOutputStream().flush();
			octopusResponse.flush();
		} catch (IOException e) {
			logger.error("Versuche, eine XML-RPC-Fault auszugeben.", e);
			try {
				XmlRpcBuffer buffer = new XmlRpcBuffer();
				buffer.appendError(0, "Error while writing xml rpc response: " + e.getMessage());
				ByteBuffer byteBuffer = buffer.toByteBuffer();
				byte[] bytes = new byte[byteBuffer.remaining()];
				byteBuffer.get(bytes);
				octopusResponse.getOutputStream().write(bytes);
				
				octopusResponse.getOutputStream().flush();
				octopusResponse.flush();
			} catch (IOException e2) {
				logger.error("Es konnte auch keine XML-RPC Fehlermeldung ausgegeben werden. Schmeiße jetzt einfach eine Exception.", e2);
				throw new ResponseProcessingException("Es ist Fehler bei der Formatierung der Ausgabe ausgetreten.", e);
			}
		}
	}
	
	public void sendNormalResponse(ResponseDescription desc, OctopusContext octopusContext, OctopusResponse octopusResponse) throws ResponseProcessingException {
		Map outputFields = getOutputFields(desc, octopusContext, octopusResponse);
		
		try {
			logger.debug("Gebe XML-RPC-Message aus. Antwort auf Methode:" + octopusResponse.getTaskName());
			XmlRpcBuffer buffer = new XmlRpcBuffer();
			if (outputFields == null || outputFields.isEmpty()) {
				buffer.appendResponse(null);
			} else if (outputFields.size() == 1) {
				buffer.appendResponse(outputFields.values().iterator().next());
			} else {
				buffer.appendResponse(outputFields);
			}
			
			ByteBuffer byteBuffer = buffer.toByteBuffer();
			byte[] bytes = new byte[byteBuffer.remaining()];
			byteBuffer.get(bytes);
			octopusResponse.getOutputStream().write(bytes);
			
			octopusResponse.getOutputStream().flush();
			octopusResponse.flush();
		} catch (IOException e) {
			logger.error("Versuche, eine XML-RPC-Fault auszugeben.", e);
			try {
				XmlRpcBuffer buffer = new XmlRpcBuffer();
				buffer.appendError(0, "Error while writing xml rpc response: " + e.getMessage());
				ByteBuffer byteBuffer = buffer.toByteBuffer();
				byte[] bytes = new byte[byteBuffer.remaining()];
				byteBuffer.get(bytes);
				octopusResponse.getOutputStream().write(bytes);
				
				octopusResponse.getOutputStream().flush();
				octopusResponse.flush();
			} catch (IOException e2) {
				logger.error("Es konnte auch keine XML-RPC Fehlermeldung ausgegeben werden. Schmeiße jetzt einfach eine Exception.", e2);
				throw new ResponseProcessingException("Es ist Fehler bei der Formatierung der Ausgabe ausgetreten.", e);
			}
		}
	}

	public void sendErrorResponse(ResponseDescription desc, OctopusContext octopusContext, OctopusResponse octopusResponse, String message, Exception exception) throws ResponseProcessingException {
		try {
			logger.debug("Gebe XML-RPC-Message aus. Antwort auf Methode:" + octopusResponse.getTaskName());
			XmlRpcBuffer buffer = new XmlRpcBuffer();
			buffer.appendError(0, message);
			
			ByteBuffer byteBuffer = buffer.toByteBuffer();
			byte[] bytes = new byte[byteBuffer.remaining()];
			byteBuffer.get(bytes);
			octopusResponse.getOutputStream().write(bytes);
			
			octopusResponse.getOutputStream().flush();
			octopusResponse.flush();
		} catch (IOException e) {
			logger.error("Versuche, eine XML-RPC-Fault auszugeben.", e);
			try {
				XmlRpcBuffer buffer = new XmlRpcBuffer();
				buffer.appendError(0, "Error while writing xml rpc response: " + e.getMessage());
				ByteBuffer byteBuffer = buffer.toByteBuffer();
				byte[] bytes = new byte[byteBuffer.remaining()];
				byteBuffer.get(bytes);
				octopusResponse.getOutputStream().write(bytes);
				
				octopusResponse.getOutputStream().flush();
				octopusResponse.flush();
			} catch (IOException e2) {
				logger.error("Es konnte auch keine XML-RPC Fehlermeldung ausgegeben werden. Schmeiße jetzt einfach eine Exception.", e2);
				throw new ResponseProcessingException("Es ist Fehler bei der Formatierung der Ausgabe ausgetreten.", e);
			}
		}
	}
	
	public void sendEmptyCallback(ResponseDescription desc, OctopusContext octopusContext, OctopusResponse octopusResponse) throws ResponseProcessingException {
		sendEmptyResponse(desc, octopusContext, octopusResponse);
	}
	
	public void sendNormalCallback(ResponseDescription desc, OctopusContext octopusContext, OctopusResponse octopusResponse) throws ResponseProcessingException {
		sendNormalResponse(desc, octopusContext, octopusResponse);
	}
	
	public void sendErrorCallback(ResponseDescription desc, OctopusContext octopusContext, OctopusResponse octopusResponse, String message, Exception exception) throws ResponseProcessingException {
		sendErrorResponse(desc, octopusContext, octopusResponse, message, exception);
	}
}
