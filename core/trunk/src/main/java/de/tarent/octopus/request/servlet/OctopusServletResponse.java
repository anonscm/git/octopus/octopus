/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.octopus.request.servlet;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;

import de.tarent.octopus.config.OctopusEnvironment;
import de.tarent.octopus.content.CookieMap;
import de.tarent.octopus.logging.LogFactory;
import de.tarent.octopus.request.OctopusResponse;

/**
 * Stellt Funktionen zur Ausgabe an den Client bereit. Diese werden an die
 * HttpServletResponse weiter geleitet. <br>
 * <br>
 * Bevor etwas ausgegeben werden kann, muss der ContentType gesetzt werden.
 * 
 * <br>
 * <br>
 * Es mag verwirren, daß die Klasse OctopusResponse im Package octopusRequest
 * ist und nicht im Package tcResponse wo sie dem Namen nach hin gehört. Das
 * macht aber so Sinn, da sie wie auch RequestProxy und OctopusRequest die
 * Schnittstelle zum Client kapselt und somit protokollspezifisches Verhalten
 * hat, vondem in allen anderen Packages völlig abstrahiert wird.
 * 
 * @author <a href="mailto:mancke@mancke-software.de">Sebastian Mancke</a>,
 *         <b>tarent GmbH</b>
 */
public class OctopusServletResponse implements OctopusResponse {
	private final static long MILLISECONDS_PER_YEAR = 1000l * 60l * 60l * 24l * 365l;
	
	public static final String WWW_AUTHENTICATE = "wwwAuthenticate";
	private static Log logger = LogFactory.getLog(OctopusResponse.class);
	protected HttpServletResponse response;
	protected PrintWriter writer;
	private String taskName;
	private String moduleName;
	private int cachingTime = -1;
	private List cachingParam = null;
	private String errorLevel = OctopusEnvironment.VALUE_RESPONSE_ERROR_LEVEL_DEVELOPMENT;
	
	private OutputStream outputStream;
	
	/**
	 * Initialisierung mit den Servletparametern.
	 * 
	 * @param response
	 *            Die Response des Servletkontainers,
	 */
	public OctopusServletResponse(HttpServletResponse response) throws IOException {
		this.response = response;
		outputStream = response.getOutputStream();
		writer = new PrintWriter(outputStream, true);
	}
	
	/**
	 * Gibt einen Writer für die Ausgabe zurück. <br>
	 * <br>
	 * Bevor etwas ausgegeben werden kann, muss der ContentType gesetzt werden.
	 */
	public PrintWriter getWriter() {
		return writer;
	}
	
	/**
	 * Setzt den Mime-Type für die Ausgabe. Das muss passiert sein, bevor etwas
	 * ausgegeben wurde.
	 */
	public void setContentType(String contentType) {
		response.setContentType(contentType);
		logger.trace("Habe ContentType: '" + contentType + "' gesetzt.");
	}
	
	/**
	 * Setzt den Status für die Ausgabe. Das muss passiert sein, bevor etwas
	 * ausgegeben wurde.
	 */
	public void setStatus(int code) {
		response.setStatus(code);
		logger.trace("Habe Status: '" + code + "' gesetzt.");
	}
	
	/**
	 * Setzt einen Header-Parameter. Das muss passiert sein, bevor etwas
	 * ausgegeben wurde.
	 */
	public void setHeader(String key, String value) {
		response.setHeader(key, value);
		if (logger.isTraceEnabled())
			logger.trace("Habe Header-Info '" + key + "' auf '" + value + "' gesetzt.");
	}
	
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}
	
	public String getTaskName() {
		return taskName;
	}
	
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	
	public String getModuleName() {
		return moduleName;
	}
	
	/**
	 * Returns the outputStream.
	 * 
	 * @return OutputStream
	 */
	public OutputStream getOutputStream() {
		return outputStream;
	}
	
	/**
	 * Gibt einen String auf den Weiter aus.
	 */
	public void print(String responseString) {
		writer.print(responseString);
	}
	
	/**
	 * Gibt einen String + "\n" auf den Weiter aus.
	 */
	public void println(String responseString) {
		writer.println(responseString);
	}
	
	/**
	 * Diese Methode sendet gepufferte Ausgaben.
	 */
	public void flush() throws IOException {
		response.flushBuffer();
		outputStream.flush();
		writer.flush();
	}
	
	/**
	 * Diese Methode schließt die Ausgaben ab.
	 */
	public void close() throws IOException {
		outputStream.close();
		writer.close();
	}
	
	public void setAuthorisationRequired(String authorisationAction) {
		if (WWW_AUTHENTICATE.equalsIgnoreCase(authorisationAction)) {
			response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			response.setHeader("WWW-Authenticate", "Basic realm=\"" + getModuleName() + "\"");
		}
	}
	
	public void setCachingTime(int millis) {
		setCachingTime(millis, null);
	}
	
	public void setCachingTime(int millis, String param) {
		cachingTime = millis;
		cachingParam = param != null ? Arrays.asList(param.split(",")) : null;
		long now = System.currentTimeMillis();
		if (cachingTime <= 0) {
			if (cachingParam == null || cachingParam.indexOf("nocachecontrol") == -1) {
				response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
				response.addHeader("Cache-Control", "post-check=0, pre-check=0");
			}
			response.setDateHeader("Expires", 0);
			response.setDateHeader("Last-Modified", now);
			if (cachingParam == null || cachingParam.indexOf("nopragma") == -1) {
				response.setHeader("Pragma", "no-cache");
			}
		} else if (cachingTime < Integer.MAX_VALUE) {
			if (cachingParam == null || cachingParam.indexOf("nocachecontrol") == -1) {
				response.setHeader("Cache-Control", "max-age=" + (cachingTime / 1000));
			}
			response.setDateHeader("Expires", now + cachingTime);
			response.setDateHeader("Last-Modified", now - now % cachingTime);
			response.setHeader("Pragma", "");
		} else {
			if (cachingParam == null || cachingParam.indexOf("nocachecontrol") == -1) {
				response.setHeader("Cache-Control", "");
			}
			response.setDateHeader("Expires", now + MILLISECONDS_PER_YEAR);
			response.setDateHeader("Last-Modified", 0);
			response.setHeader("Pragma", "");
		}
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.tarent.octopus.request.OctopusResponse#getCachingTime()
	 */
	public int getCachingTime() {
		return cachingTime;
	}
	
	/**
	 * Adds a cookie to the response. Default cookie setting can be set in the
	 * config. See {@link de.tarent.octopus.content.CookieMap} for detailed
	 * settings.
	 * 
	 * @param name
	 * @param value
	 * @param settings
	 */
	public void addCookie(String name, String value, Map settings) {
		Cookie cookie = new Cookie(name, value);
		if (settings.get(CookieMap.CONFIG_MAXAGE) != null)
			cookie.setMaxAge(Integer.valueOf((String) settings.get(CookieMap.CONFIG_MAXAGE)).intValue());
		response.addCookie(cookie);
	}
	
	/**
	 * Adds a cookie to the response.
	 * 
	 * Because the dispatched classes in the octopus-core does not know the
	 * Servlet-API and the Cookie-Object this method accepts an Object as
	 * parameter and adds this to cookies in case it is a Cookie-Object.
	 * 
	 * @param cookie
	 */
	public void addCookie(Object cookie) {
		if (cookie instanceof Cookie)
			response.addCookie((Cookie) cookie);
	}
	
	/**
	 * Set output level for errors.
	 */
	public void setErrorLevel(String errorLevel) {
		if (errorLevel != null && errorLevel.length() > 0)
			this.errorLevel = errorLevel;
	}
	
	public boolean isErrorLevelRelease() {
		return errorLevel.equals(OctopusEnvironment.VALUE_RESPONSE_ERROR_LEVEL_RELEASE);
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.tarent.octopus.request.OctopusResponse#isDirectCall()
	 */
	public boolean isDirectCall() {
		return false;
	}
}
