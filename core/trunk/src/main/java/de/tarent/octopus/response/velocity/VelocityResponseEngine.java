/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.octopus.response.velocity;

import java.io.File;
import java.io.Reader;
import java.io.Writer;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.VelocityContext;

import de.tarent.octopus.config.ModuleConfig;
import de.tarent.octopus.content.CookieMap;
import de.tarent.octopus.content.OctopusContent;
import de.tarent.octopus.logging.LogFactory;
import de.tarent.octopus.request.OctopusRequest;
import de.tarent.octopus.request.OctopusResponse;
import de.tarent.octopus.response.AbstractResponseEngine;
import de.tarent.octopus.response.ResponseDescription;
import de.tarent.octopus.response.ResponseEngine;
import de.tarent.octopus.response.ResponseProcessingException;
import de.tarent.octopus.server.OctopusContext;

/**
 * This class merge the octopus octopusContent with a velocity script.
 * 
 * @author <a href="mailto:h.helwich@tarent.de">Hendrik Helwich</a>, <b>tarent
 *         GmbH</b>
 */
public class VelocityResponseEngine extends AbstractResponseEngine implements ResponseEngine {
	/** Velocity template */
	public static final String VELOCITY_TEMPLATE = "velocityTemplate";
	/** Velocity suffix */
	public static final String VELOCITY_SUFFIX = ".vm";

	/** Velocity octopusContent-key for the {@link OctopusConfig}. */
	public static final String PARAM_NAME_CONFIG = "octopusConfig";
	/** Velocity octopusContent-key for the {@link OctopusRequest}. */
	public static final String PARAM_NAME_REQUEST = "octopusRequest";
	/** Velocity octopusContent-key for the {@link OctopusContent}. */
	public static final String PARAM_NAME_CONTENT = "octopusContent";
	/** Velocity octopusContent-key for the {@link OctopusResponse}. */
	public static final String PARAM_NAME_RESPONSE = "octopusResponse";
	/** Velocity octopusContent-key for the {@link OctopusContext}. */
	public static final String PARAM_NAME_CONTEXT = "octopusContext";
	/** Octopus octopusContent-key for the {@link Writer}. */
	private static final String OCTOPUS_RESPONSEENGINE = "octopusResponseEngine";
	/** Octopus octopusContent-key for the {@link VelocityResponseEngine}. */
	private static final String OCTOPUS_RESPONSESTREAM = "octopusResponseStream";
	/** Octopus octopusContent-key for the {@link VelocityContext}. */
	private static final String OCTOPUS_RESPONSECONTEXT = "octopusResponseContext";
	
	/** Logger instance */
	private Log logger;
	/** Velocity engine instance */
	private VelocityEngine engine;
	/** Velocity script rootpath */
	private File rootPath;
	
	public String getDefaultName() {
		return "velocity";
	}

	public String getDefaultContentType() {
		return "text/html";
	}

	/**
	 * Init this response engine.
	 */
	public void init(ModuleConfig moduleConfig) {
		logger = LogFactory.getLog(VelocityResponseEngine.class);
		engine = new VelocityEngine();
		try {
			rootPath = new File(moduleConfig.getTemplateRootPath(), "velocity");
			logger.debug("Velocity rootpath: " + rootPath);
			Properties properties = new Properties();
			updateProperty(properties, "file.resource.loader.path", rootPath.getAbsolutePath());
			updateProperty(properties, "velocimacro.library", moduleConfig.getParam("velocity.macro.library"));
			updateProperty(properties, "velocimacro.permissions.allow.inline", moduleConfig.getParam("velocity.macro.permissions.allow.inline"));
			updateProperty(properties, "velocimacro.permissions.allow.inline.to.replace.global", moduleConfig.getParam("velocity.macro.permissions.allow.inline.to.replace.global"));
			updateProperty(properties, "velocimacro.permissions.allow.inline.local.scope", moduleConfig.getParam("velocity.macro.permissions.allow.inline.local.scope"));
			updateProperty(properties, "velocimacro.context.localscope", moduleConfig.getParam("velocity.macro.context.localscope"));
			
			String loggerClass = moduleConfig.getParam("velocity.log.system.class");
			if (loggerClass != null && loggerClass.trim().length() > 0)
				updateProperty(properties, "runtime.log.logsystem.class", loggerClass);
			else
				updateProperty(properties, "runtime.log.logsystem.class", VelocityResponseLogger.class.getName());
			
			engine.init(properties);
		} catch (Exception e) {
			logger.error("Fehler beim Init der Velocity Engine.", e);
		}
	}

	private void updateProperty(Properties properties, String key, String value) {
		if (value == null || value.length() == 0)
			properties.remove(key);
		else
			properties.setProperty(key, value);
	}

	public void sendEmptyResponse(ResponseDescription desc, OctopusContext octopusContext, OctopusResponse octopusResponse) throws ResponseProcessingException {
		sendNormalResponse(desc, octopusContext, octopusResponse);
	}
	
	/**
	 * Return a response.
	 */
	public void sendNormalResponse(ResponseDescription desc, OctopusContext octopusContext, OctopusResponse octopusResponse) throws ResponseProcessingException {
		OctopusContent octopusContent = octopusContext.getContentObject();
		
		// adding cookies (e.g. set by PersonalConfig)
		Map cookiesSettings = new HashMap(1);
		cookiesSettings.put(CookieMap.CONFIG_MAXAGE, octopusContext.moduleConfig().getParam(CookieMap.PREFIX_CONFIG_MAP + "." + CookieMap.CONFIG_MAXAGE));
		Map cookiesMap = (Map) octopusContext.contentAsObject(CookieMap.PREFIX_COOKIE_MAP);
		if (cookiesMap != null) {
			Iterator iter = cookiesMap.keySet().iterator();
			while (iter.hasNext()) {
				String key = (String) iter.next();
				Map cookieMap = (Map) cookiesMap.get(key);
				if (cookieMap.get(CookieMap.COOKIE_MAP_FIELD_COOKIE) != null)
					octopusResponse.addCookie(cookieMap.get(CookieMap.COOKIE_MAP_FIELD_COOKIE));
				else
					octopusResponse.addCookie(key, (String) cookieMap.get(CookieMap.COOKIE_MAP_FIELD_VALUE), cookiesSettings);
			}
		}
		
		File template = new File(rootPath, desc.getAsString(VELOCITY_TEMPLATE));
		if (!template.exists())
			template = new File(rootPath, desc.getAsString(VELOCITY_TEMPLATE) + VELOCITY_SUFFIX);
		if (!template.exists())
			throw new ResponseProcessingException("Template '" + template + "' not found.");
		
		Writer writer = octopusResponse.getWriter();
		
		try {
			VelocityContext context = new VelocityContext();
			
			String key;
			for (Iterator it = octopusContent.getKeys(); it.hasNext();) {
				key = (String) it.next();
				context.put(key, octopusContent.get((key)));
			}
			
			octopusContent.setField(OCTOPUS_RESPONSEENGINE, this);
			octopusContent.setField(OCTOPUS_RESPONSESTREAM, writer);
			octopusContent.setField(OCTOPUS_RESPONSECONTEXT, context);
			
			context.put(PARAM_NAME_REQUEST, octopusContext.getRequestObject());
			context.put(PARAM_NAME_CONTENT, octopusContext.getContentObject());
			context.put(PARAM_NAME_RESPONSE, octopusResponse);
			context.put(PARAM_NAME_CONTEXT, octopusContext);
			
			engine.mergeTemplate(template.getName(), octopusContext.moduleConfig().getDefaultEncoding(), context, writer);
			
			writer.close();
		} catch (Exception e) {
			logger.error("Fehler beim Erzeugen der Ausgabeseite mit Velocity-Ausgabeseite '" + template + "'.", e);
			throw new ResponseProcessingException("Fehler beim Erzeugen der Velocity-Ausgabeseite '" + template + "'.", e);
		}
	}

	public void sendErrorResponse(ResponseDescription desc, OctopusContext octopusContext, OctopusResponse octopusResponse, String message, Exception exception) throws ResponseProcessingException {
		sendNormalResponse(desc, octopusContext, octopusResponse);
	}
	
	public void sendEmptyCallback(ResponseDescription desc, OctopusContext octopusContext, OctopusResponse octopusResponse) throws ResponseProcessingException {
		sendEmptyResponse(desc, octopusContext, octopusResponse);
	}
	
	public void sendNormalCallback(ResponseDescription desc, OctopusContext octopusContext, OctopusResponse octopusResponse) throws ResponseProcessingException {
		sendNormalResponse(desc, octopusContext, octopusResponse);
	}
	
	public void sendErrorCallback(ResponseDescription desc, OctopusContext octopusContext, OctopusResponse octopusResponse, String message, Exception exception) throws ResponseProcessingException {
		sendErrorResponse(desc, octopusContext, octopusResponse, message, exception);
	}
	
	/**
	 * Allow additional template merge with a reader.
	 * 
	 * @param cntx
	 * @param reader
	 * @throws Exception
	 */
	static public void mergeTemplate(OctopusContext cntx, Reader reader) throws Exception {
		VelocityResponseEngine responseEngine = (VelocityResponseEngine) cntx.contentAsObject(OCTOPUS_RESPONSEENGINE);
		responseEngine.engine.evaluate((VelocityContext) cntx.contentAsObject(OCTOPUS_RESPONSECONTEXT), (Writer) cntx.contentAsObject(OCTOPUS_RESPONSESTREAM), reader.toString(), reader);
	}
}
