/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.octopus.request;

import java.io.OutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import de.tarent.octopus.embedded.DirectCallResponse;

/**
 * Stellt Funktionen zur Ausgabe an den Client bereit. Diese werden an die
 * HttpServletResponse weiter geleitet. <br>
 * <br>
 * Bevor etwas ausgegeben werden kann, muss der ContentType gesetzt werden.
 * 
 * <br>
 * <br>
 * Es mag verwirren, daß die Klasse OctopusResponse im Package octopusRequest
 * ist und nicht im Package tcResponse wo sie dem Namen nach hin gehört. Das
 * macht aber so Sinn, da sie wie auch RequestProxy und OctopusRequest die
 * Schnittstelle zum Client kapselt und somit protokollspezifisches Verhalten
 * hat, vondem in allen anderen Packages völlig abstrahiert wird.
 * 
 * @author <a href="mailto:mancke@mancke-software.de">Sebastian Mancke</a>,
 *         <b>tarent GmbH</b>
 */
public interface OctopusResponse {
	
	/**
	 * Gibt einen Writer für die Ausgabe zurück. <br>
	 * <br>
	 * Bevor etwas ausgegeben werden kann, muss der ContentType gesetzt werden.
	 */
	public PrintWriter getWriter();
	
	/**
	 * Setzt den Mime-Type für die Ausgabe. Das muss passiert sein, bevor etwas
	 * ausgegeben wurde.
	 */
	public void setContentType(String contentType);
	
	/**
	 * Setzt den Status für die Ausgabe. Das muss passiert sein, bevor etwas
	 * ausgegeben wurde.
	 */
	public void setStatus(int code);
	
	/**
	 * Setzt das Ausgabe-Level für geworfene Fehlermeldungen.
	 */
	public void setErrorLevel(String errorLevel);
	
	/**
	 * Setzt einen Header-Parameter. Das muss passiert sein, bevor etwas
	 * ausgegeben wurde.
	 */
	public void setHeader(String key, String value);
	
	/**
	 * Diese Methode setzt die maximale Verweildauer der Antwort in einem Cache.
	 * 
	 * @param millis
	 *            maximale Caching-Dauer in Millisekunden. Negative Werte
	 *            verbieten ein Caching.
	 */
	public void setCachingTime(int millis);
	
	/**
	 * Diese Methode setzt die maximale Verweildauer der Antwort in einem Cache.
	 * 
	 * @param millis
	 *            maximale Caching-Dauer in Millisekunden. Negative Werte
	 *            verbieten ein Caching.
	 * @param param
	 *            Zusätzliche Caching Paramater. Null entspricht default
	 *            verhalten.
	 */
	public void setCachingTime(int millis, String param);
	
	/**
	 * Diese Methode liefert die maximale Verweildauer der Antwort in einem
	 * Cache
	 * 
	 * @return maximale Caching-Dauer in Millisekunden. Negative Werte verbieten
	 *         ein Caching.
	 */
	public int getCachingTime();
	
	/**
	 * Adds a cookie to the response. Default cookie setting can be set in the
	 * config. See {@link de.tarent.octopus.content.CookieMap} for detailed
	 * settings.
	 * 
	 * @param name
	 * @param value
	 * @param settings
	 */
	public void addCookie(String name, String value, Map settings);
	
	/**
	 * Adds a cookie to the response.
	 * 
	 * Because the dispatched classes in the octopus-core does not know the
	 * Servlet-API and the Cookie-Object this method accepts an Object as
	 * parameter and adds this to cookies in case it is a Cookie-Object.
	 * 
	 * @param cookie
	 */
	public void addCookie(Object cookie);
	
	public void setTaskName(String taskName);
	
	public String getTaskName();
	
	public void setModuleName(String moduleName);
	
	public String getModuleName();
	
	/**
	 * Returns the outputStream.
	 * 
	 * @return OutputStream
	 */
	public OutputStream getOutputStream();
	
	/**
	 * Write the parameter as string to the current writer.
	 */
	public void print(String responseString);
	
	/**
	 * Write the parameter as string to the current writer and appends an
	 * <code>\n</code>.
	 */
	public void println(String responseString);
	
	/**
	 * Diese Methode sendet gepufferte Ausgaben.
	 */
	public void flush() throws IOException;
	
	/**
	 * Diese Methode schließt die Ausgabe ab.
	 */
	public void close() throws IOException;
	
	/**
	 * Teilt der Response mit, dass ein Login oder eine andere Authorisierung
	 * für die gewünschte Aktion erforderlich wäre.
	 * 
	 * @param authorisationAction
	 *            Vorschlag, wie die Autentifizierung erfolgen soll.
	 */
	public void setAuthorisationRequired(String authorisationAction);
	
	/**
	 * returns <code>true</code> if the this is an instance of
	 * {@link DirectCallResponse}
	 * 
	 * @return <code>true</code> or <code>false</code>
	 */
	public boolean isDirectCall();
}
