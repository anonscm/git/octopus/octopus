/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */
package de.tarent.octopus.server;

import java.util.LinkedList;

/**
 * This class gives a static access to the OctopusContext Object associated with
 * the request of the current Thread. At the begin of the octopus request
 * processing the context is set by an ThreadLocal Variable. So it can be
 * obtained at later time. Without passing a reference to it.
 * 
 * During the processing of an Octopus request the active OctpusContext may
 * change depending on the current executed scope.
 * 
 * @author <a href="mailto:sebastian@tarent.de">Sebastian Mancke</a>, <b>tarent
 *         GmbH</b>
 * @version 1.1
 */
public class Context {
	/** Hold a stack of context informations in a {@link LinkedList}. */
	private static ThreadLocal currentContext = new ThreadLocal() {
		public Object initialValue() {
			return new LinkedList();
		}
	};
	
	/**
	 * Returns the current active OctopusContext for this thread
	 */
	public static OctopusContext getActive() {
		LinkedList stack = (LinkedList) currentContext.get();
		if (!stack.isEmpty())
			return (OctopusContext) stack.getLast();
		else
			return null;
	}
	
	/**
	 * Add the current active OctopusContext on the octopusContent stack.
	 */
	public static void addActive(OctopusContext context) {
		((LinkedList) currentContext.get()).addLast(context);
	}
	
	/**
	 * Remove one context information from the context stack.
	 */
	public static void clear() {
		LinkedList stack = (LinkedList) currentContext.get();
		if (!stack.isEmpty())
			stack.removeLast();
	}
}
