/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.octopus.data;

import java.util.ArrayList;
import java.util.List;

/**
 * Stellt eine SQL WHERE Bedingung dar. Die Klasse wird über einen Postfix
 * Ausdruck Initialisiert. Und kann einen String mit einem SQL Ausdruck liefern.
 * 
 * <b>Postfix:</b> Postfix ist eine Möglichkeit einen Ausdrucksbaum, wie in
 * einer flachen Liste eindeutig zu speichern, ohne Klammerung zu benötigen.
 * <br>
 * Der Ausdruck ((name = 'Sebastian') and (id = '2')) sieht in Postfix z.B. so
 * aus: name 'Sebastian' = id '2' "=" "AND" Dieses Modell bietet die Möglichkeit
 * Suchausdrücke in einer flachen Struktur zu speichern und ihre logische
 * Struktur dabei zu erhalten. Letzteres wäre bei der Repräsentation in einem
 * String nicht der Fall.
 * 
 * <br>
 * <br>
 * Die Klassen erkennen SQL-Schlüsselwörter in der Token Liste: <br>
 * Zwei Operanden: "=", "and", "in", "like", "or" <br>
 * Ein Operand: "not"
 * 
 * @see WhereNode
 */
public class WhereClause {
	WhereNode rootNode;
	
	/**
	 * Initialisaierung mit einer Liste von Tokens in Postfix Reihenfolge.
	 * 
	 * Wenn sich aus der Liste kein gültiger Baum aufbauen lässt, wirde eine
	 * Exception geworfen.
	 */
	public WhereClause(String[] postfixTokenList) throws MalformedWhereClauseException {
		
		rootNode = getTree(postfixTokenList);
	}
	
	/**
	 * Initialisaierung mit einer Liste von Knotenobjekten in Postfix
	 * Reihenfolge.
	 * 
	 * Wenn sich aus der Liste kein gültiger Baum aufbauen lässt, wirde eine
	 * Exception geworfen.
	 */
	public WhereClause(WhereNode[] postfixTokenList) throws MalformedWhereClauseException {
		rootNode = getTree(postfixTokenList);
	}
	
	/**
	 * Liefert einen String mit einem SQL Ausdruck zurück.
	 */
	public String toString() {
		return inOrderTraverse(rootNode).toString();
	}
	
	private WhereNode getTree(String[] postfixTokenList) throws MalformedWhereClauseException {
		WhereNode[] whereNodeList = new WhereNode[postfixTokenList.length];
		
		for (int i = 0; i < postfixTokenList.length; i++) {
			whereNodeList[i] = new WhereNode(postfixTokenList[i]);
		}
		
		return getTree(whereNodeList);
	}
	
	private WhereNode getTree(WhereNode[] postfixTokenList) throws MalformedWhereClauseException {
		try {
			List stack = new ArrayList();
			for (int i = 0; i < postfixTokenList.length; i++) {
				WhereNode newNode = postfixTokenList[i];
				if (newNode.isOperator()) {
					newNode.setSecondChild((WhereNode) stack.remove(stack.size() - 1));
					if (newNode.isDoubleOperator()) {
						newNode.setFirstChild((WhereNode) stack.remove(stack.size() - 1));
					}
				}
				stack.add(newNode);
			}
			if (stack.size() > 1)
				throw new MalformedWhereClauseException("Der Ausdruck ist keine gültige Postfix Notation. Es sind zu wehnig Operatoren für die Operanden vorhanden.");
			
			return (WhereNode) stack.remove(0);
		} catch (java.lang.ArrayIndexOutOfBoundsException e) {
			throw new MalformedWhereClauseException("Der Ausdruck ist keine gültige Postfix Notation. Es sind nicht genügend Operanden für die Operatoren vorhanden.");
		}
	}
	
	private StringBuffer inOrderTraverse(WhereNode currRootNode) {
		StringBuffer out = new StringBuffer();
		if (currRootNode != null) {
			
			if (currRootNode.isDoubleOperator())
				out.append("(");
			
			StringBuffer left = inOrderTraverse(currRootNode.getFistChild());
			if (left.length() > 0)
				out.append(left).append(" ");
			
			out.append(currRootNode.getValue());
			
			StringBuffer right = inOrderTraverse(currRootNode.getSecondChild());
			if (right.length() > 0)
				out.append(" ").append(right);
			
			if (currRootNode.isDoubleOperator())
				out.append(")");
		}
		return out;
	}
}
