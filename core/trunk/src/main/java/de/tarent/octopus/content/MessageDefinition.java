/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.octopus.content;

import java.util.ArrayList;
import java.util.List;

/**
 * Eine MessageDefinition ist eine Auflistung Ein- oder Ausgabeparameter von
 * Operationen.
 * 
 * Jeder Parameter wird durch eine MessageDefinitionPart Objekt dar gestellt.
 * 
 * Eine Ausführlichere Beschreibung des Contextes ist in PortDefinition
 * 
 * @see PortDefinition
 */
public class MessageDefinition {
	/**
	 * Constante für den Datentyp Scalar. Ein Scalar ist ein Parameter, der
	 * beliebige einteilige Daten aufnehmen kann (z.B. integer, string, float,
	 * ...)
	 */
	public static final String TYPE_SCALAR = "xsd:string";
	
	/**
	 * Constante für den Datentyp Array mit beliebigen Elementen. Ein Array ist
	 * ein Parameter, der eine Liste von anderen Parametern aufnehmen kann.
	 */
	public static final String TYPE_ARRAY = "xsd:array";
	
	/**
	 * Constante für den Datentyp Struct. Ein Struct ist ein Parameter, der eine
	 * Map (z.B. wie eine Java Map) aufnehmen kann. Darin können also andere
	 * Parameter mit ihren Namen als Keys abgelegt werden.
	 */
	public static final String TYPE_STRUCT = "tc:struct";
	
	private List parts;
	
	/**
	 * Initialisierung mit einer leeren Menge von Parametern
	 */
	public MessageDefinition() {
		parts = new ArrayList();
	}
	
	/**
	 * Erzeugt einen neuen Parameter und fügt diesen an.
	 * 
	 * @param newPart
	 *            Der neue Parameter
	 */
	public MessageDefinition addPart(MessageDefinitionPart newPart) {
		int size = parts.size();
		for (int i = 0; i < size; i++) {
			MessageDefinitionPart curr = (MessageDefinitionPart) parts.get(i);
			if (curr.getName().equals(newPart.getName())) {
				parts.remove(i);
				break;
			}
		}
		parts.add(newPart);
		return this;
	}
	
	/**
	 * Erzeugt einen neuen Parameter und fügt diesen an.
	 * 
	 * @param name
	 *            Name des Parameters
	 * @param partDataType
	 *            Datentyp des Parameters als XML-Schema-Type. Die Konstanten
	 *            der Klassen können und sollen benutzt werden.
	 * @param description
	 *            Beschreibung des Parameters
	 * @param optional
	 *            Flag, ob der Parameter optional sein soll
	 */
	public MessageDefinition addPart(String name, String partDataType, String description, boolean optional) {
		addPart(new MessageDefinitionPart(name, partDataType, description, optional));
		return this;
	}
	
	/**
	 * Erzeugt einen neuen Parameter und fügt diesen an. Optional wird auf false
	 * gesetzt.
	 * 
	 * @param name
	 *            Name des Parameters
	 * @param partDataType
	 *            Datentyp des Parameters als XML-Schema-Type. Die Konstanten
	 *            der Klassen können und sollen benutzt werden.
	 * @param description
	 *            Beschreibung des Parameters
	 */
	public MessageDefinition addPart(String name, String partDataType, String description) {
		addPart(name, partDataType, description, false);
		return this;
	}
	
	/**
	 * Fügt eine Liste von neuen Parametern an.
	 * 
	 * @param addParts
	 *            Vector mit Elementen vom Type MessageDefinitionPart
	 */
	public MessageDefinition addParts(List addParts) {
		int size = addParts.size();
		for (int i = 0; i < size; i++) {
			addPart((MessageDefinitionPart) addParts.get(i));
		}
		return this;
	}
	
	/**
	 * Liefert die MessageDefinitionParts. Also die Parameter dieser Message
	 * 
	 * @return Vector mit MessageDefinitionPart Objekten
	 */
	public List getParts() {
		return parts;
	}
	
	/**
	 * Liefert die den MessageDefinitionPart mit dem entsprechenden Namen.
	 * 
	 * @return MessageDefinitionPart mit dem Namen oder NULL, wenn es keinen
	 *         solchen gibt.
	 */
	public MessageDefinitionPart getPartByName(String partName) {
		for (int i = 0; i < parts.size(); i++) {
			MessageDefinitionPart curr = (MessageDefinitionPart) parts.get(i);
			if (curr.getName().equals(partName))
				return curr;
		}
		return null;
	}
	
	
	public String toString() {
		return parts.toString();
	}
}
