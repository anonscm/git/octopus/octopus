/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */
package de.tarent.octopus.response.soap;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.namespace.QName;

import org.apache.axis.Constants;
import org.apache.axis.encoding.SerializationContext;
import org.apache.axis.encoding.Serializer;
import org.apache.axis.wsdl.fromJava.Types;
import org.apache.commons.logging.Log;
import org.w3c.dom.Element;
import org.xml.sax.Attributes;

import de.tarent.octopus.logging.LogFactory;

public class SmartSerializer implements Serializer {
	
	/** serialVersionUID */
	private static final long serialVersionUID = 7766326226378057582L;
	
	private static final Log logger = LogFactory.getLog(SmartSerializer.class);
	
	public void serialize(QName proposedName, Attributes attributes, Object value, SerializationContext context) throws IOException {
		
		context.startElement(proposedName, null);
		try {
			if (value instanceof List) {
				for (Iterator iter = ((List) value).iterator(); iter.hasNext();) {
					Object item = iter.next();
					if (item == null) {
						context.serialize(new QName("", "item"), null, item);
					} else {
						QName childElementName = new QName("", context.getQNameForClass(item.getClass()).getLocalPart());
						context.serialize(childElementName, null, item);
					}
				}
			} else if (value instanceof Map) {
				for (Iterator iter = ((Map) value).entrySet().iterator(); iter.hasNext();) {
					Map.Entry entry = (Map.Entry) iter.next();
					QName childElementName = new QName("", "" + entry.getKey());
					context.serialize(childElementName, null, entry.getValue());
				}
			} else {
				context.writeString(context.getEncoder().encode("" + value));
			}
		} catch (Exception e) {
			logger.error("Error while serialization", e);
			if (e instanceof RuntimeException)
				throw (RuntimeException) e;
			throw new RuntimeException(e);
		} finally {
			context.endElement();
		}
	}
	
	public Element writeSchema(Class javaType, Types types) throws Exception {
		throw new RuntimeException("writeSchema not supported");
	}
	
	public String getMechanismType() {
		return Constants.AXIS_SAX;
	}
}
