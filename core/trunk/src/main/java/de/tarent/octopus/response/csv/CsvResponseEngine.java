/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.octopus.response.csv;

import java.io.IOException;
import java.io.Writer;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

import de.tarent.octopus.config.ModuleConfig;
import de.tarent.octopus.request.OctopusResponse;
import de.tarent.octopus.response.AbstractResponseEngine;
import de.tarent.octopus.response.ResponseDescription;
import de.tarent.octopus.response.ResponseEngine;
import de.tarent.octopus.response.ResponseProcessingException;
import de.tarent.octopus.server.OctopusContext;

public class CsvResponseEngine extends AbstractResponseEngine implements ResponseEngine {
	public String getDefaultName() {
		return "csv";
	}
	
	public String getDefaultContentType() {
		return "text/comma-separated-values";
	}
	
	public void init(ModuleConfig moduleConfig) {
		// nothing todo
		// should be read format informations for different csv implementations.
		// so that we can have ms-csv or ooo-csv or ...
	}

	public void sendEmptyResponse(ResponseDescription desc, OctopusContext octopusContext, OctopusResponse octopusResponse) throws ResponseProcessingException {
		Writer writer = octopusResponse.getWriter();
		
		try {
			writer.write("");
		} catch (IOException e) {
			throw new ResponseProcessingException("Error processing CSV output.", e);
		} finally {
			try {
				octopusResponse.close();
			} catch (IOException e) {
				throw new ResponseProcessingException("Error processing CSV output.", e);
			}
		}
	}
	
	public void sendNormalResponse(ResponseDescription desc, OctopusContext octopusContext, OctopusResponse octopusResponse) throws ResponseProcessingException {
		Map outputFields = getOutputFields(desc, octopusContext, octopusResponse);
		Writer writer = octopusResponse.getWriter();
		
		try {
			if (outputFields == null || outputFields.isEmpty()) {
				writer.write("null");
			} else {
				for (Iterator it = outputFields.values().iterator(); it.hasNext(); ) {
					Object data = it.next();
					if (data == null)
						writer.write("null");
					else if (data instanceof Object[][])
						generateCSV(writer, Arrays.asList(((Object[]) data)));
					else if (data instanceof Object[])
						generateCSVLine(writer, Arrays.asList(((Object[]) data)));
					else if (data instanceof Collection)
						generateCSV(writer, (Collection) data);
					else
						throw new ResponseProcessingException(
								"Unsupported data type " + data.getClass().getName() +
								" for cvs response engine. Currently supported are" +
								" Object[] and Collection.");
				}
			}
			
			writer.flush();
		} catch (IOException e) {
			throw new ResponseProcessingException("Error processing CSV output.", e);
		} finally {
			try {
				octopusResponse.close();
			} catch (IOException e) {
				throw new ResponseProcessingException("Error processing CSV output.", e);
			}
		}
	}

	public void sendErrorResponse(ResponseDescription desc, OctopusContext octopusContext, OctopusResponse octopusResponse, String message, Exception exception) throws ResponseProcessingException {
		Writer writer = octopusResponse.getWriter();
		
		try {
			writer.write(message);
		} catch (IOException e) {
			throw new ResponseProcessingException("Error processing CSV output.", e);
		} finally {
			try {
				octopusResponse.close();
			} catch (IOException e) {
				throw new ResponseProcessingException("Error processing CSV output.", e);
			}
		}
	}
	
	public void sendEmptyCallback(ResponseDescription desc, OctopusContext octopusContext, OctopusResponse octopusResponse) throws ResponseProcessingException {
		sendEmptyResponse(desc, octopusContext, octopusResponse);
	}
	
	public void sendNormalCallback(ResponseDescription desc, OctopusContext octopusContext, OctopusResponse octopusResponse) throws ResponseProcessingException {
		sendNormalResponse(desc, octopusContext, octopusResponse);
	}
	
	public void sendErrorCallback(ResponseDescription desc, OctopusContext octopusContext, OctopusResponse octopusResponse, String message, Exception exception) throws ResponseProcessingException {
		sendErrorResponse(desc, octopusContext, octopusResponse, message, exception);
	}
	
	private void generateCSV(Writer writer, Collection collection) throws ResponseProcessingException, IOException {
		// generate header
		Iterator it = collection.iterator();
		if (it.hasNext())
			//generateHeaderLine(writer, (Map) it.next());
			generateCSVLine(writer, it.next());
		while (it.hasNext())
			generateCSVLine(writer, it.next());
	}
	
	private void generateCSVLine(Writer writer, Object input) throws ResponseProcessingException, IOException {
		if (input instanceof Map)
			generateCSVLine(writer, ((Map) input).values());
		else if (input instanceof Object[])
			generateCSVLine(writer, Arrays.asList((Object[]) input));
		else if (input instanceof Collection)
			generateCSVLine(writer, (Collection) input);
		else
			throw new ResponseProcessingException(
					"Given second level data in response field has to be " +
					"either Collection, Map or Object[]. It's " +
					input.getClass() + " with value " + input);
	}
	
	private void generateCSVLine(Writer writer, Collection input) throws IOException {
		for (Iterator it = input.iterator(); it.hasNext(); ) {
			Object entry = it.next();
			writer.write("\"");
			writer.write(entry == null ? null : entry.toString().replaceAll("\"", "\"\""));
			writer.write("\"");
			if (it.hasNext())
				writer.write(";");
		}
		writer.write("\r\n");
	}
}
