/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */
package de.tarent.octopus.persistence;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.tarent.octopus.config.ModuleConfig;
import de.tarent.octopus.request.OctopusRequest;
import de.tarent.octopus.util.XmlSerializer;

/**
 * 
 * @author Alex Maier, tarent GmbH
 * 
 */
public class PersistentRequestWorker {
	
	private Map userTasks = new HashMap();
	private DBAccessManager dBAccessManager = null;
	private boolean persistsRequests = false;
	
	public PersistentRequestWorker(ModuleConfig moduleConfig) throws Exception {
		if (moduleConfig.persistsRequests()) {
			// initialize the DBAccessManager
			dBAccessManager = new DBAccessManager(moduleConfig);
			dBAccessManager.initDB("createShema.sql");
		}
		if (dBAccessManager != null)
			// the persistence was not enabled
			persistsRequests = true;
	}
	
	/**
	 * This method extract the nedeet information of the running process(task)
	 * from the given {@link OctopusRequest} <code>octopusRequest</code> and
	 * store if necessary the initialized {@link PersistentRequestItem} in the
	 * database. Additional the initialized {@link PersistentRequestItem} will
	 * be puted in the local {@link Map} <code>userTasks</code>
	 * 
	 * @param octopusRequest
	 *            The {@link OctopusRequest} from where the process information
	 *            will be extract.
	 * @param processId
	 *            The unique id of the prozess
	 * @param status
	 *            The status of the process see
	 *            {@link PersistentRequestItem#STATUS_COMPLETED} or
	 *            {@link PersistentRequestItem#STATUS_RUNNING}
	 * @throws Exception
	 */
	public void addTask(OctopusRequest octopusRequest, String processId, int status) throws Exception {
		PersistentRequestItem persistentRequestItem = new PersistentRequestItem();
		persistentRequestItem.setProcessId(processId);
		persistentRequestItem.setRequestId(octopusRequest.getRequestID());
		persistentRequestItem.setModuleName(octopusRequest.getModule());
		persistentRequestItem.setTaskName(octopusRequest.getTask());
		// serialize the requestParameters of the OctopusRequest object
		persistentRequestItem.setRequestData(XmlSerializer.objectToXml(octopusRequest.getRequestParameters()));
		if (octopusRequest.getHeaders() != null && octopusRequest.getHeaders().getReplyToAddress() != null)
			persistentRequestItem.setCallBackUri(octopusRequest.getHeaders().getReplyToAddress());
		persistentRequestItem.setStatus(status);
		if (persistsRequests)
			// store in the database
			persistentRequestItem = dBAccessManager.insertPersistentRequest(persistentRequestItem);
		// add to map
		userTasks.put(processId, persistentRequestItem);
	}
	
	/**
	 * Remove the process from the database if persistence was enabled.
	 * Additional remove the process from the map.
	 * 
	 * @param processId
	 *            The unique id of the prozess
	 * @throws Exception
	 */
	public void removeTask(String processId) throws Exception {
		PersistentRequestItem persistentRequestItem = (PersistentRequestItem) userTasks.get(processId);
		
		if (persistentRequestItem != null) {// item found?
			boolean removeFromMap = true;
			if (persistsRequests)
				// remove from the database
				removeFromMap = dBAccessManager.removePersistentRequest(persistentRequestItem);
			// if not removed from db, do not remove from the map userTasks
			if (removeFromMap)
				userTasks.remove(processId);
		} else {
			if (persistsRequests)
				// remove from the database
				dBAccessManager.removePersistentRequestByProcessId(processId);
		}
	}
	
	/**
	 * Sets the status of the task with the process id <code>processId</code>
	 * to {@link PersistentRequestItem#STATUS_COMPLETED}. Updates the
	 * {@link PersistentRequestItem} in the database if persistence was enabled.
	 * 
	 * @param processId
	 *            The unique id of the prozess
	 * @throws Exception
	 */
	public void taskCompleted(String processId) throws Exception {
		PersistentRequestItem persistentRequestItem = (PersistentRequestItem) userTasks.get(processId);
		if (persistentRequestItem != null) {// item found?
			persistentRequestItem.setStatus(PersistentRequestItem.STATUS_COMPLETED);
			if (persistsRequests) {
				// update in the database
				dBAccessManager.updatePersistentRequest(persistentRequestItem);
			}
		}
		// else nothing to do, the item was not found
	}
	
	/**
	 * If persistence was enabled, fetchs all tasks from the database and
	 * returns the result as a list with {@link PersistentRequestItem}'s.
	 * Otherwise returns <code>null</code>
	 * 
	 * @return a list with the {@link PersistentRequestItem}'s
	 * @throws Exception
	 */
	public List fetchAllTasks() throws Exception {
		if (persistsRequests)
			return dBAccessManager.fetchAllPersistentRequests();
		return null;
	}
}
