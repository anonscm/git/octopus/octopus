/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.octopus.response.xslt;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.w3c.dom.Document;
import de.tarent.octopus.config.ModuleConfig;
import de.tarent.octopus.request.OctopusResponse;
import de.tarent.octopus.resource.Resources;
import de.tarent.octopus.response.AbstractResponseEngine;
import de.tarent.octopus.response.ResponseDescription;
import de.tarent.octopus.response.ResponseEngine;
import de.tarent.octopus.response.ResponseProcessingException;
import de.tarent.octopus.server.OctopusContext;
import de.tarent.octopus.util.Xml;

/**
 * Verarbeitung mit XSLT.
 * 
 * @author <a href="mailto:H.Helwich@tarent.de">Hendrik Helwich</a>, <b>tarent
 *         GmbH</b>
 */
public class XsltResponseEngine extends AbstractResponseEngine implements ResponseEngine {
	private static final String suffix = ".xsl";
	private static final String subdir = "xslt";
	
	public String getDefaultName() {
		return "xslt";
	}
	
	public String getDefaultContentType() {
		return "text/xml";
	}

	/**
	 * @see de.tarent.octopus.response.ResponseEngine#init(ModuleConfig)
	 */
	public void init(ModuleConfig moduleConfig) {
	}
	
	public void sendEmptyResponse(ResponseDescription desc, OctopusContext octopusContext, OctopusResponse octopusResponse) throws ResponseProcessingException {
		octopusResponse.getWriter().write("<empty>ok</empty>");
	}
	
	public void sendNormalResponse(ResponseDescription desc, OctopusContext octopusContext, OctopusResponse octopusResponse) throws ResponseProcessingException {
		String xsltFileName = desc.getAsString("xsltStylesheet");
		if (xsltFileName == null || xsltFileName.trim().equals("")) {
			xsltFileName = Resources.getInstance().get("XSLTRESPONSE_XSLT_DEFAULT_FILE_NAME");
		}
		
		File templateFile = new File(new File(octopusContext.moduleConfig().getTemplateRootPath(), subdir), xsltFileName + suffix);
		
		// Nach einem gleichnamigen Template in dem Default Modul suchen
		/*
		if (!templateFile.exists()) {
			File defaultTemplateFile = new File(new File(octopusContext.moduleConfig().getTemplateRootPath(octopusContext.commonConfig().getDefaultModuleName()), subdir), xsltFileName + suffix);
			if (defaultTemplateFile.exists())
				templateFile = defaultTemplateFile;
			else
				throw new ResponseProcessingException(Resources.getInstance().get("XSLTRESPONSE_EXC_NO_TEMPLATE", templateFile, defaultTemplateFile));
		}
		*/
		
		InputStream templateStream = null;
		try {
			// FIXME warnung einfügen wenn datei nicht da !!!!
			if (templateFile.exists())
				templateStream = new FileInputStream(templateFile);
			else
				templateStream = getClass().getResourceAsStream("/default.xsl");
			
			StringReader sr = null;
			String xmlSourceName = desc.getAsString("xmlSource");
			Object dataInputSource = octopusContext.contentAsObject(xmlSourceName);
			
			if (dataInputSource != null) {
				if (dataInputSource instanceof String)
					sr = new StringReader((String) dataInputSource);
				else if (dataInputSource instanceof Document)
					sr = new StringReader(Xml.toString((Document) dataInputSource));
				else
					throw new ResponseProcessingException(Resources.getInstance().get("XSLTRESPONSE_EXC_OBJECT_UNSUPPORTED", dataInputSource.getClass().getName()));
			} else
				sr = new StringReader(Resources.getInstance().get("XSLTRESPONSE_XML_DEFAULT_SOURCE"));
			
			Transformer transformer = Xml.getXSLTTransformer(new StreamSource(templateStream));
			Iterator e = octopusContext.contentKeys();
			while (e.hasNext()) {
				String key = (String) e.next();
				putXSLTParameter(transformer, key, octopusContext.contentAsObject(key));
			}
			transformer.transform(new StreamSource(sr), new StreamResult(octopusResponse.getWriter()));
		} catch (TransformerException e) {
			throw new ResponseProcessingException(e.getLocalizedMessage(), e);
		} catch (IOException e) {
			throw new ResponseProcessingException(e.getLocalizedMessage(), e);
		} finally {
			if (templateStream != null)
				try {
					templateStream.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
	}
	
	public void sendErrorResponse(ResponseDescription desc, OctopusContext octopusContext, OctopusResponse octopusResponse, String message, Exception exception) throws ResponseProcessingException {
		octopusResponse.getWriter().write("<error>" + message + "</error>");
	}

	public void sendEmptyCallback(ResponseDescription desc, OctopusContext octopusContext, OctopusResponse octopusResponse) throws ResponseProcessingException {
		sendEmptyResponse(desc, octopusContext, octopusResponse);
	}
	
	public void sendNormalCallback(ResponseDescription desc, OctopusContext octopusContext, OctopusResponse octopusResponse) throws ResponseProcessingException {
		sendNormalResponse(desc, octopusContext, octopusResponse);
	}
	
	public void sendErrorCallback(ResponseDescription desc, OctopusContext octopusContext, OctopusResponse octopusResponse, String message, Exception exception) throws ResponseProcessingException {
		sendErrorResponse(desc, octopusContext, octopusResponse, message, exception);
	}
	
	private void putXSLTParameter(Transformer transformer, String name, Object value) {
		if (name == null)
			name = "";
		if (value instanceof Map) {
			if (name.length() > 0)
				name += '.';
			Iterator mapIt = ((Map) value).entrySet().iterator();
			while (mapIt.hasNext()) {
				Map.Entry entry = (Map.Entry) mapIt.next();
				putXSLTParameter(transformer, name + entry.getKey(), entry.getValue());
			}
		} else if (value instanceof List) {
			if (name.length() > 0)
				name += '.';
			Iterator listIt = ((List) value).iterator();
			for (int i = 0; listIt.hasNext(); i++)
				putXSLTParameter(transformer, name + i, listIt.next());
		} else
			transformer.setParameter(name, value);
	}
}
