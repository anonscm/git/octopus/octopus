/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.octopus.response.xmlrpc;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.logging.Log;
import org.apache.xmlrpc.XmlRpcRequest;
import org.apache.xmlrpc.XmlRpcRequestProcessor;

import de.tarent.octopus.logging.LogFactory;
import de.tarent.octopus.request.OctopusRequest;
import de.tarent.octopus.request.OctopusRequestException;
import de.tarent.octopus.response.soap.SoapException;

/**
 * Diese Klasse dient dem Auslesen von XmlRpc-Anfragen.
 * 
 * @author mikel
 */
public class XmlRpcEngine {
	
	/** Logger für diese Klasse */
	private static Log logger = LogFactory.getLog(XmlRpcEngine.class);
	
	
	/**
	 * Diese Methode analysiert eine XML-RPC-Anfrage.
	 * 
	 * @param inStream
	 *            die XmlRPC-Anfrage
	 * @param requestType
	 *            der Anfragetyp
	 * @param requestID
	 *            die Anfrage-ID
	 * @return ein generiertes Anfrage-Objekt
	 * @throws SoapException
	 */
	public static OctopusRequest[] readXmlRpcRequests(InputStream inStream, int requestType, String requestID) throws OctopusRequestException {
		logger.trace(XmlRpcEngine.class.getName() + " readXmlRpcRequests " + new Object[] { inStream, new Integer(requestType), requestID });
		
		XmlRpcRequest xmlRpcRequest = null;
		
		try {
			XmlRpcRequestProcessor requestProcessor = new XmlRpcRequestProcessor() {
			};
			xmlRpcRequest = requestProcessor.processRequest(inStream);
		} catch (Exception e) {
			throw new OctopusRequestException(e);
		}
		
		Map params = new HashMap();
		List paramList = xmlRpcRequest.getParameters();
		if (paramList != null && paramList.size() > 0) {
			Iterator itParams = paramList.iterator();
			for (int i = 0; itParams.hasNext(); i++)
				params.put(String.valueOf(i), itParams.next());
		}
		
		Pattern pattern = Pattern.compile("[{]([^}]*)[}](.*)");
		Matcher matcher = pattern.matcher(xmlRpcRequest.getMethodName());
		
		String method = null;
		String module = null;
		if (matcher.matches()) {
			module = matcher.group(1);
			method = matcher.group(2);
		} else
			method = xmlRpcRequest.getMethodName();
		
		OctopusRequest octRequest = new OctopusRequest(requestID);
		octRequest.setRequestType(requestType);
		octRequest.setRequestParameters(params);
		if (method != null && method.length() > 0)
			octRequest.setTask(method);
		if (module != null && module.length() > 0)
			octRequest.setModule(module);
		
		return new OctopusRequest[] { octRequest };
	}
}
