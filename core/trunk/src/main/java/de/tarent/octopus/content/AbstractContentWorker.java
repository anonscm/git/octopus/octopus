/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.octopus.content;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.apache.commons.logging.Log;

import de.tarent.octopus.logging.LogFactory;
import de.tarent.octopus.server.OctopusContext;

/**
 * Worker zur bequemeren Bedienung aller OctopusContent-Worker. Ruft automatisch
 * über Reflection die mit der Action gleichnamige Mathode auf.
 * 
 * @author <a href="mailto:H.Helwich@tarent.de">Hendrik Helwich</a>, <b>tarent
 *         GmbH</b>
 */
public abstract class AbstractContentWorker implements ContentWorker {
	private static Log logger = LogFactory.getLog(AbstractContentWorker.class);
	
	public String doAction(String actionName, OctopusContext octopusContext) throws ContentProzessException {
		String result = RESULT_error;
		Class workerClass = this.getClass();
		Class[] parameterTypes = new Class[] { OctopusContext.class };
		Method actionMethod;
		Object[] arguments = new Object[] { octopusContext };
		try {
			actionMethod = workerClass.getMethod(actionName, parameterTypes);
			if (logger.isTraceEnabled())
				logger.trace("Starte Methode \"" + workerClass.getName() + "." + actionName + "(...)\"");
			// result = (String) actionMethod.invoke(this, arguments);
			actionMethod.invoke(this, arguments);
			result = RESULT_ok;
		} catch (NoSuchMethodException e) {
			throw new ContentProzessException("Nicht unterstützte Action im Worker '" + workerClass.getName() + "': " + actionName);
		} catch (IllegalAccessException e) {
			logger.error("Fehler im Worker '" + workerClass.getName() + "'", e);
			throw new ContentProzessException(e);
		} catch (InvocationTargetException e) {
			logger.error("Fehler im Worker '" + workerClass.getName() + "'", e);
			throw new ContentProzessException(e);
		}
		return result;
	}
}
