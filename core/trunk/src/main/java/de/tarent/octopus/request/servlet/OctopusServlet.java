/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.octopus.request.servlet;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;

import de.tarent.octopus.client.OctopusConnectionFactory;
import de.tarent.octopus.logging.LogFactory;
import de.tarent.octopus.request.Octopus;
import de.tarent.octopus.request.OctopusRequest;
import de.tarent.octopus.request.OctopusRequestException;
import de.tarent.octopus.request.OctopusResponse;
import de.tarent.octopus.request.OctopusSession;
import de.tarent.octopus.resource.Resources;
import de.tarent.octopus.client.OctopusConnection;
import de.tarent.octopus.config.CommonConfig;
import de.tarent.octopus.config.ModuleConfig;
import de.tarent.octopus.config.OctopusConfigException;
import de.tarent.octopus.config.OctopusEnvironment;
import de.tarent.octopus.embedded.OctopusDirectCallConnection;
import de.tarent.octopus.embedded.OctopusInternalStarter;

/**
 * Eintrittspunkt der Anfrage und Starter des Systems, bei Verwendeung des
 * Octopus in einem Servlet. <br>
 * <br>
 * 1. Dieses Servlet startet das System indem es in seiner init() Methode die
 * dauerhaft bestehenden Komponenten aufbaut und initialisiert. <br>
 * <br>
 * 2. Es nimmt Anfragen entgegen und leitet diese an den RequestDispatcher
 * weiter. Dabei muss es unterscheiden, ob diese Anfragen normale WEB- oder
 * SOAP-Anfrgen sind. Beide werden dann in eine unabhängige Repräsentaition
 * gebracht.
 * 
 * @author <a href="mailto:mancke@mancke-software.de">Sebastian Mancke</a>,
 *         <b>tarent GmbH</b>
 */
public class OctopusServlet extends HttpServlet {
	private static final long serialVersionUID = 3257852090755133745L;
	
	private Octopus octopus = null;
	
	private static Log logger = LogFactory.getLog(OctopusServlet.class);
	
	// Fehler, der während des Initialisierens auftritt
	private Exception initError = null;
	

	/**
	 * Name dieser Webapp aus dem ContextPath (==ContextPath ohne '/' am
	 * Anfang). Wird beim ersten Request oder ueber die Servlet-Konfiguration
	 * gesetzt, da er nicht über den Servlet-Context abgerufen werden kann
	 */
	protected String webappContextPathName = null;
	
	/**
	 * Bit field with request types which are allowed for connections with this
	 * octopus servlet
	 */
	protected int allowedRequestTypes = 0x00000000;
	
	/**
	 * Inititalisiert die Komponenten, die für alle Aufrufe gleich sind. <br>
	 * <br>
	 * 
	 * <li>Env Objekt durch createEnvObject()</li>
	 * <li>Dem RequestDispatcher</li>
	 * <li>Dem Logger.</li>
	 * </ul>
	 */
	public void init() throws ServletException {
		initError = null;
		try {
			OctopusEnvironment environment = createEnvironmentObject();
			
			System.setProperty(OctopusEnvironment.KEY_PATHS_ROOT, environment.getValueAsString(OctopusEnvironment.KEY_PATHS_ROOT));
			LogFactory.initOctopusLogging(environment);
			
			// set the webappContextPathName, if it was configured in the
			// octopus-server config
			webappContextPathName = environment.getValueAsString(OctopusEnvironment.KEY_WEBAPP_CONTEXT_PATH_NAME);
			
			String types = environment.getValueAsString(OctopusEnvironment.KEY_REQUEST_ALLOWED_TYPES);
			if (types == null || types.length() == 0)
				types = OctopusEnvironment.VALUE_REQUEST_TYPE_ANY;
			
			if (types.indexOf(OctopusEnvironment.VALUE_REQUEST_TYPE_ANY) != -1)
				allowedRequestTypes = 0xffffffff;
			if (types.indexOf(OctopusEnvironment.VALUE_REQUEST_TYPE_WEB) != -1)
				allowedRequestTypes |= OctopusRequest.REQUEST_TYPE_WEB;
			if (types.indexOf(OctopusEnvironment.VALUE_REQUEST_TYPE_SOAP) != -1)
				allowedRequestTypes |= OctopusRequest.REQUEST_TYPE_SOAP;
			if (types.indexOf(OctopusEnvironment.VALUE_REQUEST_TYPE_XMLRPC) != -1)
				allowedRequestTypes |= OctopusRequest.REQUEST_TYPE_XML_RPC;
			if (types.indexOf(OctopusEnvironment.VALUE_REQUEST_TYPE_DIRECTCALL) != -1)
				allowedRequestTypes |= OctopusRequest.REQUEST_DIRECT_CALL;
			
			octopus = new Octopus();
			octopus.init(environment, new ServletModuleLookup(environment, getServletContext(), this));
			
			// Octopus für lokale Connections bekannt machen
			OctopusConnectionFactory.getInstance().setInternalOctopusInstance(octopus);
			
		} catch (Exception e) {
			logger.error(Resources.getInstance().get("REQUESTPROXY_LOG_INIT_EXCEPTION"), e);
			initError = e;
		}
	}
	
	/**
	 * Deinitialisiert das Servlet.
	 * 
	 * @see javax.servlet.GenericServlet#destroy()
	 */
	public void destroy() {
		if (octopus != null)
			try {
				octopus.deInit();
			} catch (Exception e) {
				logger.warn(Resources.getInstance().get("REQUESTPROXY_LOG_CLEANUP_EXCEPTION"), e);
			}
		LogFactory.deInitOctopusLogging();
	}
	
	/**
	 * Gibt die Anfrage an handleTheRequest weiter
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		handleTheRequest(request, response, false);
	}
	
	/**
	 * Gibt die Anfrage an handleTheRequest weiter
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		handleTheRequest(request, response, true);
	}
	
	/**
	 * Startet die Abarbeitung einer Anfrage dadurch, daß diese an den die
	 * dispatch() Methode des RequestDispatchers weiter gegeben wird. <br>
	 * <br>
	 * Dazu werden ein OctopusRequest Objekt mit createRequestObject(), <br>
	 * ein OctopusResponse Objekt, <br>
	 * sowie ein OctopusSession mit der HttpSession initialisiert und an den
	 * RequestDispatcher übergeben. <br>
	 * <br>
	 * Wenn der Request den Parameter debug=true enthält und Debugging über den
	 * DeploymentDescriptor erlaubt wurde, werden noch Debugausgaben aus
	 * gegeben.
	 */
	public void handleTheRequest(HttpServletRequest request, HttpServletResponse response, boolean post) throws IOException {
		if (webappContextPathName == null && request.getContextPath().length() >= 1)
			webappContextPathName = request.getContextPath().substring(1);
		
		String requestID = OctopusRequest.createRequestID();
		
		String requestURI = (String) request.getAttribute("javax.servlet.forward.request_uri");
		if (requestURI == null)
			requestURI = request.getRequestURI();
		if (requestURI.startsWith(request.getContextPath()))
			requestURI = requestURI.substring(request.getContextPath().length());
		
		if (logger.isInfoEnabled())
			logger.info(Resources.getInstance().get("REQUESTPROXY_LOG_REQUEST_URI", requestID, request.getContextPath(), requestURI, request.getRemoteAddr()));
		
		String requestedResponse = requestURI.substring(1);
		String requestedModuleName = null;
		String requestedTaskName = null;
		
		if (requestedResponse != null && requestedResponse.indexOf("/") != -1) {
			requestedModuleName = requestedResponse.substring(requestedResponse.indexOf("/") + 1);
			requestedResponse = requestedResponse.substring(0, requestedResponse.indexOf("/"));
		}
		if (requestedModuleName != null && requestedModuleName.indexOf("/") != -1) {
			requestedTaskName = requestedModuleName.substring(requestedModuleName.indexOf("/") + 1);
			requestedModuleName = requestedModuleName.substring(0, requestedModuleName.indexOf("/"));
			
			if (requestedModuleName.equalsIgnoreCase("requestProxy"))
				requestedModuleName = null;
		}
		if (requestedTaskName != null && requestedTaskName.indexOf("/") != -1) {
			requestedTaskName = requestedTaskName.substring(0, requestedTaskName.indexOf("/"));
		}
		
		CommonConfig commonConfig = octopus.getCommonConfig();
		if ((requestedModuleName == null || requestedModuleName.length() == 0) && commonConfig.getDefaultModuleName() != null) {
			requestedModuleName = commonConfig.getDefaultModuleName();
		}
		
		ModuleConfig moduleConfig = commonConfig.getModuleConfig(requestedModuleName);
		if ((requestedTaskName == null || requestedTaskName.length() == 0) && moduleConfig != null) {
			requestedTaskName = "default";
		}
		
		OctopusSession octopusSession;
		if (!octopus.getOctopusEnvironment().getValueAsBoolean(OctopusEnvironment.KEY_OMIT_SESSIONS)) {
			HttpSession session = request.getSession(true);
			
			// Force new session of task is login or logout.
			// TODO Do not invalid full session! Remove module values instread.
			boolean sessionWasNew = (session == null) || session.isNew();
			if (requestedTaskName != null && requestedTaskName.equals("login")) {
				if (session != null)
					session.invalidate();
				session = request.getSession(true);
			}
			
			if (sessionWasNew && !post) {
				// This relativ (manual) redirect fix a problem in the catalina
				// implementation of HttpServletResponse.sendRedirect which
				// use a wrong ("http://") absolut path for "https:/Immobilien
				// Schulz/" urls.
				// It also fix a problem with forwarder-servlets which do not
				// include the orignal request url.
				StringBuffer redirectURI = new StringBuffer(request.getContextPath().length() + requestURI.length() + 1);
				redirectURI.append(request.getContextPath());
				redirectURI.append(requestURI);
				if (request.getQueryString() != null) {
					redirectURI.append("?");
					redirectURI.append(request.getQueryString());
				} else {
					Map parameterMap = request.getParameterMap();
					if (!parameterMap.isEmpty()) {
						redirectURI.append("?");
					}
					for (Iterator it = parameterMap.entrySet().iterator(); it.hasNext();) {
						Map.Entry param = (Map.Entry) it.next();
						String key = param.getKey().toString();
						Object value = param.getValue();
						if (value instanceof String) {
							redirectURI.append(key).append("=").append(value).append("&");
						} else if (value instanceof String[]) {
							String[] values = (String[]) value;
							for (int i = 0; i < values.length; i++)
								redirectURI.append(key).append("=").append(values[i]).append("&");
						}
					}
				}
				
				response.setStatus(302);
				response.setHeader("Location", response.encodeRedirectURL(redirectURI.toString()));
				logger.info(Resources.getInstance().get("REQUESTPROXY_LOG_REDIRECT_REQUEST", requestID, redirectURI));
				return;
			}
			octopusSession = new OctopusServletSession(session);
		} else {
			// Servlet Session Ignorieren und Dummy erzeugen
			octopusSession = new OctopusServletDummySession();
		}
		
		
		
		logger.debug(Resources.getInstance().get("REQUESTPROXY_LOG_REQUEST_PROCESSING_START", requestID));
		
		OctopusResponse octopusResponse = null;
		int requestType = OctopusRequest.REQUEST_TYPE_WEB;
		try {
			octopusResponse = new OctopusServletResponse(response);
			octopusResponse.setErrorLevel(octopus.getOctopusEnvironment().getValueAsString(OctopusEnvironment.KEY_RESPONSE_ERROR_LEVEL));
			logger.trace(Resources.getInstance().get("REQUESTPROXY_LOG_RESPONSE_OBJECT_CREATED", requestID));
			
			requestType = HttpHelper.discoverRequestType(request);
			logger.info(Resources.getInstance().get("REQUESTPROXY_LOG_REQUEST_TYPE", requestID, OctopusRequest.getRequestTypeName(requestType)));
			
			// test if request type is not allowed
			if (!isRequstTypeAllowed(requestType)) {
				logger.error(Resources.getInstance().get("REQUESTPROXY_LOG_ILLEGAL_REQUEST_TYPE", new String[] { requestID, String.valueOf(requestType), String.valueOf(allowedRequestTypes), }), null);
				octopus.sendError(null, octopusResponse, octopusSession, new Exception(Resources.getInstance().get("ERROR_MESSAGE_ILLEGAL_REQUEST_TYPE")));
				return;
			}
			
			if (initError != null) {
				logger.warn(Resources.getInstance().get("REQUESTPROXY_LOG_INITERROR_STOP", requestID), initError);
				octopus.sendError(null, octopusResponse, octopusSession, initError);
				return;
			}
			
			logger.trace(Resources.getInstance().get("REQUESTPROXY_LOG_SESSION_OBJECT_CREATED", requestID));
			
			OctopusRequest[] octRequests = extractRequests(request, requestType, requestID, moduleConfig);
			logger.trace(Resources.getInstance().get("REQUESTPROXY_LOG_REQUEST_OBJECT_CREATED", requestID));
			
			for (int i = 0; i < octRequests.length; i++) {
				OctopusRequest octopusRequest = octRequests[i];
				String askForCookies = octopusRequest.getParamAsString(OctopusRequest.PARAM_ASK_FOR_COOKIES);
				octopusRequest.setAskForCookies(askForCookies == null ? false : askForCookies.equalsIgnoreCase("true"));
				
				octopusRequest.setParam(OctopusRequest.PARAM_ENCODED_URL, response.encodeURL(request.getRequestURL().toString()));
				octopusRequest.setParam(OctopusRequest.PARAM_SESSION_ID, octopusSession.getId());
				octopusRequest.setResponse(requestedResponse);
				octopusRequest.setOctopusConnection(createOctopusConnection(octopusRequest, octopusSession));
				
				octopus.dispatch(octopusRequest, octopusResponse, octopusSession);
			}
		} catch (Exception e) {
			logger.error(Resources.getInstance().get("REQUESTPROXY_LOG_PROCESSING_EXCEPTION", requestID), e);
			
			OctopusRequest octopusRequest = new OctopusRequest();
			octopusRequest.setRequestParameters(new LinkedHashMap());
			octopusRequest.setModule(requestedModuleName);
			octopusRequest.setTask(requestedTaskName);
			octopusRequest.setResponse(requestedResponse);
			octopusRequest.setParam(OctopusRequest.PARAM_ENCODED_URL, response.encodeURL(request.getRequestURL().toString()));
			octopusRequest.setParam(OctopusRequest.PARAM_SESSION_ID, octopusSession.getId());
			octopusRequest.setOctopusConnection(createOctopusConnection(octopusRequest, octopusSession));
			
			octopus.sendError(octopusRequest, octopusResponse, octopusSession, e);
		}
		if (octopusResponse != null)
			octopusResponse.flush();
	}
	
	protected boolean isRequstTypeAllowed(int requestType) {
		return (allowedRequestTypes & requestType) == requestType;
	}
	
	/**
	 * Creates an internal OctopusConnection to the same target module as in the
	 * request with the same session.
	 */
	protected OctopusConnection createOctopusConnection(OctopusRequest request, OctopusSession session) {
		OctopusDirectCallConnection con = new OctopusDirectCallConnection();
		con.setModuleName(request.getModule());
		con.setOctopusStarter(new OctopusInternalStarter(octopus, session));
		return con;
	}
	
	/**
	 * Diese Methode extrahiert aus einer Http-Anfrage die zugehörigen
	 * Octopus-Anfragen.<br>
	 * TODO: genauer testen und ggfs eine Fehlerrückgabe
	 * 
	 * @param request
	 *            HTTP-Anfrage
	 * @param requestType
	 *            Anfragetyp
	 * @param requestID
	 *            ID der Gesamtanfrage
	 * @param moduleConfig
	 *            ModuleConfig
	 * @return Array von Octopus-Anfragen
	 * @throws OctopusRequestException
	 */
	private OctopusRequest[] extractRequests(HttpServletRequest request, int requestType, String requestID, ModuleConfig moduleConfig) throws OctopusRequestException {
		
		OctopusRequest[] requests = null;
		if (OctopusRequest.isWebType(requestType)) {
			// Normaler WEB-Request
			requests = HttpHelper.readWebRequest(request, requestType, requestID);
		} else if (OctopusRequest.isSoapType(requestType)) {
			// SOAP Request
			requests = HttpHelper.readSoapRequests(request, requestType, requestID, moduleConfig);
		} else if (OctopusRequest.isXmlRpcType(requestType)) {
			// XML-RPC Request
			requests = HttpHelper.readXmlRpcRequests(request, requestType, requestID);
		} else {
			return null;
		}
		
		HttpHelper.addHttpMetaData(requests, request, requestID);
		return requests;
	}
	
	/**
	 * Erstellt ein OctopusEnvironment aus den Konfigurationsparametern Servlets
	 * <br>
	 * <br>
	 * Die Informationen aus dem ServletContext werden mit dem prefix
	 * 'servletContext.' abgelegt. <br>
	 * Die Parameter aus dem Deployment Descriptor werden so übernommen, wie sie
	 * sind und können die des ServletContextes gegebenenfalls überschreiben.
	 * 
	 * @throws OctopusConfigException
	 */
	protected OctopusEnvironment createEnvironmentObject() throws OctopusConfigException {
		// Env Objekt mit Umgebungsvariablen und Einstellungsparametern
		OctopusEnvironment environment = new OctopusEnvironment();
		
		// Default-Module auf den ContextPath setzen.
		// Dies kann an späterer Stelle überschrieben werden.
		// environment.setValue(OctopusEnvironment.KEY_DEFAULT_MODULE,
		// getServletContext().getContextPath());
		
		ServletConfig config = getServletConfig();
		for (Enumeration e = config.getInitParameterNames(); e.hasMoreElements();) {
			String key = (String) e.nextElement();
			String val = config.getInitParameter(key);
			if (val != null)
				environment.setValue(key, val);
		}
		
		ServletContext context = getServletContext();
		for (Enumeration e = context.getAttributeNames(); e.hasMoreElements();) {
			String name = (String) e.nextElement();
			environment.setValue("servletContext", name, "" + context.getAttribute(name));
		}
		
		environment.setValue(OctopusEnvironment.KEY_PATHS_ROOT, context.getRealPath("/WEB-INF") + System.getProperty("file.separator"));
		
		environment.overrideValues("base", "/de/tarent/octopus/overrides");
		
		return environment;
	}
}
