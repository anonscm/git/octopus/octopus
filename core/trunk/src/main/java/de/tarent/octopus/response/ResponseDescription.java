/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.octopus.response;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Objekt zur Beschreibung der Ausgabeseite für verscheidenen Ausgabeformate.
 * 
 * @author Sebastian Mancke, tarent GmbH
 * @author Hendrik Helwich, tarent GmbH
 * @author Christoph Jerolimov, tarent GmbH
 */
public class ResponseDescription {
	private List types;
	private Map parameters;
	
	/**
	 * Inititalisiert die Antwortbeschreibung
	 */
	public ResponseDescription(String type, Map parameters) {
		if (type != null) {
			String types[] = type.split("\\,");
			this.types = new LinkedList();
			for (int i = 0; i < types.length; i++) {
				this.types.add(types[i].trim());
			}
		}
		this.parameters = parameters;
	}

	public List getTypes() {
		return types;
	}
	
	public boolean isTypeAllowed(String type) {
		return types == null || types.contains(type);
	}
	
	public Map getParameters() {
		return parameters;
	}

	public Object getParameter(String key) {
		return parameters != null ? parameters.get(key) : null;
	}

	public String getAsString(String key) {
		Object value = getParameter(key);
		return value != null ? value.toString() : null;
	}
}
