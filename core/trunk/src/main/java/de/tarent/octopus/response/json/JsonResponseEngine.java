/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package de.tarent.octopus.response.json;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONNull;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
import net.sf.json.util.CycleDetectionStrategy;

import de.tarent.octopus.config.ModuleConfig;
import de.tarent.octopus.request.OctopusResponse;
import de.tarent.octopus.response.AbstractResponseEngine;
import de.tarent.octopus.response.ResponseDescription;
import de.tarent.octopus.response.ResponseEngine;
import de.tarent.octopus.response.ResponseProcessingException;
import de.tarent.octopus.server.OctopusContext;

/**
 * This class is an implementation of the {@link ResponseEngine} and will be
 * used to transform the response in JSON objects.
 * 
 * @author Alex Maier, tarent GmbH
 */
public class JsonResponseEngine extends AbstractResponseEngine implements ResponseEngine {
	public String getDefaultName() {
		return "json";
	}

	public String getDefaultContentType() {
		return "application/json";
	}
	
	public void init(ModuleConfig moduleConfig) {
		JsonConfig.getInstance().setCycleDetectionStrategy(
				new CycleDetectionStrategy() {
					public JSONArray handleRepeatedReferenceAsArray(Object arg0) {
						return JSONArray.fromObject(arg0);
					}
					
					public JSONObject handleRepeatedReferenceAsObject(Object arg0) {
						return JSONObject.fromObject(arg0);
					}
				});
	}

	public void sendEmptyResponse(ResponseDescription desc, OctopusContext octopusContext, OctopusResponse octopusResponse) throws ResponseProcessingException {
		try {
			JSONNull json = JSONNull.getInstance();
			json.write(octopusResponse.getWriter());
		} catch (Exception e) {
			throw new ResponseProcessingException(e.getLocalizedMessage(), e);
		}
		
		octopusResponse.getWriter().flush();
	}
	
	public void sendNormalResponse(ResponseDescription desc, OctopusContext octopusContext, OctopusResponse octopusResponse) throws ResponseProcessingException {
		Map outputFields = getOutputFields(desc, octopusContext, octopusResponse);
		
		Object value;
		if (outputFields == null || outputFields.isEmpty())
			value = null;
		else if (outputFields.size() == 1)
			value = outputFields.values().iterator().next();
		else
			value = outputFields;
		
		try {
			if (value == null) {
				JSONNull json = JSONNull.getInstance();
				json.write(octopusResponse.getWriter());
			} else if (value instanceof Object[]) {
				JSONArray json = new JSONArray();
				json.addAll(Arrays.asList((Object[]) value));
				json.write(octopusResponse.getWriter());
			} else if (value instanceof Collection) {
				JSONArray json = new JSONArray();
				json.addAll((Collection) value);
				json.write(octopusResponse.getWriter());
			} else if (value instanceof Map) {
				JSONObject json = new JSONObject();
				json.putAll((Map) value);
				json.write(octopusResponse.getWriter());
			} else {
				JSONArray json = new JSONArray();
				json.add(value);
				json.write(octopusResponse.getWriter());
			}
		} catch (Exception e) {
			throw new ResponseProcessingException(e.getLocalizedMessage(), e);
		}
		
		octopusResponse.getWriter().flush();
	}

	public void sendErrorResponse(ResponseDescription desc, OctopusContext octopusContext, OctopusResponse octopusResponse, String message, Exception exception) throws ResponseProcessingException {
		try {
			JSONArray json = new JSONArray();
			json.add(message);
			json.write(octopusResponse.getWriter());
		} catch (Exception e) {
			throw new ResponseProcessingException(e.getLocalizedMessage(), e);
		}
		
		octopusResponse.getWriter().flush();
	}
	
	public void sendEmptyCallback(ResponseDescription desc, OctopusContext octopusContext, OctopusResponse octopusResponse) throws ResponseProcessingException {
		sendEmptyResponse(desc, octopusContext, octopusResponse);
	}
	
	public void sendNormalCallback(ResponseDescription desc, OctopusContext octopusContext, OctopusResponse octopusResponse) throws ResponseProcessingException {
		sendNormalResponse(desc, octopusContext, octopusResponse);
	}
	
	public void sendErrorCallback(ResponseDescription desc, OctopusContext octopusContext, OctopusResponse octopusResponse, String message, Exception exception) throws ResponseProcessingException {
		sendErrorResponse(desc, octopusContext, octopusResponse, message, exception);
	}
}
