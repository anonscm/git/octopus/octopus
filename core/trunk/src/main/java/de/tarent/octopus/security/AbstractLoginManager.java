/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.octopus.security;

import java.net.PasswordAuthentication;
import java.util.HashMap;
import java.util.Map;

import de.tarent.octopus.config.CommonConfig;
import de.tarent.octopus.config.OctopusConfigException;
import de.tarent.octopus.config.Task;
import de.tarent.octopus.config.TaskList;
import de.tarent.octopus.request.OctopusRequest;
import de.tarent.octopus.request.OctopusSession;
import de.tarent.octopus.server.LoginManager;
import de.tarent.octopus.server.PersonalConfig;
import de.tarent.octopus.server.UserManager;

/**
 * Abstrakte Implementierung eines LoginManagers unter verwendeung des
 * Template-Method-Pattern <br>
 * <br>
 * 
 * @author <a href="mailto:mancke@mancke-software.de">Sebastian Mancke</a>,
 *         <b>tarent GmbH</b>
 */
public abstract class AbstractLoginManager implements LoginManager {
	
	public static String PREFIX_PERSONAL_CONFIGS = "personalConfig-";
	
	protected Map configuration = new HashMap();
	
	public void setConfiguration(Map configuration) {
		assert configuration != null;
		this.configuration = configuration;
	}
	
	protected Object getConfigurationParam(Object key) {
		return (key != null) ? configuration.get(key) : null;
	}
	
	protected String getConfigurationString(Object key) {
		Object value = getConfigurationParam(key);
		return (value != null) ? value.toString() : null;
	}
	
	
	public void handleAuthentication(CommonConfig config, OctopusRequest octopusRequest, OctopusSession theSession) throws OctopusSecurityException {
		
		PersonalConfig pConfig = getPersonalConfig(config, octopusRequest, theSession);
		boolean wasNew = false;
		if (pConfig == null) {
			wasNew = true;
			try {
				pConfig = config.createNewConfigImpl(octopusRequest.getModule());
			} catch (OctopusConfigException e) {
				throw new OctopusSecurityException(e.getMessage(), e.getCause());
			}
			theSession.setAttribute(PREFIX_PERSONAL_CONFIGS + octopusRequest.getModule(), pConfig);
		} else if (pConfig.isUserInGroup(PersonalConfig.GROUP_LOGGED_OUT))
			pConfig.setUserGroups(new String[] { PersonalConfig.GROUP_ANONYMOUS });
		
		String task = octopusRequest.getTask();
		PasswordAuthentication pwdAuth = octopusRequest.getPasswordAuthentication();
		if (TASK_LOGIN.equals(octopusRequest.get(task)) || TASK_LOGIN_SOAP.equals(task) || pwdAuth != null) {
			
			if (pwdAuth == null)
				throw new OctopusSecurityException(OctopusSecurityException.ERROR_INCOMPLETE_USER_DATA);
			
			doLogin(config, pConfig, octopusRequest);
		}

		// Wenn nicht eingeloggt wird
		// und eine neue Session gestartet wurde
		// und das Task nicht für Anonymous freigegeben ist
		// ==> Meldung, dass die Session ungültig ist
		else if (wasNew) {
			TaskList taskList = config.getTaskList(octopusRequest.getModule());
			Task t = taskList.getTask(octopusRequest.getTask());
			String[] taskGroups = t.getGroups();
			
			if (!arrayContains(taskGroups, PersonalConfig.GROUP_ANONYMOUS)) {
				throw new OctopusSecurityException(OctopusSecurityException.ERROR_NO_VALID_SESSION);
			}
		}
		
		if (TASK_LOGOUT.equals(task) || TASK_LOGOUT_SOAP.equals(task)) {
			doLogout(config, pConfig, octopusRequest);
		}
	}
	
	/**
	 * Template-Method Pattern
	 */
	protected abstract void doLogin(CommonConfig config, PersonalConfig pConfig, OctopusRequest octopusRequest) throws OctopusSecurityException;
	
	/**
	 * Template-Method Pattern
	 */
	protected abstract void doLogout(CommonConfig config, PersonalConfig pConfig, OctopusRequest octopusRequest) throws OctopusSecurityException;
	
	
	public PersonalConfig getPersonalConfig(CommonConfig config, OctopusRequest octopusRequest, OctopusSession theSession) {
		return (PersonalConfig) theSession.getAttribute(PREFIX_PERSONAL_CONFIGS + octopusRequest.getModule());
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.tarent.octopus.server.LoginManager#isUserManagementSupported()
	 */
	public boolean isUserManagementSupported() {
		return false;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.tarent.octopus.server.LoginManager#getUserManager()
	 */
	public UserManager getUserManager() {
		return null;
	}
	
	protected boolean arrayContains(String[] array, String item) {
		for (int i = 0; i < array.length; i++)
			if (array[i].equals(item))
				return true;
		return false;
	}
	
}
