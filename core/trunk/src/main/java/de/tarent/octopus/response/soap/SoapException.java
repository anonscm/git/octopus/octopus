/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.octopus.response.soap;

import java.io.OutputStream;

import org.apache.axis.AxisFault;
import org.apache.axis.Message;

import de.tarent.octopus.security.OctopusSecurityException;

public class SoapException extends Exception {
	/**
	 * serialVersionUID = -4535324668548567738L
	 */
	private static final long serialVersionUID = -4535324668548567738L;
	
	protected AxisFault axisFault;
	
	public SoapException(String message) {
		super(message);
		axisFault = new AxisFault(message);
		axisFault.clearFaultDetails();
	}
	
	public SoapException(Exception e) {
		if (e instanceof AxisFault)
			axisFault = (AxisFault) e;
		else if (e instanceof OctopusSecurityException) {
			OctopusSecurityException se = (OctopusSecurityException) e;
			axisFault = new AxisFault(se.getMessage(), se);
			axisFault.setFaultString(se.getMessage());
			if (se.getDetailMessage() != null) {
				axisFault.addFaultDetailString(se.getDetailMessage());
			}
			axisFault.setFaultCode(se.getSoapFaultCode());
		} else
			axisFault = AxisFault.makeFault(e);
	}
	
	public void writeTo(OutputStream out) throws java.io.IOException, javax.xml.soap.SOAPException {
		Message outMessage = new Message(axisFault);
		outMessage.writeTo(out);
	}
	
	public AxisFault getAxisFault() {
		return axisFault;
	}
}
