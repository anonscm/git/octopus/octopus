/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.octopus.request;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.logging.Log;

import de.tarent.octopus.config.CommonConfig;
import de.tarent.octopus.config.ModuleConfig;
import de.tarent.octopus.config.TaskManager;
import de.tarent.octopus.content.OctopusContent;
import de.tarent.octopus.content.ContentWorker;
import de.tarent.octopus.content.ContentWorkerFactory;
import de.tarent.octopus.logging.LogFactory;
import de.tarent.octopus.resource.Resources;
import de.tarent.octopus.response.ResponseProcessingException;
import de.tarent.octopus.response.ResponseDescription;
import de.tarent.octopus.security.OctopusSecurityException;
import de.tarent.octopus.server.Closeable;
import de.tarent.octopus.server.Context;
import de.tarent.octopus.server.LoginManager;
import de.tarent.octopus.server.OctopusContext;
import de.tarent.octopus.server.OctopusContextImpl;
import de.tarent.octopus.server.PersonalConfig;
import de.tarent.octopus.util.Threads;

/**
 * Wichtigste Steuerkomponente für den Ablauf und die Abarbeitung einer Anfrage.
 * <br>
 * <br>
 * Initialisiert das ganze System mit dem Konstructor und arbeitet eine Anfrage
 * mit dispatch() ab.
 * 
 * @author Sebastian Mancke, tarent GmbH
 * @author Michael Klink, tarent GmbH
 * @author Alex Maier, tarent GmbH
 * @author Christoph Jerolimov, tarent GmbH
 */
public class OctopusRequestDispatcher {
	private static final String DEFAULT_TASK_NAME = "default";
	
	private CommonConfig commonConfig;
	private static transient Log logger = LogFactory.getLog(OctopusRequestDispatcher.class);
	
	/**
	 * Initialisierung des Systes. Die Komponenten ResponseCreator,
	 * SecurityManager und CommonConfig werden aufgebaut und initialisiert.
	 * 
	 * @param common
	 *            gemeinsames Basiskonfigurationsobjekt
	 */
	public OctopusRequestDispatcher(CommonConfig common) {
		commonConfig = common;
	}
	
	/**
	 * Abarbeitung einer Anfrage.
	 * <ol>
	 * <b>Ablauf:</b>
	 * <li>Wenn der Request als Task 'login' oder 'logout' hat, wird über den
	 * SecutityManager versucht, dies zu tun.</li>
	 * <li>Als nächstes wird kontrolliert, ob der Benutzer eingeloggt ist,
	 * gegebenenfalls wird versucht, einen Login zu erzwingen. Wenn dies der
	 * Fall ist wird die Config mit der Personal- und CommonConfig erstellt.</li>
	 * <li>Nun wird das Task abgearbeitet. Dazu werden abhängig vom
	 * TaskManager, der die Tasksteuerung übernimmt, die ActionWorker gestartet,
	 * die den OctopusContent verarbeiten.</li>
	 * <li>Als letztes wird die Ausgabe über den ResponseCreator erstellt und
	 * ausgegeben. Welche Ausgabe gemacht werden soll, bestimmt der TaskManager.</li>
	 * </ol>
	 * 
	 * @param octopusRequest
	 *            Anfrageparameter.
	 * @param octopusResponse
	 *            Objekt, das Methoden für die Ausgabe des Ergebnisses bereit
	 *            stellt. Es wird an den ResponseCreator weiter gegeben.
	 * @param theSession
	 *            Sessionobjekt, das die PersonalConfig aufnehmen kann.
	 */
	public void dispatch(OctopusRequest octopusRequest, OctopusResponse octopusResponse, OctopusSession theSession) throws ResponseProcessingException {
		
		String requestID = octopusRequest.getRequestID();
		
		// ==================================================
		// 1. Task- und Modulname feststellen und verifizieren
		
		String module = octopusRequest.getModule();
		String task = octopusRequest.getTask();
		
		if ((module == null || module.length() == 0) && commonConfig.getDefaultModuleName() != null) {
			module = commonConfig.getDefaultModuleName();
			octopusRequest.setModule(module);
		}
		
		ModuleConfig moduleConfig = commonConfig.getModuleConfig(module);
		if (moduleConfig == null) {
			logger.error(Resources.getInstance().get("REQUESTDISPATCHER_LOG_NO_MODULE", requestID, module));
			throw new ResponseProcessingException(Resources.getInstance().get("REQUESTDISPATCHER_EXC_NO_MODULE", module));
		}
		
		if (task == null || task.length() == 0) {
			task = DEFAULT_TASK_NAME;
			octopusRequest.setTask(task);
		}
		if (moduleConfig.getTaskList().getTask(task) == null) {
			logger.error(Resources.getInstance().get("REQUESTDISPATCHER_LOG_NO_TASK", requestID, task, module));
			throw new ResponseProcessingException(Resources.getInstance().get("REQUESTDISPATCHER_EXC_NO_TASK", task, module));
		}
		
		octopusResponse.setModuleName(module);
		octopusResponse.setTaskName(task);
		
		logger.info(Resources.getInstance().get("REQUESTDISPATCHER_LOG_DISPATCHING", requestID, module, task));
		
		//
		// Wirklich auszuführendes Module und Task sind nun identifiziert.
		//
		
		
		
		// ==================================================
		// 2. Security
		PersonalConfig personalConfig = null;
		OctopusContent octopusContent = new OctopusContent();
		OctopusContext octopusContext = new OctopusContextImpl(
				octopusRequest,
				octopusContent,
				personalConfig,
				commonConfig,
				moduleConfig);
		ClassLoader outerLoader = null;
		
		try {
			Context.addActive(octopusContext);
			outerLoader = Threads.setContextClassLoader(moduleConfig.getClassLoader());
			LoginManager loginManager = commonConfig.getLoginManager(moduleConfig);
			loginManager.handleAuthentication(commonConfig, octopusRequest, theSession);
			personalConfig = loginManager.getPersonalConfig(commonConfig, octopusRequest, theSession);
			personalConfig.testTaskAccess(commonConfig, octopusRequest);
			Threads.setContextClassLoader(outerLoader);
		} catch (Exception e) {
			// Für diese Aktion ist eine andere Berechtigung nötig
			if (logger.isInfoEnabled())
				logger.info(Resources.getInstance().get("REQUESTDISPATCHER_LOG_SESSION_ERROR", requestID, module, task), e);
			
			if (!(e instanceof OctopusSecurityException))
				e = new OctopusSecurityException(OctopusSecurityException.ERROR_SERVER_AUTH_ERROR, e);
			
			// FIXME sendError(new OctopusConfig(commonConfig, null, moduleConfig.getName()), moduleConfig, octopusRequest, octopusResponse, e.getLocalizedMessage(), (OctopusSecurityException) e);
			
			if (logger.isInfoEnabled())
				logger.debug("Authentication Error wurde an den Client gesendet. Kehre nun zurück.");
			
			return;
		} finally {
			Threads.setContextClassLoader(outerLoader);
			Context.clear();
		}
		
		// Berechtigung erfolgreich geprüft!
		
		// FIXME octopusContext.setPersonalConfig(personalConfig);
		

		//
		// Authentifizierung ist beendet.
		//
		
		// ==================================================
		// 3. Abarbeitung des auszuführenden Task
		TaskManager taskManager = new TaskManager(octopusContext, octopusResponse);
		try {
			Context.addActive(octopusContext);
			outerLoader = Threads.setContextClassLoader(moduleConfig.getClassLoader());
			
			putStandardParams(octopusContext);
			
			taskManager.start(true);
			
			if (!taskManager.isCurrentTaskAsynchron())
				// run the task job in synchron mode
				taskManager.run();
			else {
				taskManager.createResponseDescription();
				// send the empty response
				octopusContext.getResponseCreator().sendEmptyResponse(taskManager.getCurrentResponseDescription(), octopusContext, octopusResponse);
				// start in an other thread, because the task is asynchron
				new Thread(taskManager).start();
			}
			
		} catch (Exception e) {
			logger.error(Resources.getInstance().get("REQUESTDISPATCHER_LOG_TASK_ERROR", requestID, task), e);
			sendError(octopusContext, octopusResponse, Resources.getInstance().get("REQUESTDISPATCHER_OUT_TASK_ERROR", task), e);
		} finally {
			// run the cleanup hooks; this should never cause an exception
			// processCleanupCode(requestID, octopusContent);
			
			Threads.setContextClassLoader(outerLoader);
			Context.clear();
		}
	}
	
	/**
	 * This helper method executes all {@link Runnable} and {@link Closeable}
	 * objects in the cleanup list within the context. This list is expected to
	 * be associated with the key
	 * {@link OctopusContext#CLEANUP_OBJECT_LIST "octopus.cleanup.objects"}.
	 * During this process the cleanup list is emptied.
	 */
	public static void processCleanupCode(String requestID, OctopusContext octopusContext) {
		Object cleanupObjectList = octopusContext.getContextField(OctopusContext.CLEANUP_OBJECT_LIST);
		if (cleanupObjectList instanceof Collection) {
			for (Iterator iter = ((Collection) cleanupObjectList).iterator(); iter.hasNext();) {
				Object cleanupObject = iter.next();
				try {
					if (cleanupObject instanceof Runnable) {
						((Runnable) cleanupObject).run();
						// remove after processing
						iter.remove();
					} else if (cleanupObject instanceof Closeable) {
						((Closeable) cleanupObject).close();
						// remove after processing
						iter.remove();
					} else
						logger.error(Resources.getInstance().get("REQUESTDISPATCHER_LOG_CLEANUPOBJECT_WRONG_TYPE", requestID, (cleanupObject == null ? null : cleanupObject.getClass().getName()), cleanupObject));
				} catch (Throwable t) {
					logger.error(Resources.getInstance().get("REQUESTDISPATCHER_LOG_CLEANUPOBJECT_CLOSE_ERROR", requestID, cleanupObject), t);
				}
			}
		} else if (cleanupObjectList != null)
			logger.warn(Resources.getInstance().get("REQUESTDISPATCHER_LOG_INVALID_CLEANUPLIST", requestID, cleanupObjectList.getClass().getName()));
	}
	
	/**
	 * Einige Parameter, die fast immer in der Ausgabe benötigt werden, schonmal
	 * in den OctopusContent schieben
	 */
	public void putStandardParams(OctopusContext octopusContext) {
		String standardParamWorker = Resources.getInstance().get("REQUESTDISPATCHER_CLS_PARAM_WORKER");
		try {
			ContentWorker worker = ContentWorkerFactory.getContentWorker(
					octopusContext.moduleConfig(),
					standardParamWorker,
					octopusContext.getRequestObject().getRequestID());
			worker.doAction("putMinimal", octopusContext);
		} catch (Exception e) {
			logger.error(Resources.getInstance().get("REQUESTDISPATCHER_LOG_PARAM_SET_ERROR", octopusContext.getRequestObject().getRequestID(), standardParamWorker), e);
		}
	}
	
	/**
	 * Diese Methode liefert das enthaltene Konfigurationsobjekt.
	 * 
	 * @return Konfigurationsobjekt.
	 */
	public CommonConfig getCommonConfig() {
		return commonConfig;
	}

	public void sendError(OctopusRequest octopusRequest, OctopusResponse octopusResponse, Exception exception) {
		String requestID = octopusRequest.getRequestID();
		
		// ==================================================
		// 1. Task- und Modulname feststellen und verifizieren
		
		String module = octopusRequest.getModule();
		String task = octopusRequest.getTask();
		
		if ((module == null || module.length() == 0) && commonConfig.getDefaultModuleName() != null) {
			module = commonConfig.getDefaultModuleName();
			octopusRequest.setModule(module);
		}
		
		ModuleConfig moduleConfig = commonConfig.getModuleConfig(module);
		if (moduleConfig == null) {
			logger.error(Resources.getInstance().get("REQUESTDISPATCHER_LOG_NO_MODULE", requestID, module));
		}
		
		if (task == null || task.length() == 0) {
			task = DEFAULT_TASK_NAME;
			octopusRequest.setTask(task);
		}
		if (moduleConfig != null && moduleConfig.getTaskList().getTask(task) == null) {
			logger.error(Resources.getInstance().get("REQUESTDISPATCHER_LOG_NO_TASK", requestID, task, module));
		}
		
		octopusResponse.setModuleName(module);
		octopusResponse.setTaskName(task);
		
		logger.info(Resources.getInstance().get("REQUESTDISPATCHER_LOG_DISPATCHING", requestID, module, task));
		
		//
		// Wirklich auszuführendes Module und Task sind nun identifiziert.
		//
		
		
		// ==================================================
		// 2. Security
		PersonalConfig personalConfig = null;
		OctopusContent octopusContent = new OctopusContent();
		OctopusContext octopusContext = new OctopusContextImpl(
				octopusRequest,
				octopusContent,
				personalConfig,
				commonConfig,
				moduleConfig);
		
		try {
			sendError(octopusContext, octopusResponse, null, exception);
		} catch (ResponseProcessingException e) {
			logger.error(e.toString(), e);
		}
	}
	
	public void sendError(OctopusContext octopusContext, OctopusResponse octopusResponse, String message, Exception exception) throws ResponseProcessingException {
		if (exception instanceof OctopusSecurityException && !OctopusRequest.isWebType(octopusContext.getRequestObject().getRequestType())) {
			sendAuthenticationError(octopusContext, octopusResponse, message, (OctopusSecurityException) exception);
			return;
		}
		
		ResponseDescription responseDescription;
		if (exception instanceof OctopusSecurityException)
			responseDescription = new ResponseDescription("soap", null);
		else
			responseDescription = new ResponseDescription("soap", null);
		
		OctopusContent theContent = new OctopusContent(exception);
		Map responseParams = new HashMap();
		responseParams.put("message", message);
		theContent.setField("responseParams", responseParams);
		putStandardParams(octopusContext);
		logger.debug(Resources.getInstance().get("REQUESTDISPATCHER_LOG_SENDING_ERROR", octopusContext.getRequestObject().getRequestID(), message), exception);
		
		octopusContext.getResponseCreator().sendErrorResponse(responseDescription, octopusContext, octopusResponse, message, exception);
	}
	
	/**
	 * Senden von Informationen zu den Fehlern, die bei der Authentifizierung
	 * aufgetreten sind.
	 */
	private void sendAuthenticationError(OctopusContext octopusContext, OctopusResponse octopusResponse, String message, OctopusSecurityException securityException)
			throws ResponseProcessingException {
		
		// String message = securityException.getMessage();
		
		octopusResponse.setAuthorisationRequired(octopusContext.moduleConfig().getOnUnauthorizedAction());
		
		if (OctopusRequest.isWebType(octopusContext.getRequestObject().getRequestType())) {
			// Web-Type über sendResponse abwickeln.
			sendError(octopusContext, octopusResponse, null, securityException);
		} else {
			// Eine SOAP Anfrage bekommt auch eine SOAP Fehlermeldung
			// throw new ResponseProcessingException(message, new
			// SoapException(message));
			if (logger.isDebugEnabled())
				logger.debug(Resources.getInstance().get("REQUESTDISPATCHER_LOG_SENDING_ERROR", octopusContext.getRequestObject().getRequestID(), securityException.getMessage()));
			if (logger.isTraceEnabled())
				logger.trace(Resources.getInstance().get("REQUESTDISPATCHER_LOG_SENDING_ERROR", octopusContext.getRequestObject().getRequestID(), securityException.getMessage()), securityException);
			// FIXME octopus.sendError(octopusRequest, new
			// SoapException(securityException));
			return;
		}
	}
}
