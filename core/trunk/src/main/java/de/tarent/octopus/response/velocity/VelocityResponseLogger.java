/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.octopus.response.velocity;

import org.apache.commons.logging.Log;
import org.apache.velocity.runtime.RuntimeServices;
import org.apache.velocity.runtime.log.LogSystem;

import de.tarent.octopus.logging.LogFactory;

public class VelocityResponseLogger implements LogSystem {
	/** Der Logger */
	private static Log logger = LogFactory.getLog(VelocityResponseLogger.class);
	
	public VelocityResponseLogger() {
		// do Nothing
	}
	
	public void init(RuntimeServices rsvc) {
		// do Nothing
	}
	
	public void logVelocityMessage(int level, String message) {
		switch (level) {
		case 1:
			logger.trace(message);
			break;
		case 2:
			logger.debug(message);
			break;
		case 3:
			logger.warn(message);
			break;
		default:
			logger.error("[Unknown Level (" + level + ")] " + message);
			break;
		}
	}
}
