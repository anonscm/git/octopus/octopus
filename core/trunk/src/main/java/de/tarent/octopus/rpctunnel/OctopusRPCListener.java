/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */
package de.tarent.octopus.rpctunnel;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import de.tarent.octopus.client.OctopusConnection;
import de.tarent.octopus.client.OctopusConnectionFactory;
import de.tarent.octopus.client.OctopusResult;
import de.tarent.octopus.config.CommonConfig;
import de.tarent.octopus.request.Octopus;

class OctopusRPCListener implements RPCListener {
	private static final String CONNECTION_NAME = "octopus";
	
	public OctopusRPCListener(Octopus octopus, CommonConfig commonconfig) {
	}
	
	public Map execute(String myRole, String partnerRole, String module, String task, Map parameters) {
		OctopusConnectionFactory ocConnectionFactory = OctopusConnectionFactory.getInstance();
		
		Map configuration = new HashMap();
		configuration.put(OctopusConnectionFactory.CONNECTION_TYPE_KEY, OctopusConnectionFactory.CONNECTION_TYPE_INTERNAL);
		configuration.put(OctopusConnectionFactory.MODULE_KEY, module);
		ocConnectionFactory.setConfiguration(CONNECTION_NAME, configuration);
		OctopusConnection ocConnection = ocConnectionFactory.getConnection(CONNECTION_NAME);
		
		OctopusResult ocResult = ocConnection.callTask(task, parameters);
		Iterator iter = ocResult.getDataKeys();
		
		// Create result map
		Map data = new HashMap();
		while (iter.hasNext()) {
			String key = (String) iter.next();
			data.put(key, ocResult.getData(key));
		}
		return data;
	}
}
