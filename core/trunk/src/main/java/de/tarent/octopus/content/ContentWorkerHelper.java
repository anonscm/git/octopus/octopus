/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

/* $Id: ContentWorkerHelper.java,v 1.3 2007/06/11 13:05:36 christoph Exp $
 * Created on 27.10.2003
 */
package de.tarent.octopus.content;

import de.tarent.octopus.request.OctopusRequest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Diese Klasse enthält Hilfsmethoden für ContentWorker.
 * 
 * @author mikel
 */
public class ContentWorkerHelper {
	/**
	 * Diese Methode liefert einen Parameter zurück, wobei zunächst im
	 * OctopusContent, dann im Request gesucht wird.
	 * 
	 * @param octopusRequest
	 *            Request-Objekt
	 * @param octopusContent
	 *            OctopusContent-Objekt
	 * @param key
	 *            Schlüssel des gesuchten Parameters
	 * @return Wert des Parameters
	 */
	public static Object getParam(OctopusRequest octopusRequest, OctopusContent octopusContent, String key) {
		Object param = octopusContent.getAsObject(key);
		return (param == null) ? octopusRequest.getParam(key) : param;
	}
	
	/**
	 * Diese Methode liefert einen Parameter als Liste, wobei zunächst im
	 * OctopusContent, dann im Request gesucht wird.
	 * 
	 * @param octopusRequest
	 *            Request-Objekt
	 * @param octopusContent
	 *            OctopusContent-Objekt
	 * @param key
	 *            Schlüssel des gesuchten Parameters
	 * @return Wert des Parameters
	 */
	public static List getParamAsList(OctopusRequest octopusRequest, OctopusContent octopusContent, String key) {
		Object param = octopusContent.get(key);
		if (param == null)
			param = octopusRequest.getParam(key);
		List result = null;
		if (param instanceof List)
			result = (List) param;
		else if (param instanceof Object[])
			result = Arrays.asList((Object[]) param);
		else {
			result = new ArrayList();
			if (param != null)
				result.add(param);
		}
		return result;
	}
	
	/**
	 * Diese Methode liefert einen Parameter als String, wobei zunächst im
	 * OctopusContent, dann im Request gesucht wird.
	 * 
	 * @param octopusRequest
	 *            Request-Objekt
	 * @param octopusContent
	 *            OctopusContent-Objekt
	 * @param key
	 *            Schlüssel des gesuchten Parameters
	 * @return Wert des Parameters
	 */
	public static String getParamAsString(OctopusRequest octopusRequest, OctopusContent octopusContent, String key) {
		String param = octopusContent.getAsString(key);
		return (param == null) ? octopusRequest.get(key) : param;
	}
	
	/**
	 * Diese Methode liefert einen Parameter als int, wobei zunächst im
	 * OctopusContent, dann im Request gesucht wird.
	 * 
	 * @param octopusRequest
	 *            Request-Objekt
	 * @param octopusContent
	 *            OctopusContent-Objekt
	 * @param key
	 *            Schlüssel des gesuchten Parameters
	 * @param defaultValue
	 *            default-Wert für den Parameter
	 * @return Wert des Parameters
	 */
	public static int getParamAsInt(OctopusRequest octopusRequest, OctopusContent octopusContent, String key, int defaultValue) {
		String stringVal = getParamAsString(octopusRequest, octopusContent, key);
		if (stringVal != null)
			try {
				return Integer.parseInt(stringVal);
			} catch (NumberFormatException e) {
			}
		return defaultValue;
	}
	
	/**
	 * Diese Methode liefert einen Parameter als boolean, wobei zunächst im
	 * OctopusContent, dann im Request gesucht wird.
	 * 
	 * @param octopusRequest
	 *            Request-Objekt
	 * @param octopusContent
	 *            OctopusContent-Objekt
	 * @param key
	 *            Schlüssel des gesuchten Parameters
	 * @param defaultValue
	 *            default-Wert für den Parameter
	 * @return Wert des Parameters
	 */
	public static boolean getParamAsBoolean(OctopusRequest octopusRequest, OctopusContent octopusContent, String key, boolean defaultValue) {
		String stringVal = getParamAsString(octopusRequest, octopusContent, key);
		if (stringVal != null)
			return Boolean.valueOf(stringVal).booleanValue();
		return defaultValue;
	}
	
	/**
	 * Dieser private Konstruktor verbietet das instanziieren dieser Klasse.
	 * 
	 */
	private ContentWorkerHelper() {
		super();
	}
}
