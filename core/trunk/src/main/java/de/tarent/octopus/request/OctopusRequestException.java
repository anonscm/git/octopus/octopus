/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */
package de.tarent.octopus.request;

/**
 * Illegal request format exception or parse error.
 * 
 * @author Christoph Jerolimov, tarent GmbH
 */
public class OctopusRequestException extends Exception {
	private static final long serialVersionUID = -1004060579375291115L;
	
	public OctopusRequestException() {
		// nothing
	}
	
	public OctopusRequestException(String message) {
		super(message);
	}
	
	public OctopusRequestException(Throwable throwable) {
		super(throwable);
	}
	
	public OctopusRequestException(String message, Throwable throwable) {
		super(message, throwable);
	}
}
