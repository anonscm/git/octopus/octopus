/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */


package de.tarent.octopus.extensions;

import org.apache.commons.logging.Log;

import de.tarent.octopus.logging.LogFactory;

/**
 * A simple extension loader that relieves the calling code of extensive error
 * handling on instantiation of the selected extension. TODO: this should be
 * extended to include a simple configurable extension description and automatic
 * loading and running of extensions from a configuration.
 * 
 * @author Michael Kleinhenz (m.kleinhenz@tarent.de)
 */
public class OctopusExtensionLoader {
	private static Log logger = LogFactory.getLog(OctopusExtensionLoader.class);
	
	/**
	 * Loads, initializes and starts the extension given by classname. On
	 * initialization, the given parameter is used.
	 * 
	 * @param classname
	 *            Extension class to be initialized and started.
	 * @param param
	 *            Parameter passed to the extension's initialize method.
	 * @return Extension instance or null if the extension loading failed.
	 */
	public static OctopusExtension load(String classname, Object param) {
		logger.info("Enabling octopus extension: " + classname);
		
		OctopusExtension extension = null;
		try {
			extension = (OctopusExtension) Class.forName(classname).newInstance();
		} catch (InstantiationException e) {
			logger.error("Error getting extension instance: " + classname, e);
		} catch (IllegalAccessException e) {
			logger.error("Illegal Access getting extension instance: " + classname, e);
		} catch (ClassNotFoundException e) {
			logger.info("Extension class not found: " + classname, e);
		}
		
		if (extension != null) {
			extension.initialize(param);
			extension.start();
		}
		
		return extension;
	}
}
