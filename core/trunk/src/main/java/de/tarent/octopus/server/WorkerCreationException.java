/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.octopus.server;

/**
 * Exception, für Fehler beim Erstellen einer neuen Worker-Instatnz
 * 
 * @see de.tarent.octopus.content.ContentWorker
 * @author <a href="mailto:mancke@mancke-software.de">Sebastian Mancke</a>,
 *         <b>tarent GmbH</b>
 */
public class WorkerCreationException extends Exception {
	/**
	 * serialVersionUID = 46639696610147426L;
	 */
	private static final long serialVersionUID = 46639696610147426L;
	
	public WorkerCreationException() {
		super();
	}
	
	public WorkerCreationException(String msg) {
		super(msg);
	}
	
	public WorkerCreationException(String msg, Throwable cause) {
		super(msg, cause);
	}
	
	public WorkerCreationException(Throwable cause) {
		super(cause);
	}
}
