/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */
package de.tarent.octopus.request;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.prefs.Preferences;

import org.apache.commons.logging.Log;

import de.tarent.octopus.config.ModuleLookup;
import de.tarent.octopus.config.CommonConfig;
import de.tarent.octopus.config.OctopusConfigException;
import de.tarent.octopus.config.ModuleConfig;
import de.tarent.octopus.config.OctopusEnvironment;
import de.tarent.octopus.config.Task;
import de.tarent.octopus.config.TaskList;
import de.tarent.octopus.config.TaskManager;
import de.tarent.octopus.config.TaskProzessingException;
import de.tarent.octopus.content.OctopusContent;
import de.tarent.octopus.content.ContentProzessException;
import de.tarent.octopus.extensions.OctopusExtension;
import de.tarent.octopus.extensions.OctopusExtensionLoader;
import de.tarent.octopus.jndi.OctopusContextJndiFactory;
import de.tarent.octopus.jndi.OctopusInstanceJndiFactory;
import de.tarent.octopus.logging.LogFactory;
import de.tarent.octopus.resource.Resources;
import de.tarent.octopus.response.ResponseProcessingException;
import de.tarent.octopus.rpctunnel.OctopusRPCTunnel;
import de.tarent.octopus.server.Context;
import de.tarent.octopus.server.OctopusContext;
import de.tarent.octopus.server.OctopusContextImpl;

/**
 * Diese Klasse dient als Wrapper für Octopus-Funktionalitäten, der auch
 * außerhalb von Web-Applikationskontexten benutzt werden kann.
 * 
 * @author Sebastian Mancke, tarent GmbH
 * @author Michael Klink, tarent GmbH
 * @author Christoph Jerolimov, tarent GmbH
 */
public class Octopus {
	private OctopusRequestDispatcher dispatcher;
	private OctopusEnvironment octopusEnvironment;
	private CommonConfig commonConfig;
	private ModuleLookup moduleLookup;
	
	private static Log logger = LogFactory.getLog(Octopus.class);
	
	public static final String TASKNAME_CLEANUP = "cleanup";
	public static final String TASKNAME_AUTOSTART = "autostart";
	
	// optional JMX extension
	private OctopusExtension jmxManagementServer = null;
	
	/*
	 * Konstruktoren
	 */
	public Octopus() {
	}
	
	/*
	 * öffentliche Methoden
	 */

	/**
	 * Diese Methode initialisiert den Octopus. Dieser Schritt ist notwendig,
	 * damit der Octopus Anfragen bearbeiten kann und damit Autostart-Tasks
	 * abgearbeitet werden..
	 * 
	 * Diese Methode initialisiert den Octopus. Dieser Schritt ist notwendig,
	 * damit der Octopus Anfragen bearbeiten kann und damit Autostart-Tasks
	 * abgearbeitet werden..
	 * 
	 * @param environment
	 *            Eine Sammlung diverser Parameter.
	 * @param moduleLookup
	 *            Lookup context for modules.
	 */
	public void init(OctopusEnvironment octopusEnvironment, ModuleLookup moduleLookup) throws OctopusConfigException, ClassCastException {
		this.octopusEnvironment = octopusEnvironment;
		this.moduleLookup = moduleLookup;
		
		commonConfig = new CommonConfig(octopusEnvironment, this);
		commonConfig.setModuleLookup(moduleLookup);
		
		dispatcher = new OctopusRequestDispatcher(commonConfig);
		
		new OctopusInstanceJndiFactory().bind();
		new OctopusContextJndiFactory().bind();
		
		preloadModules(commonConfig);
		
		// Initalizing the optional JMX subsystem
		String jmxEnabledString = commonConfig.getConfigData(OctopusEnvironment.KEY_JMX_ENABLED);
		if (Boolean.valueOf(jmxEnabledString).booleanValue()) {
			logger.info("Enabling optional JMX subsystem.");
			
			Map params = new HashMap();
			params.put("octopus", this);
			params.put("config", commonConfig);
			
			jmxManagementServer = OctopusExtensionLoader.load("de.tarent.octopus.jmx.OctopusManagement", params);
		}
		else
        {
        	logger.info("Optional JMX subsystem is disabled.");
        }
		
        // Initalizing the optional rpc tunnel subsystem
        String rpcTunnelEnabledString = commonConfig.getConfigData(OctopusEnvironment.KEY_RPCTUNNEL_ENABLED);
        if (Boolean.valueOf(rpcTunnelEnabledString).booleanValue())
        {
            logger.info("Enabling optional RPC-tunnel.");
            
            OctopusRPCTunnel.createInstance(this, commonConfig);
        }
        else
        {
        	logger.info("Optional RPC-tunnel is disabled.");
        }
	}
	
	/**
	 * Diese Methode deinitialisert den Octopus. Dieser Schritt ist notwendig,
	 * damit Cleanup-Tasks abgearbeitet werden.
	 * 
	 * @throws ClassCastException
	 * @throws TaskProzessingException
	 * @throws ClassNotFoundException
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws ContentProzessException
	 * @throws OctopusConfigException
	 */
	public void deInit() throws ClassCastException, TaskProzessingException, ContentProzessException, OctopusConfigException {
		
		// shutting down the JMX subsystem
		if (jmxManagementServer != null)
			jmxManagementServer.stop();
		
		cleanupModules(dispatcher);
	}
	
	/**
	 * Diese Methode führt einen Request aus.
	 * 
	 * @param octopusRequest
	 * @param octopusResponse
	 * @param octopusSession
	 * @throws ResponseProcessingException
	 */
	public void dispatch(OctopusRequest octopusRequest, OctopusResponse octopusResponse, OctopusSession octopusSession) throws ResponseProcessingException {
		logger.trace(getClass().getName() + " dispatch " + new Object[] { octopusRequest, octopusResponse, octopusSession });
		dispatcher.dispatch(octopusRequest, octopusResponse, octopusSession);
		logger.trace(getClass().getName() + " dispatch");
	}
	
	public void sendError(OctopusRequest octopusRequest, OctopusResponse octopusResponse, OctopusSession octopusSession, Exception exception) {
		logger.trace(getClass().getName() + " dispatch " + new Object[] { octopusRequest, octopusResponse, octopusSession });
		dispatcher.sendError(octopusRequest, octopusResponse, exception);
		logger.trace(getClass().getName() + " dispatch");
	}
	
	public Preferences getModulePreferences(String moduleName) {
		Preferences modulePreferences = getOctopusPreferences().node("modules");
		if (moduleName == null)
			return modulePreferences;
		while (moduleName.startsWith("/"))
			moduleName = moduleName.substring(1);
		while (moduleName.endsWith("/"))
			moduleName = moduleName.substring(0, moduleName.length() - 1);
		return modulePreferences.node(moduleName);
	}
	
	/*
	 * geschätzte Methoden
	 */
	private void preloadModules(CommonConfig commonConfig) {
		String preloadString = commonConfig.getConfigData(OctopusEnvironment.KEY_PRELOAD_MODULES);
		if (preloadString == null || preloadString.length() == 0)
			return;
		
		List preloads = Arrays.asList(preloadString.split("[\\ ,\\,,\\;]"));
		
		for (Iterator it = preloads.iterator(); it.hasNext();) {
			String module = it.next().toString().trim();
			if (module.length() == 0)
				continue;
			
			logger.debug(Resources.getInstance().get("OCTOPUS_LOG_PRELOAD_MODULE", module));
			ModuleConfig moduleConfig = commonConfig.getModuleConfig(module);
			
			if (moduleConfig == null)
				logger.warn(Resources.getInstance().get("OCTOPUS_LOG_PRELOAD_MODULE_ERROR", module));
		}
	}
	
	private void callTask(String moduleName, CommonConfig config, String taskname) throws ContentProzessException, TaskProzessingException, OctopusConfigException {
		OctopusRequest octopusRequest = new OctopusRequest(OctopusRequest.createRequestID());
		octopusRequest.setRequestParameters(new HashMap());
		octopusRequest.setModule(moduleName);
		octopusRequest.setTask(taskname);
		
		OctopusContext octopusContext = new OctopusContextImpl(
				octopusRequest,
				new OctopusContent(),
				null,
				commonConfig,
				commonConfig.getModuleConfig(moduleName));
		
		try {
			Context.addActive(octopusContext);
			
			TaskManager taskmanager = new TaskManager(octopusContext, null);
			taskmanager.start(false);
			
			while (taskmanager.doNextStep()) { /* Do nothing here */
			}
			
		} finally {
			OctopusRequestDispatcher.processCleanupCode(octopusRequest.getRequestID(), octopusContext);
			Context.clear();
		}
	}
	
	/**
	 * @param modulename
	 *            Name des Moduls, in dem der Autostart durchgeführt werden soll
	 * @param commonConfig
	 *            die Config
	 */
	public void doAutostart(String modulename, CommonConfig commonConfig) throws TaskProzessingException, ContentProzessException, OctopusConfigException {
		ModuleConfig moduleconfig = commonConfig.getModuleConfig(modulename);
		TaskList tasklist = moduleconfig.getTaskList();
		Task task = tasklist.getTask(TASKNAME_AUTOSTART);
		if (task != null) {
			logger.debug(Resources.getInstance().get("OCTOPUS_LOG_AUTOSTART_MODULE", modulename));
			callTask(modulename, commonConfig, TASKNAME_AUTOSTART);
		}
	}
	
	
	protected void cleanupModules(OctopusRequestDispatcher dispatcher) throws ClassCastException, TaskProzessingException, ContentProzessException, OctopusConfigException {
		CommonConfig commonConfig = dispatcher.getCommonConfig();
		Iterator it = commonConfig.getExistingModuleNames();
		// Durchlaufe alle Module
		while (it.hasNext()) {
			String modulename = it.next().toString();
			doCleanup(modulename, commonConfig);
		}
	}
	
	/**
	 * @param modulename
	 * @param commonConfig
	 */
	public void doCleanup(String modulename, CommonConfig commonConfig) throws TaskProzessingException, ClassCastException, ContentProzessException, OctopusConfigException {
		ModuleConfig moduleconfig = commonConfig.getModuleConfig(modulename);
		TaskList tasklist = moduleconfig.getTaskList();
		Task task = tasklist.getTask(TASKNAME_CLEANUP);
		if (task != null) {
			logger.debug(Resources.getInstance().get("OCTOPUS_LOG_CLEANUP_MODULE", modulename));
			callTask(modulename, commonConfig, TASKNAME_CLEANUP);
		}
	}
	
	public ModuleLookup getModuleLookup() {
		return moduleLookup;
	}
	
	public CommonConfig getCommonConfig() {
		return commonConfig;
	}
	
	public OctopusEnvironment getOctopusEnvironment() {
		return octopusEnvironment;
	}
	
	private Preferences getOctopusPreferences() {
		return Preferences.systemRoot().node("/de/tarent/octopus");
	}
}
