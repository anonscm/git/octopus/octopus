/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */
package de.tarent.octopus.request.servlet;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.PasswordAuthentication;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadBase;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.servlet.ServletRequestContext;
import org.apache.commons.logging.Log;
import org.apache.xmlrpc.Base64;

import de.tarent.octopus.config.ModuleConfig;
import de.tarent.octopus.logging.LogFactory;
import de.tarent.octopus.request.OctopusRequest;
import de.tarent.octopus.request.OctopusRequestException;
import de.tarent.octopus.resource.Resources;
import de.tarent.octopus.response.soap.SoapEngine;
import de.tarent.octopus.response.soap.SoapException;
import de.tarent.octopus.response.xmlrpc.XmlRpcEngine;

/**
 * Diese Klasse enthält Hilfsmethoden für die Octopus-spezifische
 * Http-Request-Verarbeitung.
 * 
 * @author Michael Klink, tarent GmbH
 */
public class HttpHelper {
	private static Log logger = LogFactory.getLog(HttpHelper.class);
	
	//
	// * HTTP-OctopusContent-Types:
	//
	/** OctopusContent-Type text/html */
	public static final String CONTENT_TYPE_HTML = Resources.getInstance().get("CONTENT_TYPE_HTML");
	/** OctopusContent-Type text/xml */
	public static final String CONTENT_TYPE_XML = Resources.getInstance().get("CONTENT_TYPE_XML");
	/** OctopusContent-Type application/x-gzip */
	public static final String CONTENT_TYPE_GZIP_SOAP = Resources.getInstance().get("CONTENT_TYPE_GZIP_SOAP");
	/** OctopusContent-Type application/vnd.tarent.soap.pgp */
	public static final String CONTENT_TYPE_PGP_SOAP = Resources.getInstance().get("CONTENT_TYPE_PGP_SOAP");
	
	//
	// * HTTP-Header-Bezeichner
	//
	/** Header OctopusContent-Type */
	public static String HEADER_CONTENT_TYPE = "OctopusContent-Type";
	/** Header SOAPAction */
	public static String HEADER_SOAP_ACTION = "SOAPAction";
	
	/**
	 * Expliziter Header RequestType, der angibt, von welchem Typ eine Anfrage
	 * ist
	 */
	public static String HEADER_REQUEST_TYPE = "RequestType";
	
	/** XML-RPC Wert für den Header 'RequestType' */
	public static String HEADER_REQUEST_TYPE_XML_RPC = "xml-rpc";
	/** SOAP Wert für den Header 'RequestType' */
	public static String HEADER_REQUEST_TYPE_SOAP = "soap";
	/** WEB Wert für den Header 'RequestType' */
	public static String HEADER_REQUEST_TYPE_WEB = "web";
	
	/**
	 * Diese Methode entnimmt einer HTTP-Anfrage den Octopus-Anfragetyp. Das
	 * Verfahren dabei ist wie folgt:
	 * <ul>
	 * <li>GET-Anfragen sind <b>web</b>
	 * <li>Berücksichtigung des expliziten Octopus-Http-Headers 'RequestType'
	 * <li>SOAPAction-Header: <b>soap</b> mit Bestimmung der Untertypen über
	 * den mime-Type
	 * <li>Default ist <b>web</b>
	 * </ul>
	 * 
	 * @param request
	 *            die HTTP-Anfrage.
	 * @return der zugehörige Octopus-Anfragetyp.
	 */
	protected static int discoverRequestType(HttpServletRequest request) {
		String contentTypeHeader = request.getHeader(HEADER_CONTENT_TYPE);
		
		// SOAP-Anfragen werden nicht mit GET gestellt
		if ("GET".equals(request.getMethod()) || contentTypeHeader == null)
			return OctopusRequest.REQUEST_TYPE_WEB;
		
		String requestTypeHeader = request.getHeader(HEADER_REQUEST_TYPE);
		if (requestTypeHeader != null) {
			if (requestTypeHeader.startsWith(HEADER_REQUEST_TYPE_WEB))
				return OctopusRequest.REQUEST_TYPE_WEB;
			
			if (requestTypeHeader.startsWith(HEADER_REQUEST_TYPE_XML_RPC))
				return OctopusRequest.REQUEST_TYPE_XML_RPC;
		}
		
		String soapActionHeader = request.getHeader(HEADER_SOAP_ACTION);
		
		// Für SOAP muss der SOAPAction-Header existieren und einer von
		// bestimmten OctopusContent-Types vorliegen.
		if (soapActionHeader != null || (requestTypeHeader != null && requestTypeHeader.startsWith(HEADER_REQUEST_TYPE_SOAP))) {
			if (contentTypeHeader.startsWith(CONTENT_TYPE_GZIP_SOAP))
				return OctopusRequest.REQUEST_TYPE_GZIP_SOAP;
			
			if (contentTypeHeader.startsWith(CONTENT_TYPE_PGP_SOAP))
				return OctopusRequest.REQUEST_TYPE_PGP_SOAP;
			
			return OctopusRequest.REQUEST_TYPE_SOAP;
		}
		return OctopusRequest.REQUEST_TYPE_WEB;
	}
	
	/**
	 * This method adds the header-metadata, protocol, URL, and the requested
	 * response engine type to the OctopusRequest objects. This informations
	 * contains the HTTP-Request object.
	 * 
	 * @param requests
	 *            The OctopusRequest objects, which may be filled with
	 *            additional data.
	 * @param request
	 *            HttpServletRequest, that contains the metadata.
	 */
	public static void addHttpMetaData(OctopusRequest[] requests, HttpServletRequest request, String requestID) {
		// Headerfeld 'Accept-Language' als Locale eintragen
		Locale localeValue = getHttpLanguage(request.getHeader("Accept-Language"));
		// Basic-Authentisierung oder RemoteUser eintragen
		PasswordAuthentication pwdAuth = getPasswordAuthentication(requestID, request);
		// Cookie-Support ermitteln
		boolean supportCookies = request.getCookies() != null;
		// PathInfo eintragen, ggfs Modul und Task ableiten
		String module = null;
		String task = null;
		String pathInfo = request.getPathInfo();
		if (pathInfo != null && pathInfo.length() > 0) {
			if (pathInfo.startsWith("/"))
				pathInfo = pathInfo.substring(1);
			int slashIndex = pathInfo.indexOf('/');
			if (slashIndex < 0) {
				module = pathInfo;
				pathInfo = null;
			} else {
				module = pathInfo.substring(0, slashIndex);
				pathInfo = pathInfo.substring(slashIndex + 1);
				slashIndex = pathInfo.indexOf('/');
				if (slashIndex < 0) {
					task = pathInfo;
					pathInfo = null;
				} else {
					task = pathInfo.substring(0, slashIndex);
					pathInfo = pathInfo.substring(slashIndex + 1);
				}
			}
			if ("requestProxy".equalsIgnoreCase(module))
				module = null;
		}
		
		// in allen Requests passend setzen
		for (int i = 0; i < requests.length; i++) {
			requests[i].setParam(OctopusRequest.PARAM_LOCALE, localeValue);
			requests[i].setRemoteAddress(request.getRemoteAddr());
			if (requests[i].getPasswordAuthentication() == null)
				requests[i].setPasswordAuthentication(pwdAuth);
			requests[i].setSupportCookies(supportCookies);
			// adding Cookies to request
			if (supportCookies) {
				Cookie[] cookies = request.getCookies();
				Map cookiesMap = new HashMap(cookies.length);
				for (int j = 0; j < cookies.length; j++) {
					cookiesMap.put(cookies[j].getName(), cookies[j].getValue());
				}
				requests[i].setParam(OctopusRequest.PARAM_COOKIES, cookiesMap);
			}
			
			if (pathInfo != null)
				requests[i].setParam(OctopusRequest.PARAM_PATH_INFO, pathInfo);
			if (requests[i].getModule() == null)
				requests[i].setModule(module);
			if (requests[i].getTask() == null)
				requests[i].setTask(task);
		}
	}
	
	/**
	 * Diese Methode analysiert eine Web-Anfrage.
	 * 
	 * @param request
	 *            die HTTP-Anfrage
	 * @param requestType
	 *            der Anfragetyp
	 * @param requestID
	 *            die Anfrage-ID
	 * @return ein generiertes Anfrage-Objekt
	 * @throws OctopusRequestException
	 */
	public static OctopusRequest[] readWebRequest(HttpServletRequest request, int requestType, String requestID) throws OctopusRequestException {
		Map requestParams = new HashMap();
		
		for (Enumeration e = request.getParameterNames(); e.hasMoreElements();) {
			String key = (String) e.nextElement();
			Object val = null;
			if (request.getParameterValues(key).length == 1)
				val = request.getParameterValues(key)[0];
			else
				val = request.getParameterValues(key);
			
			requestParams.put(key, val);
			
			// Map übergeben
			int bracketStart = key.indexOf("[");
			if (bracketStart > -1) {
				String mapName = key.substring(0, bracketStart);
				String mapKey = key.substring(bracketStart + 1, key.length() - 1);
				// TODO: Was, wenn unter dem Namen schon was da ist...?
				if (!(requestParams.get(mapName) instanceof Map))
					requestParams.put(mapName, new HashMap());
				Map map = (Map) requestParams.get(mapName);
				map.put(mapKey, val);
			}
		}
		
		// Headerfelder als Map eintragen
		Map header = new HashMap();
		for (Enumeration headerNames = request.getHeaderNames(); headerNames.hasMoreElements();) {
			String key = headerNames.nextElement().toString();
			String value = request.getHeader(key);
			header.put(key, value);
		}
		requestParams.put(OctopusRequest.PARAM_HEADER, header);
		
		// Parse multipart objects
		ServletRequestContext fileuploadRequest = new ServletRequestContext(request);
		if (FileUploadBase.isMultipartContent(fileuploadRequest))
			try {
				// Create a factory for disk-based file items
				DiskFileItemFactory factory = new DiskFileItemFactory();
				// Sets the size threshold beyond which files are written
				// directly to disk.
				factory.setSizeThreshold(500 * 1024);
				// Sets the directory used to temporarily store files that are
				// larger
				// than the configured size threshold.
				factory.setRepository(new File(System.getProperty("java.io.tmpdir")));
				ServletFileUpload upload = new ServletFileUpload(factory);
				
				List items = upload.parseRequest(request);
				for (Iterator it = items.iterator(); it.hasNext();) {
					FileItem fileItem = (FileItem) it.next();
					if (fileItem.getContentType() == null) {
						requestParams.put(fileItem.getFieldName(), fileItem.getString());
					} else {
						Map file = new HashMap();
						file.put(OctopusRequest.PARAM_FILE_CONTENT_TYPE, fileItem.getContentType());
						file.put(OctopusRequest.PARAM_FILE_CONTENT_NAME, fileItem.getName());
						file.put(OctopusRequest.PARAM_FILE_CONTENT_SIZE, new Long(fileItem.getSize()));
						file.put(OctopusRequest.PARAM_FILE_CONTENT_STREAM, fileItem.getInputStream());
						requestParams.put(fileItem.getFieldName(), file);
					}
				}
			} catch (FileUploadException ex) {
				logger.warn(Resources.getInstance().get("HTTPHELPER_LOG_FILE_UPLOAD_ERROR", requestID), ex);
				throw new OctopusRequestException(ex);
			} catch (IOException ex) {
				logger.warn(Resources.getInstance().get("HTTPHELPER_LOG_FILE_UPLOAD_IO_ERROR", requestID), ex);
				throw new OctopusRequestException(ex);
			}
		
		// Request-Objekt erzeugen
		OctopusRequest octopusRequest = new OctopusRequest(requestID);
		octopusRequest.setRequestType(requestType);
		octopusRequest.setRequestParameters(requestParams);
		return new OctopusRequest[] { octopusRequest };
	}
	
	/**
	 * Diese Methode delegiert die Analyse einer SOAP-Anfrage an die SoapEngine.
	 * 
	 * @param request
	 *            die HTTP-Anfrage
	 * @param requestType
	 *            der Anfragetyp
	 * @param requestID
	 *            die Anfrage-ID
	 * @param moduleConfig
	 *            ModuleConfig
	 * @return ein generiertes Anfrage-Objekt
	 * @throws OctopusRequestException
	 */
	public static OctopusRequest[] readSoapRequests(HttpServletRequest request, int requestType, String requestID, ModuleConfig moduleConfig) throws OctopusRequestException {
		
		SoapEngine soapEngine = null;
		
		logger.trace(HttpHelper.class.getName() + " readSoapRequests " + new Object[] { request, new Integer(requestType), requestID, soapEngine });
		try {
			InputStream inputStream = request.getInputStream();
			
			if (requestType == OctopusRequest.REQUEST_TYPE_PGP_SOAP) {
				inputStream = SoapEngine.addPGPFilterToInputStream(inputStream);
			}
			
			// PGP_SOAP Nachrichten wurden vor dem PGP auch nochmal mit GZIP
			// komprimiert.
			if (requestType == OctopusRequest.REQUEST_TYPE_GZIP_SOAP || requestType == OctopusRequest.REQUEST_TYPE_PGP_SOAP) {
				inputStream = SoapEngine.addGZIPFilterToInputStream(inputStream);
			}
			
			return soapEngine.readSoapRequests(inputStream, requestType, requestID, moduleConfig.getName());
		} catch (IOException e) {
			throw new OctopusRequestException(e);
		} catch (SoapException e) {
			throw new OctopusRequestException(e);
		}
	}
	
	/**
	 * Diese Methode analysiert eine XML-RPC-Anfrage.
	 * 
	 * @param request
	 *            die HTTP-Anfrage
	 * @param requestType
	 *            der Anfragetyp
	 * @param requestID
	 *            die Anfrage-ID
	 * @return ein generiertes Anfrage-Objekt
	 * @throws OctopusRequestException
	 */
	public static OctopusRequest[] readXmlRpcRequests(HttpServletRequest request, int requestType, String requestID) throws OctopusRequestException {
		logger.trace(HttpHelper.class.getName() + " readXmlRpcRequests " + new Object[] { request, new Integer(requestType), requestID });
		
		try {
			InputStream inStream = logInput(request.getInputStream(), "INFO", "HTTPHELPER_LOG_XML_RPC_INPUT");
			return XmlRpcEngine.readXmlRpcRequests(inStream, requestType, requestID);
		} catch (IOException e) {
			throw new OctopusRequestException(e);
		} finally {
			logger.trace(HttpHelper.class.getName() + " readXmlRpcRequests");
		}
	}
	
	/**
	 * @deprecated Use same Method in OctopusRequest instead Diese Methode
	 *             bestimmt, ob der übergebene Anfragetyp ein Web-Typ (HTML)
	 *             ist.
	 * 
	 * @param requestType
	 *            ein Anfragetyp-Wert
	 * @return true, falls der Parameter ein Web-Anfragetyp ist.
	 */
	public static boolean isWebType(int requestType) {
		return OctopusRequest.isWebType(requestType);
	}
	
	/**
	 * @deprecated Use same Method in OctopusRequest instead Diese Methode
	 *             bestimmt, ob der übergebene Anfragetyp ein SOAP-Typ ist.
	 * 
	 * @param requestType
	 *            ein Anfragetyp-Wert
	 * @return true, falls der Parameter ein Web-Anfragetyp ist.
	 */
	public static boolean isSoapType(int requestType) {
		return OctopusRequest.isSoapType(requestType);
	}
	
	/**
	 * @deprecated Use same Method in OctopusRequest instead Diese Methode
	 *             bestimmt, ob der übergebene Anfragetyp ein XML-RPC-Typ ist.
	 * 
	 * @param requestType
	 *            ein Anfragetyp-Wert
	 * @return true, falls der Parameter ein Web-Anfragetyp ist.
	 */
	public static boolean isXmlRpcType(int requestType) {
		return OctopusRequest.isXmlRpcType(requestType);
	}
	
	/**
	 * Liefert zu einem HTTP-Accept-Language-String die entsprechende Locale mit
	 * Language und Country.
	 * 
	 * @see java.util.Locale
	 * 
	 * @param acceptLanguage
	 * @return userLocale oder <code>Locale.getDefault()</code>
	 */
	public final static Locale getHttpLanguage(String acceptLanguage) {
		// TODO: Default-Sprache nicht durch VM, sondern durch config
		// definieren?!
		if (acceptLanguage == null || acceptLanguage.length() == 0)
			return Locale.getDefault();
		String[] language = acceptLanguage.split("[,]");
		for (int i = 0; i < language.length; i++) {
			int d1 = language[i].indexOf('-');
			int d2 = language[i].indexOf('_');
			int d3 = language[i].indexOf(';');
			if (d2 != -1 && d1 < d2)
				d1 = d2;
			if (d3 == -1)
				d3 = language[i].length();
			if (d1 == -1) {
				return new Locale(language[i].substring(0, d3));
			} else {
				return new Locale(language[i].substring(0, d1), language[i].substring(d1 + 1, d3));
			}
		}
		return Locale.getDefault();
	}
	
	/**
	 * Diese Methode liefert eine Passwort-Authentifizierung aus einem
	 * HttpServletRequest.
	 * 
	 * @param request
	 *            ein HttpServletRequest
	 * @return eine Passwort-Authentifizierung oder <code>null</code>.
	 */
	public final static PasswordAuthentication getPasswordAuthentication(String requestID, HttpServletRequest request) {
		String authorization = request.getHeader("authorization");
		if (authorization != null && authorization.startsWith("Basic ")) {
			authorization = authorization.substring("Basic ".length());
			authorization = new String(Base64.decode(authorization.getBytes()));
			String[] authParts = authorization.split("[:]", 2);
			if (authParts.length == 2) {
				logger.debug("Authorisierung aus Header übernommen: " + Arrays.asList(authParts));
				return new PasswordAuthentication(authParts[0], authParts[1].toCharArray());
			}
		}
		if (request.getRemoteUser() != null) {
			logger.info(Resources.getInstance().get("HTTPHELPER_LOG_REMOTE_USER", requestID, request.getRemoteUser()));
			return new PasswordAuthentication(request.getRemoteUser(), new char[0]);
		}
		return null;
	}
	
	/**
	 * Diese Methode gibt je nach LogLevel-Angabe die Mitteilung in das Log aus
	 * und liefert einen InputStream zurück, der wieder auslesbar ist.
	 * 
	 * TODO: Schlechte Effizienz: Sobald das Log-Level != null ist muss die
	 * gesamte Nachricht in einen neuen InputStream Kopiert werden. Besser wäre
	 * es, den Stream nur dann zu kopieren, wenn er auch wirklich geloggt wurde.
	 * 
	 * @param message
	 *            die Eingabe
	 * @param logLevel
	 *            der Level, mit dem die Eingabe gelogt werden soll. Bei null
	 *            und "" wird nichts getan.
	 * @param logResource
	 *            Schlüssel zum Ressourcen-Eintrag, der als Mitteilung geloggt
	 *            wird. {0} im Eintrag wird durch die Mitteilung ersetzt.
	 * @return ein wieder verwendbarer Mitteilungs-Eingabe-Strom.
	 * @throws IOException
	 */
	public static InputStream logInput(InputStream message, String logLevel, String logResource) throws IOException {
		if (logLevel != null && logLevel.length() > 0 && HttpHelper.isLoggable(logLevel)) {
			StringBuffer sb = new StringBuffer();
			int c;
			while (0 <= (c = message.read()))
				sb.append((char) c);
			HttpHelper.log(logLevel, Resources.getInstance().get(logResource, sb));
			message = new ByteArrayInputStream(sb.toString().getBytes());
		}
		return message;
	}
	
	public static boolean isLoggable(String logLevel) {
		if (logLevel == "SEVERE")
			return logger.isErrorEnabled();
		else if (logLevel == "WARNING")
			return logger.isWarnEnabled();
		else if (logLevel == "INFO")
			return logger.isInfoEnabled();
		else if (logLevel == "CONFIG")
			return logger.isDebugEnabled();
		else if (logLevel == "FINE")
			return logger.isDebugEnabled();
		else if (logLevel == "FINER")
			return logger.isDebugEnabled();
		else if (logLevel == "FINEST")
			return logger.isTraceEnabled();
		return false;
	}
	
	public static void log(String level, String msg) {
		if (level == "SEVERE")
			logger.error(msg);
		else if (level == "WARNING")
			logger.warn(msg);
		else if (level == "INFO")
			logger.info(msg);
		else if (level == "CONFIG")
			logger.debug(msg);
		else if (level == "FINE")
			logger.debug(msg);
		else if (level == "FINER")
			logger.debug(msg);
		else if (level == "FINEST")
			logger.trace(msg);
	}
}
