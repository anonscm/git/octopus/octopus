/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.octopus.response;

import java.util.LinkedHashMap;
import java.util.Map;

import de.tarent.octopus.config.ModuleConfig;
import de.tarent.octopus.config.OctopusEnvironment;
import de.tarent.octopus.request.OctopusResponse;
import de.tarent.octopus.server.OctopusContext;

/**
 * View Komponente, die die Ausgabe steuert. <br>
 * <br>
 * Abhängig von einer ResponseDescription wird eine Engine gestartet, die für
 * den entsprechenden ResponseType zuständig ist. Dieser ResponseType kann über
 * die ResponseDescription abgefragt werden.
 * 
 * Der OctopusContent-Type der Ausgabe wird hier auf
 * <code>octopusContent.get( "responseParams.ContentType" )</code> oder auf
 * <code>config.getDefaultContentType()</code> gesetzt, kann aber von der
 * benutzten Engine später überschrieben werden.
 * 
 * @author <a href="mailto:mancke@mancke-software.de">Sebastian Mancke</a>,
 *         <b>tarent GmbH</b>
 * @author <a href="mailto:H.Helwich@tarent.de">Hendrik Helwich</a>, <b>tarent
 *         GmbH</b>
 */
public class ResponseCreator {
	private Map responseEngines;
	
	/**
	 * Initialisierung mit Logger
	 */
	public ResponseCreator(ModuleConfig moduleConfig) {
		responseEngines = new LinkedHashMap();
		//moduleConfig.get
	}
	
	public void sendEmptyResponse(ResponseDescription desc, OctopusContext octopusContext, OctopusResponse octopusResponse) throws ResponseProcessingException {
		ResponseEngine responseEngine = getResponseEngine(desc, octopusContext, octopusResponse);
		responseEngine.performEncoding(desc, octopusContext, octopusResponse);
		responseEngine.performContentType(desc, octopusContext, octopusResponse);
		responseEngine.sendEmptyResponse(desc, octopusContext, octopusResponse);
	}
	
	public void sendNormalResponse(ResponseDescription desc, OctopusContext octopusContext, OctopusResponse octopusResponse) throws ResponseProcessingException {
		ResponseEngine responseEngine = getResponseEngine(desc, octopusContext, octopusResponse);
		responseEngine.performEncoding(desc, octopusContext, octopusResponse);
		responseEngine.performContentType(desc, octopusContext, octopusResponse);
		responseEngine.sendNormalResponse(desc, octopusContext, octopusResponse);
	}
	
	public void sendErrorResponse(ResponseDescription desc, OctopusContext octopusContext, OctopusResponse octopusResponse, String message, Exception exception) throws ResponseProcessingException {
		ResponseEngine responseEngine = getResponseEngine(desc, octopusContext, octopusResponse);
		responseEngine.performEncoding(desc, octopusContext, octopusResponse);
		responseEngine.performContentType(desc, octopusContext, octopusResponse);
		if (message == null && exception != null)
			message = exception.getLocalizedMessage();
		else if (message == null || message.length() == 0)
			message = "ERROR!";
		responseEngine.sendErrorResponse(desc, octopusContext, octopusResponse, message, exception);
	}
	
	public void sendEmptyCallback(ResponseDescription desc, OctopusContext octopusContext, OctopusResponse octopusResponse) throws ResponseProcessingException {
		ResponseEngine responseEngine = getResponseEngine(desc, octopusContext, octopusResponse);
		responseEngine.performEncoding(desc, octopusContext, octopusResponse);
		responseEngine.performContentType(desc, octopusContext, octopusResponse);
		responseEngine.sendEmptyCallback(desc, octopusContext, octopusResponse);
	}
	
	public void sendNormalCallback(ResponseDescription desc, OctopusContext octopusContext, OctopusResponse octopusResponse) throws ResponseProcessingException {
		ResponseEngine responseEngine = getResponseEngine(desc, octopusContext, octopusResponse);
		responseEngine.performEncoding(desc, octopusContext, octopusResponse);
		responseEngine.performContentType(desc, octopusContext, octopusResponse);
		responseEngine.sendNormalCallback(desc, octopusContext, octopusResponse);
	}
	
	public void sendErrorCallback(ResponseDescription desc, OctopusContext octopusContext, OctopusResponse octopusResponse, String message, Exception exception) throws ResponseProcessingException {
		ResponseEngine responseEngine = getResponseEngine(desc, octopusContext, octopusResponse);
		responseEngine.performEncoding(desc, octopusContext, octopusResponse);
		responseEngine.performContentType(desc, octopusContext, octopusResponse);
		if (message == null && exception != null)
			message = exception.getLocalizedMessage();
		else if (message == null || message.length() == 0)
			message = "ERROR!";
		responseEngine.sendErrorCallback(desc, octopusContext, octopusResponse, message, exception);
	}
	
	/**
	 * Initialize the response engine {@link ResponseEngine} <br>
	 * <br>
	 * Dependent on the value of {@link ResponseDescription.getRequestType} will
	 * be the required engine loaded and the response will be created.
	 * 
	 * When the given flag <code>empty</code> has the value <code>true</code>,
	 * the {@link ResponseEngine} will be forced to create an empty response.
	 * Otherwise the response is dependent on the request.
	 * 
	 * @param desc
	 *            The response description {@link ResponseDescription}
	 * @param octopusContext
	 *            The octopusContext {@link OctopusContext}
	 * @throws ResponseProcessingException
	 */
	private ResponseEngine getResponseEngine(ResponseDescription desc, OctopusContext octopusContext, OctopusResponse octopusResponse) throws ResponseProcessingException {
		
		String engineCacheKey = octopusContext.getModuleName() + "#" + octopusContext.getRequestedResponseType();
		ResponseEngine engine = (ResponseEngine) responseEngines.get(engineCacheKey);
		
		// in case the engine is not currently registered, try to initialize it.
		if (engine == null) {
			
			String requestedResponseType = octopusContext.getRequestedResponseType();
			
			if (!desc.isTypeAllowed(requestedResponseType)) {
				throw new ResponseProcessingException("Die Response-Engine \"" + octopusContext.getRequestedResponseType() + "\" ist in diesem Task nicht erlaubt. Erlaubt sind: " + desc.getTypes());
			}
			
			try {
				String requestedResponseEngineClassName = getResponseTypeClassName(requestedResponseType, octopusContext.moduleConfig());
				if (requestedResponseEngineClassName == null || requestedResponseEngineClassName.length() == 0)
					throw new ResponseProcessingException("Keine Response-Engine für den Requesttype \"" + desc.getTypes() + "\" konfiguriert.");
				engine = (ResponseEngine) Class.forName(requestedResponseEngineClassName).newInstance();
				engine.init(octopusContext.moduleConfig());
			} catch (InstantiationException e) {
				throw new ResponseProcessingException("Die Response-Engine \"" + desc.getTypes() + "\" ist nicht bekannt.");
			} catch (IllegalAccessException e) {
				throw new ResponseProcessingException("Die Response-Engine \"" + desc.getTypes() + "\" ist nicht bekannt.");
			} catch (ClassNotFoundException e) {
				throw new ResponseProcessingException("Die Response-Engine \"" + desc.getTypes() + "\" ist nicht bekannt.");
			}
			responseEngines.put(engineCacheKey, engine);
		}
		
		String contentType = octopusContext.contentAsString("responseParams.ContentType");
		if (contentType == null)
			contentType = octopusContext.moduleConfig().getDefaultContentType();
		octopusResponse.setContentType(contentType);
		
		String cacheTime = desc.getAsString("CachingTime"); // millis
		String cacheParam = desc.getAsString("CachingParam"); // free
																						// text
		if (cacheTime == null && cacheParam == null) {
			octopusResponse.setCachingTime(0);
		} else {
			if (cacheTime == null) {
				octopusResponse.setCachingTime(0, cacheParam);
			} else {
				octopusResponse.setCachingTime(Integer.parseInt(cacheTime), cacheParam);
			}
		}
		
		// FIXME CHECK if this check needed?
		// if (engine instanceof RpcResponseEngine || octopusResponse instanceof
		// DirectCallResponse) {
		// pushRPCOutputParams((DirectCallResponse) octopusResponse,
		// octopusContent, desc);
		// }
		return engine;
	}
	
	/**
	 * Returns the fully qualified name of the ResponseEngine class. First will
	 * be looked for the customer implementation for the given response type
	 * responseType. When no customer implementation are available the default
	 * implementation will be used. In case, that no implementations are
	 * available, return null.
	 * 
	 * @param responseType
	 *            The response type e.g soap, velocity ...
	 * @param moduleConfig
	 *            The module configuration
	 * @param environment
	 *            The Octopus configuration
	 * @return the fully qualified name of the ResponseEngine class.
	 */
	private String getResponseTypeClassName(String responseType, ModuleConfig moduleConfig) {
		Map customerResponseTypes = (Map) moduleConfig.getParamAsObject(OctopusEnvironment.KEY_RESPONSE_TYPES);
		if (customerResponseTypes != null && customerResponseTypes.containsKey(responseType))
			return (String) customerResponseTypes.get(responseType);
		return null;
	}
}
