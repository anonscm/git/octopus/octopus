/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */


package de.tarent.octopus.content;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import de.tarent.octopus.server.OctopusContextImpl;

/**
 * Session-Manager
 * 
 * Sollte beim Laden eines Worker initalisiert werden und in der pre (und post)
 * Methode-Verwendet werden.
 * 
 * Durchsucht OctopusContent und Request nach neuen den Parametern und speichert
 * diese in der Session ab. Sollte der Parameter nicht übergeben werden, wird
 * der Wert aus der Session geladen bzw. der Default-Wert genommen.
 * 
 * @see #add(String, Object)
 * @see #sync(OctopusContextImpl)
 * 
 * @author Heiko Ferger
 */
public class SessionManager {
	private final Map _enabledVars = new HashMap();
	
	/**
	 * @deprecated Die Liste der erlauben variablen sollte nicht gelöscht werden
	 * 
	 */
	public void clear() {
		_enabledVars.clear();
	}
	
	/**
	 * Funktion zum registrieren der erlaubten Session-Variablen es ist nicht
	 * möglich den Wert (Defaultwert) der erlaubten Variable ein zweites mal zu
	 * ändern.
	 * 
	 * Erlaubte Variable ist dann also nach definition "final"
	 * 
	 * @param name
	 *            Key für den Request, OctopusContent und die Session.
	 * @param def
	 *            Default-Wert.
	 */
	public void add(String name, Object def) {
		if (!_enabledVars.containsKey(name))
			_enabledVars.put(name, def);
	}
	
	public void sync(OctopusContextImpl octopusContext) {
		Iterator it = _enabledVars.keySet().iterator();
		
		while (it.hasNext()) {
			String name = (String) it.next();
			
			if (octopusContext.contentContains(name)) {
				octopusContext.setSession(name, octopusContext.contentAsObject(name));
			} else if (octopusContext.requestContains(name)) {
				octopusContext.setContent(name, octopusContext.requestAsObject(name));
				octopusContext.setSession(name, octopusContext.requestAsObject(name));
			} else if (octopusContext.sessionAsObject(name) != null) {
				octopusContext.setContent(name, octopusContext.sessionAsObject(name));
			} else {
				octopusContext.setContent(name, _enabledVars.get(name));
			}
		}
	}
	
	/**
	 * Speichert alle Registrierten Variablen in der HashMap 'callByName' Dient
	 * als erweiterung es VTL auch auf variablen per "Name" zugreifen (dies in
	 * der Session registriert sind) meistens werden diese Variablen in Velocity
	 * generich erzeugt z.b. Tabelle_[Navigation]
	 */
	public void setCallByName(OctopusContextImpl octopusContext) {
		HashMap callByName = new HashMap();
		Iterator it = _enabledVars.keySet().iterator();
		
		while (it.hasNext()) {
			String name = (String) it.next();
			if (octopusContext.sessionAsObject(name) != null) {
				callByName.put(name, octopusContext.sessionAsObject(name));
			}
		}
		octopusContext.setContent("callByName", callByName);
	}
	
	public String toString() {
		return super.toString() + " " + _enabledVars;
	}
}
