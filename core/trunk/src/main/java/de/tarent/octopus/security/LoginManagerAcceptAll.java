/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.octopus.security;

import java.net.PasswordAuthentication;

import de.tarent.octopus.config.CommonConfig;
import de.tarent.octopus.request.OctopusRequest;
import de.tarent.octopus.server.PersonalConfig;

/**
 * Implementierung eines LoginManagers, die alle User immer akzeptiert,
 * unabhängig davon, ob sie existieren oder ihr Passwort gültig ist. Die User
 * landen alle in der DEFAULT_GROUP. Diese könnte später konfigurierbar sein.
 * <br>
 * <br>
 * a
 * 
 * @author <a href="mailto:mancke@mancke-software.de">Sebastian Mancke</a>,
 *         <b>tarent GmbH</b>
 */
public class LoginManagerAcceptAll extends AbstractLoginManager {
	
	public static final String DEFAULT_GROUP = PersonalConfig.GROUP_USER;
	
	protected void doLogin(CommonConfig commonConfig, PersonalConfig pConfig, OctopusRequest octopusRequest) throws OctopusSecurityException {
		PasswordAuthentication pwdAuth = octopusRequest.getPasswordAuthentication();
		pConfig.setUserGroups(new String[] { DEFAULT_GROUP });
		pConfig.userLoggedIn(pwdAuth != null ? pwdAuth.getUserName() : "?");
	}
	
	protected void doLogout(CommonConfig commonConfig, PersonalConfig pConfig, OctopusRequest octopusRequest) throws OctopusSecurityException {
		pConfig.setUserGroups(new String[] { PersonalConfig.GROUP_LOGGED_OUT });
		pConfig.userLoggedOut();
	}
}
