/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.octopus.content;

import org.apache.commons.logging.Log;

import de.tarent.octopus.config.ContentWorkerDeclaration;
import de.tarent.octopus.logging.LogFactory;
import de.tarent.octopus.resource.Resources;
import de.tarent.octopus.server.SpecialWorkerFactory;
import de.tarent.octopus.server.WorkerCreationException;

/**
 * Instantiiert eine Klasse, die das ContentWorker-Interface direkt unterstützt.
 * 
 * 
 * @see ContentWorker
 * @author <a href="mailto:mancke@mancke-software.de">Sebastian Mancke</a>,
 *         <b>tarent GmbH</b>
 */
public class DirectWorkerFactory implements SpecialWorkerFactory {
	
	private static Log logger = LogFactory.getLog(DirectWorkerFactory.class);
	private static Class[] emptyClassArray = new Class[] {};
	private static Object[] emptyObjectArray = new Object[] {};
	
	/**
	 * Läd die als ImplementationSource angegebene Klasse und gibt sie gecastet
	 * ContentWorker zurück.
	 * 
	 * @param workerDeclaration
	 *            Beschreibung zur Instanziierung des Workers.
	 */
	public ContentWorker createInstance(ClassLoader classLoader, ContentWorkerDeclaration workerDeclaration) throws WorkerCreationException {
		try {
			logger.debug(Resources.getInstance().get("WORKERFACTORY_LOADING_WORKER", getClass().getName(), workerDeclaration.getWorkerName(), workerDeclaration.getImplementationSource()));
			Class workerClass = classLoader.loadClass(workerDeclaration.getImplementationSource());
			return (ContentWorker) workerClass.getConstructor(emptyClassArray).newInstance(emptyObjectArray);
		} catch (ClassCastException castException) {
			throw new WorkerCreationException(Resources.getInstance().get("WORKERFACTORY_CAST_EXC_LOADING_WORKER", getClass().getName(), workerDeclaration.getWorkerName(), workerDeclaration.getImplementationSource()));
		} catch (Exception reflectionException) {
			throw new WorkerCreationException(Resources.getInstance().get("WORKERFACTORY_EXC_LOADING_WORKER", getClass().getName(), workerDeclaration.getWorkerName(), workerDeclaration.getImplementationSource()), reflectionException);
		}
	}
}
