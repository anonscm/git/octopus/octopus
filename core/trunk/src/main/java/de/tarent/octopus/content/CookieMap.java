/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */
package de.tarent.octopus.content;

/**
 * @author Jens Neumaier, tarent GmbH
 * 
 */
public interface CookieMap {
	
	/** Prefix for the Map in which cookies will be stored * */
	public static final String PREFIX_COOKIE_MAP = "cookies";
	
	/**
	 * New cookies will be stored in the octopus-octopusContent. Each cookie is
	 * saved in another Map either as a Cookie-Object in the field "cookie" that
	 * has to be assigned manually or in the field "value" which will
	 * automatically be used if a user preference is saved in the
	 * PersonalConfig.
	 */
	public static final String COOKIE_MAP_FIELD_VALUE = "value";
	public static final String COOKIE_MAP_FIELD_COOKIE = "cookie";
	
	/**
	 * Configuration settings for default cookie creation
	 * 
	 * e.g. <param name="cookies.defaultMaxAge" value="5000000"/>
	 */
	public static final String PREFIX_CONFIG_MAP = "cookies";
	public static final String CONFIG_MAXAGE = "defaultMaxAge";
}
