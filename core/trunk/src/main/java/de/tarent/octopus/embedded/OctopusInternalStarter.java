/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */
package de.tarent.octopus.embedded;

import java.util.Map;

import org.apache.commons.logging.Log;

import de.tarent.octopus.logging.LogFactory;
import de.tarent.octopus.request.Octopus;
import de.tarent.octopus.request.OctopusRequest;
import de.tarent.octopus.request.OctopusSession;
import de.tarent.octopus.resource.Resources;

/**
 * Kapselt das Ansprechen des Octopus
 * 
 * @author <a href="mailto:mancke@mancke-software.de">Sebastian Mancke</a>,
 *         <b>tarent GmbH</b>
 */
public class OctopusInternalStarter implements OctopusStarter {
	

	private static Log logger = LogFactory.getLog(OctopusStarter.class);
	
	OctopusSession octopusSession;
	
	Octopus octopus;
	
	/**
	 * Creates an OctopusStarter with a new dummy Session
	 */
	public OctopusInternalStarter(Octopus octopus) {
		this.octopus = octopus;
		octopusSession = new DirectCallSession();
	}
	
	/**
	 * Creates an OctopusStarter with the supplied session
	 */
	public OctopusInternalStarter(Octopus octopus, OctopusSession session) {
		this.octopus = octopus;
		this.octopusSession = session;
	}
	
	/**
	 * Startet die Abarbeitung einer Anfrage
	 */
	public OctopusDirectCallResult request(Map requestParams) throws DirectCallException {
		
		logger.debug(Resources.getInstance().get("REQUESTPROXY_LOG_REQUEST_PROCESSING_START"));
		
		try {
			DirectCallResponse response = new DirectCallResponse();
			logger.trace(Resources.getInstance().get("REQUESTPROXY_LOG_RESPONSE_OBJECT_CREATED"));
			// response.setSoapEngine(soapEngine);
			
			logger.trace(Resources.getInstance().get("REQUESTPROXY_LOG_SESSION_OBJECT_CREATED"));
			
			OctopusRequest request = new OctopusRequest();
			request.setRequestParameters(requestParams);
			request.setParam(OctopusRequest.PARAM_SESSION_ID, octopusSession.getId());
			logger.trace(Resources.getInstance().get("REQUESTPROXY_LOG_REQUEST_OBJECT_CREATED"));
			
			octopus.dispatch(request, response, octopusSession);
			
			response.flush();
			return new OctopusDirectCallResult(response);
		} catch (Exception e) {
			logger.error(Resources.getInstance().get("REQUESTPROXY_LOG_PROCESSING_EXCEPTION"), e);
			throw new DirectCallException(Resources.getInstance().get("REQUESTPROXY_LOG_PROCESSING_EXCEPTION"), e);
		}
		
	}
	
}
