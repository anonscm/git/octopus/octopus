/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.octopus.response.raw;

import java.util.Iterator;
import java.util.Map;

import de.tarent.octopus.config.ModuleConfig;
import de.tarent.octopus.request.OctopusResponse;
import de.tarent.octopus.response.AbstractResponseEngine;
import de.tarent.octopus.response.ResponseDescription;
import de.tarent.octopus.response.ResponseEngine;
import de.tarent.octopus.response.ResponseProcessingException;
import de.tarent.octopus.response.binary.BinaryResponseEngine;
import de.tarent.octopus.server.OctopusContext;

/**
 * Diese Klasse arbeitet als direkte Ausgabe Engine.
 * 
 * @author <a href="mailto:mancke@mancke-software.de">Sebastian Mancke</a>,
 *         <b>tarent GmbH</b>
 */
public class RawResponseEngine extends AbstractResponseEngine implements ResponseEngine {
	public String getDefaultName() {
		return "raw";
	}

	public String getDefaultContentType() {
		return BinaryResponseEngine.HTTP_DETAULT_MIMETYPE;
	}
	
	public void init(ModuleConfig moduleConfig) {
	}
	
	public void sendEmptyResponse(ResponseDescription desc, OctopusContext octopusContext, OctopusResponse octopusResponse) throws ResponseProcessingException {
		octopusResponse.getWriter().write("ok");
	}
	
	public void sendNormalResponse(ResponseDescription desc, OctopusContext octopusContext, OctopusResponse octopusResponse) throws ResponseProcessingException {
		Map outputFields = getOutputFields(desc, octopusContext, octopusResponse);
		if (outputFields == null || outputFields.isEmpty()) {
			octopusResponse.getWriter().println();
		} else if (outputFields.size() == 1) {
			octopusResponse.getWriter().println(outputFields.values().iterator().next());
		} else {
			for (Iterator it = outputFields.values().iterator(); it.hasNext(); ) {
				octopusResponse.getWriter().println(it.next());
			}
		}
		
		octopusResponse.getWriter().flush();
		octopusResponse.getWriter().close();
	}
	
	public void sendErrorResponse(ResponseDescription desc, OctopusContext octopusContext, OctopusResponse octopusResponse, String message, Exception exception) throws ResponseProcessingException {
		octopusResponse.getWriter().write(message);
	}
	
	public void sendEmptyCallback(ResponseDescription desc, OctopusContext octopusContext, OctopusResponse octopusResponse) throws ResponseProcessingException {
		sendEmptyResponse(desc, octopusContext, octopusResponse);
	}
	
	public void sendNormalCallback(ResponseDescription desc, OctopusContext octopusContext, OctopusResponse octopusResponse) throws ResponseProcessingException {
		sendNormalResponse(desc, octopusContext, octopusResponse);
	}
	
	public void sendErrorCallback(ResponseDescription desc, OctopusContext octopusContext, OctopusResponse octopusResponse, String message, Exception exception) throws ResponseProcessingException {
		sendErrorResponse(desc, octopusContext, octopusResponse, message, exception);
	}
}
