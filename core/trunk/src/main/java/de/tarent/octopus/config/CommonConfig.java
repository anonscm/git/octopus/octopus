/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.octopus.config;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.prefs.Preferences;

import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.logging.Log;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import de.tarent.octopus.data.DBConnection;
import de.tarent.octopus.data.DataAccessException;
import de.tarent.octopus.data.GenericDataAccessWrapper;
import de.tarent.octopus.logging.LogFactory;
import de.tarent.octopus.request.Octopus;
import de.tarent.octopus.resource.Resources;
import de.tarent.octopus.security.OctopusSecurityException;
import de.tarent.octopus.server.LoginManager;
import de.tarent.octopus.server.PersonalConfig;
import de.tarent.octopus.util.DataFormatException;
import de.tarent.octopus.util.Xml;

/**
 * Kapselt Einstellungen und Informationen, die für alle Benutzer gleich sind.
 * <br>
 * <br>
 * Diese Informationen stammen aus der Configdatei (./WEB-INF/config/config.xml)
 * und dem OctopusEnvironment Container, der mit Servletinformationen und den
 * Parametern aus dem Deployment-Descriptor gefüllt ist.
 * 
 * @author <a href="mailto:mancke@mancke-software.de">Sebastian Mancke</a>,
 *         <b>tarent GmbH</b>
 */
public class CommonConfig {
	/** Der Logger */
	private static Log logger = LogFactory.getLog(CommonConfig.class);
	
	/**
	 * Die Daten aus der Haupt Konfigurations Datei und Environment Werte aus
	 * der ServletEnv
	 */
	private OctopusEnvironment octopusEnvironment;
	
	/**
	 * Dieses Objekt erlaubt ein dynamisches Nachladen neuer Module.
	 */
	private ModuleLookup moduleLookup;
	
	/**
	 * Login Manager
	 */
	protected LoginManager loginManager = null;
	
	/**
	 * 
	 */
	protected Octopus octopus;
	
	/**
	 * Die Konfigurationen der einzelnen Module. Mit ihren Namen als String Keys
	 * und ModuleConfig Objekten als Values.
	 */
	private Map moduleConfigs;
	
	/**
	 * Die ModuleConfig, die benutzt wird, wenn nichts anderes angegeben ist.
	 */
	protected ModuleConfig defaultModuleConfig;
	
	/**
	 * Puffer Mit Zugriffsklassen für die Verschiedenen Datenquellen. Die Keys
	 * sind String in der Form: 'ModulName.dataSourceName', wobei der ModulName
	 * der Name des Modules ist, indem die Datenquelle Deklariert wurde Die
	 * Values sind Objekte vom Typ GenericDataAccessWrapper oder
	 * Spezialisierungen davon.
	 */
	protected Map bufferedDataAccessWrappers;
	
	/**
	 * Initialisierung: Parst die Haupt Konfigurations Datei.
	 * 
	 * @param octopusEnvironment
	 *            Ein Datenkontainer, mit mindestens den Einträgen:
	 *            config.configRoot, config.configData
	 * @throws FileNotFoundException
	 */
	public CommonConfig(OctopusEnvironment environment, Octopus octopus) throws OctopusConfigException {
		this.octopusEnvironment = environment;
		this.octopus = octopus;
		if (environment != null)
			environment.parseConfigFile();
		bufferedDataAccessWrappers = new HashMap();
		moduleConfigs = new HashMap();
		logger.debug(Resources.getInstance().get("COMMONCONFIG_LOG_INITIALIZED"));
	}
	
	/**
	 * Liefert die Wrapperklasse für Datenzugriffe Diese Wrapperklassen werden
	 * alle hier verwaltet und vorrätig gehalten. Wenn jemand nach einer
	 * Datenzugriffsklasse fragt, wird erst im aktuellen Modul unter dem Namen
	 * nach einer Datenquelle gesucht. Wenn dort nichts vorhanden ist, wird das
	 * Defaultmodule benutzt.
	 * 
	 * @param dataAccessName
	 *            Name einer Datenquelle im angegebenen Modul oder dem
	 *            Defaultmodule
	 * @param moduleName
	 *            Name des Modules, indem gesucht werden soll.
	 * @return In dem Attribut, des dataAccess Elementes in der Config Datei,
	 *         angegebener Subtype von GenericDataAccessWrapper oder
	 *         GenericDataAccessWrapper als Default
	 */
	public GenericDataAccessWrapper getDataAccess(String moduleName, String dataAccessName) throws DataAccessException {
		
		String dataAccessKey = Resources.getInstance().get("COMMONCONFIG_KEY_ACCESS_WRAPPER", moduleName, dataAccessName);
		
		GenericDataAccessWrapper accessWrapper;
		
		accessWrapper = (GenericDataAccessWrapper) bufferedDataAccessWrappers.get(dataAccessKey);
		if (accessWrapper != null && accessWrapper.isOld()) {
			bufferedDataAccessWrappers.remove(dataAccessKey);
			try {
				accessWrapper.disconnect();
				logger.debug(Resources.getInstance().get("COMMONCONFIG_LOG_CLOSED_ACCESS_WRAPPER", dataAccessKey));
			} catch (SQLException e) {
				logger.warn(Resources.getInstance().get("COMMONCONFIG_LOG_ERROR_CLOSING_WRAPPER", dataAccessKey), e);
			}
			accessWrapper = null;
		}
		
		if (accessWrapper == null) {
			// Neu erstellen
			logger.debug(Resources.getInstance().get("COMMONCONFIG_LOG_CREATING_ACCESS_WRAPPER", dataAccessKey));
			Map source = null;
			
			ModuleConfig module = getModuleConfig(moduleName);
			if (module != null) {
				Map dataAccess = module.getDataAccess();
				source = (Map) dataAccess.get(dataAccessName);
			}
			if (source == null && defaultModuleConfig != null) {
				Map dataAccess = defaultModuleConfig.getDataAccess();
				source = (Map) dataAccess.get(dataAccessName);
			}
			
			logger.debug(Resources.getInstance().get("COMMONCONFIG_LOG_ACCESS_WRAPPER_DATA", source));
			if (module == null || source == null) {
				String msg = Resources.getInstance().get("COMMONCONFIG_LOG_ACCESS_WRAPPER_NO_DATA", dataAccessKey);
				logger.error(msg);
				throw new DataAccessException(msg);
			}
			
			DBConnection dbConnection = new DBConnection(source);
			try {
				Class clazz = module.getClassLoader().loadClass(dbConnection.getAccessWrapperClassName());
				accessWrapper = (GenericDataAccessWrapper) clazz.newInstance();
				accessWrapper.setDBConnection(dbConnection);
				logger.debug(Resources.getInstance().get("COMMONCONFIG_LOG_CREATED_ACCESS_WRAPPER", dbConnection.getAccessWrapperClassName()));
			} catch (Exception e) {
				String msg = Resources.getInstance().get("COMMONCONFIG_LOG_ACCESS_WRAPPER_ERROR", dataAccessKey);
				logger.error(msg, e);
				throw new DataAccessException(msg, e);
			}
			bufferedDataAccessWrappers.put(dataAccessKey, accessWrapper);
		}
		
		return accessWrapper;
	}
	
	/**
	 * Diese Methode verwirft die Datenzugriffsinstanzen eines Moduls.
	 * 
	 * @param moduleName
	 *            Name des betroffenen Moduls
	 */
	public void dropModuleDataAccess(String moduleName) {
		if (moduleName == null)
			return;
		MessageFormat format = new MessageFormat(Resources.getInstance().get("COMMONCONFIG_KEY_ACCESS_WRAPPER"));
		StringBuffer buffer = new StringBuffer();
		Iterator itAccessWrappers = bufferedDataAccessWrappers.entrySet().iterator();
		int i;
		String key = null;
		for (i = 0; itAccessWrappers.hasNext(); i++)
			try {
				Map.Entry entry = (Map.Entry) itAccessWrappers.next();
				key = entry.getKey().toString();
				Object[] refs = format.parse(key);
				if (moduleName.equals(refs[0])) {
					GenericDataAccessWrapper wrapper = (GenericDataAccessWrapper) entry.getValue();
					try {
						wrapper.disconnect();
					} catch (SQLException e) {
						logger.warn(Resources.getInstance().get("COMMONCONFIG_LOG_ERROR_CLOSING_WRAPPER", key), e);
					}
					itAccessWrappers.remove();
					if (buffer.length() > 0)
						buffer.append(", ");
					buffer.append(key);
				}
			} catch (ParseException pe) {
				logger.warn(Resources.getInstance().get("COMMONCONFIG_LOG_INVALID_ACCESS_KEY", key), pe);
			}
		logger.debug(Resources.getInstance().get("COMMONCONFIG_LOG_REMOVED_ACCESS_WRAPPERS", moduleName, new Integer(i), buffer));
	}
	
	/**
	 * Diese Methode liefert eine strukturierte Liste der Tasks des angegebenen
	 * Moduls.
	 * 
	 * @param moduleName
	 *            Name des betreffenden Moduls.
	 * @return eine strukturierte Taskliste zum betreffenden Modul oder null.
	 */
	public TaskList getTaskList(String moduleName) {
		ModuleConfig module = getModuleConfig(moduleName);
		return module != null ? module.getTaskList() : null;
	}
	
	/**
	 * Verzeichiss, indem sich die Statischen Web Ressourcen für das angegebene
	 * Modul befinden. Relativ zum Rootverzeichnis des Servers
	 * 
	 * @return Verzeichnissname mit abschließendem /
	 */
	public String getRelativeWebRootPath(String moduleName) {
		ModuleConfig moduleConfig = getModuleConfig(moduleName);
		return (moduleConfig != null) ? moduleConfig.getWebPathStatic() : '/' + moduleName + '/';
	}
	
	
	/**
	 * Returns the configuration of the module. If the flag
	 * {@link ModuleConfig#PREFS_IS_IN_TEST_ENVIRONMENT} is 'true', the
	 * configuration will be reloadet.
	 * 
	 * @param moduleName
	 *            The name of the module
	 * @return The configuration of the module
	 */
	public ModuleConfig getModuleConfig(String moduleName) {
		if (moduleConfigs.containsKey(moduleName))
			if (!((ModuleConfig) moduleConfigs.get(moduleName)).isInTestEnvironment()) {
				return (ModuleConfig) moduleConfigs.get(moduleName);
			} else {// reload the configuration
				try {
					ModuleConfig mc = (ModuleConfig) moduleConfigs.get(moduleName);
					mc.updateConfiguration(reloadConfigFile(moduleName));
					moduleConfigs.put(moduleName, mc);
					return mc;
				} catch (SAXParseException e) {
					logger.error(Resources.getInstance().get("REQUESTPROXY_LOG_MODULE_PARSE_SAX_EXCEPTION", new Integer(e.getLineNumber()), new Integer(e.getColumnNumber())), e);
				} catch (DataFormatException e) {
					logger.error(Resources.getInstance().get("REQUESTPROXY_LOG_MODULE_PARSE_FORMAT_EXCEPTION"), e);
				} catch (Exception e) {
					logger.error(Resources.getInstance().get("REQUESTPROXY_LOG_MODULE_PARSE_EXCEPTION"), e);
				}
			}
		
		synchronized (moduleConfigs) {
			if (moduleLookup == null) {
				logger.error(Resources.getInstance().get("COMMONCONFIG_LOG_MODULELOOKUP_NA", moduleName));
				return null;
			}
			
			Preferences modulePreferences = octopus.getModulePreferences(moduleName);
			File modulePath = moduleLookup.getModulePath(moduleName);
			
			if (modulePath == null) {
				String modulePrefPath = modulePreferences.get(ModuleLookup.PREF_NAME_REAL_PATH, null);
				logger.info(Resources.getInstance().get("OCTOPUS_STARTER_LOG_MODULE_PATH_PREFERENCES", moduleName));
				if (modulePrefPath != null && modulePrefPath.length() != 0)
					modulePath = new File(modulePrefPath);
			}
			
			File configFile = new File(modulePath, "config.xml");
			if (!configFile.exists()) {
				configFile = new File(modulePath, "module-config.xml");
			}
			if (!configFile.exists()) {
				String configEnvFile = (String) octopusEnvironment.getValue(OctopusEnvironment.KEY_MODULE_CONFIGFILE_LOCATION_PREFIX + moduleName);
				if (configEnvFile != null) {
					if (File.separatorChar != '/')
						configEnvFile = configEnvFile.replace('/', File.separatorChar);
					if (new File(configEnvFile).isAbsolute())
						configFile = new File(configEnvFile);
					else
						configFile = new File(System.getProperty("user.dir"), configEnvFile);
				}
			}
			if (!configFile.exists()) {
				logger.error(Resources.getInstance().get("OCTOPUS_STARTER_LOG_MODULE_CONFIG_PATH_INVALID", moduleName, configFile));
				return null;
			}
			
			try {
				if (moduleConfigs.containsKey(moduleName)) {
					deregisterModule(moduleName);
					moduleConfigs.remove(moduleName);
				}
				
				logger.debug(Resources.getInstance().get("REQUESTPROXY_LOG_PARSING_MODULE_CONFIG", configFile, moduleName));
				Document document = Xml.getParsedDocument(Resources.getInstance().get("REQUESTPROXY_URL_MODULE_CONFIG", configFile.getAbsolutePath()));
				ModuleConfig moduleConfig = new ModuleConfig(moduleName, modulePath, document, modulePreferences);
				moduleConfigs.put(moduleName, moduleConfig);
				
				modulePreferences.put(ModuleLookup.PREF_NAME_REAL_PATH, configFile.getParent());
				octopusEnvironment.setValue(OctopusEnvironment.KEY_MODULE_CONFIGFILE_LOCATION_PREFIX + moduleName, configFile.getAbsolutePath());
				
				registerModule(moduleName, moduleConfig);
				
				return moduleConfig;
			} catch (SAXParseException e) {
				logger.error(Resources.getInstance().get("REQUESTPROXY_LOG_MODULE_PARSE_SAX_EXCEPTION", new Integer(e.getLineNumber()), new Integer(e.getColumnNumber())), e);
			} catch (DataFormatException e) {
				logger.error(Resources.getInstance().get("REQUESTPROXY_LOG_MODULE_PARSE_FORMAT_EXCEPTION"), e);
			} catch (Exception e) {
				logger.error(Resources.getInstance().get("REQUESTPROXY_LOG_MODULE_PARSE_EXCEPTION"), e);
			}
			
			modulePreferences.remove(ModuleLookup.PREF_NAME_REAL_PATH);
			octopusEnvironment.remove(OctopusEnvironment.KEY_MODULE_CONFIGFILE_LOCATION_PREFIX + moduleName);
			
			return null;
		}
	}
	
	
	/**
	 * Returns the {@link Document} which contains the actually configuration
	 * parameters of the module moduleName.
	 * 
	 * @param moduleName
	 *            the module name
	 * @return The {@link Document} which contains the actually configuration
	 *         parameters
	 * @throws SAXException
	 * @throws IOException
	 * @throws ParserConfigurationException
	 * @throws FactoryConfigurationError
	 */
	private Document reloadConfigFile(String moduleName) throws SAXException, IOException, ParserConfigurationException, FactoryConfigurationError {
		Preferences modulePreferences = octopus.getModulePreferences(moduleName);
		File modulePath = moduleLookup.getModulePath(moduleName);
		
		if (modulePath == null) {
			String modulePrefPath = modulePreferences.get(ModuleLookup.PREF_NAME_REAL_PATH, null);
			logger.info(Resources.getInstance().get("OCTOPUS_STARTER_LOG_MODULE_PATH_PREFERENCES", moduleName));
			if (modulePrefPath != null && modulePrefPath.length() != 0)
				modulePath = new File(modulePrefPath);
		}
		
		File configFile = new File(modulePath, "config.xml");
		if (!configFile.exists()) {
			configFile = new File(modulePath, "module-config.xml");
		}
		if (!configFile.exists()) {
			String configEnvFile = (String) octopusEnvironment.getValue(OctopusEnvironment.KEY_MODULE_CONFIGFILE_LOCATION_PREFIX + moduleName);
			if (configEnvFile != null) {
				if (File.separatorChar != '/')
					configEnvFile = configEnvFile.replace('/', File.separatorChar);
				if (new File(configEnvFile).isAbsolute())
					configFile = new File(configEnvFile);
				else
					configFile = new File(System.getProperty("user.dir"), configEnvFile);
			}
		}
		if (!configFile.exists()) {
			logger.error(Resources.getInstance().get("OCTOPUS_STARTER_LOG_MODULE_CONFIG_PATH_INVALID", moduleName, configFile));
			return null;
		}
		logger.debug(Resources.getInstance().get("REQUESTPROXY_LOG_PARSING_MODULE_CONFIG", configFile, moduleName));
		return Xml.getParsedDocument(Resources.getInstance().get("REQUESTPROXY_URL_MODULE_CONFIG", configFile.getAbsolutePath()));
	}
	
	
	/**
	 * Diese Methode registriert ein Modul mittels der übergebenen
	 * Modulkonfiguration.
	 * 
	 * @param moduleName
	 *            Modulname
	 * @param moduleConfig
	 *            Modulkonfiguration
	 */
	public void registerModule(String moduleName, ModuleConfig moduleConfig) {
		if (moduleConfig == null)
			return;
		Map taskErrorsMap = moduleConfig.getTaskList().getErrors();
		if (taskErrorsMap != null) {
			Iterator itTaskErrors = taskErrorsMap.entrySet().iterator();
			while (itTaskErrors.hasNext()) {
				Map.Entry taskErrors = (Map.Entry) itTaskErrors.next();
				Object task = taskErrors.getKey();
				if (!(taskErrors.getValue() instanceof List)) {
					logger.warn(Resources.getInstance().get("COMMONCONFIG_LOG_TASK_ERROR_INVALID", moduleName, task, taskErrors.getValue()));
					continue;
				}
				Iterator itErrors = ((List) taskErrors.getValue()).iterator();
				while (itErrors.hasNext())
					logger.warn(Resources.getInstance().get("COMMONCONFIG_LOG_TASK_ERROR", moduleName, task, itErrors.next()));
			}
		}
		try {
			octopus.doAutostart(moduleName, this);
		} catch (Exception e) {
			logger.warn(Resources.getInstance().get("COMMONCONFIG_LOG_AUTOSTART_ERROR", moduleName), e);
		}
	}
	
	/**
	 * Diese Methode deregistriert ein Modul.
	 * 
	 * @param moduleName
	 *            Modulname
	 */
	public void deregisterModule(String moduleName) {
		if (moduleConfigs.containsKey(moduleName)) {
			try {
				octopus.doCleanup(moduleName, this);
			} catch (Exception e) {
				logger.warn(Resources.getInstance().get("COMMONCONFIG_LOG_CLEANUP_ERROR", moduleName), e);
			}
			try {
				dropModuleDataAccess(moduleName);
			} catch (Exception e) {
				logger.warn(Resources.getInstance().get("COMMONCONFIG_LOG_CLEANUP_ERROR", moduleName), e);
			}
			moduleConfigs.remove(moduleName);
		}
	}
	
	/**
	 * Liefert einen <code>Iterator</code> aller existierenden Module.
	 * 
	 * @return Iterator
	 */
	public Iterator getExistingModuleNames() {
		return moduleConfigs.keySet().iterator();
	}
	
	/**
	 * Verzeichnis, indem die Daten des angegebenen Modules liegen.
	 * 
	 * @return Verzeichnis
	 */
	public File getModuleRootPath(String moduleName) {
		ModuleConfig moduleConfig = getModuleConfig(moduleName);
		return (moduleConfig != null) ? moduleConfig.getRealPath() : null;
	}
	
	/**
	 * Liefert das Default-Modul
	 */
	public String getDefaultModuleName() {
		return octopusEnvironment.get(OctopusEnvironment.KEY_DEFAULT_MODULE);
	}
	
	/**
	 * Diese Methode erzeug eine neue {@link PersonalConfig}-Instanz. Die
	 * verwendete Klasse kann über den Modul-Parameter
	 * {@link ModuleConfig#CONFIG_PARAM_PERSONAL_CONFIG_CLASS} gesetzt werden,
	 * Vorgabe ist {@link PersonalConfigImpl}.
	 * 
	 * @param module
	 * @return eine leere {@link PersonalConfig}-Instanz
	 * @throws OctopusConfigException
	 */
	public PersonalConfig createNewConfigImpl(String module) throws OctopusConfigException {
		PersonalConfig pConfig = null;
		if (module != null && module.length() > 0)
			pConfig = getModuleConfig(module).createNewConfigImpl();
		else
			pConfig = new PersonalConfigImpl();
		pConfig.setUserGroups(new String[] { PersonalConfig.GROUP_ANONYMOUS });
		pConfig.setUserLogin("anonymous");
		pConfig.setUserID(new Integer(0));
		pConfig.setUserGivenName("givenName");
		pConfig.setUserLastName("lastName");
		pConfig.setUserEmail("anonymous@anonymous.org");
		return pConfig;
	}
	
	
	/**
	 * Diese Methode liefert einen LoginManager für ein bestimmtes Modul. Liegt
	 * im Modul keine (ausreichende) LoginManager-Konfiguration vor, so wird der
	 * Default-LoginManager des Octopus geliefert.
	 * 
	 * @param module
	 *            Name des Moduls, für den ein LoginManager gesucht wird.
	 * @return LoginManager für das angegebene Modul
	 * @see #getLoginManager()
	 */
	public LoginManager getLoginManager(String module) throws OctopusSecurityException {
		ModuleConfig moduleConfig = getModuleConfig(module);
		return getLoginManager(moduleConfig);
	}
	
	/**
	 * Diese Methode liefert einen LoginManager für ein bestimmtes Modul. Liegt
	 * im Modul keine (ausreichende) LoginManager-Konfiguration vor, so wird der
	 * Default-LoginManager des Octopus geliefert.
	 * 
	 * @param moduleConfig
	 *            Konfiguration des Moduls, für den ein LoginManager gesucht
	 *            wird.
	 * @return LoginManager für das angegebene Modul
	 * @see #getLoginManager()
	 */
	public LoginManager getLoginManager(ModuleConfig moduleConfig) throws OctopusSecurityException {
		LoginManager loginManager = moduleConfig != null ? moduleConfig.getLoginManager() : null;
		return loginManager != null ? loginManager : getLoginManager();
	}
	
	/**
	 * Diese Methode liefert einen Default-LoginManager.
	 * 
	 * @return Default-LoginManager
	 * @throws OctopusSecurityException
	 */
	public synchronized LoginManager getLoginManager() throws OctopusSecurityException {
		if (loginManager == null) {
			Map loginManagerParams = getConfigLoginManager();
			if (!loginManagerParams.isEmpty()) {
				logger.debug("Default-Login-Manager-Erstellung");
				// Octopus möchte seine Config selber machen...
				String loginManagerClassName = (String) loginManagerParams.get("loginManagerClass");
				if (loginManagerClassName != null) {
					logger.debug("Lade LoginManager Klasse: " + loginManagerClassName);
					try {
						// Eintrag ist angegeben...
						Class loginManagerClass = Class.forName(loginManagerClassName);
						loginManager = (LoginManager) loginManagerClass.newInstance();
						loginManager.setConfiguration(loginManagerParams);
					} catch (Exception e) {
						logger.error("Fehler beim Laden des LoginManagers." + e.getMessage());
						throw new OctopusSecurityException(OctopusSecurityException.ERROR_SERVER_AUTH_ERROR, e);
					}
				}
			}
			if (loginManager == null) {
				String msg = Resources.getInstance().get("COMMONCONFIG_LOG_INVALID_AUTH_METHOD");
				throw new OctopusSecurityException(msg);
			}
		}
		return loginManager;
	}
	
	

	/**
	 * Methode, die bestimmte Config ausliest
	 * 
	 * @param key -
	 *            der auszulesende Key
	 * @return Wert
	 */
	public String getConfigData(String key) {
		return octopusEnvironment.get(key);
	}
	
	/**
	 * Returns an iterator over all available configuration items.
	 * 
	 * @return Iterator.
	 */
	public Iterator getConfigKeys() {
		return octopusEnvironment.keySet().iterator();
	}
	
	public void setModuleLookup(ModuleLookup moduleLookup) {
		this.moduleLookup = moduleLookup;
	}
	
	public ModuleLookup getModuleLookup() {
		return moduleLookup;
	}
	
	/**
	 * Diese Methode liefert die Umgebungswerte
	 * 
	 * @return Umgebungsobjekt.
	 */
	public OctopusEnvironment getEnvironment() {
		return octopusEnvironment;
	}
	
	/**
	 * Sets the enviromet to the given {@link OctopusEnvironment}
	 * 
	 * @param octopusEnvironment
	 *            The new {@link OctopusEnvironment}
	 */
	public void setEnvironment(OctopusEnvironment environment) {
		this.octopusEnvironment = environment;
	}
	
	/**
	 * @return Returns the configLoginManager.
	 */
	public Map getConfigLoginManager() {
		return octopusEnvironment.getConfigLoginManager();
	}
}
