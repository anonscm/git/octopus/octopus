/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */
package de.tarent.octopus.persistence;

import java.util.Date;

/**
 * This class represent the persistent object of the request, which is needed by
 * reloadable tasks or by asynchronous tasks.
 * 
 * @author Alex Maier, tarent GmbH
 * 
 */
public class PersistentRequestItem {
	
	public static int STATUS_COMPLETED = 0;
	public static int STATUS_RUNNING = 1;
	

	private int pk;
	private String processId;
	private String requestId;
	private String moduleName;
	private String taskName;
	private String requestData;
	private String callBackUri;
	private int status;
	private Date creationDate;
	private Date updateDate;
	
	public int getPk() {
		return this.pk;
	}
	
	public void setPk(int pk) {
		this.pk = pk;
	}
	
	public String getProcessId() {
		return this.processId;
	}
	
	public void setProcessId(String processId) {
		this.processId = processId;
	}
	
	public String getRequestId() {
		return this.requestId;
	}
	
	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}
	
	public String getModuleName() {
		return this.moduleName;
	}
	
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	
	public String getTaskName() {
		return this.taskName;
	}
	
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}
	
	public String getRequestData() {
		return this.requestData;
	}
	
	public void setRequestData(String requestData) {
		this.requestData = requestData;
	}
	
	public String getCallBackUri() {
		return this.callBackUri;
	}
	
	public void setCallBackUri(String callBackUri) {
		this.callBackUri = callBackUri;
	}
	
	public int getStatus() {
		return this.status;
	}
	
	public void setStatus(int status) {
		this.status = status;
	}
	
	public Date getCreationDate() {
		return this.creationDate;
	}
	
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	
	public Date getUpdateDate() {
		return this.updateDate;
	}
	
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
	public String toString() {
		StringBuffer sb = new StringBuffer("[");
		sb.append("\r\n pk: ");
		sb.append(getPk());
		sb.append("\r\n processId: ");
		sb.append(getProcessId());
		sb.append("\r\n requestId: ");
		sb.append(getRequestId());
		sb.append("\r\n moduleName: ");
		sb.append(getModuleName());
		sb.append("\r\n taskName: ");
		sb.append(getTaskName());
		sb.append("\r\n callBackUri: ");
		sb.append(getCallBackUri());
		sb.append("\r\n status: ");
		sb.append(getStatus());
		sb.append("\r\n creationDate ");
		sb.append(getCreationDate() == null ? "not setted yet" : getCreationDate().toString());
		sb.append("\r\n updateDate ");
		sb.append(getUpdateDate() == null ? "not setted yet" : getUpdateDate().toString());
		sb.append("\r\n]");
		return sb.toString();
	}
}
