/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.octopus.response.soap;

import java.io.IOException;
import java.io.OutputStream;

import javax.xml.soap.SOAPException;

import org.apache.axis.Message;
import org.apache.axis.message.RPCElement;
import org.apache.axis.message.RPCParam;
import org.apache.axis.message.SOAPEnvelope;


/**
 * Bereitstellung und Kapselung von SOAP Funktionalität für die Erstellung einer
 * Antwort auf einen RPC Aufruf.
 * 
 * @author <a href="mailto:mancke@mancke-software.de">Sebastian Mancke</a>,
 *         <b>tarent GmbH</b>
 */
public class SoapResponse {
	protected RPCElement rpc;
	protected SoapEngine soapEngine;
	
	public static final String METHOD_RESPONSE_SUFFIX = "Response";
	
	public SoapResponse(SoapEngine soapEngine, String namespace, String callingMethodName) {
		this.soapEngine = soapEngine;
		rpc = new RPCElement(namespace, callingMethodName + METHOD_RESPONSE_SUFFIX, null);
	}
	
	public void addParam(String name, Object value) {
		rpc.addParam(new RPCParam(name, value));
	}
	
	/**
	 * Method writeTo.
	 * 
	 * @param outputStream
	 */
	public void writeTo(OutputStream outputStream) throws IOException, SOAPException {
		SOAPEnvelope env = new SOAPEnvelope();
		env.addBodyElement(rpc);
		
		Message outMessage = new Message(env);
		outMessage.setMessageContext(soapEngine.createMessageContext());
		outMessage.writeTo(outputStream);
	}
}
