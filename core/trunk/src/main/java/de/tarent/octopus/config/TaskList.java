/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.octopus.config;

import de.tarent.octopus.content.PortDefinition;
import de.tarent.octopus.util.Xml;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.w3c.dom.Element;

/**
 * Klasse zur Repräsentation einer Sammlung von Tasks
 * 
 * @author <a href="mailto:mancke@mancke-software.de">Sebastian Mancke</a>,
 *         <b>tarent GmbH</b>
 */
public class TaskList {
	protected String nameSpace;
	protected Map tasks = new HashMap();
	protected String description;
	
	public TaskList() {
	}
	
	public Task getTask(String name) {
		return (Task) tasks.get(name);
	}
	
	/**
	 * Auslesen der TaskInformationen Kopie auf Common Config Soll später nur
	 * noch hier existieren.
	 */
	public void parseTasks(Element tasksNode, ModuleConfig moduleConfig) {
		// Evtl. auch nicht vorhanden
		nameSpace = tasksNode.getAttribute("name");
		if (nameSpace == null || nameSpace.length() == 0)
			nameSpace = moduleConfig.getName();
		
		Element currTaskElement = Xml.getFirstChildElement(tasksNode);
		while (currTaskElement != null) {
			Task currTask = new Task(this, currTaskElement, moduleConfig);
			tasks.put(currTask.getName(), currTask);
			
			currTaskElement = Xml.getNextSiblingElement(currTaskElement);
		}
	}
	
	public PortDefinition getPortDefinition() {
		PortDefinition pd = new PortDefinition(nameSpace, description);
		for (Iterator e = tasks.values().iterator(); e.hasNext();) {
			Task currTask = (Task) e.next();
			pd.addOperation(currTask.getOperationDefinition());
		}
		return pd;
	}
	
	/**
	 * Liefert eine Map. Diese enthält für jedes Task, das fehlerhaft ist einen
	 * Eintrag mit dem Tasknamen als String Key und Fehlerliste als String
	 * Vector. Oder null, wenn kein Task fehlerhalt ist.
	 * 
	 * @return Map mit Tasknamen als String Keys und Vectoren von Strings als
	 *         Values, oder null
	 */
	public Map getErrors() {
		Map out = new HashMap();
		for (Iterator e = tasks.values().iterator(); e.hasNext();) {
			Task currTask = (Task) e.next();
			List errList = currTask.getTaskErrors();
			if (errList.size() > 0)
				out.put(currTask.getName(), errList);
		}
		if (out.size() > 0)
			return out;
		return null;
	}
	
	/**
	 * Liefert die Tasks
	 * 
	 * @return Map
	 */
	public Iterator getTasksKeys() {
		return tasks.keySet().iterator();
	}
}
