/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.octopus.embedded;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import de.tarent.octopus.client.OctopusCallException;
import de.tarent.octopus.client.OctopusResult;
import de.tarent.octopus.client.OctopusTask;

/**
 * Aufruf eines Task des Octopus.
 * 
 * @author <a href="mailto:sebastian@tarent.de">Sebastian Mancke</a>, <b>tarent
 *         GmbH</b>
 */
public class DirectCallTask implements OctopusTask {
	
	OctopusStarter octopusStarter;
	String moduleName;
	Map params;
	
	public DirectCallTask(OctopusStarter octopusStarter) {
		this.octopusStarter = octopusStarter;
		params = new HashMap();
	}
	
	public OctopusTask add(String paramName, Object paramValue) {
		Object param = params.get(paramName);
		if (param == null) {
			params.put(paramName, paramValue);
		} else {
			if (param instanceof List) {
				((List) param).addLast(paramValue);
			} else {
				List list = new List();
				list.add(param);
				list.addLast(paramValue);
				params.put(paramName, list);
			}
		}
		return this;
	}
	
	public OctopusResult invoke() throws OctopusCallException {
		
		OctopusDirectCallResult res = null;
		try {
			res = octopusStarter.request(params);
		} catch (Exception e) {
			throw new OctopusCallException("Error while calling octopus directly", e);
		}
		if (res.errorWhileProcessing())
			throw new OctopusCallException(res.getErrorMessage(), res.getErrorException());
		return res;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.tarent.octopus.client.OctopusTask#setConnectionTracking(boolean)
	 */
	public void setConnectionTracking(boolean contrack) {
		// TODO Auto-generated method stub
		
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.tarent.octopus.client.OctopusTask#isConnectionTracing()
	 */
	public boolean isConnectionTracking() {
		// TODO Auto-generated method stub
		return false;
	}
	
	/**
	 * This is just a marker extension of {@link LinkedList} to tag internal
	 * parameters with more than one value.
	 */
	private static class List extends LinkedList {
		/** serialVersionUID */
		private static final long serialVersionUID = 1L;
		// nothing more than a marker
	}
}
