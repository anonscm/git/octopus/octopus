/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.octopus.embedded;

import java.util.Iterator;
import java.util.Map;

import de.tarent.octopus.client.OctopusCallException;
import de.tarent.octopus.client.OctopusConnection;
import de.tarent.octopus.client.OctopusResult;
import de.tarent.octopus.client.OctopusTask;
import de.tarent.octopus.client.UserDataProvider;

/**
 * Liefert eine Verbindung zu einem Octopus im lokalen Prozessraum.
 * 
 * @author <a href="mailto:sebastian@tarent.de">Sebastian Mancke</a>, <b>tarent
 *         GmbH</b>
 */
public class OctopusDirectCallConnection implements OctopusConnection {
	

	public static final String PARAM_MODULE = "module";
	public static final String PARAM_TASK = "task";
	
	OctopusStarter octopusStarter;
	String moduleName;
	
	
	/**
	 * Liefert ein CallObject, dass für den Aufruf dieses Task verwendet werden
	 * kann.
	 */
	public OctopusTask getTask(String taskName) throws OctopusCallException {
		OctopusTask task = new DirectCallTask(getOctopusStarter());
		task.add(PARAM_MODULE, getModuleName());
		task.add(PARAM_TASK, taskName);
		return task;
	}
	
	public OctopusResult callTask(String taskName, Map paramMap) throws OctopusCallException {
		
		OctopusTask task = getTask(taskName);
		if (paramMap != null)
			for (Iterator iter = paramMap.keySet().iterator(); iter.hasNext();) {
				String key = (String) iter.next();
				task.add(key, paramMap.get(key));
			}
		return task.invoke();
	}
	
	
	public OctopusStarter getOctopusStarter() {
		return octopusStarter;
	}
	
	public void setOctopusStarter(OctopusStarter newOctopusStarter) {
		this.octopusStarter = newOctopusStarter;
	}
	
	public String getModuleName() {
		return moduleName;
	}
	
	public void setModuleName(String newModuleName) {
		this.moduleName = newModuleName;
	}
	
	public void setPassword(String newPassword) {
		// Do Nothing at the Moment
	}
	
	public void setUsername(String newUsername) {
		// Do Nothing at the Moment
	}
	
	public void login() throws OctopusCallException {
		// Do Nothing at the Moment
	}
	
	public void logout() throws OctopusCallException {
		// Do Nothing at the Moment
	}
	
	public void setUserDataProvider(UserDataProvider provider) {
		// Do Nothing at the Moment
	}
	

}
