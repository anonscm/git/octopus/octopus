/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

/* $Id: AbstractJndiFactory.java,v 1.6 2007/06/11 13:05:37 christoph Exp $
 * tarent-octopus, Webservice Data Integrator and Applicationserver
 * Copyright (C) 2002 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus'
 * (which makes passes at compilers) written
 * by Sebastian Mancke and Michael Klink.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package de.tarent.octopus.jndi;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.Name;
import javax.naming.NamingException;
import javax.naming.spi.ObjectFactory;

import org.apache.commons.logging.Log;

import de.tarent.octopus.logging.LogFactory;

/**
 * Diese Klasse stellt eine JNDI-{@link ObjectFactory} zum Zugriff auf den
 * aktuellen Octopus-Kontext dar.<br>
 * Sie ist angelehnt an das Beispiel für das erzeugen spezialisierter Resource
 * Factories <a
 * href="http://jakarta.apache.org/tomcat/tomcat-5.0-doc/jndi-resources-howto.html#Adding
 * Custom Resource Factories">hier</a> in der Tomcat-Dokumentation.<br>
 * Zur Nutzung muss diese Factory der JNDI-Machinerie bekannt gemacht werden.
 * Analog obigem Beispiel ist dies im Projekt octopus/webapp getan worden,
 * entsprechende Einträge stehen dort in den Dateien <code>octopus.xml</code>
 * und <code>web.xml</code> im Verzeichnis <code>webapp/WEB-INF</code>.
 */
public abstract class AbstractJndiFactory implements ObjectFactory {
	protected Log logger = LogFactory.getLog(getClass());
	
	/**
	 * Currently only support the apache tomcat (and maby more servlet
	 * container?) as JNDI context provider. See
	 * <code>http://tomcat.apache.org/tomcat-5.5-doc/jndi-resources-howto.html</code>
	 * for more informations about this configuration.
	 * 
	 * @return naming context
	 */
	protected Context getContext() {
		try {
			return (Context) new InitialContext().lookup("java:comp/env");
		} catch (NamingException e) {
			logger.info("No JNDI context available. Can not bind context '" + getLookupPath() + "'.", e);
			return null;
		}
	}
	
	public boolean bind() {
		Context context = getContext();
		if (context == null)
			return false;
		
		try {
			context.addToEnvironment(Context.OBJECT_FACTORIES, getClass().getName());
		} catch (NamingException e) {
			logger.warn("Can not add current class '" + getClass().getName() + "' " + "to the object factory list.");
		}
		
		try {
			Object object = context.lookup(getLookupPath());
			if (object != null && object.getClass().getName().equals(getClass().getName())) {
				logger.info("JNDI context available. " + "Context '" + getLookupPath() + "' already binded.");
				return true;
			} else if (object != null) {
				logger.info("JNDI context available. " + "Wrong class '" + object.getClass().getName() + "' " + "for context '" + getLookupPath() + "' binded, will rebind it now.");
			} else {
				logger.info("JNDI context available. " + "Context '" + getLookupPath() + "' not binded yet, will do it now.");
			}
		} catch (NamingException e) {
			logger.info("JNDI context available. " + "Exception '" + e.getLocalizedMessage() + "' while lookup " + "context '" + getLookupPath() + "' catched, will rebind it now.");
		}
		
		try {
			context.rebind(getLookupPath(), this);
			logger.info("JNDI context '" + getLookupPath() + "' successful binded.");
			return true;
		} catch (NamingException e) {
			logger.info("JNDI context available, but can not bind context '" + getLookupPath() + "'.");
			return false;
		}
	}
	
	public boolean unbind() {
		Context context = getContext();
		if (context == null)
			return false;
		
		try {
			context.unbind(getLookupPath());
			logger.info("JNDI context '" + getLookupPath() + "' successful unbinded.");
			return true;
		} catch (NamingException e) {
			logger.error("JNDI context available, but can not unbind context '" + getLookupPath() + "'.", e);
			return false;
		}
	}
	
	abstract protected String getLookupPath();
	
	public Object getObjectInstance(Object object, Name name, Context context, Hashtable environment) throws Exception {
		return this;
	}
}
