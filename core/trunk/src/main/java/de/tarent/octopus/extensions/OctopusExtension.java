/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */


package de.tarent.octopus.extensions;

/**
 * This implements a simple extension framework for the Octopus application
 * server. An extension has to be implement this interface and can then be
 * initialized and called without relying on reflections.
 * 
 * @author Michael Kleinhenz (m.kleinhenz@tarent.de)
 */
public interface OctopusExtension {
	/**
	 * Initializes the extension. Runtime parameters are encoded in the
	 * parameter.
	 * 
	 * @param param
	 *            Runtime parameters for this extension.
	 */
	public void initialize(Object param);
	
	/**
	 * Starts the extension. This should be called on startup of regular server
	 * operation.
	 */
	public void start();
	
	/**
	 * Stops the extension. This should be called before the server shuts down.
	 */
	public void stop();
}
