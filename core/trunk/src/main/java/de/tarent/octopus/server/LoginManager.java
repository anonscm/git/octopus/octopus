/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.octopus.server;

import java.util.Map;

import de.tarent.octopus.config.CommonConfig;
import de.tarent.octopus.request.OctopusRequest;
import de.tarent.octopus.request.OctopusSession;
import de.tarent.octopus.security.OctopusSecurityException;

/**
 * Ein LoginManager regelt die Authentifizierung. Er stellt Wissen darüber
 * bereit, welche Aktionen vom Benutzer ausgeführt werden dürfen. <br>
 * <br>
 * Seine Hauptaufgabe ist es, eine PersonalConfig modulweit bereit zu stellen.
 * 
 * 
 * @author <a href="mailto:mancke@mancke-software.de">Sebastian Mancke</a>,
 *         <b>tarent GmbH</b>
 */
public interface LoginManager {
	public static final String TASK_LOGIN = "login";
	public static final String TASK_LOGIN_SOAP = "login_SOAP";
	public static final String TASK_LOGOUT = "logout";
	public static final String TASK_LOGOUT_SOAP = "logout_SOAP";
	
	/**
	 * Setzt eine Menge von Implementierungsspezifischen
	 * Konfigurationsparametern. Die Verwendung von Parametern in einer
	 * Implementierung ist optional.
	 */
	
	public void setConfiguration(Map configuration);
	
	/**
	 * Regelt Login-Logout operationen. Als Ergebnis wird eine
	 * PersonalConfigImpl für das aktuelle Modul in der Session abgelegt. In
	 * dieser PersonalConfigImpl können die Informationen zum Login und den
	 * Rechten hinterlegt sein.
	 * 
	 * @throws OctopusSecurityException
	 *             bei einer Authentifizierung, die Fehlerhaft war
	 */
	public void handleAuthentication(CommonConfig cConfig, OctopusRequest octopusRequest, OctopusSession theSession) throws OctopusSecurityException;
	
	
	/**
	 * Liefert die zuvor erstellte PersonalConfigImpl dieses Moduls zurück.
	 * 
	 * @return PersonalConfigImpl des Benutzers
	 */
	public PersonalConfig getPersonalConfig(CommonConfig config, OctopusRequest octopusRequest, OctopusSession theSession);
	
	/**
	 * Stellt fest, ob der LoginManager auch selber die Userverwaltung
	 * Übernehmen kann.
	 * 
	 * @return <code>true</code> falls Userverwaltung möglich,
	 *         <code>false</code> sonst.
	 */
	public boolean isUserManagementSupported();
	
	/**
	 * Liefert den zuständigen UserManager zurück.
	 * 
	 * @return UserManager oder <code>null</code>, falls LoginManager kein
	 *         UserManagement unterstützt.
	 */
	public UserManager getUserManager();
	
}
