/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.octopus.content;

import de.tarent.octopus.config.ModuleConfig;
import de.tarent.octopus.server.OctopusContext;

/**
 * Jeglicher Zugriff auf Daten erfolgt über diese ContentWorker.
 * 
 * @author <a href="mailto:mancke@mancke-software.de">Sebastian Mancke</a>,
 *         <b>tarent GmbH</b>
 */
public interface ContentWorker {
	/**
	 * Diese Methode wird nach Erzeugung des Workers aufgerufen, so dass dieser
	 * sich im Kontext seines Moduls konfigurieren kann.
	 * 
	 * @param config
	 *            Modulkonfiguration.
	 */
	public void init(ModuleConfig config);
	
	/**
	 * Abarbeiten einer Action mit diesem ContentWorker Die Ergebnisse werden in
	 * dem tcContent-Kontainer abgelegt. Ein ContentWorker kann für mehrere
	 * Actions zuständig sein.
	 * @param actionName
	 *            Name der Aktion, die von diesem Worker ausgeführt werden soll.
	 * @param octopusContext Octopus context for processing current request.
	 * 
	 * @return String mit einem Statuscode z.B. ok oder error
	 */
	public String doAction(String actionName, OctopusContext octopusContext) throws ContentProzessException;
	
	/**
	 * Liefert eine Beschreibgung der Actions und deren Eingabeparameter, die
	 * von diesem Worker bereit gestellt werden.
	 * 
	 * @return Eine Abstrakte Beschreibung der Methoden und Parameter
	 */
	public PortDefinition getWorkerDefinition();
	
	/**
	 * Standard-Ergebnis für den Erfolgsfall.
	 */
	public final static String RESULT_ok = "ok";
	
	/**
	 * Standard-Ergebnis für den Fehlerfall.
	 */
	public final static String RESULT_error = "error";
}
