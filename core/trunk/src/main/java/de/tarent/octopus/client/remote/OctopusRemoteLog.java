/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */
package de.tarent.octopus.client.remote;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.text.MessageFormat;
import java.util.Date;
import java.util.List;

import org.apache.axis.AxisFault;
import org.apache.axis.Message;
import org.apache.axis.client.Call;
import org.apache.commons.logging.Log;

import de.tarent.octopus.logging.LogFactory;

/**
 * Klasse, die Logs zur Remotekommunikation anlegt...
 * 
 * @author Philipp Kirchner, tarent GmbH
 */
public class OctopusRemoteLog {
	private Log logger = LogFactory.getLog(this.getClass());
	
	private String taskName;
	private Date taskStart;
	private Date taskEnd;
	private long taskDataTransfer;
	private List taskParams;
	private PrintWriter conlog;
	private PrintWriter condumplog;
	
	private OctopusRemoteTask task;
	
	public OctopusRemoteLog(String taskName, int requestSize, List params) {
		this.taskName = taskName;
		this.taskStart = new Date();
		taskDataTransfer = requestSize;
		taskParams = params;
		try {
			conlog = new PrintWriter(new FileOutputStream(new File(System.getProperty("user.home") + File.separator + "ContactClient.con.log")));
			condumplog = new PrintWriter(new FileOutputStream(new File(System.getProperty("user.home") + File.separator + "ContactClient.condump.log")));
		} catch (FileNotFoundException e) {
			logger.warn("Fehler beim Dateizugriff!", e);
		}
	}
	
	/**
	 * @param task
	 */
	public OctopusRemoteLog(OctopusRemoteTask task) {
		this(task.getTaskName(), 0, task.params);
		this.task = task;
	}
	
	public void startLogEntry(OctopusRemoteTask task) {
		this.task = task;
		long size = -1;
		try {
			if (task != null) {
				Call soapCall = task.axisSoapCall;
				if (soapCall != null) {
					Message message = soapCall.getResponseMessage();
					if (message != null) {
						size = message.getContentLength();
					}
				}
			}
		} catch (AxisFault e) {
			// Ignorieren, wir möchten ja nur die Größe wissen
		}
		assert task != null;
		startLogEntry(task.getTaskName(), size, task.params);
	}
	
	public void startLogEntry(String taskName, long size, List params) {
		this.taskName = taskName;
		this.taskStart = new Date();
		taskDataTransfer = size;
		taskParams = params;
	}
	
	public void commitLogEntry(long resultSize) {
		taskEnd = new Date();
		taskDataTransfer += resultSize;
		Date duration = new Date(taskEnd.getTime() - taskStart.getTime());
		String message = MessageFormat.format("Task: {0}, Duration: {1}, Size: {2}, Params: {3}", new Object[] { taskName, new Long(duration.getTime()), new Long(taskDataTransfer), taskParams });
		conlog.println(message);
		condumplog.println("==========");
		condumplog.println(message);
		try {
			condumplog.println(task.axisSoapCall.getMessageContext().getRequestMessage().getSOAPPartAsString());
			condumplog.println("----------");
			if (task.axisSoapCall.getResponseMessage() != null)
				condumplog.println(task.axisSoapCall.getResponseMessage().getSOAPPartAsString());
			else
				condumplog.println("axis call response is NULL");
		} catch (AxisFault e) {
			logger.error(e.toString(), e);
		}
		conlog.flush();
		condumplog.flush();
	}
	
	/**
	 * 
	 */
	public void commitLogEntry() {
		long size = -1;
		try {
			if (task != null) {
				Call soapCall = task.axisSoapCall;
				if (soapCall != null) {
					Message message = soapCall.getResponseMessage();
					if (message != null) {
						size = message.getContentLength();
					}
				}
			}
		} catch (AxisFault e) {
			// Ignorieren, wir möchten ja nur die Größe wissen
		}
		commitLogEntry(size);
	}
}
