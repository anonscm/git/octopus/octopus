/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */
package de.tarent.octopus.server;

import de.tarent.octopus.security.OctopusSecurityException;

/**
 * @author kirchner
 * 
 * Interface für einen UserManager.
 */
public interface UserManager {
	/**
	 * Hinzufügen eines Users zur Authentifizierungs-DB.
	 * 
	 * @param userID
	 *            userID des neuen Users
	 * @param firstName
	 *            Vorname des Users
	 * @param lastName
	 *            Nachname des Users
	 * @param password
	 *            Passwort des Users
	 */
	public void addUser(String userID, String firstName, String lastName, String password) throws OctopusSecurityException;
	
	/**
	 * Modifizieren eines Users in der Authentifizierungs-DB.
	 * 
	 * @param userID
	 *            userID des zu modifizierenden Users
	 * @param firstName
	 *            Vorname des Users
	 * @param lastName
	 *            Nachname des Users
	 * @param password
	 *            Password des Users
	 */
	public void modifyUser(String userID, String firstName, String lastName, String password) throws OctopusSecurityException;
	
	/**
	 * Setzt einen Parameter zu einem bestimmten User.
	 * 
	 * @param userID
	 *            ID des Users
	 * @param paramname
	 *            Name des Parameters
	 * @param paramvalue
	 *            Wert des Parameters
	 */
	public void setUserParam(String userID, String paramname, Object paramvalue) throws OctopusSecurityException;
	
	/**
	 * Liest einen UserParameter aus.
	 * 
	 * @param userID
	 *            ID des Users
	 * @param paramname
	 *            Name des Parameters
	 * @return Wert des Parameters, falls vorhanden, <code>null</code> sonst.
	 */
	public Object getUserParam(String userID, String paramname) throws OctopusSecurityException;
	
	/**
	 * Löschen eines Users aus der Authentifizierungs-DB.
	 * 
	 * @param userID
	 *            userID des zu löschenden Users
	 */
	public void deleteUser(String userID) throws OctopusSecurityException;
}
