/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */
package de.tarent.octopus.persistence;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;

import de.tarent.octopus.config.ModuleConfig;
import de.tarent.octopus.logging.LogFactory;

/**
 * @author Alex Maier, tarent GmbH
 * 
 */
public class DBAccessManager {
	
	private Connection connection = null;
	
	private final static String CREATE_TABLE = "CREATE TABLE";
	private final static String SELECT = "SELECT";
	private final static String STATEMENT_SELECT_MAX_PK = SELECT + " MAX(pk_octopustask) " + "FROM " + "octopustask";
	
	private final static String STATEMENT_SELECT_ALL_OCTOPUS_TASKS = "SELECT " + "pk_octopustask, " + "process_id, " + "request_id, " + "module_name, " + "task_name, " + "request_data, " + "call_back_uri, " + "status, " + "creation_date, "
			+ "update_date " + "FROM octopustask";
	
	private final static String STATEMENT_FOR_INSERT_OCTOPUS_TASK = "INSERT INTO octopustask (" + "pk_octopustask, " + "process_id, " + "request_id, " + "module_name, " + "task_name, " + "request_data, " + "call_back_uri, " + "status, "
			+ "creation_date, " + "update_date) " + "VALUES (?,?,?,?,?,?,?,?,?,?)";
	
	private final static String STATEMENT_FOR_UPDATE_OCTOPUS_TASK = "UPDATE octopustask SET " + "process_id = ?, " + "request_id = ?, " + "module_name = ?, " + "task_name = ?, " + "request_data = ?, " + "call_back_uri = ?, " + "status = ?, "
			+ "creation_date = ?, " + "update_date = ? " + "WHERE pk_octopustask = ?";
	
	private final static String STATEMENT_FOR_DELETE_OCTOPUS_TASK = "DELETE FROM octopustask " + "WHERE pk_octopustask = ?";
	private final static String STATEMENT_FOR_DELETE_OCTOPUS_TASK_BY_PROCESS_ID = "DELETE FROM octopustask " + "WHERE process_id = ?";
	
	private final static String FROM = "FROM";
	private final static String ITEM_SEPARATOR = " ";
	private final static String SELECT_TABLE_EXISTS = SELECT + ITEM_SEPARATOR + "*" + ITEM_SEPARATOR + FROM + ITEM_SEPARATOR;
	
	// Constants for the JDBC connection
	public static final String CFG_JDBC_DRIVER = "jdbcDriver";
	public static final String CFG_JDBC_CONNECTION_STRING = "jdbcConnectionString";
	public static final String CFG_JDBC_USERNAME = "jdbcUsername";
	public static final String CFG_JDBC_PASSWORD = "jdbcPassword";
	
	private String jdbcDriver = null;
	private String jdbcConnectionString = null;
	private String jdbcUsername = null;
	private String jdbcPassword = null;
	
	private static Log logger = LogFactory.getLog(DBAccessManager.class);
	
	/**
	 * Initialize the {@link DBAccessManager}
	 * 
	 * @param jdbcDriver
	 *            The JDBC driver class name
	 * @param jdbcConnectionString
	 *            The connection URL to the database
	 * @param jdbcUsername
	 *            The user name to access the database
	 * @param jdbcPassword
	 *            The password to access the database
	 */
	public DBAccessManager(String jdbcDriver, String jdbcConnectionString, String jdbcUsername, String jdbcPassword) {
		this.jdbcDriver = jdbcDriver;
		this.jdbcConnectionString = jdbcConnectionString;
		this.jdbcUsername = jdbcUsername;
		this.jdbcPassword = jdbcPassword;
	}
	
	/**
	 * Initialize the {@link DBAccessManager} by reading the database
	 * configuration from the Map
	 * {@link ModuleConfig#getDataAccess()#get(ModuleConfig.DB_PERSISTENT)}.
	 * 
	 * @param moduleConfig
	 *            The {@link de.tarent.octopus.config.ModuleConfig}
	 */
	public DBAccessManager(ModuleConfig moduleConfig) {
		Map dataAccess = (Map) moduleConfig.getDataAccess().get(ModuleConfig.DB_PERSISTENT);
		this.jdbcDriver = (String) dataAccess.get(CFG_JDBC_DRIVER);
		this.jdbcConnectionString = (String) dataAccess.get(CFG_JDBC_CONNECTION_STRING);
		this.jdbcUsername = (String) dataAccess.get(CFG_JDBC_USERNAME);
		this.jdbcPassword = (String) dataAccess.get(CFG_JDBC_PASSWORD);
	}
	
	
	/**
	 * Stores the given {@link PersistentRequestItem}
	 * <code>persistentRequest</code> in the database. The primary key will be
	 * setted to the maximum value +1. The creation and update date will be
	 * setted to the current system date. The stored
	 * {@link PersistentRequestItem} will be returned.
	 * 
	 * @param persistentRequest
	 * @return the stored {@link PersistentRequestItem}
	 *         <code>persistentRequest</code>.
	 * @throws Exception
	 */
	public synchronized PersistentRequestItem insertPersistentRequest(PersistentRequestItem persistentRequest) throws Exception {
		PreparedStatement pstmt = null;
		logger.info("Generate the next id");
		persistentRequest.setPk(getNextId());
		Timestamp currentTime = new Timestamp(System.currentTimeMillis());
		persistentRequest.setCreationDate(currentTime);
		persistentRequest.setUpdateDate(currentTime);
		logger.info("Try to store the request to the database " + persistentRequest.toString());
		logger.debug("Try to store the request to the database " + persistentRequest.toString() + " requestData: " + persistentRequest.getRequestData());
		try {
			pstmt = getConnection().prepareStatement(STATEMENT_FOR_INSERT_OCTOPUS_TASK);
			pstmt.setInt(1, persistentRequest.getPk());
			pstmt.setString(2, persistentRequest.getProcessId());
			pstmt.setString(3, persistentRequest.getRequestId());
			pstmt.setString(4, persistentRequest.getModuleName());
			pstmt.setString(5, persistentRequest.getTaskName());
			pstmt.setString(6, persistentRequest.getRequestData());
			pstmt.setString(7, persistentRequest.getCallBackUri() == null || persistentRequest.getCallBackUri().equals("") ? " " : persistentRequest.getCallBackUri());
			pstmt.setInt(8, persistentRequest.getStatus());
			pstmt.setTimestamp(9, (Timestamp) persistentRequest.getCreationDate());
			pstmt.setTimestamp(10, (Timestamp) persistentRequest.getUpdateDate());
			pstmt.executeUpdate();
			pstmt.close();
		} catch (SQLException sqle) {
			logger.error("Trying to store the request in the database fails: ", sqle);
			throw sqle;
		} catch (Exception e) {
			throw e;
		}
		logger.info("The request was successful stored in the database");
		return persistentRequest;
	}
	
	/**
	 * Sets the update date to the current system date. Updates the given
	 * {@link PersistentRequestItem} <code>persistentRequest</code> in the
	 * database.
	 * 
	 * @param persistentRequest
	 *            The {@link PersistentRequestItem} which should be updated.
	 * @throws Exception
	 */
	public synchronized void updatePersistentRequest(PersistentRequestItem persistentRequest) throws Exception {
		PreparedStatement pstmt = null;
		logger.info("Try to update the request from the database " + persistentRequest.toString());
		try {
			pstmt = getConnection().prepareStatement(STATEMENT_FOR_UPDATE_OCTOPUS_TASK);
			pstmt.setString(1, persistentRequest.getProcessId());
			pstmt.setString(2, persistentRequest.getRequestId());
			pstmt.setString(3, persistentRequest.getModuleName());
			pstmt.setString(4, persistentRequest.getTaskName());
			pstmt.setString(5, persistentRequest.getRequestData());
			pstmt.setString(6, persistentRequest.getCallBackUri() == null ? " " : persistentRequest.getCallBackUri());
			pstmt.setInt(7, persistentRequest.getStatus());
			pstmt.setTimestamp(8, (Timestamp) persistentRequest.getCreationDate());
			pstmt.setTimestamp(9, new Timestamp(System.currentTimeMillis()));
			pstmt.setInt(10, persistentRequest.getPk());
			pstmt.executeUpdate();
			pstmt.close();
		} catch (SQLException sqle) {
			logger.error("Trying to update the request in the database fails: ", sqle);
			throw sqle;
		} catch (Exception e) {
			throw e;
		}
		logger.info("The request was successful updated in the database");
	}
	
	/**
	 * Removes the given {@link PersistentRequestItem}
	 * <code>persistentRequest</code> from the database. Returns
	 * <code>true</code> on success. Otherwise returns <code>false</code>.
	 * 
	 * @param persistentRequest
	 *            The {@link PersistentRequestItem} to be removed.
	 * @throws Exception
	 */
	public synchronized boolean removePersistentRequest(PersistentRequestItem persistentRequest) throws Exception {
		PreparedStatement pstmt = null;
		logger.info("Try to remove the request from the database " + persistentRequest.toString());
		int count = 0;
		try {
			pstmt = getConnection().prepareStatement(STATEMENT_FOR_DELETE_OCTOPUS_TASK);
			pstmt.setInt(1, persistentRequest.getPk());
			count = pstmt.executeUpdate();
			pstmt.close();
		} catch (SQLException sqle) {
			logger.error("Trying to remove the request from the database fails: ", sqle);
			throw sqle;
		} catch (Exception e) {
			throw e;
		}
		logger.info("The request was successful removed from the database");
		if (count == 0)
			return false;
		else
			return true;
	}
	
	/**
	 * Removes the record from the database by the given <code>processId</code>.
	 * 
	 * @param processId
	 *            The process Id of {@link PersistentRequestItem} which should
	 *            be removed.
	 * @throws Exception
	 */
	public synchronized void removePersistentRequestByProcessId(String processId) throws Exception {
		PreparedStatement pstmt = null;
		logger.info("Try to remove the request from the database where the request_id is equal " + processId);
		try {
			pstmt = getConnection().prepareStatement(STATEMENT_FOR_DELETE_OCTOPUS_TASK_BY_PROCESS_ID);
			pstmt.setString(1, processId);
			pstmt.executeUpdate();
			pstmt.close();
		} catch (SQLException sqle) {
			logger.error("Trying to remove the request from the database fails: ", sqle);
			throw sqle;
		} catch (Exception e) {
			throw e;
		}
		logger.info("The request was successful removed from the database");
	}
	
	/**
	 * Fetchs all rows of the table 'octopustask' from the database and returns
	 * the result as a list with {@link PersistentRequestItem}'s
	 * 
	 * @return a list with the {@link PersistentRequestItem}'s
	 * @throws Exception
	 */
	public List fetchAllPersistentRequests() throws Exception {
		List result = new ArrayList();
		Statement stmt = null;
		ResultSet rs = null;
		PersistentRequestItem persistentRequest = null;
		logger.info("Load all request from the database.");
		try {
			stmt = getConnection().createStatement();
			rs = stmt.executeQuery(STATEMENT_SELECT_ALL_OCTOPUS_TASKS);
			
			while (rs.next()) {
				persistentRequest = new PersistentRequestItem();
				persistentRequest.setPk(rs.getInt(1));
				persistentRequest.setProcessId(rs.getString(2));
				persistentRequest.setRequestId(rs.getString(3));
				persistentRequest.setModuleName(rs.getString(4));
				persistentRequest.setTaskName(rs.getString(5));
				persistentRequest.setRequestData(rs.getString(6));
				persistentRequest.setCallBackUri(rs.getString(7));
				persistentRequest.setStatus(rs.getInt(8));
				persistentRequest.setCreationDate(rs.getTimestamp(9));
				persistentRequest.setUpdateDate(rs.getTimestamp(10));
				result.add(persistentRequest);
			}
		} catch (SQLException sqle) {
			logger.error("An exception occurs during loading the request from the database: ", sqle);
			throw sqle;
		} catch (Exception e) {
			throw e;
		} finally {
			if (rs != null)
				rs.close();
			if (stmt != null)
				stmt.close();
		}
		logger.info("All request are successful loaded from the database");
		return result;
	}
	
	/**
	 * Returns the maximum value of the id + 1.
	 * 
	 * @return the next id.
	 * @throws Exception
	 */
	private int getNextId() throws Exception {
		String nextId = null;
		ResultSet rs = null;
		try {
			rs = getConnection().createStatement().executeQuery(STATEMENT_SELECT_MAX_PK);
			while (rs.next())
				nextId = rs.getString(1);
		} catch (SQLException sqle) {
			logger.error("Exception during generating the next id: " + sqle);
			throw sqle;
		} catch (Exception e) {
			throw e;
		}
		if (nextId == null || nextId.equals(""))
			return 1;
		else
			return Integer.valueOf(nextId).intValue() + 1;
	}
	
	/**
	 * Initialize the database. The tables which are already availible in the
	 * database will be ignored. Whether the record is already availible, will
	 * be not verified.
	 * 
	 * @param fileName
	 *            The name of the file, which contains the sql querys.
	 * @throws Exception
	 */
	public void initDB(String fileName) throws Exception {
		String sqlScript = null;
		BufferedInputStream inputstream = null;
		ByteArrayOutputStream outputstream = null;
		try {
			inputstream = new BufferedInputStream(DBAccessManager.class.getResourceAsStream("/" + fileName));
			outputstream = new ByteArrayOutputStream(1024);
			byte[] buffer = new byte[4096];
			int len;
			
			if (inputstream == null) {
				logger.error("Could not open file: " + fileName);
				throw new Exception("Could not read the SQL Script from ressource: " + fileName);
			}
			
			while ((len = inputstream.read(buffer)) > 0) {
				outputstream.write(buffer, 0, len);
			}
			sqlScript = outputstream.toString();
		} finally {
			try {
				if (outputstream != null)
					outputstream.close();
			} catch (IOException ioe) {
				logger.warn("Could not close the output stream", ioe);
			}
			try {
				if (inputstream != null)
					inputstream.close();
			} catch (IOException ioe) {
				logger.warn("Could not close the input stream", ioe);
			}
		}
		String[] updateQuerys = sqlScript.split(";");
		Statement stmt = null;
		
		for (int i = 0; i < updateQuerys.length; i++) {
			if (!updateQuerys[i].trim().equals("")) {
				stmt = getConnection().createStatement();
				String updateQuery = updateQuerys[i].trim();
				if (updateQuery.toUpperCase().startsWith(CREATE_TABLE)) {
					if (!tableExists(updateQuery)) {// check that the table does
													// not exist
						try {
							stmt.executeUpdate(updateQuery);
						} catch (SQLException sqle) {
							logger.error("Could not create the table by executing the following query: " + updateQuery, sqle);
							throw sqle;
						} finally {
							// close the satement
							stmt.close();
						}
					}
				} else {
					try {
						stmt.executeUpdate(updateQuerys[i].trim());
					} catch (SQLException sqle) {
						logger.error("The SQLException occurs during executing the following query: " + updateQuery, sqle);
						throw sqle;
					} finally {
						// close the satement
						stmt.close();
					}
				}
			}
		}
	}
	
	/**
	 * Check whether the table already exists in the database.
	 * 
	 * @param query
	 *            the query string which contains the table name
	 * @throws Exception
	 */
	private boolean tableExists(String query) throws Exception {
		String tableName = null;
		Statement stmt = null;
		ResultSet rs = null;
		if (query == null)
			return false;
		tableName = query.substring((CREATE_TABLE + ITEM_SEPARATOR).length(), query.indexOf(ITEM_SEPARATOR + "("));
		if (tableName == null)
			return false;
		try {
			stmt = getConnection().createStatement();
			rs = stmt.executeQuery(SELECT_TABLE_EXISTS + tableName);
			rs.next();
			logger.info("Trying to create the: " + tableName + ". But this table already exists in the database. The table will be ignored.");
			return true; // the table already exists
		} catch (SQLException sqle) {
			// the table does not exist
			logger.info("Trying to create the: " + tableName + ". The database does not contain this table. The table will be created.");
			return false;
		} finally {
			if (stmt != null)
				stmt.close();
		}
	}
	
	/**
	 * Returns the connection to the database. When the connection is null or
	 * the connection is closed, so first connects to the database.
	 * 
	 * @return The connection to the database
	 * @throws Exception
	 */
	public Connection getConnection() throws Exception {
		if (connection == null || connection.isClosed())
			connectToDB();
		return connection;
	}
	
	/**
	 * Loads the driver class and connects to the database.
	 * 
	 * @throws Exception
	 */
	private void connectToDB() throws Exception {
		try {
			logger.info("Loading database driver: " + jdbcDriver);
			Class.forName(jdbcDriver).newInstance();
			logger.info("Database driver: " + jdbcDriver + " successfully loaded");
		} catch (Exception e) {
			logger.error("Could not find the driver class :" + jdbcDriver, e);
			throw e;
		}
		
		try {
			this.connection = DriverManager.getConnection(jdbcConnectionString, jdbcUsername, jdbcPassword);
		} catch (SQLException sqle) {
			logger.error("Try to get connection to the database with the jdbcConnectionString: " + jdbcConnectionString + " jdbcUsername: " + jdbcUsername, sqle);
			throw sqle;
		}
	}
	
}
