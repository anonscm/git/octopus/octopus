/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */
package de.tarent.octopus.config;

/**
 * Klasse zur Kapselung der Informaionen zu einem ContentWorker.
 * 
 * @see de.tarent.octopus.content.ContentWorker
 * @author <a href="mailto:mancke@mancke-software.de">Sebastian Mancke</a>,
 *         <b>tarent GmbH</b>
 */
public class ContentWorkerDeclaration {
	
	String workerName;
	String implementationSource;
	String factory;
	boolean singletonInstantiation = true;
	
	
	/**
	 * Gibt an ob von dem Worker immer neue Instanzen erzeugt werden sollen,
	 * oder immer die gleiche verwendet wird. Default ist singletonInstantiation =
	 * true
	 */
	public boolean isSingletonInstantiation() {
		return singletonInstantiation;
	}
	
	public void setSingletonInstantiation(final boolean newSingletonInstantiation) {
		this.singletonInstantiation = newSingletonInstantiation;
	}
	
	
	/**
	 * Liefert die Factory, von der dieser Worker erzeugt werden kann.
	 * 
	 * @return Voll qualifizierter Klassenname der Factory
	 */
	public String getFactory() {
		return factory;
	}
	
	public void setFactory(final String newFactory) {
		this.factory = newFactory;
	}
	
	
	/**
	 * Liefert die Factoryspezifische Quelle des Workers. Dies kann z.B. der
	 * Klassenname oder Scriptname des Workers sein.
	 */
	public String getImplementationSource() {
		return implementationSource;
	}
	
	public void setImplementationSource(final String newImplementationSource) {
		this.implementationSource = newImplementationSource;
	}
	
	

	/**
	 * Eindeutiger Bezeichner, unter dem den Worker in dem Modul verfügbar ist.
	 */
	public String getWorkerName() {
		return workerName;
	}
	
	public void setWorkerName(final String newWorkerName) {
		this.workerName = newWorkerName;
	}
	
}
