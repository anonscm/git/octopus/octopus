/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.octopus.request.servlet;

import java.util.Enumeration;

import de.tarent.octopus.request.OctopusSession;
import javax.servlet.http.HttpSession;

/**
 * Kapselt das Sessionobjekt des Servletkontainers um eine eventuelle spätere
 * Umstellung leichter zu machen und eine Unabhängigkeit von der Umgebung zu
 * ereichen. <br>
 * <br>
 * Zur Zeit werden keine weiteren Methoden als die der
 * javax.servlet.http.HttpSession bereit gestellt.
 * 
 * @author <a href="mailto:mancke@mancke-software.de">Sebastian Mancke</a>,
 *         <b>tarent GmbH</b>
 */
public class OctopusServletSession implements OctopusSession {
	protected HttpSession session;
	
	public HttpSession getHttpSession() {
		return session;
	}
	
	public OctopusServletSession(HttpSession session) {
		this.session = session;
	}
	
	public Object getAttribute(String name) {
		return session.getAttribute(name);
	}
	
	public Enumeration getAttributeNames() {
		return session.getAttributeNames();
	}
	
	public long getCreationTime() {
		return session.getCreationTime();
	}
	
	public String getId() {
		return session.getId();
	}
	
	public long getLastAccessedTime() {
		return session.getLastAccessedTime();
	}
	
	public int getMaxInactiveInterval() {
		return session.getMaxInactiveInterval();
	}
	
	public void invalidate() {
		session.invalidate();
	}
	
	public boolean isNew() {
		return session.isNew();
	}
	
	public void removeAttribute(java.lang.String name) {
		session.removeAttribute(name);
	}
	
	public void setAttribute(String name, Object value) {
		session.setAttribute(name, value);
	}
	
	public void setMaxInactiveInterval(int interval) {
		session.setMaxInactiveInterval(interval);
	}
}
