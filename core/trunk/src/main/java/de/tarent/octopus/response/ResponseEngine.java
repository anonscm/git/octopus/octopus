/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.octopus.response;

import de.tarent.octopus.config.ModuleConfig;
import de.tarent.octopus.request.OctopusResponse;
import de.tarent.octopus.server.OctopusContext;

/**
 * This interface specify methods for all response engines.
 * 
 * @author Hendrik Helwich, tarent GmbH
 * @author Alex Maier, tarent GmbH
 * @author Christoph Jerolimov, tarent GmbH
 */
public interface ResponseEngine {
	/**
	 * Return the default name of this response engine.
	 * 
	 * @return
	 */
	public String getDefaultName();
	
	/**
	 * Return the default mime type of this response engine.
	 * 
	 * @return
	 */
	public String getDefaultContentType();
	
	/**
	 * Return the encoding parameter of this response engine.
	 * 
	 * @return
	 */
	public String getEncodingParameter();
	
	/**
	 * Return the content type parameter of this respones engine.
	 * 
	 * @return
	 */
	public String getContentTypeParameter();
	
	/**
	 * Return the output parameter of this response engine.
	 * 
	 * @return
	 */
	public String getOutputParameter();
	
	/**
	 * Init the response engine for one module.
	 * 
	 * @param moduleConfig
	 */
	public void init(ModuleConfig moduleConfig);
	
	/**
	 * This method will be invoked before one of the response or callback method
	 * are called to set the encoding.
	 */
	public void performEncoding(ResponseDescription desc, OctopusContext octopusContext, OctopusResponse octopusResponse);
	
	/**
	 * This method will be invoked before one of the response or callback method
	 * are called to set the content type.
	 */
	public void performContentType(ResponseDescription desc, OctopusContext octopusContext, OctopusResponse octopusResponse);
	
	/**
	 * Send an empty response, is necessary for using the asynchron
	 * communications.
	 * 
	 * @param desc
	 *            The {@link ResponseDescription}
	 * @param octopusContext
	 *            The {@link OctopusContext} which includes the response
	 *            parameters and the configuration.
	 * @param octopusResponse
	 *            The {@link OctopusResponse} for returning the data.
	 * @throws ResponseProcessingException
	 */
	public void sendEmptyResponse(ResponseDescription desc, OctopusContext octopusContext, OctopusResponse octopusResponse) throws ResponseProcessingException;
	
	/**
	 * Send a normal response.
	 * 
	 * @param desc
	 *            The {@link ResponseDescription}
	 * @param octopusContext
	 *            The {@link OctopusContext} which includes the response
	 *            parameters and the configuration.
	 * @param octopusResponse
	 *            The {@link OctopusResponse} for returning the data.
	 * @throws ResponseProcessingException
	 */
	public void sendNormalResponse(ResponseDescription desc, OctopusContext octopusContext, OctopusResponse octopusResponse) throws ResponseProcessingException;
	
	/**
	 * Send a normal response.
	 * 
	 * @param desc
	 *            The {@link ResponseDescription}
	 * @param octopusContext
	 *            The {@link OctopusContext} which includes the response
	 *            parameters and the configuration.
	 * @param octopusResponse
	 *            The {@link OctopusResponse} for returning the data.
	 * @param message Optional message.
	 * @param exception Optional exception.
	 * @throws ResponseProcessingException
	 */
	public void sendErrorResponse(ResponseDescription desc, OctopusContext octopusContext, OctopusResponse octopusResponse, String message, Exception exception) throws ResponseProcessingException;

	/**
	 * the call back response, is necessary for using the asynchron
	 * communications
	 * 
	 * @param desc
	 *            The {@link ResponseDescription}
	 * @param octopusContext
	 *            The {@link OctopusContext} which includes the response
	 *            parameters and the configuration.
	 * @param octopusResponse
	 *            The {@link OctopusResponse} for returning the data.
	 * @throws ResponseProcessingException
	 */
	public void sendEmptyCallback(ResponseDescription desc, OctopusContext octopusContext, OctopusResponse octopusResponse) throws ResponseProcessingException;
	
	/**
	 * the call back response, is necessary for using the asynchron
	 * communications
	 * 
	 * @param desc
	 *            The {@link ResponseDescription}
	 * @param octopusContext
	 *            The {@link OctopusContext} which includes the response
	 *            parameters and the configuration.
	 * @param octopusResponse
	 *            The {@link OctopusResponse} for returning the data.
	 * @throws ResponseProcessingException
	 */
	public void sendNormalCallback(ResponseDescription desc, OctopusContext octopusContext, OctopusResponse octopusResponse) throws ResponseProcessingException;
	
	/**
	 * the call back response, is necessary for using the asynchron
	 * communications
	 * 
	 * @param desc
	 *            The {@link ResponseDescription}
	 * @param octopusContext
	 *            The {@link OctopusContext} which includes the response
	 *            parameters and the configuration.
	 * @param octopusResponse
	 *            The {@link OctopusResponse} for returning the data.
	 * @param message Optional message.
	 * @param exception Optional exception.
	 * @throws ResponseProcessingException
	 */
	public void sendErrorCallback(ResponseDescription desc, OctopusContext octopusContext, OctopusResponse octopusResponse, String message, Exception exception) throws ResponseProcessingException;
}
