/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */
package de.tarent.octopus.server;

/**
 * A Closeable is a Resource, which may be closed if not needed any longer. This
 * interace ist semanticaly the same as {@link java.io.Closeable} introduced
 * with JDK 1.5. <br>
 * <br>
 * Within the octopus, this interface may be used to safely close resources
 * after the processing of a request. This is done by a List of Closeable-Object
 * within the OctopusContext. After the generation of the octopus response, the
 * close method is called for each object in this list. This will be done even
 * if the request processing will throw an exception. Use the
 * {@link OctopusContext#addCleanupCode(Runnable runnable)} to add an Object to
 * the list of closeable objects.
 * 
 * @author <a href="mailto:sebastian@tarent.de">Sebastian Mancke</a>, <b>tarent
 *         GmbH</b>
 * @version 1.0
 */
public interface Closeable {
	
	/**
	 * Closes the resource.
	 * 
	 * @throws Exception
	 *             This method may throw any exception, but should not rely on
	 *             an proper handling by the calling code.
	 */
	public void close();
}
