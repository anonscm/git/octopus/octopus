/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.octopus.content;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;

import de.tarent.octopus.config.ModuleConfig;
import de.tarent.octopus.logging.LogFactory;
import de.tarent.octopus.request.OctopusRequest;
import de.tarent.octopus.server.OctopusContext;
import de.tarent.octopus.server.OctopusContextImpl;

/**
 * Refection Worker
 * 
 * Deprecated: Die Worker sollen seit Octopus Version 1.2.0 den
 * ReflectedWorkerWrapper verwenden, der normale Java-Klassen als Worker
 * ansprechen kann.
 * 
 * @author Wolfgang Klein
 */
abstract public class ReflectedWorker implements ContentWorker {
	// TODO Logging verbessern und evtl. mit Octopus auf Log4J umstellen
	// TODO Initalisieren der Aktions in die init-Funktion verschieben?
	
	// Konstanten
	private final static Class[] PREPOST_PARAMETER = { OctopusContextImpl.class, String.class };
	
	/**
	 * Sammlung der zur Verfügung stehenden Aktions.
	 * 
	 * Key = Methodenname Value = WorkerAction bzw. ActionDeclarationException
	 */
	private final Map _serviceActions = new HashMap();
	private Method pre = null;
	private Method post = null;
	
	/**
	 * Initalisiert die Aktionen des Workers.
	 */
	public ReflectedWorker() {
		Log logger = LogFactory.getLog(getClass());
		
		try {
			pre = getClass().getMethod("pre", PREPOST_PARAMETER);
		} catch (SecurityException e) {
			logger.error(e.getMessage(), e);
		} catch (NoSuchMethodException e) {
		}
		
		try {
			post = getClass().getMethod("post", PREPOST_PARAMETER);
		} catch (SecurityException e) {
			logger.error(e.getMessage(), e);
		} catch (NoSuchMethodException e) {
		}
		
		Method[] methods = getClass().getMethods();
		for (int i = 0; i < methods.length; i++) {
			Method method = methods[i];
			// Method.Modifier 17 = public final
			// Method.Modifier 25 = public static final
			if (method.getModifiers() == 25) {
				String methodname = method.getName().toUpperCase();
				Class[] paramTypes;
				String[] paramNames;
				boolean[] mandatory;
				String output = null;
				try {
					try {
						Field field = getClass().getField("INPUT_".concat(methodname));
						paramNames = (String[]) field.get(null);
						paramTypes = method.getParameterTypes();
						if (paramNames.length + 1 != paramTypes.length || !paramTypes[0].equals(OctopusContextImpl.class)) {
							throw new ActionDeclarationException("Serverfehler: Für die Action '" + method.getName() + "' im Worker '" + getClass().getName()
									+ "' stimmt die Anzahl der Argumente (der Methode, exkl. OctopusContextImpl) nicht mit dennen des statischen String[] 'INPUT_" + methodname + "' überein.");
						}
					} catch (NoSuchFieldException e) {
						throw new ActionDeclarationException("Serverfehler: Für die Action '" + method.getName() + "' im Worker '" + getClass().getName() + "' muss ein statisches String[] Feld 'INPUT_" + methodname + "' übergeben sein.");
					}
					try {
						Field field = getClass().getField("MANDATORY_".concat(methodname));
						mandatory = (boolean[]) field.get(null);
					} catch (NoSuchFieldException e) {
						throw new ActionDeclarationException("Serverfehler: Für die Action '" + method.getName() + "' im Worker '" + getClass().getName() + "' muss ein statisches boolean[] Feld 'MANDATORY_" + methodname + "' übergeben sein.");
					}
					try {
						Field field = getClass().getField("OUTPUT_".concat(methodname));
						output = (String) field.get(null);
					} catch (NoSuchFieldException e) {
						throw new ActionDeclarationException("Serverfehler: Für die Action '" + method.getName() + "' im Worker '" + getClass().getName() + "' muss ein statisches String Feld 'OUTPUT_" + methodname + "' übergeben sein.");
					}
					_serviceActions.put(method.getName(), new WorkerAction(method, method.getName(), paramNames, paramTypes, mandatory, output));
				} catch (ActionDeclarationException e) {
					logger.error(e.getMessage(), e);
					_serviceActions.put(method.getName(), e);
				} catch (IllegalAccessException e) {
					logger.error(e.getMessage(), e);
					_serviceActions.put(method.getName(), new ActionDeclarationException(e));
				} catch (SecurityException e) {
					logger.error(e.getMessage(), e);
					_serviceActions.put(method.getName(), new ActionDeclarationException(e));
				}
			}
		}
	}
	
	public void init(ModuleConfig config) {
	}
	
	/**
	 * Führt impl. die Aktion aus.
	 * 
	 * Fehler werden als <code>ContentProzessException</code> an den Octopus
	 * weiter gereicht und NICHT geloggt.
	 * 
	 * @return OctopusContent-Status
	 * @throws ContentProzessException
	 */
	public final String doAction(String actionName, OctopusContext octopusContext) throws ContentProzessException {
		try {
			WorkerAction action = getAction(actionName);
			
			// pre
			if (pre != null) {
				pre.invoke(null, new Object[] { octopusContext, action._actionName });
			}
			
			// action
			octopusContext.setContent(action.getOutputField(), action.invoke(octopusContext));
			
			// post
			if (post != null) {
				post.invoke(null, new Object[] { octopusContext, action._actionName });
			}
			
			// return OctopusContent-Status
			return octopusContext.getStatus();
			
		} catch (ActionDeclarationException e) { // WorkerAction#getAction
			throw new ContentProzessException(e);
		} catch (ActionInvocationException e) { // WorkerAction#getAction
			ContentProzessException cpe = new ContentProzessException(e.getMessage());
			cpe.setStackTrace(e.getStackTrace());
			throw cpe;
		} catch (IllegalArgumentException e) { // Method#invoke
			ContentProzessException cpe = new ContentProzessException(e.getMessage());
			cpe.setStackTrace(e.getStackTrace());
			throw cpe;
		} catch (IllegalAccessException e) { // Method#invoke
			ContentProzessException cpe = new ContentProzessException(e.getMessage());
			cpe.setStackTrace(e.getStackTrace());
			throw cpe;
		} catch (InvocationTargetException e) { // Method#invoke
			Throwable cause = e.getCause();
			if (cause instanceof ContentProzessException) {
				throw (ContentProzessException) cause;
			} else {
				throw new ContentProzessException(cause);
			}
		}
	}
	
	public final PortDefinition getWorkerDefinition() {
		PortDefinition definition = new PortDefinition(getClass().getName(), "n/a");
		OperationDefinition operation;
		MessageDefinition message;
		
		Iterator k = _serviceActions.keySet().iterator();
		Iterator v = _serviceActions.values().iterator();
		while (k.hasNext() && v.hasNext()) {
			String key = (String) k.next();
			Object value = v.next();
			if (value instanceof WorkerAction) {
				// Worker-Action-Definition
				WorkerAction action = (WorkerAction) value;
				operation = new OperationDefinition(key, "n/a");
				
				// Input-Definition
				if (action._paramNames.length != 0) {
					message = new MessageDefinition();
					for (int i = 0; i < action._paramNames.length; i++) {
						message.addPart(action._paramNames[i], action._paramTypes[i + 1].getName(), "n/a", action._mandatory[i]);
					}
					operation.setInputMessage(message);
				}
				
				// Output-Definition
				if (action._output != null) {
					message = new MessageDefinition();
					message.addPart(action._output, action._method.getReturnType().getName(), "n/a");
					operation.setOutputMessage(message);
				}
				
				definition.addOperation(operation);
			} else if (value instanceof ActionDeclarationException) {
				operation = new OperationDefinition(key, ((ActionDeclarationException) value).getMessage());
				definition.addOperation(operation);
			}
		}
		return definition;
	}
	
	public String getVersion() {
		return null;
	}
	
	private final WorkerAction getAction(String actionName) throws ActionDeclarationException, ActionInvocationException {
		Object action = _serviceActions.get(actionName);
		if (action instanceof WorkerAction) {
			return (WorkerAction) action;
		} else if (action instanceof ActionDeclarationException) {
			throw (ActionDeclarationException) action;
		} else {
			throw new ActionInvocationException("Anfragefehler: Die angegebene Action '" + actionName + "' ist im Worker '" + getClass().getName() + "' nicht definiert.");
		}
	}
	
	static private final class WorkerAction {
		private final Method _method;
		private final String _actionName;
		private final String[] _paramNames;
		private final Class[] _paramTypes;
		private final boolean[] _mandatory;
		private final String _output;
		
		private WorkerAction(Method method, String actionName, String[] paramNames, Class[] paramTypes, boolean[] mandatory, String output) {
			_method = method;
			_actionName = actionName;
			_paramNames = paramNames;
			_paramTypes = paramTypes;
			_mandatory = mandatory;
			_output = output;
		}
		
		private Object invoke(OctopusContext octopusContext) throws ActionInvocationException, ContentProzessException, IllegalArgumentException, IllegalAccessException, InvocationTargetException {
			int requestType = octopusContext.requestType();
			List params = new ArrayList(_paramTypes.length);
			params.add(octopusContext);
			Class type;
			for (int i = 0; i < _paramNames.length; i++) {
				type = _paramTypes[i + 1];
				// Daten aus OctopusContent und ggf. aus dem Request
				Object param = octopusContext.contentAsObject(_paramNames[i]);
				if (param == null) {
					param = octopusContext.requestAsObject(_paramNames[i]);
				}
				
				switch (requestType) {
				/**
				 * RequestType 'WEB'
				 *  - Boolean wird bei null explizit false. - Parameter muss
				 * wenn 'mandatory' wahr ist angegeben sein. - Wenn der
				 * Parameter null ist wird null übergeben. - Integer, Long und
				 * Double können konvertiert werden.
				 */
				case OctopusRequest.REQUEST_TYPE_WEB:
					if (type.equals(Boolean.class)) {
						params.add((param != null) ? Boolean.valueOf(param.toString()) : Boolean.FALSE);
					} else if (_mandatory[i] && param == null) {
						throw new ActionInvocationException("Anfragefehler: Der Parameter '" + _paramNames[i] + "' muss übergeben werden. (" + _method.getDeclaringClass().getName() + "#" + _actionName + ")");
					} else if (param == null) {
						params.add(null);
					} else if (type.isInstance(param)) {
						params.add(param);
					} else if (type.equals(Integer.class)) {
						try {
							params.add(Integer.valueOf(param.toString()));
						} catch (NumberFormatException e) {
							if (_mandatory[i]) {
								throw new ActionInvocationException("Anfragefehler: Der Parameter '" + _paramNames[i] + "' muss vom Typ '" + type.getName() + "' sein (tatsächlicher Typ: '" + param.getClass().getName() + "', Wert: '" + param.toString()
										+ "'). (" + _method.getDeclaringClass().getName() + "#" + _actionName + ")");
							} else {
								params.add(null);
							}
						}
					} else if (type.equals(Long.class)) {
						try {
							params.add(Long.valueOf(param.toString()));
						} catch (NumberFormatException e) {
							if (_mandatory[i]) {
								throw new ActionInvocationException("Anfragefehler: Der Parameter '" + _paramNames[i] + "' (Wert: '" + param.toString() + "') konnte nicht nach '" + type.getName() + "' gecastet werden. ("
										+ _method.getDeclaringClass().getName() + "#" + _actionName + ")");
							} else {
								params.add(null);
							}
						}
					} else if (type.equals(Double.class)) {
						try {
							params.add(Double.valueOf(param.toString()));
						} catch (NumberFormatException e) {
							if (_mandatory[i]) {
								throw new ActionInvocationException("Anfragefehler: Der Parameter '" + _paramNames[i] + "' (Wert: '" + param.toString() + "') konnte nicht nach '" + type.getName() + "' gecastet werden. ("
										+ _method.getDeclaringClass().getName() + "#" + _actionName + ")");
							} else {
								params.add(null);
							}
						}
					} else if (type.equals(List.class) && param instanceof Object[]) {
						params.add(Arrays.asList((Object[]) param));
					} else if (type.equals(List.class)) {
						params.add(Collections.singletonList(param));
					} else {
						throw new ActionInvocationException("Anfragefehler: Der Parameter '" + _paramNames[i] + "' muss vom Typ '" + type.getName() + "' sein (tatsächlicher Typ: '" + param.getClass().getName() + "', Wert: '" + param.toString() + "'). ("
								+ _method.getDeclaringClass().getName() + "#" + _actionName + ")");
					}
					break;
				/**
				 * RequestType 'default'
				 *  - Datentyp und Parametertyp müssen übereinstimmen. -
				 * Object-Arrays werden, wenn das Ziel eine Liste ist
				 * automatisch umgewandelt (z.B. bei Vectoren) - Wenn
				 * 'mandatory' nicht wahr ist, kann <code>null</code>
				 * übergeben werden.
				 */
				default:
					if (_mandatory[i] && param == null) {
						throw new ActionInvocationException("Anfragefehler: Der Parameter '" + _paramNames[i] + "' muss übergeben werden. (" + _method.getDeclaringClass().getName() + "#" + _actionName + ")");
					} else if (param == null) {
						params.add(null);
					} else if (type.isInstance(param)) {
						params.add(param);
					} else if (type.equals(List.class) && param instanceof Object[]) {
						params.add(Arrays.asList((Object[]) param));
					} else {
						throw new ActionInvocationException("Anfragefehler: Der Parameter '" + _paramNames[i] + "' muss vom Typ '" + type.getName() + "' sein. (tatsächlicher Typ: '" + param.getClass().getName() + "', Wert: '" + param.toString() + "'). ("
								+ _method.getDeclaringClass().getName() + "#" + _actionName + ")");
					}
					break;
				}
			}
			
			return _method.invoke(null, params.toArray());
		}
		
		private String getOutputField() {
			return _output;
		}
	}
	
	
	// Diese Exceptions sollen eigentlich entfernt werden.
	// Sind aber aus kompatibilitätsgründen zu alten Workern noch drinn.
	
	/**
	 * @depracated Die nicht eingebetteten Varianten dieser Exceptions sollen
	 *             verwendet werden.
	 */
	static public class ActionDeclarationException extends Exception {
		/**
		 * serialVersionUID = 4871294463116415598L
		 */
		private static final long serialVersionUID = 4871294463116415598L;
		
		public ActionDeclarationException(String msg) {
			super(msg);
		}
		
		public ActionDeclarationException(Throwable t) {
			super(t);
		}
	}
	
	/**
	 * @depracated Die nicht eingebetteten Varianten dieser Exceptions sollen
	 *             verwendet werden.
	 */
	static public class ActionInvocationException extends ContentProzessException {
		/**
		 * serialVersionUID = 493630509826405394L
		 */
		private static final long serialVersionUID = 493630509826405394L;
		
		public ActionInvocationException(String msg) {
			super(msg);
		}
		
		public ActionInvocationException(Throwable t) {
			super(t);
		}
	}
	

}
