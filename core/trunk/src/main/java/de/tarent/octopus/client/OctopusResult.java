/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.octopus.client;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.IOException;
import java.util.Iterator;

/**
 * Aufruf eines Task des Octopus.
 * 
 * @author <a href="mailto:sebastian@tarent.de">Sebastian Mancke</a>, <b>tarent
 *         GmbH</b>
 */
public interface OctopusResult {
	
	public boolean hasStreamContent();
	
	public InputStream getContent();
	
	public String getContentType();
	
	public void writeContent(OutputStream to) throws IOException;
	
	
	public boolean hasMoreData();
	
	public Iterator getDataKeys();
	
	public Object getData(String key);
	
	public Object nextData();
	
	// /**
	// * @throws java.lang.ClassCastException
	// */
	// public Object nextDataAs(Class type);
	
	/**
	 * @throws java.lang.ClassCastException
	 */
	public String nextDataAsString();
	
	// /**
	// * @throws java.lang.ClassCastException
	// */
	// public int nextDataAsInt();
	
	// /**
	// * @throws java.lang.ClassCastException
	// */
	// public float nextDataAsFloat();
	
	// /**
	// * @throws java.lang.ClassCastException
	// */
	// public byte[] nextDataAsByteArray();
}
