/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */
package de.tarent.octopus.util;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;

import com.thoughtworks.xstream.XStream;

/**
 * @author Alex Maier, tarent GmbH
 * 
 */
public class XmlSerializer {
	
	public static final String CHAR_SET_ISO_8859_1 = "ISO-8859-1";
	public static final String CHAR_SET_UTF8 = "UTF-8";
	
	
	/**
	 * Serialize the given Object <code>o</code> to XML using the given
	 * charset encoding <code>charset</code>
	 * 
	 * @param o
	 *            Object which should be serilized
	 * @param charset
	 *            Charset encoding, which should be used by converting the bytes
	 *            to characters
	 * 
	 * @return the XML string of the serialized object
	 * @throws UnsupportedEncodingException
	 */
	public static String objectToXml(Object o, String charset) throws UnsupportedEncodingException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		// XStream xstream = new XStream(new DomDriver());
		XStream xstream = new XStream();
		xstream.alias(o.getClass().getName(), o.getClass());
		xstream.toXML(o, baos);
		
		return baos.toString(charset);
	}
	
	/**
	 * Serialize the given Object <code>o</code> to XML using the default
	 * charset encoding {@link #CHAR_SET_UTF8}
	 * 
	 * @param o
	 *            Object which should be serilized
	 * @return the XML string of the serialized object
	 * @throws UnsupportedEncodingException
	 */
	public static String objectToXml(Object o) throws UnsupportedEncodingException {
		return objectToXml(o, CHAR_SET_UTF8);
	}
}
