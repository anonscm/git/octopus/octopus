/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.octopus.response.soap;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.zip.GZIPInputStream;

import javax.servlet.http.HttpServletRequest;

import org.apache.axis.AxisEngine;
import org.apache.axis.AxisFault;
import org.apache.axis.ConfigurationException;
import org.apache.axis.Constants;
import org.apache.axis.EngineConfiguration;
import org.apache.axis.Message;
import org.apache.axis.MessageContext;
import org.apache.axis.configuration.FileProvider;
import org.apache.axis.encoding.DefaultSOAPEncodingTypeMappingImpl;
import org.apache.axis.encoding.TypeMapping;
import org.apache.axis.encoding.TypeMappingRegistry;
import org.apache.axis.encoding.ser.SimpleDeserializerFactory;
import org.apache.axis.encoding.ser.SimpleSerializerFactory;
import org.apache.axis.message.MessageElement;
import org.apache.axis.message.RPCElement;
import org.apache.axis.message.RPCParam;
import org.apache.axis.message.SOAPEnvelope;
import org.apache.axis.message.SOAPHeaderElement;
import org.apache.axis.server.AxisServer;
import org.apache.commons.logging.Log;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import de.tarent.octopus.config.ModuleConfig;
import de.tarent.octopus.logging.LogFactory;
import de.tarent.octopus.request.OctopusRequest;
import de.tarent.octopus.resource.Resources;

/**
 * Bereitstellung und Kapselung von SOAP Funktionalität
 * 
 * @author <a href="mailto:mancke@mancke-software.de">Sebastian Mancke</a>,
 *         <b>tarent GmbH</b>
 */
public class SoapEngine {
	private static Log logger = LogFactory.getLog(SoapEngine.class);
	public static final String NAMESPACE_URI = "http://schemas.tarent.de/";
	
	public static AxisEngine engine;
	
	public SoapEngine(ModuleConfig moduleConfig) {
		final File axisConfigFile = new File(moduleConfig.getRealPath(), "axis-config.wsdd");
		
		if (axisConfigFile.exists()) {
			logger.info(Resources.getInstance().get("SOAPENGINE_LOG_USING_AXIS_CONFIGURATION_FILE", axisConfigFile));
			EngineConfiguration engineConfiguration = new FileProvider(axisConfigFile.getAbsolutePath()) {
				public TypeMappingRegistry getTypeMappingRegistry() {
					try {
						return super.getTypeMappingRegistry();
					} catch (ConfigurationException e) {
						// log the configuration error
						logger.error(Resources.getInstance().get(
								"SOAPENGINE_LOG_AXIS_CONFIGURATION_ERROR",
								axisConfigFile.getAbsolutePath()), e);
					}
					return null;
				}
			};
			engine = new AxisServer(engineConfiguration);
		} else {
			logger.info(Resources.getInstance().get("SOAPENGINE_LOG_USING_AXIS_DEFAULT_CONFIGURATION"));
			engine = new AxisServer();
			registerTypes(engine.getTypeMappingRegistry(), moduleConfig);
		}
	}
	
	/**
	 * Registrieren der gewollten Default Type Mappings, wenn keine Axis
	 * Konfiguration angegeben wurde
	 * 
	 * @deprecated not used since engine configuration is read from an engine
	 *             configuration file
	 */
	protected void registerTypes(TypeMappingRegistry reg, ModuleConfig moduleConfig) {
		TypeMapping mapping = DefaultSOAPEncodingTypeMappingImpl.createWithDelegate();
		mapping.register(StringBuffer.class, Constants.XSD_STRING, new SimpleSerializerFactory(StringBuffer.class, Constants.XSD_STRING), new SimpleDeserializerFactory(String.class, Constants.XSD_STRING));
		
		// mapping.setSupportedEncodings(Constants.URIS_SOAP_ENC);
		for (int i = 0; i < Constants.URIS_SOAP_ENC.length; i++) {
			reg.register(Constants.URIS_SOAP_ENC[i], mapping);
		}
		reg.registerDefault(mapping);
	}
	
	

	/**
	 * Diese Methode analysiert eine SOAP-Anfrage.
	 * 
	 * @param inStream
	 *            Die Anfrage
	 * @param requestType
	 *            der Anfragetyp
	 * @param requestID
	 *            die Anfrage-ID
	 * @param pathInfo
	 *            the path info {@link HttpServletRequest#getPathInfo()}
	 * @return ein generiertes Anfrage-Objekt
	 * @throws SoapException
	 */
	public OctopusRequest[] readSoapRequests(InputStream inStream, int requestType, String requestID, String moduleName) throws SoapException {
		logger.trace(SoapEngine.class.getName() + " readSoapRequests " + new Object[] { inStream, new Integer(requestType), requestID });
		
		if (inStream != null) {
			List taskList = new ArrayList();
			List paramsList = new ArrayList();
			List headerList = new ArrayList();
			analyseSoapRequest(inStream, headerList, taskList, paramsList);
			OctopusRequest[] requests = new OctopusRequest[paramsList.size()];
			Iterator itTasks = taskList.iterator();
			Iterator itParams = paramsList.iterator();
			for (int i = 0; itParams.hasNext(); i++) {
				Map params = (Map) itParams.next();
				OctopusRequest octRequest = new OctopusRequest(requestID);
				octRequest.setRequestType(requestType);
				octRequest.setRequestParameters(params);
				octRequest.setModule(moduleName);
				octRequest.setTask(itTasks.next().toString());
				if (!headerList.isEmpty())
					octRequest.setHeaders(new SoapRequestHeaders(headerList));
				requests[i] = octRequest;
			}
			return requests;
		} else
			return new OctopusRequest[0];
	}
	
	
	/**
	 * Interpretiert die Eingabe als SOAP Message im RPC Style
	 * 
	 * Alle Bestandteile der Aufrufe werden in Maps zurückgegeben. Dabei wird
	 * der Methodenname unter dem Key "task" abgelegt, der Namespace der Methode
	 * unter "taskNamespaceURI" und die Übergabeparameter unter ihren Namen. Sie
	 * können auch Maps und Vektoren sein.
	 */
	public void analyseSoapRequest(InputStream message, List headers, List tasks, List params) throws SoapException {
		if (headers == null)
			headers = new ArrayList();
		if (tasks == null)
			tasks = new ArrayList();
		if (params == null)
			params = new ArrayList();
		
		Message requestMsg = new Message(message, false);
		MessageContext msgContext = createMessageContext(engine);
		requestMsg.setMessageContext(msgContext);
		
		SOAPEnvelope env = null;
		List bodyElements = null;
		Iterator itElements = null;
		List headerElements = null;
		try {
			env = requestMsg.getSOAPEnvelope();
			bodyElements = env.getBodyElements();
			headerElements = env.getHeaders();
		} catch (AxisFault e) {
			logger.warn(Resources.getInstance().get("SOAPENGINE_LOG_SOAP_MSG_ERROR"), e);
			throw new SoapException(e);
		}
		if (bodyElements != null)
			itElements = bodyElements.iterator();
		while (itElements != null && itElements.hasNext())
			try {
				Object element = itElements.next();
				if (!(element instanceof RPCElement))
					continue;
				RPCElement rpcElem = (RPCElement) element;
				tasks.add(rpcElem.getMethodName());
				params.add(bodyPartToMap(rpcElem));
			} catch (SAXException ex) {
				logger.warn(Resources.getInstance().get("SOAPENGINE_LOG_SAX_BODY_ERROR"), ex);
				throw new SoapException(ex);
			} catch (ClassCastException cce) {
				logger.warn(Resources.getInstance().get("SOAPENGINE_LOG_CAST_BODY_ERROR"), cce);
				throw new SoapException(cce);
			}
		logger.trace("BODIES: " + params);
		if (headerElements != null)
			itElements = headerElements.iterator();
		while (itElements != null && itElements.hasNext())
			try {
				SOAPHeaderElement hdrElem = (SOAPHeaderElement) itElements.next();
				headers.add(headerPartToMap(hdrElem));
			} catch (SAXException ex) {
				logger.warn(Resources.getInstance().get("SOAPENGINE_LOG_SAX_HEADER_ERROR"), ex);
				throw new SoapException(ex);
			} catch (ClassCastException cce) {
				logger.warn(Resources.getInstance().get("SOAPENGINE_LOG_CAST_HEADER_ERROR"), cce);
				throw new SoapException(cce);
			}
		logger.trace("HEADERS: " + headers);
	}
	
	/**
	 * Diese Methode macht aus einem RPC-Element eine Map mit Parametern eines
	 * Octopus-Requests.
	 * 
	 * @param bodyPart
	 *            Body-RPC-Element
	 * @return Map der Parameter, des Moduls und des Tasks im RPC-Element
	 * @throws SAXException
	 */
	private Map bodyPartToMap(RPCElement bodyPart) throws SAXException {
		Map request = new TreeMap();
		List params = bodyPart.getParams();
		for (int i = 0; i < params.size(); i++) {
			RPCParam rpcParam = (RPCParam) params.get(i);
			Object value = replaceArrayWithList(rpcParam.getObjectValue());
			request.put(rpcParam.getName(), value);
		}
		return request;
	}
	
	/**
	 * Traversiert die den Map-List-Baum und ersetzt alle Vorkommen von Array
	 * durch Listen. <br>
	 * Vorsicht: Es werden nur Maps, Listen und Arrays traversiert. Wenn ein
	 * Array in einem anderen Datencontainer enthalten ist, wird es nicht
	 * gefunden <br>
	 * TODO: Besser wäre natürlich ein direktes Deserialisieren als List durch
	 * Axis (derzeit nicht unterstützt).
	 */
	protected Object replaceArrayWithList(Object o) {
		Object out = o;
		if (out instanceof Object[]) {
			out = Arrays.asList((Object[]) out);
		}
		
		if (out instanceof List) {
			List list = (List) out;
			for (int i = 0; i < list.size(); i++) {
				Object element = list.get(i);
				Object replacement = replaceArrayWithList(element);
				
				// Hier ist ein echtes == gemeint, kein equals,
				// da nur ausgetauscht werden muss, wenn sich die Objektinstanz
				// wirklich geändert hat.
				if (replacement != element)
					list.set(i, replacement);
			}
		} else if (out instanceof Map) {
			Map map = (Map) out;
			for (Iterator iter = map.entrySet().iterator(); iter.hasNext();) {
				Map.Entry entry = (Map.Entry) iter.next();
				
				Object element = entry.getValue();
				Object replacement = replaceArrayWithList(element);
				
				// Hier ist ein echtes == gemeint, kein equals,
				// da nur ausgetauscht werden muss, wenn sich die Objektinstanz
				// wirklich geändert hat.
				if (replacement != element)
					map.put(entry.getKey(), replacement);
			}
		}
		return out;
	}
	
	/**
	 * This methods returns a map with the header's name as key and a
	 * MessageElement as value. These maps will be put in a list structure and
	 * not directly in a map because multiple headers of the same name may
	 * exists. The load overhead should not be relevant because normaly headers
	 * should only be delivered if needed. If the header has DOM-Elements as
	 * children the will be recursively processed by this method and provided in
	 * a <code>List</code> of Singleton-Maps as well.
	 * 
	 * @param headerPart
	 *            header to process
	 * @return a singleton map with the header's name as key and a
	 *         <code>MessageElement</code> or a <code>List</code> of
	 *         subheaders as value
	 * @throws SAXException
	 */
	private Map headerPartToMap(MessageElement headerPart) throws SAXException {
		// test if child elements are present, pretending no elements follow if
		// the first child is a text node
		if (headerPart.getFirstChild() != null && headerPart.getFirstChild().getNodeType() != Node.TEXT_NODE) {
			List subheaderNodes = headerPart.getChildren();
			List subheaderList = new ArrayList(subheaderNodes.size());
			Iterator iter = subheaderNodes.iterator();
			while (iter.hasNext()) {
				MessageElement subheader = (MessageElement) iter.next();
				subheaderList.add(headerPartToMap(subheader));
			}
			return Collections.singletonMap(headerPart.getName(), subheaderList);
		}
		return Collections.singletonMap(headerPart.getName(), headerPart.getRealElement());
	}
	
	/**
	 * Erstellt einen Context mit Parametern der Nachricht Da unser System die
	 * Informationen, für die der MessageContext ist anderweitig bereit stellt,
	 * müssen wir da nicht viel rein tun.
	 */
	private MessageContext createMessageContext(AxisEngine engine) {
		MessageContext msgContext = new MessageContext(engine);
		
		msgContext.setTransportName("http");
		return msgContext;
	}
	
	/**
	 * Leitet an createMessageContext( AxisEngine engine ) mit engine =
	 * this.engine weiter;
	 */
	public MessageContext createMessageContext() {
		return createMessageContext(engine);
	}
	
	/**
	 * Hängt einen GZIP Filter über den übergebenen Stream
	 */
	public static InputStream addGZIPFilterToInputStream(InputStream inStream) throws java.io.IOException {
		return new GZIPInputStream(inStream);
	}
	
	/**
	 * Soll Später einen PGP Filter über den übergebenen Stream legen. Ist aber
	 * nocht nicht implementiert
	 */
	public static InputStream addPGPFilterToInputStream(InputStream inStream) {
		return inStream;
	}
	
	/**
	 * Liefert den Namespacebezeichner, unter dem das Schema eines Modules
	 * festgelegt ist.
	 * 
	 * Im Moment wird einfach der Modulname an eine bestimmte URL angehängt
	 * 
	 * @param module
	 *            Der Name eines Modules
	 * @return Namespacebezeichner zu dem Modul
	 */
	public String getNamespaceByModuleName(String module) {
		if (module == null || module.equals("") || module.equals("default"))
			throw new IllegalArgumentException("Can not create namespace for module: " + module);
		return NAMESPACE_URI + module;
	}
}
