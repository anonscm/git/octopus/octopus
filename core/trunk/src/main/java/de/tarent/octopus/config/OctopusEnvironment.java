/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.octopus.config;


import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import org.apache.commons.logging.Log;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXParseException;

import de.tarent.octopus.logging.LogFactory;
import de.tarent.octopus.resource.Resources;
import de.tarent.octopus.util.DataFormatException;
import de.tarent.octopus.util.Xml;

/**
 * Ein key=value Container für Einstellungswerte.
 * 
 * Die Keys sollten ein Prefix besitzen, das bezeichet, für welches Modul oder
 * Package diese Einstellungen relevant sind. <br>
 * z.B. "response.templatePath" <br>
 * <br>
 * Wenn eine Einstellung für mehrere Module ist, soll das prefix "global"
 * benutzt werden
 * 
 * @author <a href="mailto:mancke@mancke-software.de">Sebastian Mancke</a>,
 *         <b>tarent GmbH</b>
 */
public class OctopusEnvironment extends HashMap {
	//
	// private statische Variablen
	//	
	/** Logger für diese Klasse */
	private static Log logger = LogFactory.getLog(OctopusEnvironment.class);
	/**
	 * serialVersionUID = -3477717156564940686L
	 */
	private static final long serialVersionUID = -3477717156564940686L;
	
	/**
	 * Map mit Einträgen der LoginManager Konfiguration
	 */
	private Map configLoginManager = new HashMap();
	
	public OctopusEnvironment() throws OctopusConfigException {
		// load the default configurations (is inside of octopus jar)
		parseConfigFile(loadDocument(
				OctopusEnvironment.class.getResourceAsStream("/" +
				DEFAULT_CONFIG_FILE)));
	}
	
	public void parseConfigFile() throws OctopusConfigException {
		String fileName;
		fileName = Resources.getInstance().get("COMMONCONFIG_URL_CONFIG_FILE", get(OctopusEnvironment.KEY_PATHS_ROOT), get(OctopusEnvironment.KEY_PATHS_CONFIG_ROOT), get(OctopusEnvironment.KEY_PATHS_CONFIG_FILE));
		parseConfigFile(loadDocument(fileName));
	}
	
	/**
	 * Returns the customer configuration as Document loadet from given
	 * InputStream inputStream
	 * 
	 * @param inputStream
	 *            the InputStream
	 * @return Document
	 * @throws OctopusConfigException
	 */
	protected Document loadDocument(InputStream inputStream) throws OctopusConfigException {
		Document document = null;
		try {
			logger.debug(Resources.getInstance().get("COMMONCONFIG_LOG_PARSE_START"));
			document = Xml.getParsedDocument(inputStream);
			logger.debug(Resources.getInstance().get("COMMONCONFIG_LOG_PARSE_STOP"));
		} catch (SAXParseException se) {
			logger.error(Resources.getInstance().get("COMMONCONFIG_LOG_PARSE_SAX_EXCEPTION", new Integer(se.getLineNumber()), new Integer(se.getColumnNumber())), se);
			throw new OctopusConfigException(Resources.getInstance().get("COMMONCONFIG_EXC_PARSE_ERROR"), se);
		} catch (Exception e) {
			logger.error(Resources.getInstance().get("COMMONCONFIG_LOG_PARSE_ERROR"), e);
			throw new OctopusConfigException(Resources.getInstance().get("COMMONCONFIG_EXC_PARSE_ERROR"), e);
		}
		return document;
	}
	
	/**
	 * Returns the customer configuration as Document loadet from file filename
	 * 
	 * @param fileName
	 *            the path to the configuration file
	 * @return Document
	 * @throws OctopusConfigException
	 */
	protected Document loadDocument(String fileName) throws OctopusConfigException {
		Document document = null;
		try {
			logger.debug(Resources.getInstance().get("COMMONCONFIG_LOG_CONFIG_FILE", fileName));
			logger.debug(Resources.getInstance().get("COMMONCONFIG_LOG_PARSE_START"));
			document = Xml.getParsedDocument(fileName);
			logger.debug(Resources.getInstance().get("COMMONCONFIG_LOG_PARSE_STOP"));
		} catch (SAXParseException se) {
			logger.error(Resources.getInstance().get("COMMONCONFIG_LOG_PARSE_SAX_EXCEPTION", new Integer(se.getLineNumber()), new Integer(se.getColumnNumber())), se);
			throw new OctopusConfigException(Resources.getInstance().get("COMMONCONFIG_EXC_PARSE_ERROR"), se);
		} catch (Exception e) {
			logger.error(Resources.getInstance().get("COMMONCONFIG_LOG_PARSE_ERROR"), e);
			throw new OctopusConfigException(Resources.getInstance().get("COMMONCONFIG_EXC_PARSE_ERROR"), e);
		}
		return document;
	}
	
	/**
	 * Parsen der Haupt Konfigurations Datei.
	 * 
	 * Wenn Fehler auftreten, werden diese nur durch den Logger protokolliert
	 * und nicht zurück gegeben.
	 */
	protected void parseConfigFile(Document document) throws OctopusConfigException {
		NodeList sections = document.getDocumentElement().getChildNodes();
		// Auslesen der Sections
		for (int i = 0; i < sections.getLength(); i++) {
			Node currNode = sections.item(i);
			if ("application".equals(currNode.getNodeName())) {
				try {
					setAllValues(Xml.getParamMap(currNode));
				} catch (DataFormatException dfe) {
					String msg = Resources.getInstance().get("COMMONCONFIG_LOG_PARSE_APPLICATION_ERROR");
					logger.error(msg, dfe);
					throw new OctopusConfigException(msg, dfe);
				}
			} else if ("loginManager".equals(currNode.getNodeName())) {
				try {
					configLoginManager.putAll(Xml.getParamMap(currNode));
				} catch (DataFormatException dfe) {
					String msg = Resources.getInstance().get("COMMONCONFIG_LOG_PARSE_APPLICATION_ERROR");
					logger.error(msg, dfe);
					throw new OctopusConfigException(msg, dfe);
				}
			}
		}
		overrideValues("common", "/de/tarent/octopus/overrides/common");
		
		// defaultModuleConfig setzen
		if (get(OctopusEnvironment.KEY_DEFAULT_MODULE) == null) {
			String msg = Resources.getInstance().get("COMMONCONFIG_LOG_DEFAULT_ENTRY_MISSING");
			logger.debug(msg);
		}
	}
	
	
	/**
	 * Setzt die Map mit den Einstellungen. Alle vorhandenen Werte werden
	 * überschrieben.
	 * 
	 * @param envMap
	 *            Erwartet eine Map mit Strings als key und Strings und
	 *            Stringarrays als Values
	 */
	public void setAllValues(Map envMap) {
		this.putAll(envMap);
	}
	
	/**
	 * Setzt eine Einstellung. Ein vorhandener Wert wird überschrieben.
	 * 
	 * @param key
	 *            Der Key mit Prefix, z.B. "global.sessionTimeout"
	 * @param value
	 *            Wert
	 * @return Den Wert, der vorher unter diesem Key gespeichert war
	 */
	public Object setValue(String key, String value) {
		return this.put(key, value);
	}
	
	/**
	 * Setzt eine Einstellung. Ein vorhandener Wert wird überschrieben.
	 * 
	 * @param prefix
	 *            Das Prefix, z.B. "global"
	 * @param key
	 *            Der Key ohne Prefix, z.B. "sessionTimeout"
	 * @param value
	 *            Wert
	 * @return Den Wert, der vorher unter diesem Key gespeichert war
	 */
	public Object setValue(String prefix, String key, String value) {
		return this.put(prefix + "." + key, value);
	}
	
	/**
	 * Setzt ein Einstellungsarray. Ein vorhandener Wert wird überschrieben.
	 * 
	 * @param key
	 *            Der Key mit Prefix, z.B. "global.sessionTimeout"
	 * @param values
	 *            Werte als Stringarray
	 * @return Den Wert, der vorher unter diesem Key gespeichert war
	 */
	public Object setValue(String key, String[] values) {
		return this.put(key, values);
	}
	
	/**
	 * Setzt ein Einstellungsarray. Ein vorhandener Wert wird überschrieben.
	 * 
	 * @param prefix
	 *            Das Prefix, z.B. "global"
	 * @param key
	 *            Der Key ohne Prefix, z.B. "sessionTimeout"
	 * @param values
	 *            Werte als Stringarray
	 * @return Den Wert, der vorher unter diesem Key gespeichert war
	 */
	public Object setValue(String prefix, String key, String[] values) {
		return this.put(prefix + "." + key, values);
	}
	
	/**
	 * Gibt einen Wert als String oder String[] zurück.
	 * 
	 * @param key
	 *            Der Key mit Prefix, z.B. "global.sessionTimeout"
	 * @return Ein String oder String[] Objekt
	 */
	public Object getValue(String key) {
		return this.get(key);
	}
	
	/**
	 * Gibt einen Wert als Object zurück.
	 * 
	 * @param key
	 * @return value
	 */
	public Object getValueAsObject(String key) {
		return super.get(key);
	}
	
	/**
	 * Gibt einen Wert als String zurück
	 * 
	 * @param key
	 *            Der Key mit Prefix, z.B. "global.sessionTimeout"
	 * @return Einen String. Wenn ein String[] gespeichert ist, wird das erste
	 *         Element zurück gegeben.
	 */
	public String getValueAsString(String key) {
		Object value = this.get(key);
		if (value instanceof String)
			return (String) value;
		else if (value instanceof String[] && ((String[]) value).length > 0)
			return ((String[]) value)[0];
		else
			return null;
	}
	
	/**
	 * Gibt einen Wert als String zurück Wrapper für getValueAsString
	 * 
	 * @param key
	 *            Der Key mit Prefix, z.B. "global.sessionTimeout"
	 * @return Einen String. Wenn ein String[] gespeichert ist, wird das erste
	 *         Element zurück gegeben.
	 */
	public String get(String key) {
		Object value = super.get(key);
		if (value instanceof String)
			return (String) value;
		else if (value instanceof String[] && ((String[]) value).length > 0)
			return ((String[]) value)[0];
		else
			return null;
	}
	
	/**
	 * Gibt einen Wert als String[] zurück
	 * 
	 * @param key
	 *            Der Key mit Prefix, z.B. "global.sessionTimeout"
	 * @return Einen String. Wenn ein String[] gespeichert ist, wird das erste
	 *         Element zurück gegeben.
	 */
	public String[] getValueAsStringArray(String key) {
		Object value = this.get(key);
		if (value instanceof String)
			return new String[] { (String) value };
		else if (value instanceof String[])
			return (String[]) value;
		else
			return null;
	}
	
	/**
	 * Gibt einen Wert als boolean zurück
	 * 
	 * @param key
	 *            Der Key mit Prefix, z.B. "global.sessionTimeout"
	 * @return true, wenn der String, oder der erste Eintrag im String-Array
	 *         "true" ist, false sonst. Nicht case-Sensitiv.
	 */
	public boolean getValueAsBoolean(String key) {
		Object value = super.get(key);
		String stringValue;
		if (value instanceof String)
			stringValue = (String) value;
		else if (value instanceof String[])
			stringValue = ((String[]) value)[0];
		else
			return false;
		
		return stringValue.toLowerCase().equals("true");
	}
	
	/**
	 * Gibt einen Wert als Zahl zurück
	 * 
	 * @param key
	 *            Der Key mit Prefix, z.B. "global.sessionTimeout"
	 * @return Zahlwert oder 0, wenn es kein gültiger Zahlwert ist.
	 */
	public int getValueAsInt(String key) {
		Object value = super.get(key);
		String stringValue;
		if (value instanceof String)
			stringValue = (String) value;
		else if (value instanceof String[])
			stringValue = ((String[]) value)[0];
		else
			return 0;
		
		try {
			return Integer.parseInt(stringValue);
		} catch (Exception e) {
			return 0;
		}
	}
	
	/**
	 * Gibt einen Wert als Object zurück
	 * 
	 * @param prefix
	 *            Das Prefix, z.B. "global"
	 * @param key
	 *            Der Key ohne Prefix, z.B. "sessionTimeout"
	 * @return Ein String oder String[] Objekt
	 */
	public Object getValue(String prefix, String key) {
		key = prefix + "." + key;
		return this.get(key);
	}
	
	/**
	 * Gibt einen Wert als String zurück
	 * 
	 * @param prefix
	 *            Das Prefix, z.B. "global"
	 * @param key
	 *            Der Key ohne Prefix, z.B. "sessionTimeout"
	 * @return Einen String. Wenn ein String[] gespeichert ist, wird das erste
	 *         Element zurück gegeben.
	 */
	public String getValueAsString(String prefix, String key) {
		key = prefix + "." + key;
		Object value = this.get(key);
		if (value instanceof String)
			return (String) value;
		else if (value instanceof String[] && ((String[]) value).length > 0)
			return ((String[]) value)[0];
		else
			return null;
	}
	
	/**
	 * Gibt einen Wert als String[] zurück
	 * 
	 * @param prefix
	 *            Das Prefix, z.B. "global"
	 * @param key
	 *            Der Key ohne Prefix, z.B. "sessionTimeout"
	 * @return Einen String. Wenn ein String[] gespeichert ist, wird das erste
	 *         Element zurück gegeben.
	 */
	public String[] getValueAsStringArray(String prefix, String key) {
		key = prefix + "." + key;
		Object value = this.get(key);
		if (value instanceof String)
			return new String[] { (String) value };
		else if (value instanceof String[])
			return (String[]) value;
		else
			return null;
	}
	
	/**
	 * Gibt einen Wert als boolean zurück
	 * 
	 * @param prefix
	 *            Das Prefix, z.B. "global"
	 * @param key
	 *            Der Key ohne Prefix, z.B. "sessionTimeout"
	 * @return true, wenn der String, oder der erste Eintrag im String-Array
	 *         "true" ist, false sonst. Nicht case-Sensitiv.
	 */
	public boolean getValueAsBoolean(String prefix, String key) {
		key = prefix + "." + key;
		Object value = this.get(key);
		String stringValue;
		if (value instanceof String)
			stringValue = (String) value;
		else if (value instanceof String[])
			stringValue = ((String[]) value)[0];
		else
			return false;
		
		return stringValue.toLowerCase().equals("true");
	}
	
	/**
	 * Gibt einen Wert als Zahl zurück
	 * 
	 * @param key
	 *            Der Key mit Prefix, z.B. "global.sessionTimeout"
	 * @return true, wenn der String, oder der erste Eintrag im String-Array
	 *         "true" ist, false sonst. Nicht case-Sensitiv.
	 */
	public int getValueAsInt(String prefix, String key) {
		key = prefix + "." + key;
		Object value = this.get(key);
		String stringValue;
		if (value instanceof String)
			stringValue = (String) value;
		else if (value instanceof String[])
			stringValue = ((String[]) value)[0];
		else
			return 0;
		
		return Integer.parseInt(stringValue);
	}
	
	/**
	 * Diese Methode überschreibt Werte dieses Environments aus den Preferences.
	 * 
	 * @param context
	 *            Kontext für Log-Ausgaben
	 * @param systemNodeName
	 *            Systemknotenbezeichner für den Override-Bereich.
	 */
	public void overrideValues(String context, String systemNodeName) {
		Preferences overrides = Preferences.systemRoot().node(systemNodeName);
		String[] keys;
		try {
			keys = overrides.keys();
			if (keys != null && keys.length > 0) {
				for (int i = 0; i < keys.length; i++) {
					String key = keys[i];
					String value = overrides.get(key, get(key));
					logger.debug("[" + context + "] Override for " + key + ": " + value);
					setValue(key, value);
				}
			}
		} catch (BackingStoreException e) {
			logger.error("[" + context + "] Preferences-API-Zugriff", e);
		}
	}
	
	//
	// Object
	//
	/**
	 * Diese Methode liefert eine String-Darstellung des Environments für
	 * Debug-Zwecke.
	 */
	public String toString() {
		StringBuffer sb = new StringBuffer();
		
		sb.append("OctopusEnvironment:\n");
		for (Iterator it = this.entrySet().iterator(); it.hasNext();) {
			Map.Entry entry = (Map.Entry) it.next();
			if (entry.getValue() instanceof String[]) {
				String[] sArr = (String[]) entry.getValue();
				sb.append(entry.getKey() + " =>  (");
				for (int i = 0; i < sArr.length; i++) {
					sb.append(" \"" + sArr[i] + "\" ");
				}
				sb.append(")\n");
			} else {
				sb.append(entry.getKey() + " => " + entry.getValue() + "\n");
			}
		}
		return sb.toString();
	}
	
	/**
	 * Returns the configuration parameters for the login manager.
	 * 
	 * @return The Map configLoginManager
	 */
	public Map getConfigLoginManager() {
		return this.configLoginManager;
	}
	
	//
	// Schlüsselkonstanten
	//
	
	/**
	 * Prefix für den Key, unter dem der direkte Pfad zu einer Configdatei eines
	 * Modules angegeben werden kann Der gesamte key setzt sich zusammen aus:
	 * KEY_MODULE_CONFIGFILE_LOCATION_PREFIX + modulname
	 */
	public final static String KEY_MODULE_CONFIGFILE_LOCATION_PREFIX = "moduleConfig.";
	public final static String KEY_OMIT_SESSIONS = "omitSessions";
	public final static String KEY_PATHS_LOGFILE = "paths.logfile";
	public final static String KEY_PATHS_ROOT = "paths.root";
	public final static String KEY_LOGGING_BASELOGGER = "logging.baseLogger";
	public final static String KEY_LOGGING_LEVEL = "logging.level";
	public final static String KEY_LOGGING_PATTERN = "logging.pattern";
	public final static String KEY_LOGGING_LIMIT = "logging.limit";
	public final static String KEY_LOGGING_COUNT = "logging.count";
	public final static String KEY_LOGGING_PORT = "logging.port";
	public final static String KEY_LOG_SOAP_REQUEST_LEVEL = "logging.soap.request.level";
	public final static String KEY_JMX_ENABLED = "jmxenabled"; // default == false
	public final static String KEY_RPCTUNNEL_ENABLED = "rpctunnelenabled"; // default == false
	public final static String KEY_PATHS_CONFIG_ROOT = "paths.configRoot";
	public final static String KEY_PATHS_CONFIG_FILE = "paths.configFile";
	public final static String KEY_PATHS_MODULES = "paths.modules";
	public final static String KEY_MODULES = "modules";
	public final static String KEY_MODULE_SOURCE = "source";
	public final static String KEY_DEFAULT_MODULE = "defaultModule";
	public final static String KEY_PRELOAD_MODULES = "preloadModules";
	public final static String KEY_WEBAPP_CONTEXT_PATH_NAME = "webappContextPathName";
	public final static String KEY_PATHS_TEMPLATE_ROOT = "paths.templateRoot";
	public final static String KEY_PATHS_WEB_ROOT = "paths.webRoot";
	public final static String KEY_PATHS_AXIS_CONFIGURATION = "paths.axisConfiguration";
	public final static String KEY_DEFAULT_RESPONSE_TYPE = "defaultResponseType";
	public final static String KEY_DEFAULT_ERROR_DESCRIPTION = "defaultErrorDescriptionName";
	public final static String KEY_DEFAULT_ENCODING = "defaultEncoding";
	public final static String KEY_DEFAULT_CONTENT_TYPE = "defaultContentType";
	public final static String KEY_AUTHENTICATION_TYPE = "auth.type";
	public final static String KEY_LDAP_URL = "ldapurl";
	public final static String KEY_LDAP_BASE_DN = "ldapbasedn";
	public final static String KEY_LDAP_RELATIVE = "ldaprelative";
	public final static String KEY_LDAP_AUTHORIZATION = "ldapauth";
	public final static String KEY_LDAP_USER = "ldapuser";
	public final static String KEY_LDAP_PWD = "ldappwd";
	public final static String KEY_RESPONSE_ERROR_LEVEL = "response.errorLevel";
	public final static String KEY_REQUEST_ALLOWED_TYPES = "request.allowedTypes";
	public final static String KEY_RESPONSE_TYPES = "responseTypes";
	
	public final static String VALUE_RESPONSE_ERROR_LEVEL_DEVELOPMENT = "development";
	public final static String VALUE_RESPONSE_ERROR_LEVEL_RELEASE = "release";
	public final static String VALUE_REQUEST_TYPE_ANY = "ANY";
	public final static String VALUE_REQUEST_TYPE_SOAP = "SOAP";
	public final static String VALUE_REQUEST_TYPE_WEB = "WEB";
	public final static String VALUE_REQUEST_TYPE_XMLRPC = "XMLRPC";
	public final static String VALUE_REQUEST_TYPE_DIRECTCALL = "DIRECTCALL";
	public final static String VALUE_MODULE_SOURCE_SERVLET_PREFIX = "servlet:";
	public final static String VALUE_MODULE_SOURCE_FILE_PREFIX = "file:";
	
	public final static String DEFAULT_CONFIG_FILE = "default_octopus_config.xml";
}
