/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.octopus.content;

import java.util.HashMap;
import java.util.Map;

/**
 * Beschreibgung einer Operation eines Ports. Diese OperationDefinition enthält
 * Input-, Output- und FaultMessages
 * 
 * Eine Ausführlichere Beschreibung ist in PortDefinition
 * 
 * @see PortDefinition
 */
public class OperationDefinition {
	private String name;
	private String description;
	
	private MessageDefinition inputMessage;
	private MessageDefinition outputMessage;
	private Map faultMessages;
	private Map faultMessageDescriptions;
	
	/**
	 * Initialisiert eine OperationDefinition
	 * 
	 * @param name
	 *            Name der PortDefinition
	 * @param description
	 *            Sinnvolle Beschreibung des Ports
	 */
	public OperationDefinition(String name, String description) {
		this.name = name;
		this.description = description;
		faultMessages = new HashMap();
		faultMessageDescriptions = new HashMap();
	}
	
	/**
	 * Liefert den Namen, der im Constructor übergeben wurde.
	 * 
	 * @return String mit dem Namen
	 */
	public String getName() {
		return this.name;
	}
	
	/**
	 * Liefert die Beschreibung, die im Constructor übergeben wurde.
	 * 
	 * @return String mit der Beschreibung
	 */
	public String getDescription() {
		return this.description;
	}
	
	/**
	 * Liefert die InputMessage, das sind die Parameter, die diese Operation als
	 * Eingabe benötigt.
	 * 
	 * @return MessageDefinition mit den Inputparametern als Inhalt
	 */
	public MessageDefinition getInputMessage() {
		return inputMessage;
	}
	
	/**
	 * Liefert die OutputMessage, das sind die Parameter, die diese Operation
	 * als Ausgabe liefert.
	 * 
	 * @return MessageDefinition mit den Ausgabeparametern als Inhalt
	 */
	public MessageDefinition getOutputMessage() {
		return outputMessage;
	}
	
	/**
	 * Liefert die FaultMessages. Das ist eine Map, die für jede Fehlerrückgabe
	 * der Operation einen MessageBeschreibung der Ausgabeparameter enthält.
	 * 
	 * @return Map mit den Namen der Fehlerzustände als String Keys, und den
	 *         MessageDefinitions dieser Fehler als Values.
	 */
	public Map getFaultMessages() {
		return faultMessages;
	}
	
	/**
	 * Liefert eine FaultMessage, zu ihrem zugehörigen Statuscode
	 * 
	 * @param faultStatusCode
	 *            Der Fehlerstatus, zu dem die Beschreibung zurück gegeben
	 *            werden soll.
	 * @return Die Beschreibung der Rückgabeparameter bei einem bestimmten
	 *         FaultCode
	 */
	public MessageDefinition getFaultMessage(String faultStatusCode) {
		return (MessageDefinition) faultMessages.get(faultStatusCode);
	}
	
	/**
	 * Liefert die Beschreibung zu einem faultStatusCode. Das soll eine
	 * Beschreibung sein, die den Fehler kurz Charakterisiert.
	 * 
	 * @return Beschreibung der Fehlerrückgabe
	 */
	public String getFaultMessageDescription(String faultStatusCode) {
		return (String) faultMessageDescriptions.get(faultStatusCode);
	}
	
	/**
	 * Setzt die InputMessage mit einem bereits existierenden Objekt. Eine
	 * Operation hat genau eine InputMessage.
	 * 
	 * @param inputMessage
	 *            Die Beschreibung der Parameter der Operation
	 * @return Gibt die neu gesetzte InputMessage zurück.
	 */
	public MessageDefinition setInputMessage(MessageDefinition inputMessage) {
		this.inputMessage = inputMessage;
		return inputMessage;
	}
	
	/**
	 * Erzeugt ein neues InputMessage Objekt und fügt es dieser Operation hinzu.
	 * Eine Operation hat genau eine InputMessage.
	 * 
	 * @return Gibt die neu erzeugte InputMessage zurück.
	 */
	public MessageDefinition setInputMessage() {
		return setInputMessage(new MessageDefinition());
	}
	
	/**
	 * Setzt die OutputMessage mit einem bereits existierenden Objekt. Eine
	 * Operation hat genau eine OutputMessage.
	 * 
	 * @param outputMessage
	 *            Die Beschreibung der Ausgabeparameter der Operation
	 * @return Gibt die neu gesetzte OutputMessage zurück.
	 */
	public MessageDefinition setOutputMessage(MessageDefinition outputMessage) {
		this.outputMessage = outputMessage;
		return outputMessage;
	}
	
	/**
	 * Erzeugt ein neues OutputMessage Objekt und fügt es dieser Operation
	 * hinzu. Eine Operation hat genau eine OutputMessage.
	 * 
	 * @return Gibt die neu erzeugte OutputMessage zurück.
	 */
	public MessageDefinition setOutputMessage() {
		return setOutputMessage(new MessageDefinition());
	}
	
	/**
	 * Fügt eine MessageDefinition für einen bestimmten Statuscode an.
	 * 
	 * @param faultStatusCode
	 *            Der Fehlercode zu der diese FaultMessage gehört.
	 * @param faultDescription
	 *            Liefert die Beschreibung zu einem faultStatusCode. Das soll
	 *            eine Beschreibung sein, die den Fehler kurz Charakterisiert.
	 * @param faultMessage
	 *            Die Beschreibung der Ausgabeparameter der Operation bei einem
	 *            Fehler mit dem faultStatusCode
	 * @return Die gesetzte FaultMessage
	 */
	public MessageDefinition addFaultMessage(String faultStatusCode, String faultDescription, MessageDefinition faultMessage) {
		
		faultMessages.put(faultStatusCode, faultMessage);
		faultMessageDescriptions.put(faultStatusCode, faultDescription);
		return faultMessage;
	}
	
	/**
	 * Erstellt eine neue MessageDefinition und fügt sie für einen bestimmten
	 * Statuscode an.
	 * 
	 * @param faultStatusCode
	 *            Der Fehlercode zu der diese FaultMessage gehört.
	 * @param faultDescription
	 *            Liefert die Beschreibung zu einem faultStatusCode. Das soll
	 *            eine Beschreibung sein, die den Fehler kurz Charakterisiert.
	 * @return Die neu erzeugte FaultMessage
	 */
	public MessageDefinition addFaultMessage(String faultStatusCode, String faultDescription) {
		return addFaultMessage(faultStatusCode, faultDescription, new MessageDefinition());
	}
}
