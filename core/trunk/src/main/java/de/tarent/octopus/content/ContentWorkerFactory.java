/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.octopus.content;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;

import de.tarent.octopus.config.ContentWorkerDeclaration;
import de.tarent.octopus.config.ModuleConfig;
import de.tarent.octopus.logging.LogFactory;
import de.tarent.octopus.resource.Resources;
import de.tarent.octopus.server.SpecialWorkerFactory;
import de.tarent.octopus.server.WorkerCreationException;

/**
 * Factory Klasse mit statischen Methoden zur Lieferung von WorkerInstanzen
 * 
 * @see ContentWorker
 * @author <a href="mailto:mancke@mancke-software.de">Sebastian Mancke</a>,
 *         <b>tarent GmbH</b>
 */
public class ContentWorkerFactory {
	private static Log logger = LogFactory.getLog(ContentWorkerFactory.class);
	
	/**
	 * Map mit ContentWorkern. Keys der Map sind die ClassLoader der Module.
	 * Values sind wiederum Maps mit ("workername"=>Instance). Dadurch werden
	 * die Worker gepuffert und müssen nicht für jede Anfrage neu erstellt
	 * werden.
	 */
	protected static Map workers = new HashMap();
	
	/**
	 * Map mit den Speziellen Factorys zur Instanziierung von Workern. Keys der
	 * Map sind die ClassLoader der Module. Values sind wiederum Maps mit
	 * ("factoryClassName"=>Instance).
	 */
	protected static Map factorys = new HashMap();
	
	/**
	 * Liefert ContentWorker. Es wird ein bereits vorhandener geliefert, oder
	 * ein neuer geladen.
	 * 
	 * Da der Worker dynamisch nach seinem Namen geladen wird kann eine
	 * Exception auftreten, die nach obern weiter gegeben wird.
	 * 
	 * @return Einen Worker mit dem entsprechenden Namen
	 */
	public static ContentWorker getContentWorker(ModuleConfig config, String workerName, String requestID) throws WorkerCreationException {
		
		ContentWorkerDeclaration workerDeclaration = config.getContentWorkerDeclaration(workerName);
		if (null == workerDeclaration) {
			logger.error(Resources.getInstance().get("WORKERFACTORY_LOG_UNDECLARED_WORKER", requestID, workerName, config.getName()));
			throw new WorkerCreationException(Resources.getInstance().get("WORKERFACTORY_EXC_UNDECLARED_WORKER", workerName, config.getName()));
		}
		
		// Bei einem Singleton cachen wir die Instanz,
		if (workerDeclaration.isSingletonInstantiation()) {
			
			// Da jedes Modul einen eigenen Classloader besitzt müssen die
			// Worker unter diesem Classloader im Cache verwendet werden.
			ClassLoader moduleLoader = config.getClassLoader();
			Map moduleWorkers = (Map) workers.get(moduleLoader);
			if (null == moduleWorkers) {
				moduleWorkers = new HashMap();
				workers.put(moduleLoader, moduleWorkers);
			}
			
			if (!moduleWorkers.containsKey(workerName)) {
				ContentWorker worker = getNewWorkerInstance(config, workerDeclaration);
				moduleWorkers.put(workerName, worker);
			}
			return (ContentWorker) moduleWorkers.get(workerName);
		}
		// sonst erzeugen wir jedes mal eine neue.
		else {
			return getNewWorkerInstance(config, workerDeclaration);
		}
	}
	
	protected static ContentWorker getNewWorkerInstance(ModuleConfig config, ContentWorkerDeclaration workerDeclaration) throws WorkerCreationException {
		
		// Da jedes Modul einen eigenen Classloader besitzt müssen
		// auch die Factorys mit diesem Classloader geladen werden.
		// Nur so ist es möglich einem Modul eine eigene Factory hinzu zu fügen.
		ClassLoader moduleLoader = config.getClassLoader();
		
		Map moduleFactorys = (Map) factorys.get(moduleLoader);
		if (null == moduleFactorys) {
			moduleFactorys = new HashMap();
			factorys.put(moduleLoader, moduleFactorys);
		}
		
		SpecialWorkerFactory factory = (SpecialWorkerFactory) moduleFactorys.get(workerDeclaration.getFactory());
		if (null == factory) {
			try {
				logger.debug(Resources.getInstance().get("WORKERFACTORY_LOADING_FACTORY", workerDeclaration.getFactory()));
				factory = (SpecialWorkerFactory) moduleLoader.loadClass(workerDeclaration.getFactory()).newInstance();
			} catch (Exception reflectionException) {
				throw new WorkerCreationException(Resources.getInstance().get("WORKERFACTORY_EXC_LOADING_FACTORY", workerDeclaration.getFactory(), workerDeclaration.getWorkerName()), reflectionException);
			}
			moduleFactorys.put(workerDeclaration.getFactory(), factory);
		}
		ContentWorker worker = factory.createInstance(moduleLoader, workerDeclaration);
		worker.init(config);
		return worker;
	}
}
