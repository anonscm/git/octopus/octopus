/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.octopus.client;

/**
 * Aufruf eines Task des Octopus.
 * 
 * @author <a href="mailto:sebastian@tarent.de">Sebastian Mancke</a>, <b>tarent
 *         GmbH</b>
 */
public class OctopusCallException extends RuntimeException {
	/**
	 * serialVersionUID = -604458204528968362L
	 */
	private static final long serialVersionUID = -604458204528968362L;
	
	protected String errorCode;
	protected String causeMessage;
	
	public OctopusCallException(String msg, Exception e) {
		super(msg, e);
		if (e != null)
			causeMessage = e.getMessage();
	}
	
	public OctopusCallException(String errorCode, String msg, Exception e) {
		super(msg, e);
		this.errorCode = errorCode;
		if (e != null)
			causeMessage = e.getMessage();
	}
	
	public String getCauseMessage() {
		return causeMessage;
	}
	
	public String getErrorCode() {
		return errorCode;
	}
}
