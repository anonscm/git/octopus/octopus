/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.octopus.embedded;

import de.tarent.octopus.request.OctopusResponse;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

/**
 * Stellt Funktionen zur direkten Übergabe der Ausgaben an einen Aufrufer
 * bereit.
 * 
 * @author <a href="mailto:mancke@mancke-software.de">Sebastian Mancke</a>,
 *         <b>tarent GmbH</b>
 */
public class DirectCallResponse implements OctopusResponse {
	
	ByteArrayOutputStream outputStream = null;
	PrintWriter printWriter = null;
	String contentType = null;
	int statusCode = -1;
	int cachingTime = -1;
	Map header = null;
	String taskName = null;
	String moduleName = null;
	
	List responseObjects;
	Map responseObjectsMap;
	
	boolean errorWhileProcessing = false;
	String errorMsg = null;
	Exception errorException = null;
	
	
	public void addResponseObject(String responseObjectName, Object o) {
		if (responseObjects == null) {
			responseObjects = new LinkedList();
			responseObjectsMap = new LinkedHashMap();
		}
		responseObjects.add(o);
		responseObjectsMap.put(responseObjectName, o);
	}
	
	public Iterator getResponseObjectKeys() {
		if (responseObjectsMap != null)
			return responseObjectsMap.keySet().iterator();
		return new EmptyIterator();
	}
	
	public Object getResponseObject(String key) {
		if (responseObjectsMap != null)
			return responseObjectsMap.get(key);
		return null;
	}
	
	public Object readNextResponseObject() {
		return responseObjects.remove(0);
	}
	
	public boolean hasMoreResponseObjects() {
		return responseObjects.size() > 0;
	}
	
	/**
	 * Gibt einen Writer für die Ausgabe zurück. <br>
	 * <br>
	 * Bevor etwas ausgegeben werden kann, muss der ContentType gesetzt werden.
	 */
	public PrintWriter getWriter() {
		assertOutputStreamExistance();
		return printWriter;
	}
	
	/**
	 * Returns the ByteArrayOutputStream.
	 * 
	 * @return OutputStream implemented as an ByteArrayOutputStream;
	 */
	public OutputStream getOutputStream() {
		assertOutputStreamExistance();
		return outputStream;
	}
	
	/**
	 * Sets the outputStream.
	 * 
	 * @param outputStream
	 *            The outputStream to set
	 */
	public void setOutputStream(ByteArrayOutputStream outputStream) {
		this.outputStream = outputStream;
		printWriter = new PrintWriter(outputStream, true);
	}
	
	/**
	 * Gibt einen String auf den Weiter aus.
	 */
	public void print(String responseString) {
		printWriter.print(responseString);
	}
	
	/**
	 * Gibt einen String + "\n" auf den Weiter aus.
	 */
	public void println(String responseString) {
		printWriter.println(responseString);
	}
	
	/**
	 * Diese Methode sendet gepufferte Ausgaben.
	 */
	public void flush() throws IOException {
		if (printWriter != null)
			printWriter.flush();
		if (outputStream != null)
			outputStream.flush();
	}
	
	/**
	 * Diese Methode schließt die Ausgaben ab.
	 */
	public void close() throws IOException {
		if (printWriter != null)
			printWriter.close();
		if (outputStream != null)
			outputStream.close();
	}
	
	private void assertOutputStreamExistance() {
		if (outputStream == null) {
			setOutputStream(new ByteArrayOutputStream());
			printWriter = new PrintWriter(outputStream);
		}
	}
	
	
	/**
	 * Setzt den Mime-Type für die Ausgabe. Das muss passiert sein, bevor etwas
	 * ausgegeben wurde.
	 */
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	
	public String getContentType() {
		return this.contentType;
	}
	
	/**
	 * Setzt den Status für die Ausgabe. Das muss passiert sein, bevor etwas
	 * ausgegeben wurde.
	 */
	public void setStatus(int code) {
		code = statusCode;
	}
	
	/**
	 * Setzt einen Header-Parameter. Das muss passiert sein, bevor etwas
	 * ausgegeben wurde.
	 */
	public void setHeader(String key, String value) {
		if (header == null)
			header = new LinkedHashMap();
		header.put(key, value);
	}
	
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}
	
	public String getTaskName() {
		return taskName;
	}
	
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	
	public String getModuleName() {
		return moduleName;
	}
	
	public void sendError(int responseType, String requestID, String header, Exception e) {
		errorWhileProcessing = true;
		errorMsg = header;
		errorException = e;
	}
	
	public boolean errorWhileProcessing() {
		return errorWhileProcessing;
	}
	
	public String getErrorMessage() {
		if (errorMsg != null)
			return errorMsg;
		else if (errorException != null)
			return errorException.getMessage();
		return null;
	}
	
	public Exception getErrorException() {
		return errorException;
	}
	
	
	public void setAuthorisationRequired(String authorisationAction) {
		// TODO: Geeignete Umsetzung finden.
	}
	
	
	class EmptyIterator implements Iterator {
		
		public boolean hasNext() {
			return false;
		}
		
		public Object next() throws NoSuchElementException {
			throw new NoSuchElementException();
		}
		
		public void remove() throws UnsupportedOperationException {
			throw new UnsupportedOperationException();
		}
	}
	
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.tarent.octopus.request.OctopusResponse#setCachingTime(int)
	 */
	public void setCachingTime(int millis) {
		cachingTime = millis;
	}
	
	public void setCachingTime(int millis, String param) {
		setCachingTime(millis);
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.tarent.octopus.request.OctopusResponse#getCachingTime()
	 */
	public int getCachingTime() {
		return cachingTime;
	}
	
	public void addCookie(String name, String value, Map settings) {
	}
	
	public void addCookie(Object cookie) {
	}
	
	public void setErrorLevel(String errorLevel) {
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.tarent.octopus.request.OctopusResponse#isDirectCall()
	 */
	public boolean isDirectCall() {
		return true;
	}
}
