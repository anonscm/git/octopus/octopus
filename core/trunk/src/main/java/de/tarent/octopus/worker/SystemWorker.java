/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */
package de.tarent.octopus.worker;

import de.tarent.octopus.config.ModuleConfig;
import de.tarent.octopus.content.ContentProzessException;
import de.tarent.octopus.content.ContentWorker;
import de.tarent.octopus.content.OperationDefinition;
import de.tarent.octopus.content.PortDefinition;
import de.tarent.octopus.server.OctopusContext;

/**
 * Dieser Worker stellt einige Systemfunktionen zur Verfügung, etwa das Neuladen
 * eines Moduls, was dem Neueinlesen der Konfiguration entspricht.
 * 
 * @author mikel
 */
public class SystemWorker implements ContentWorker {
	//
	// Schnittstelle ContentWorker
	//
	/**
	 * Diese Methode wird nach Erzeugung des Workers aufgerufen, so dass dieser
	 * sich im Kontext seines Moduls konfigurieren kann.
	 * 
	 * @param config
	 *            Modulkonfiguration.
	 * @see de.tarent.octopus.content.ContentWorker#init(de.tarent.octopus.config.ModuleConfig)
	 */
	public void init(ModuleConfig config) {
	}
	
	/**
	 * Abarbeiten einer Action mit diesem ContentWorker Die Ergebnisse werden in
	 * dem tcContent-Kontainer abgelegt. Ein ContentWorker kann für mehrere
	 * Actions zuständig sein.
	 * @param actionName
	 *            Name der Aktion, die von diesem Worker ausgeführt werden soll.
	 * 
	 * @return String mit einem Statuscode z.B. ok oder error
	 * @see de.tarent.octopus.content.ContentWorker#doAction(java.lang.String, OctopusContext)
	 */
	public String doAction(String actionName, OctopusContext octopusContext) throws ContentProzessException {
		// ACTION_RELOAD_MODULE
		if (ACTION_RELOAD_MODULE.equals(actionName)) {
			String moduleName = octopusContext.moduleConfig().getName();
			octopusContext.commonConfig().deregisterModule(moduleName);
			
			ModuleConfig moduleConfig = octopusContext.commonConfig().getModuleConfig(moduleName);
			return moduleConfig != null ? RESULT_ok : RESULT_error;
		} else {
			throw new ContentProzessException("Nicht unterstützte Aktion im SystemWorker: " + actionName);
		}
	}
	
	/**
	 * Liefert eine Beschreibgung der Actions und deren Eingabeparameter, die
	 * von diesem Worker bereit gestellt werden.
	 * 
	 * @return Eine Abstrakte Beschreibung der Methoden und Parameter
	 * @see de.tarent.octopus.content.ContentWorker#getWorkerDefinition()
	 */
	public PortDefinition getWorkerDefinition() {
		PortDefinition port = new PortDefinition(SystemWorker.class.getName(), "Worker für Systemfunktionen");
		
		OperationDefinition operation = port.addOperation(ACTION_RELOAD_MODULE, "Neuladen des aufrufenden Moduls");
		operation.setInputMessage();
		operation.setOutputMessage();
		
		return port;
	}
	
	//
	// Konstanten
	//
	final static String ACTION_RELOAD_MODULE = "reloadModule";
}
