/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.octopus.config;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;

import de.tarent.octopus.content.ContentProzessException;
import de.tarent.octopus.content.ContentWorker;
import de.tarent.octopus.content.ContentWorkerFactory;
import de.tarent.octopus.logging.LogFactory;
import de.tarent.octopus.request.OctopusRequest;
import de.tarent.octopus.request.OctopusRequestDispatcher;
import de.tarent.octopus.request.OctopusResponse;
import de.tarent.octopus.resource.Resources;
import de.tarent.octopus.response.ResponseDescription;
import de.tarent.octopus.response.ResponseProcessingException;
import de.tarent.octopus.security.OctopusSecurityException;
import de.tarent.octopus.server.OctopusContext;

/**
 * Diese Klasse verwaltet den Ablauf der Verarbeitung eines Tasks.
 * 
 * @author Sebastian Mancke, tarent GmbH
 * @author Michael Klink, tarent GmbH
 * @author Alex Maier, tarent GmbH
 * @author Christoph Jerolimov, tarent GmbH
 */
public class TaskManager implements Runnable {
	private static Log logger = LogFactory.getLog(TaskManager.class);
	
	/**
	 * Strukturierte Liste der Tasks des aktuellen Moduls. Wird erst gesetzt,
	 * wenn eine Abarbeitung mit start() gestartet wird.
	 */
	private TaskList taskList;
	
	/**
	 * Benötigt die Config, um sich eine Liste mit Tasks zum aktuellen Modul zu
	 * holen.
	 */
	private final OctopusContext octopusContext;
	private final OctopusResponse octopusResponse;
	
	// Merkt sich die aktuelle Position
	private Task.TNode position;
	
	// Merkt sich die Positionen (TNodes), an die nach dem Aufruf von
	// anderen tasks zurück gesprungen werden soll.
	// Wird erst initialisiert, wenn er wirklich gebraucht wird.
	List trace;
	
	public static final String ON_ERROR_ACTION_RESUME_NEXT = "resumeNext";
	
	/**
	 * Dieser String enthält die Aktion, die im Fehlerfall ausgeführt werden
	 * soll
	 */
	private String onErrorAction = null;
	

	// Status der letzten aktion
	private String status;
	

	/**
	 * Vom letzten ResponseNode erzeugte ResponseDescription
	 */
	ResponseDescription responseDescription;
	
	
	public TaskManager(OctopusContext octopusContext, OctopusResponse octopusResponse) {
		this.octopusContext = octopusContext;
		this.octopusResponse = octopusResponse;
	}
	
	/**
	 * Setzt die Position auf ein Task, so daß der Aufruf von next() auf die
	 * erste Action ziehlt, und prüft, ob dieses Task das Attribut public hat.
	 * Wenn nicht, wird eine TaskProzessingException ausgelößt
	 */
	public void start(boolean testAccess) throws TaskProzessingException {
		String moduleName = octopusContext.getRequestObject().getModule();
		String taskName = octopusContext.getRequestObject().getTask();
		
		taskList = octopusContext.commonConfig().getTaskList(moduleName);
		if (taskList == null)
			throw new TaskProzessingException(Resources.getInstance().get("TASK_MANAGER_STRING_NO_TASK_LIST", moduleName));
		Task task = taskList.getTask(taskName);
		if (task == null)
			throw new TaskProzessingException(Resources.getInstance().get("TASK_MANAGER_STRING_NO_TASK", taskName, moduleName));
		position = task.rootNode;
		onErrorAction = octopusContext.moduleConfig().getParam("onErrorAction");
		status = ContentWorker.RESULT_ok;
		
		if (testAccess && (!"public".equals(((Task.TaskNode) position).access)))
			throw new TaskProzessingException(Resources.getInstance().get("TASK_MANAGER_STRING_TASK_NOT_PUBLIC", taskName));
	}
	
	public boolean doNextStep() throws TaskProzessingException, ContentProzessException {
		
		if (position instanceof Task.ResponseNode)
			// createResponseDescription
			createResponseDescription();
		
		try {
			position.perform(this, octopusContext);
			return next();
		} catch (ContentProzessException cpe) {
			if (ON_ERROR_ACTION_RESUME_NEXT.equalsIgnoreCase(getCurrentOnErrorAction())) {
				logger.warn(Resources.getInstance().get("TASK_MANAGER_LOG_ERROR_RESUME", octopusContext.getRequestObject().getRequestID()), cpe);
				status = ContentWorker.RESULT_error;
				octopusContext.setError(cpe);
				return next();
			} else {
				throw cpe;
			}
		}
	}
	
	
	/**
	 * This method initializes the {@link ResponseDescription} object.
	 * 
	 * 1. If the task description does not configured any response types, so
	 * provides this task all possible response types. The requested response
	 * type (determine from the URL) will be used.
	 * 
	 * 2. If exactly one response type was configured, so provides this task
	 * only this response type. The requested response type (determine from the
	 * URL) will be ignored.
	 * 
	 * 3. If the task was configured with more than one response types, there
	 * are possible two casses: a) When the configured response types contains
	 * the requested response type (determine from the URL), the requested
	 * response type (determine from the URL) will be used. b) When the
	 * configured response types does not contains the requested response type
	 * (determine from the URL), it will be checked whether the default response
	 * ty was configured. When the configured response types not contains the
	 * default response type, the default response type will be used. c)
	 * Otherwise the {@link TaskProzessingException} will be thrown.
	 * 
	 * @throws TaskProzessingException
	 */
	public void createResponseDescription() throws TaskProzessingException {
		Task.ResponseNode respNode = (Task.ResponseNode) position;
		
		String requestedResponseEngineType = octopusContext.getRequestedResponseType();
		Set taskConfiguredResponseEngineTypes = respNode.type != null ?
				new HashSet(Arrays.asList(respNode.type.split(","))) : null;
		
		if (octopusResponse != null && octopusResponse.isDirectCall()) {
			responseDescription = new ResponseDescription("soap", null);
		} else {
			if (taskConfiguredResponseEngineTypes == null || taskConfiguredResponseEngineTypes.isEmpty()) {
				// all response types are provided, use requested type(from URL)
				responseDescription = new ResponseDescription(respNode.type, respNode.paramMap);
			} else if (taskConfiguredResponseEngineTypes.size() == 1) {
				// exactly one response type is provided
				responseDescription = new ResponseDescription(respNode.type, respNode.paramMap);
			} else if (taskConfiguredResponseEngineTypes.size() > 1) {
				if (taskConfiguredResponseEngineTypes.contains(requestedResponseEngineType)) {
					// the requested engine type is provided for this task, use
					// the requested type(from URL)
					responseDescription = new ResponseDescription(respNode.type, respNode.paramMap);
				} else {
					responseDescription = null;
					throw new TaskProzessingException("Keine response engine konfiguriert.");
				}
			}
		}
		
		// FIXME SECURITY check if requested response desc is allowed here 
	}
	
	
	/**
	 * Setzt die Position auf die nächste Action, eines Task. oder ausgabeseite,
	 * abhängig vonm status.
	 * 
	 * @return gibt true zurück, wenn die die neue Pointerposition auf eine
	 *         Action zeigt, also als nächstes eine Action abgearbeitet werden
	 *         soll. Wenn als nächstes die Response generiert werden soll wird
	 *         false zurück gegeben.
	 * @throws TaskProzessingException
	 */
	protected boolean next() throws TaskProzessingException {
		logger.debug("Next called with '" + status + "' at " + position);
		if (position == null)
			throw new TaskProzessingException("Position steht nicht auf einem Node Objekt. Das verarbeiten der Tasks muss mit start() beginnen.");
		
		// end processing if it is a response node
		if (position instanceof Task.ResponseNode)
			return false;
		
		Task.TNode next = null;
		
		// Entweder das erste Kind suchen
		next = position.getChild(status);
		
		// oder den ersten Nachbar suchen
		if (next == null) {
			logger.debug("No child, trying next");
			next = position.getNext();
		}
		
		// Wenn weder Kinder noch Nachbarn existieren muss
		// nach dem ersten Vater gesucht werden, der Nachfolger hat.
		if (next == null) {
			logger.debug("No next, stepping up");
			Task.TNode parent = position.getParent();
			do {
				if (parent != null && !(parent instanceof Task.TaskNode)) {
					next = parent.getNext();
					parent = parent.getParent();
				} else if (trace != null && trace.size() > 0) {
					position = (Task.TNode) trace.remove(trace.size() - 1);
					// ATTENTION: the perform in a TNode is called twice,
					// because here we move the position pointer to the tnode
					// again.
					return true;
				} else {
					throw new TaskProzessingException("Unerwartetes Ende des Tasks bei: " + position);
				}
			} while (next == null);
		}
		
		// Jetzt sollte next auf dem nächsten Element stehen.
		// Dieses kann also nun ausgewertet werden
		if (next instanceof Task.DoTaskNode) {
			Task.DoTaskNode doTaskNode = (Task.DoTaskNode) next;
			String callStatus = doTaskNode.doStatus;
			if (callStatus == null || callStatus.length() == 0)
				callStatus = status;
			if (trace == null)
				trace = new ArrayList();
			trace.add(next);
			position = doTaskNode.getReferingTask(octopusContext);
			if (position == null) {
				throw new TaskProzessingException("Undefiniertes Task '" + doTaskNode.name + "' bei: " + doTaskNode);
			}
			return true;
		} else {
			position = next;
			return true;
		}
		
	}
	
	/**
	 * Liefert die aktuelle ResponseDescription
	 */
	public ResponseDescription getCurrentResponseDescription() {
		return responseDescription;
	}
	
	public String getCurrentOnErrorAction() {
		return onErrorAction;
	}
	
	public void setOnErrorAction(String newOnErrorAction) {
		onErrorAction = newOnErrorAction;
	}
	
	
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String newStatus) {
		this.status = newStatus;
	}
	
	/**
	 * First call the method
	 * {@link #start(OctopusRequest, OctopusResponse, boolean)} to initialize
	 * the {@link TaskList}.
	 * 
	 * Returns <code>true</code> is the task definition contains the flag type
	 * with the value <code>reloadable</code>. Otherwise returns
	 * <code>false</code>.
	 * 
	 * @param taskName
	 *            the task name
	 * @return <code>true</code> or <code>false</code>
	 */
	public boolean isCurrentTaskReloadable() {
		String taskType = taskList.getTask(octopusContext.getRequestObject().getTask()).getType();
		// TODO first write the processId and callBackURI to a file
		return taskType != null && taskType.equals("reloadable");
	}
	
	/**
	 * Returns <code>true</code> when the
	 * {@link de.tarent.octopus.request.OctopusRequest#getHeaders#getReplyToAddress()}
	 * is not null. Otherwise returns <code>false</code>.
	 * 
	 * @return <code>true</code> or <code>false</code>
	 */
	public boolean isCurrentTaskAsynchron() {
		return
				octopusContext.getRequestObject().getHeaders() != null &&
				octopusContext.getRequestObject().getHeaders().getReplyToAddress() != null &&
				octopusContext.getRequestObject().getHeaders().getReplyToAddress().length() != 0;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Runnable#run()
	 */
	public void run() {
		
		try {
			if (isCurrentTaskReloadable()) {
				try {
					octopusContext.moduleConfig().getPersistentRequestWorker().addTask(
							octopusContext.getRequestObject(),
							Thread.currentThread().getId() +
							octopusContext.getRequestObject().getRequestID(), 1);
				} catch (Exception e) {
					logger.error("Could not store the request in the database", e);
				}
			}
			while (doNextStep()) { /* Do nothing here */
			}
			

			if (isCurrentTaskAsynchron())
				octopusContext.getResponseCreator().sendNormalCallback(getCurrentResponseDescription(), octopusContext, octopusResponse);
			else
				// Sending the response
				octopusContext.getResponseCreator().sendNormalResponse(getCurrentResponseDescription(), octopusContext, octopusResponse);
			
			if (isCurrentTaskReloadable()) {
				try {
					octopusContext.moduleConfig().getPersistentRequestWorker().taskCompleted(
							Thread.currentThread().getId() +
							octopusContext.getRequestObject().getRequestID());
				} catch (Exception e) {
					logger.error("Could not update the request in the database", e);
				}
			}
			
		} catch (TaskProzessingException e) {
			if (logger.isErrorEnabled())
				logger.error(e.toString(), e);
		} catch (ContentProzessException cpe) {
			logger.error(Resources.getInstance().get("REQUESTDISPATCHER_LOG_TASK_ERROR", octopusContext.getRequestObject().getRequestID(), octopusContext.getRequestObject().getTask()), cpe);
			if ("soapFault".equalsIgnoreCase(getCurrentOnErrorAction()))
				try {
					octopusContext.getResponseCreator().sendErrorResponse(getCurrentResponseDescription(), octopusContext, octopusResponse, null, cpe);
				} catch (ResponseProcessingException e) {
					if (logger.isErrorEnabled())
						logger.error(e.toString(), e);
				}
			else
				try {
					sendError(octopusContext.moduleConfig(), octopusResponse, octopusContext.getRequestObject(), Resources.getInstance().get("REQUESTDISPATCHER_OUT_TASK_ERROR", octopusContext.getRequestObject().getTask()), cpe);
				} catch (Exception e) {
					if (logger.isErrorEnabled())
						logger.error(e.toString(), e);
				}
		} catch (ResponseProcessingException rpe) {
			logger.error(Resources.getInstance().get("REQUESTDISPATCHER_LOG_RESPONSE_ERROR", octopusContext.getRequestObject().getRequestID()), rpe);
			try {
				sendError(octopusContext.moduleConfig(), octopusResponse, octopusContext.getRequestObject(), Resources.getInstance().get("REQUESTDISPATCHER_OUT_RESPONSE_ERROR"), rpe);
			} catch (ResponseProcessingException e) {
				logger.error(e.toString(), e);
			}
			
		}

		finally {
			OctopusRequestDispatcher.processCleanupCode(octopusContext.getRequestObject().getRequestID(), octopusContext);
		}
		
	}
	
	// ==========Copy of the private methods from the
	// OctopusRequestDispatcher=============//
	private void sendError(ModuleConfig moduleConfig, OctopusResponse octopusResponse, OctopusRequest octopusRequest, String message, Exception exception) throws ResponseProcessingException {
		if (exception instanceof OctopusSecurityException && !OctopusRequest.isWebType(octopusRequest.getRequestType())) {
			sendAuthenticationError(moduleConfig, octopusContext.commonConfig(), octopusRequest, octopusResponse, (OctopusSecurityException) exception);
			return;
		}
		
		ResponseDescription responseDescription;
		if (exception instanceof OctopusSecurityException)
			responseDescription = new ResponseDescription("soap", null); // FIXME remove fixed respones type
		else
			responseDescription = new ResponseDescription("soap", null); // FIXME remove fixed respones type
		
		Map responseParams = new HashMap();
		responseParams.put("message", message);
		octopusContext.setContent("responseParams", responseParams);
		putStandardParams(octopusContext);
		logger.debug(Resources.getInstance().get("REQUESTDISPATCHER_LOG_SENDING_ERROR", octopusRequest.getRequestID(), message), exception);
		
		octopusContext.getResponseCreator().sendErrorResponse(responseDescription, octopusContext, octopusResponse, message, exception);
	}
	
	/**
	 * Senden von Informationen zu den Fehlern, die bei der Authentifizierung
	 * aufgetreten sind.
	 */
	private void sendAuthenticationError(ModuleConfig moduleConfig, CommonConfig config, OctopusRequest octopusRequest, OctopusResponse octopusResponse, OctopusSecurityException securityException) throws ResponseProcessingException {
		octopusResponse.setAuthorisationRequired(moduleConfig.getOnUnauthorizedAction());
		
		if (OctopusRequest.isWebType(octopusRequest.getRequestType())) {
			// Web-Type über sendResponse abwickeln.
			sendError(moduleConfig, octopusResponse, octopusRequest, null, securityException);
		} else {
			// Eine SOAP Anfrage bekommt auch eine SOAP Fehlermeldung
			// throw new ResponseProcessingException(message, new
			// SoapException(message));
			if (logger.isDebugEnabled())
				logger.debug(Resources.getInstance().get("REQUESTDISPATCHER_LOG_SENDING_ERROR", octopusRequest.getRequestID(), securityException.getMessage()));
			if (logger.isTraceEnabled())
				logger.trace(Resources.getInstance().get("REQUESTDISPATCHER_LOG_SENDING_ERROR", octopusRequest.getRequestID(), securityException.getMessage()), securityException);
			octopusContext.getResponseCreator().sendErrorResponse(responseDescription, octopusContext, octopusResponse, null, securityException);
		}
	}
	
	/**
	 * Einige Parameter, die fast immer in der Ausgabe benötigt werden, schonmal
	 * in den OctopusContent schieben
	 */
	public void putStandardParams(OctopusContext octopusContext) {
		String standardParamWorker = Resources.getInstance().get("REQUESTDISPATCHER_CLS_PARAM_WORKER");
		try {
			
			ContentWorker worker = ContentWorkerFactory.getContentWorker(octopusContext.moduleConfig(), standardParamWorker, octopusContext.getRequestObject().getRequestID());
			worker.doAction("putMinimal", octopusContext);
		} catch (Exception e) {
			logger.error(Resources.getInstance().get("REQUESTDISPATCHER_LOG_PARAM_SET_ERROR", octopusContext.getRequestObject().getRequestID(), standardParamWorker), e);
		}
	}
}
