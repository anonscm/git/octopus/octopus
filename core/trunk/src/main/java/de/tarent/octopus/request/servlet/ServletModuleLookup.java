/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

/* $Id: ServletModuleLookup.java,v 1.8 2007/06/11 13:05:37 christoph Exp $
 * tarent-octopus, Webservice Data Integrator and Applicationserver
 * Copyright (C) 2002 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus'
 * (which makes passes at compilers) written
 * by Sebastian Mancke and Michael Klink.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */
package de.tarent.octopus.request.servlet;

import java.io.File;
import java.util.Map;

import javax.servlet.ServletContext;

import org.apache.commons.logging.Log;

import de.tarent.octopus.config.ModuleLookup;
import de.tarent.octopus.config.OctopusEnvironment;
import de.tarent.octopus.logging.LogFactory;
import de.tarent.octopus.resource.Resources;

/**
 * Diese Klasse liefert dem Octopus notwendige Daten.
 */
class ServletModuleLookup implements ModuleLookup {
	private final transient Log logger = LogFactory.getLog(ServletModuleLookup.class);
	
	/** Octopus common config */
	private final transient OctopusEnvironment octopusEnvironment;
	
	/** Octopus servlet context */
	private final transient ServletContext servletContext;
	
	/** Octopus servlet */
	private final transient OctopusServlet octopusServlet;
	
	/**
	 * @param octopusEnvironment
	 * @param servletContext
	 * @param octopusServlet
	 */
	ServletModuleLookup(OctopusEnvironment octopusEnvironment, ServletContext servletContext, OctopusServlet octopusServlet) {
		this.octopusEnvironment = octopusEnvironment;
		this.servletContext = servletContext;
		this.octopusServlet = octopusServlet;
	}
	
	OctopusEnvironment getEnvironment() {
		return octopusEnvironment;
	}
	
	ServletContext getServletContext() {
		return servletContext;
	}
	
	public File getModulePath(String module) {
		Map modules = (Map) getEnvironment().getValueAsObject(OctopusEnvironment.KEY_MODULES);
		if (modules != null) {
			// Find modules parameters by module name
			// or use the default behind the '*'.
			Map parameters = (Map) modules.get(module);
			if (parameters == null) {
				parameters = (Map) modules.get("*");
				if (parameters == null) {
					logger.warn(Resources.getInstance().get("OCTOPUS_MODULELOOKUP_PARAMETERS_NOT_FOUND", module, parameters));
					return null;
				} else {
					logger.info(Resources.getInstance().get("OCTOPUS_MODULELOOKUP_USE_DEFAULT_PARAMETERS", module, parameters));
				}
			} else {
				logger.info(Resources.getInstance().get("OCTOPUS_MODULELOOKUP_PARAMETERS_FOUND", module, parameters));
			}
			
			String source = (String) parameters.get(OctopusEnvironment.KEY_MODULE_SOURCE);
			if (source == null || source.length() == 0) {
				logger.warn(Resources.getInstance().get("OCTOPUS_MODULELOOKUP_NO_SOURCE_PARAMETER", module));
				return null;
			} else {
				source = source.replaceAll("\\*", module);
			}
			
			logger.info(Resources.getInstance().get("OCTOPUS_MODULELOOKUP_USE_SOURCE", module, source));
			if (source.startsWith(OctopusEnvironment.VALUE_MODULE_SOURCE_SERVLET_PREFIX)) {
				return getModuleByServletContext(source.substring(OctopusEnvironment.VALUE_MODULE_SOURCE_SERVLET_PREFIX.length()));
			} else if (source.startsWith(OctopusEnvironment.VALUE_MODULE_SOURCE_FILE_PREFIX)) {
				String sourcePath = source.substring(OctopusEnvironment.VALUE_MODULE_SOURCE_FILE_PREFIX.length());
				if (new File(sourcePath).isAbsolute()) {
					return new File(sourcePath);
				} else {
					String octopusPath = getServletContext().getRealPath("");
					return new File(octopusPath, sourcePath);
				}
			} else {
				logger.error(Resources.getInstance().get("OCTOPUS_MODULELOOKUP_ILLEGAL_SOURCE", module, source));
				return null;
			}
		} else {
			// Default behavior when no modules are configured.
			if (!module.startsWith("/"))
				module = "/" + module;
			logger.info(Resources.getInstance().get("OCTOPUS_MODULELOOKUP_PARAMETERS_NOT_FOUND", module));
			logger.info(Resources.getInstance().get("OCTOPUS_MODULELOOKUP_USE_SOURCE", module, OctopusEnvironment.VALUE_MODULE_SOURCE_SERVLET_PREFIX + module));
			return getModuleByServletContext(module);
		}
	}
	
	public File getModuleByServletContext(String module) {
		ServletContext moduleContext = null;
		if (module.equals(octopusServlet.webappContextPathName) || module.trim().equals(""))
			moduleContext = servletContext;
		else {
			moduleContext = servletContext.getContext(module);
		}
		
		if (moduleContext == null) {
			logger.info(Resources.getInstance().get("REQUESTPROXY_LOG_NO_MODULE_CONTEXT", module));
			return null;
		}
		
		String realPath = moduleContext.getRealPath("/OCTOPUS/");
		if (realPath == null || realPath.length() == 0) {
			logger.info(Resources.getInstance().get("REQUESTPROXY_LOG_NO_MODULE_CONTEXT", module));
			return null;
		}
		
		return new File(realPath);
	}
}
