/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.octopus.config;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Constructor;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import javax.wsdl.Definition;
import javax.wsdl.WSDLException;
import javax.wsdl.factory.WSDLFactory;
import javax.wsdl.xml.WSDLWriter;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.axis.encoding.TypeMappingRegistry;
import org.apache.axis.encoding.ser.BeanDeserializerFactory;
import org.apache.axis.encoding.ser.BeanSerializerFactory;
import org.apache.commons.logging.Log;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import de.tarent.octopus.content.DirectWorkerFactory;
import de.tarent.octopus.logging.LogFactory;
import de.tarent.octopus.persistence.PersistentRequestWorker;
import de.tarent.octopus.resource.Resources;
import de.tarent.octopus.response.ResponseCreator;
import de.tarent.octopus.response.soap.SoapEngine;
import de.tarent.octopus.security.OctopusSecurityException;
import de.tarent.octopus.server.LoginManager;
import de.tarent.octopus.server.PersonalConfig;
import de.tarent.octopus.util.DataFormatException;
import de.tarent.octopus.util.Xml;
import de.tarent.octopus.worker.PutParams;

/**
 * Beinhaltet die Einstellungen zu eimem Module. Ein Module ist eine Sammlung
 * von zusammengehörigen Dateien und Einstellungen. Dies können z.B. ein paar
 * ContentWorker und ein paar Templates sein, die zusammen eine bestimmte
 * Anwendung bilden.
 * 
 * @author <a href="mailto:mancke@mancke-software.de">Sebastian Mancke</a>,
 *         <b>tarent GmbH</b>
 */
public class ModuleConfig {
	//
	// Konstanten
	//
	public final static String DEFAULT_CONFIG_FILE = "default_module_config.xml";

	public final static String PREFS_DATA_ACCESS = "dataAccess";
	public final static String PREFS_PARAMS = "params";
	public static final String CONFIG_PARAM_ON_UNAUTHORIZED = "onUnauthorized";
	public static final String CONFIG_PARAM_PERSONAL_CONFIG_CLASS = "personalConfigClass";
	public final static String PREFS_IS_IN_TEST_ENVIRONMENT = "isInTestEnvironment";
	public final static String PREFS_PERSISTS_REQUESTS = "persistsRequests";
	public final static String DB_PERSISTENT = "persistentDB";
	/**
	 * Default Factory, die verwendet wird, wenn in der Config keine Angaben zu
	 * einem Worker gemacht wurden.
	 */
	public static final String DEFAULT_WORKER_FACTORY_NAME = DirectWorkerFactory.class.getName();
	
	//
	// Variablen
	//
	protected String escapedUserHome = System.getProperty("user.home").replaceAll("\\\\", "\\\\\\\\");
	protected String name = null;
	protected File realPath = null;
	protected String description = "";
	protected ClassLoader classLoader = null;
	protected LoginManager loginManager = null;
	protected ResponseCreator responseCreator = null;
	protected Constructor personalConfigConstructor = null;
	protected PersistentRequestWorker persistentRequestWorker = null;
	
	/** Der Logger */
	private static Log logger = LogFactory.getLog(ModuleConfig.class);
	
	/*
	 * Config Parameter
	 */
	protected Map configParams;
	private final Map rawConfigParams;
	private final Preferences configPrefs;
	
	/**
	 * Map mit den Einstellungen für den LoginManager.
	 */
	protected Map loginManagerParams = new LinkedHashMap();
	
	/**
	 * Map mit den verfügbaren Datenquellen. <br>
	 * Eine Map, in der unter den Namen der Datenquellen wieder Maps mit String
	 * Keys und String Values abgelegt sind.
	 */
	protected Map dataAccess = new LinkedHashMap();
	
	/**
	 * Map mit den deklarierten Workern. Eindeutige Namen als String Keys
	 * ContentWorkerDeclatarion-Objekte als Values.
	 */
	protected Map contentWorkersDeclarations;
	
	/**
	 * Verfügbare Tasks
	 */
	protected TaskList taskList;
	protected Definition wsdlDefinition;
	
	private Map otherNodes = new LinkedHashMap();
	
	/**
	 * Initialisiert diese Config mit einem geparsten Document. Dieses Document
	 * muss der DTD ModuleConfig genügen.
	 * 
	 * @throws DataFormatException
	 * @throws OctopusConfigException
	 */
	public ModuleConfig(String name, File realPath, Document document, Preferences preferences) throws DataFormatException, OctopusConfigException {
		this.name = name;
		this.realPath = realPath;
		this.configPrefs = preferences;
		this.responseCreator = new ResponseCreator(this);
		this.rawConfigParams = new LinkedHashMap();
		
		parseDefaultConfigFile();
		collectSectionsFromDocument(document);
		
		this.configParams = new LinkedHashMap(rawConfigParams);
		
		override("Parameter von " + name, configParams, preferences.node(PREFS_PARAMS));
		
		if (taskList != null) {
			try {
				logger.debug("Exportiere WSDL-Darstellung des Moduls");
				wsdlDefinition = taskList.getPortDefinition().getWsdlDefinition(true, "http://schemas.tarent.de/" + name, name, "http://localhost:8080/octopus");
				OutputStream os = new FileOutputStream(new File(realPath, "module.wsdl"));
				WSDLFactory factory;
				factory = WSDLFactory.newInstance();
				WSDLWriter writer = factory.newWSDLWriter();
				writer.writeWSDL(wsdlDefinition, os);
				os.close();
				// logger.debug(name + ": " + sw.toString());
			} catch (WSDLException e) {
				logger.error("WSDL-Fehler", e);
			} catch (IOException e) {
				logger.error("IO-Fehler", e);
			}
		}
		
		initializePersistentRequestWorker();
	}

	/**
	 * Only for creation of MockUps!
	 * 
	 * @throws DataFormatException
	 * @throws OctopusConfigException
	 */
	public ModuleConfig(String basedir, Map rawConfigParams) throws DataFormatException, OctopusConfigException {
		this.name = "mockup-module";
		this.realPath = new File(basedir);
		this.rawConfigParams = rawConfigParams;
		this.configParams = rawConfigParams;
		this.configPrefs = Preferences.systemRoot().node("/de/tarent/octopus");
		
		parseDefaultConfigFile();
	}
	
	/**
	 * Load the default octopus config file.
	 * 
	 * @return Document
	 * @throws OctopusConfigException
	 * @throws DataFormatException 
	 */
	protected void parseDefaultConfigFile() throws OctopusConfigException, DataFormatException {
		InputStream inputStream = ModuleConfig.class.getResourceAsStream("/" +
				DEFAULT_CONFIG_FILE);
		try {
			logger.debug(Resources.getInstance().get("COMMONCONFIG_LOG_PARSE_START"));
			collectSectionsFromDocument(Xml.getParsedDocument(inputStream));
			logger.debug(Resources.getInstance().get("COMMONCONFIG_LOG_PARSE_STOP"));
		} catch (SAXParseException se) {
			logger.error(Resources.getInstance().get("COMMONCONFIG_LOG_PARSE_SAX_EXCEPTION", new Integer(se.getLineNumber()), new Integer(se.getColumnNumber())), se);
			throw new OctopusConfigException(Resources.getInstance().get("COMMONCONFIG_EXC_PARSE_ERROR"), se);
		} catch (Exception e) {
			logger.error(Resources.getInstance().get("COMMONCONFIG_LOG_PARSE_ERROR"), e);
			throw new OctopusConfigException(Resources.getInstance().get("COMMONCONFIG_EXC_PARSE_ERROR"), e);
		}
		try {
			inputStream.close();
		} catch (IOException e) {
		}
	}
	
	/**
	 * Initialize the {@link PersistentRequestWorker} when required.
	 */
	private void initializePersistentRequestWorker() {
		if (persistsRequests()) {
			try {
				persistentRequestWorker = new PersistentRequestWorker(this);
			} catch (Exception e) {
				logger.error("Cannot initialize the PersistentRequestWorker", e);
			}
		}
	}
	
	/**
	 * Updates the module configuration.
	 * 
	 * @param document
	 * @throws DataFormatException
	 */
	public void updateConfiguration(Document document) throws DataFormatException {
		collectSectionsFromDocument(document);
		configParams = new LinkedHashMap(rawConfigParams);
		override("Parameter von " + name, configParams, this.configPrefs.node(PREFS_PARAMS));
		initializePersistentRequestWorker();
	}
	
	/**
	 * @param document
	 * @param preferences
	 * @throws DataFormatException
	 */
	private void collectSectionsFromDocument(Document document) throws DataFormatException {
		
		NodeList sections = document.getDocumentElement().getChildNodes();
		

		// Auslesen der übrigen Section mit den in der Application angegebenen
		// Namen
		for (int i = 0; i < sections.getLength(); i++) {
			Node currNode = sections.item(i);
			
			if ("include".equals(currNode.getNodeName())) {
				
				Document includeDocument = null;
				
				NamedNodeMap attributes = currNode.getAttributes();
				
				if (attributes.getNamedItem("file") != null) {
					String path = attributes.getNamedItem("file").getNodeValue();
					
					File configFile = new File(path);
					if (!configFile.isAbsolute()) {
						configFile = new File(realPath, path);
					}
					logger.info("Loading file '" + configFile.getAbsolutePath() + "'.");
					
					if (configFile.exists()) {
						try {
							includeDocument = Xml.getParsedDocument(Resources.getInstance().get("REQUESTPROXY_URL_MODULE_CONFIG", configFile.getAbsolutePath()));
						} catch (Exception e) {
							logger.error("Error while loading config file '" + configFile + "'. Parsing aborted.", e);
						}
					} else {
						logger.warn("Config file '" + configFile + "' not found. Will be ignored.");
					}
				}

				// classpath
				else if (attributes.getNamedItem("packagename") != null || attributes.getNamedItem("classpath") != null) {
					String resource;
					if (attributes.getNamedItem("packagename") != null) {
						resource = attributes.getNamedItem("packagename").getNodeValue().replace('.', '/');
						resource += "/config.xml";
					} else {
						resource = attributes.getNamedItem("classpath").getNodeValue().replace('.', '/');
					}
					
					try {
						logger.info("Loading file '" + resource + "' from module classpath.");
						
						InputStream inputStream = getClassLoader().getResourceAsStream(resource);
						
						if (inputStream != null) {
							includeDocument = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(inputStream);
						} else {
							logger.info("Config file '" + resource + "' not found in module classpath. Try octopus classpath.");
							
							inputStream = getClass().getClassLoader().getResourceAsStream(resource);
							
							if (inputStream != null) {
								includeDocument = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(inputStream);
							} else {
								logger.warn("Config file '" + resource + "' not found in module nor octopus classpath.");
							}
						}
						
					} catch (SAXException e) {
						logger.error("Error while parsing included config file from classpath.", e);
					} catch (ParserConfigurationException e) {
						logger.error("Error while parsing included config file from classpath.", e);
					} catch (IOException e) {
						logger.error("Error while loading included config file from classpath.", e);
					}
				} else {
					logger.error("Illegal include attributes. No 'file', 'packagename' or 'classpath' found.");
				}
				
				if (includeDocument != null) {
					collectSectionsFromDocument(includeDocument);
				}
				
			} else if ("params".equals(currNode.getNodeName())) {
				try {
					rawConfigParams.putAll(Xml.getParamMap(currNode));
				} catch (DataFormatException dfe) {
					logger.error("Fehler beim Parsen der Config-Parameter.", dfe);
					throw dfe;
				}
			} else if ("tasks".equals(currNode.getNodeName())) {
				if (taskList == null)
					taskList = new TaskList();
				taskList.parseTasks((Element) currNode, this);
			} else if ("loginManager".equals(currNode.getNodeName())) {
				try {
					loginManagerParams.putAll(Xml.getParamMap(currNode));
				} catch (DataFormatException dfe) {
					logger.error("Fehler beim Parsen der Config-Parameter.", dfe);
					throw dfe;
				}
			} else if ("dataAccess".equals(currNode.getNodeName())) {
				try {
					dataAccess.putAll(parseDataAccess(currNode, this.configPrefs.node(PREFS_DATA_ACCESS)));
				} catch (DataFormatException dfe) {
					logger.error("Fehler beim Parsen des Data Access Abschnitts.", dfe);
					throw dfe;
				}
			} else if ("contentWorkerDeklaration".equals(currNode.getNodeName())) {
				try {
					if (contentWorkersDeclarations == null)
						contentWorkersDeclarations = new LinkedHashMap();
					contentWorkersDeclarations.putAll(parseContentWorkerDeklarations(currNode));
				} catch (DataFormatException dfe) {
					logger.error("Fehler beim Parsen der OctopusContent Worker Deklaration.", dfe);
					throw dfe;
				}
			} else if ("description".equals(currNode.getNodeName())) {
				StringBuffer sb = new StringBuffer();
				NodeList childs = currNode.getChildNodes();
				String content;
				for (int j = 0; j < childs.getLength(); j++) {
					content = childs.item(j).getNodeValue();
					if (content != null)
						sb.append(content);
				}
				if (description != null && description.length() != 0) {
					description += " - " + sb.toString();
				} else {
					description = sb.toString();
				}
			} else if ("types".equals(currNode.getNodeName())) {
				try {
					// TODO: richtig machen.
					// Joh watt dän?
					Class javaClass = getClassLoader().loadClass("de.tarent.schemas.Groupware_xsd.OptionMapType");
					QName qname = new QName("http://schemas.tarent.de/Groupware.xsd", "OptionMapType");
					TypeMappingRegistry reg = SoapEngine.engine.getTypeMappingRegistry();
					reg.getDefaultTypeMapping().register(javaClass, qname, new BeanSerializerFactory(javaClass, qname), new BeanDeserializerFactory(javaClass, qname));
					javaClass = getClassLoader().loadClass("de.tarent.schemas.Groupware_xsd.EntryType");
					qname = new QName("http://schemas.tarent.de/Groupware.xsd", "EntryType");
					reg.getDefaultTypeMapping().register(javaClass, qname, new BeanSerializerFactory(javaClass, qname), new BeanDeserializerFactory(javaClass, qname));
				} catch (ClassNotFoundException e1) {
					logger.error("Typenübergabe", e1);
				}
			} else if (currNode instanceof Element) {
				otherNodes.put(currNode.getNodeName(), currNode);
			}
		}
	}
	
	/**
	 * Creates an Config Object for testing purpose
	 * 
	 * @param rawConfigParams
	 *            the config parameters
	 * 
	 * @throws OctopusConfigException 
	 * @throws DataFormatException 
	 */
	public static ModuleConfig createMockupModuleConfig(String basedir, Map rawConfigParams) throws DataFormatException, OctopusConfigException {
		return new ModuleConfig(basedir, rawConfigParams);
	}
	
	

	/**
	 * Auslesen der OctopusContent Worker Deklarationen
	 */
	protected Map parseContentWorkerDeklarations(Node context) throws DataFormatException {
		Map declarationMap = new LinkedHashMap();
		
		// Es gibt einen Default-Worker,
		// der für das Hinzufügen von Parameern in den OctopusContent genutzt
		// wird.
		ContentWorkerDeclaration putParamWorker = new ContentWorkerDeclaration();
		putParamWorker.setWorkerName(Resources.getInstance().get("REQUESTDISPATCHER_CLS_PARAM_WORKER"));
		putParamWorker.setImplementationSource(PutParams.class.getName());
		putParamWorker.setSingletonInstantiation(true);
		putParamWorker.setFactory(DEFAULT_WORKER_FACTORY_NAME);
		declarationMap.put(putParamWorker.getWorkerName(), putParamWorker);
		

		NodeList nodes = context.getChildNodes();
		Node currNode;
		for (int i = 0; i < nodes.getLength(); i++) {
			currNode = nodes.item(i);
			
			// z.B. <worker name="SQLExecutor"
			// implementation="de.tarent.demo.SQLExecutor" singleton="true"
			// factory="direct"/>
			if (currNode instanceof Element && "worker".equals(currNode.getNodeName())) {
				ContentWorkerDeclaration workerDek = new ContentWorkerDeclaration();
				Element workerElement = (Element) currNode;
				
				// name
				Attr name = workerElement.getAttributeNode("name");
				if (name == null)
					throw new DataFormatException("Eine Worker-Deklaration muss ein name-Attribut haben.");
				workerDek.setWorkerName(name.getValue());
				
				// implementation
				Attr implementation = workerElement.getAttributeNode("implementation");
				if (implementation == null)
					throw new DataFormatException("Eine Worker-Deklaration muss ein implementation-Attribut haben.");
				workerDek.setImplementationSource(implementation.getValue());
				
				// singleton
				Attr singleton = workerElement.getAttributeNode("singleton");
				if (singleton != null)
					workerDek.setSingletonInstantiation(new Boolean(singleton.getValue()).booleanValue());
				
				// factory
				Attr factory = workerElement.getAttributeNode("factory");
				if (factory != null)
					workerDek.setFactory(getExpandedWorkerFactoryName(factory.getValue()));
				else
					workerDek.setFactory(DEFAULT_WORKER_FACTORY_NAME);
				
				declarationMap.put(workerDek.getWorkerName(), workerDek);
			}
			// Zur Erhaltung der Kompatibilität sind auch <param
			// name="workername" value="Klasse"/> erlaubt.
			else if (currNode instanceof Element && "param".equals(currNode.getNodeName())) {
				ContentWorkerDeclaration workerDek = new ContentWorkerDeclaration();
				Element workerElement = (Element) currNode;
				
				// name
				Attr name = workerElement.getAttributeNode("name");
				if (name == null)
					throw new DataFormatException("Eine param-Element muss ein name-Attribut haben.");
				workerDek.setWorkerName(name.getValue());
				
				// value
				Attr value = workerElement.getAttributeNode("value");
				if (value == null)
					throw new DataFormatException("Ein param-Element muss ein value-Attribut haben.");
				workerDek.setImplementationSource(value.getValue());
				
				workerDek.setFactory(DEFAULT_WORKER_FACTORY_NAME);
				
				declarationMap.put(workerDek.getWorkerName(), workerDek);
			}
		}
		return declarationMap;
	}
	
	/**
	 * Liefert einen voll Qualifizierten Klassennamen zurück. Wenn 'shortname'
	 * keinen Punkt enthält wird er als Kurzname interpretiert, und entsprechend
	 * expandiert. Sonst wird davon aus gegangen, dass es sich bereits um einen
	 * kompletten Klassennamen handelt.
	 * 
	 * Methode zur Expansion ist: "de.tarent.octopus.content."+
	 * UK_FIRST_LOWER_REST(shortname) +"WorkerFactory"
	 * 
	 * @param shortname
	 *            Ein Kurzname oder bereits ein voll qualifizierter Name
	 * @return Den vollen Klassennamen für shortname, oder shortname, wenn es
	 *         bereits ein Klassenname ist.
	 */
	protected String getExpandedWorkerFactoryName(String shortname) {
		if (shortname.indexOf(".") < 0)
			return "de.tarent.octopus.content." + shortname.substring(0, 1).toUpperCase() + shortname.substring(1).toLowerCase() + "WorkerFactory";
		return shortname;
	}
	
	/**
	 * Auslesen der DataAccess Informationen
	 */
	protected Map parseDataAccess(Node dataAccessNode, Preferences preferences) throws DataFormatException {
		
		Map sources = new LinkedHashMap();
		
		NodeList nodes = dataAccessNode.getChildNodes();
		Node currNode;
		for (int i = 0; i < nodes.getLength(); i++) {
			currNode = nodes.item(i);
			if ("dataSource".equals(currNode.getNodeName())) {
				NamedNodeMap attributes = currNode.getAttributes();
				Node nameNode = attributes.getNamedItem("name");
				String name = null;
				if (nameNode != null)
					name = nameNode.getNodeValue();
				if (name == null)
					throw new DataFormatException("Ein dataSource-Element muss ein name-Element haben.");
				
				Map currentSource = Xml.getParamMap(currNode);
				override("DataSource " + name, currentSource, preferences.node(name));
				currentSource.put("name", name);
				sources.put(name, currentSource);
				
				NodeList children = currNode.getChildNodes();
				for (int j = 1; j <= children.getLength(); j++) {
					Node node = children.item(j - 1);
					if (node.getNodeName().equals("options")) {
						Map options = Xml.getParamMap(node);
						Iterator itOptions = options.keySet().iterator();
						while (itOptions.hasNext()) {
							Object key = itOptions.next();
							currentSource.put(key, options.get(key));
						}
					}
				}
			}
		}
		return sources;
	}
	
	/**
	 * Liefert die verfügbaren Datenquellen
	 * 
	 * @return Eine Map, in der unter den Namen der Datenquellen wieder Maps mit
	 *         String Keys und String Values abgelegt sind.
	 */
	public Map getDataAccess() {
		return dataAccess;
	}
	
	/**
	 * Liefert das Declaration Objekt zu einem ContentWorker
	 * 
	 * @param workerName
	 *            Eindeutiger Bezeichner des Workers in diesem Modul
	 * @return Objekt mit Informationen zur Instantiierung des Workers
	 */
	public ContentWorkerDeclaration getContentWorkerDeclaration(String workerName) {
		return (ContentWorkerDeclaration) contentWorkersDeclarations.get(workerName);
	}
	
	/**
	 * Liefert einen Parameter, der in der Config gesetzt wurde.
	 */
	public String getParam(String key) {
		return (String) getParamAsObject(key);
	}
	
	
	/**
	 * Liefert einen Parameter, und konvertiert bei Bedarf den Fileseparator von /
	 * auf File.separatorChar
	 */
	public String getParamForPath(String key) {
		if (File.separatorChar == '/')
			return getParam(key);
		
		String val = getParam(key);
		if (val != null)
			return val.replace('/', File.separatorChar);
		return null;
	}
	
	/**
	 * Ersetzt variablen in den Parameterwerten mit Werten. Im Moment wird nur:
	 * ${HOME} => Homeverzeichnis ${user.home} => Homeverzeichnis ${module.path} =>
	 * Modulverzeichnis unterstützt.
	 */
	public String substituteVars(String value) {
		if (value == null)
			return null;
		value = value.replaceAll("\\$\\{HOME\\}", escapedUserHome);
		value = value.replaceAll("\\$\\{user\\.home\\}", realPath.getAbsolutePath());
		value = value.replaceAll("\\$\\{module\\.path\\}", realPath.getAbsolutePath());
		return value;
	}
	
	public Object getParamAsObject(String key) {
		Object result = getField(configParams, key);
		if (result instanceof String)
			return substituteVars((String) result);
		return result;
	}
	
	protected Object getField(Map data, String key) {
		Object result = data.get(key);
		if (result == null && key.indexOf(".") != -1) {
			Object sub = data.get(key.substring(0, key.indexOf(".")));
			if (sub instanceof Map) {
				return getField((Map) sub, key.substring(key.indexOf(".") + 1));
			}
		}
		return result;
	}
	
	/**
	 * Diese Methode Überschreibt einen Parameter in den Preferences.
	 * 
	 * @param key
	 *            Schlüssel
	 * @param value
	 *            neuer Wert; falls <code>null</code>, so wird der Wert aus
	 *            den Preferences entfernt.
	 */
	public void setParam(String key, String value) {
		if (key == null || key.length() == 0)
			return;
		if (value == null) {
			getPreferences(PREFS_PARAMS).remove(key);
			if (rawConfigParams.containsKey(key))
				configParams.put(key, rawConfigParams.get(key));
			else
				configParams.remove(key);
		} else {
			getPreferences(PREFS_PARAMS).put(key, value);
			configParams.put(key, value);
		}
	}
	
	/**
	 * Liefert einen Parameter, der in der Config gesetzt wurde als Liste. Wenn
	 * in der Config nicht das Attribut array gesetzt wurde, wird einfach ein
	 * einelementiger Vector erzeugt.
	 */
	public List getParamAsList(String key) {
		Object value = configParams.get(key);
		if (value instanceof List)
			return (List) value;
		else if (value != null)
			return Collections.singletonList(value);
		else
			return Collections.EMPTY_LIST;
	}
	
	/**
	 * Liefert die Modulbeschreibung
	 * 
	 * @return Modulbeschreibung.
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * Liefert den Pfad des Moduls im lokalen Dateisystem.
	 * 
	 * @return Modulpfad.
	 */
	public File getRealPath() {
		return realPath;
	}
	
	/**
	 * Liefert den Pfad der statischen Inhalte des Moduls im Web.
	 * 
	 * @return WebPfad.
	 */
	public String getWebPathStatic() {
		String webPath = getName();
		if (webPath != null) {
			webPath = webPath.trim();
			if (!webPath.startsWith("/"))
				webPath = '/' + webPath;
			if (!webPath.endsWith("/"))
				webPath += '/';
		}
		return webPath;
	}
	
	/**
	 * Liefert den Modulnamen.
	 * 
	 * @return Modulname.
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Liefert den ClassLoader für das Modul.
	 * 
	 * @return Modul-ClassLoader.
	 */
	public ClassLoader getClassLoader() {
		if (classLoader == null) {
			logger.debug("ClassLoader-Erstellung für das Modul '" + getName() + "'.");
			
			Set urlSet = new LinkedHashSet();
			if (getRealPath().exists()) {
				appendClasses(urlSet, new File(getRealPath(), "../WEB-INF/classes"));
				appendJars(urlSet, new File(getRealPath(), "../WEB-INF/lib"));
				appendClasses(urlSet, new File(getRealPath(), "../OCTOPUS/classes"));
				appendJars(urlSet, new File(getRealPath(), "../OCTOPUS/lib"));
			} else {
				logger.warn("Modulverzeichnis '" + getRealPath() + "' des Moduls '" + getName() + "' existiert nicht.");
			}
			
			classLoader = new URLClassLoader((URL[]) urlSet.toArray(new URL[urlSet.size()]), getClass().getClassLoader());
		}
		
		return classLoader;
	}
	
	private void appendClasses(Set urlSet, File classesDir) {
		if (classesDir.exists()) {
			try {
				urlSet.add(classesDir.toURL());
			} catch (MalformedURLException e) {
				logger.warn("Fehler beim Wandeln des Modul-classes-Pfads '" + classesDir + "' in eine URL.");
			}
		}
	}
	
	private void appendJars(Set urlSet, File libDir) {
		if (libDir.exists()) {
			File libs[] = libDir.listFiles();
			if (libs != null) {
				for (int i = 0; i < libs.length; i++)
					try {
						if (libs[i].getName().endsWith(".jar"))
							urlSet.add(libs[i].toURL());
					} catch (MalformedURLException e) {
						logger.warn("Fehler beim Wandeln des Modul-lib-Pfads '" + libs[i] + "' in eine URL.");
					}
			}
		}
	}
	
	/**
	 * Liefert die Liste der Tasks
	 * 
	 * @return TaskList
	 */
	public TaskList getTaskList() {
		return taskList;
	}
	
	/**
	 * Diese Methode liefert einen Preferences-Knoten im Modul-Zweig.
	 * 
	 * @param subNode
	 *            relativer Pfad des Unterknotens. Falls <code>null</code>,
	 *            so wird der Modul-Basisknoten geliefert.
	 * @return ausgewählter Preferences-Knoten
	 */
	public Preferences getPreferences(String subNode) {
		while (subNode != null && subNode.startsWith("/"))
			subNode = subNode.substring(1);
		return subNode == null ? configPrefs : configPrefs.node(subNode);
	}
	
	/**
	 * Diese Methode liefert die Parameter mit Überschreibungen aus den
	 * Preferences.
	 * 
	 * @return manipulierte Modul-Parameter.
	 */
	public Map getParams() {
		return Collections.unmodifiableMap(configParams);
	}
	
	/**
	 * Diese Methode liefert die rohen Parameter, wie sie in der Konfiguration
	 * stehen.
	 * 
	 * @return rohe Modul-Parameter.
	 */
	public Map getRawParams() {
		return Collections.unmodifiableMap(rawConfigParams);
	}
	
	/**
	 * Diese Methode liefert einen DOM-Knoten in der Konfiguration, der nicht zu
	 * den Standardknoten gehört
	 * 
	 * @param nodeName
	 *            Name des Knotens unter moduleConfig.
	 * @return bezeichnetes Element, ggfs <code>null</code>.
	 */
	public Element getOtherNode(String nodeName) {
		return (Element) otherNodes.get(nodeName);
	}
	
	/**
	 * @return Returns the log4jNode.
	 * @deprecated {@link #getOtherNode(String)} benutzen
	 */
	public Element getLog4jNode() {
		return getOtherNode("log4j:configuration");
	}
	
	public String getOnUnauthorizedAction() {
		return getParam(CONFIG_PARAM_ON_UNAUTHORIZED);
	}
	
	/**
	 * Diese Methode überschreibt Werte in einer Map mit Werten in einem
	 * Preferences-Knoten.
	 */
	public final static void override(String context, Map map, Preferences preferences) {
		String[] keys;
		try {
			keys = preferences.keys();
			if (keys != null && keys.length > 0) {
				for (int i = 0; i < keys.length; i++) {
					String key = keys[i];
					String value = preferences.get(key, asString(map.get(key)));
					logger.debug("[" + context + "] Override for " + key + ": " + value);
					map.put(key, value);
				}
			}
		} catch (BackingStoreException e) {
			logger.error("[" + context + "] Preferences-API-Zugriff", e);
		}
	}
	
	public final static String asString(Object o) {
		return o != null ? o.toString() : null;
	}
	
	/**
	 * Diese Methode liefert einen LoginManager für dieses Modul. Liegt hier
	 * keine (ausreichende) LoginManager-Konfiguration vor, so wird
	 * <code>null</code> geliefert.
	 * 
	 * @return LoginManager für das angegebene Modul
	 * @see #getLoginManager()
	 */
	public synchronized LoginManager getLoginManager() throws OctopusSecurityException {
		if (loginManager == null) {
			if (!loginManagerParams.isEmpty()) {
				logger.debug("Login-Manager-Erstellung für das Modul " + getName());
				// Modul möchte seine Config selber machen...
				String loginManagerClassName = (String) loginManagerParams.get("loginManagerClass");
				if (loginManagerClassName != null) {
					logger.debug("Lade LoginManager-Implementierung: " + loginManagerClassName);
					try {
						// Eintrag ist angegeben...
						Class loginManagerClass = getClassLoader().loadClass(loginManagerClassName);
						loginManager = (LoginManager) loginManagerClass.newInstance();
						loginManager.setConfiguration(loginManagerParams);
					} catch (Exception e) {
						logger.error("Fehler beim Laden des LoginManagers.", e);
						throw new OctopusSecurityException(OctopusSecurityException.ERROR_SERVER_AUTH_ERROR, e);
					}
				}
			}
		}
		return loginManager;
	}
	
	/**
	 * Diese Methode erzeug eine neue {@link PersonalConfig}-Instanz. Die
	 * verwendete Klasse kann über den Modul-Parameter
	 * {@link #CONFIG_PARAM_PERSONAL_CONFIG_CLASS} gesetzt werden, Vorgabe ist
	 * {@link PersonalConfigImpl}.
	 * 
	 * @return eine leere {@link PersonalConfig}-Instanz
	 * @throws OctopusConfigException
	 */
	public PersonalConfig createNewConfigImpl() throws OctopusConfigException {
		synchronized (this) {
			if (personalConfigConstructor == null) {
				try {
					String className = getParam(CONFIG_PARAM_PERSONAL_CONFIG_CLASS);
					Class classClass = className != null ? getClassLoader().loadClass(className) : PersonalConfigImpl.class;
					if (!PersonalConfig.class.isAssignableFrom(classClass)) {
						String msg = "Fehler beim Laden des Konstruktors für PersonalConfigs; angegebene Klasse implementiert nicht PersonalConfig.";
						logger.error(msg);
						throw new OctopusConfigException(msg);
					}
					personalConfigConstructor = classClass.getConstructor(new Class[0]);
				} catch (Exception e) {
					logger.error("Fehler beim Laden des Konstruktors für PersonalConfigs.", e);
					throw new OctopusConfigException("Fehler beim Laden des Konstruktors für PersonalConfigs.", e);
				}
			}
		}
		try {
			return (PersonalConfig) personalConfigConstructor.newInstance(new Object[0]);
		} catch (Exception e) {
			logger.error("Fehler beim Ausführen des Konstruktors für PersonalConfigs.", e);
			throw new OctopusConfigException("Fehler beim Ausführen des Konstruktors für PersonalConfigs.", e);
		}
	}
	
	/**
	 * Returns <code>true</code> when the system property
	 * octopus.PREFS_IS_IN_TEST_ENVIRONMENT has value 'true'. Returns
	 * <code>true</code> when the module configuration parameter
	 * PREFS_IS_IN_TEST_ENVIRONMENT has value 'true'. Otherwise returns
	 * <code>false</code>
	 * 
	 * @return <code>true</code> or <code>false</code>
	 */
	public boolean isInTestEnvironment() {
		// read the system property
		String isInTestEnvironment = System.getProperty("octopus." + PREFS_IS_IN_TEST_ENVIRONMENT);
		if (isInTestEnvironment == null)
			// read the configuration parameter from the config file of the
			// module
			isInTestEnvironment = getParam(PREFS_IS_IN_TEST_ENVIRONMENT);
		if (isInTestEnvironment != null && isInTestEnvironment.equals("true"))
			return true;
		return false;
	}
	
	/**
	 * Returns <code>true</code> when the module configuration parameter
	 * PREFS_PERSISTS_REQUESTS has value 'true'. Otherwise returns
	 * <code>false</code>.
	 * 
	 * @return <code>true</code> or <code>false</code>
	 */
	public boolean persistsRequests() {
		String persistsRequests = getParam(PREFS_PERSISTS_REQUESTS);
		if (persistsRequests != null && persistsRequests.equals("true"))
			return true;
		return false;
	}
	
	/**
	 * Returns the instance of the {@link PersistentRequestWorker}.
	 * 
	 * @return the {@link PersistentRequestWorker}
	 */
	public PersistentRequestWorker getPersistentRequestWorker() {
		return persistentRequestWorker;
	}

	/**
	 * Returns the instance of the {@link ResponseCreator}.
	 * 
	 * @return the {@link ResponseCreator}
	 */
	public ResponseCreator getResponseCreator() {
		return responseCreator;
	}

	/**
	 * Liefert den ContentType, der gesetzt werden soll, wenn nichts anderes
	 * angegeben ist.
	 */
	public String getDefaultContentType() {
		String result = getParam(OctopusEnvironment.KEY_DEFAULT_CONTENT_TYPE);
//		if (result == null || result.length() == 0)
//			result = octopusEnvironment.get(OctopusEnvironment.KEY_DEFAULT_CONTENT_TYPE);
		return result;
	}

	/**
	 * Verzeichnis, indem die Templates für ein bestimmtes Modul liegen. Relativ
	 * zum Rootverzeichnis der Module
	 * 
	 * @return Verzeichnisname mit abschließendem /
	 */
	public Object getRelativeWebRootPath() {
		return getParam(OctopusEnvironment.KEY_PATHS_TEMPLATE_ROOT);
	}

	/**
	 * Liefert das Default-Encoding z.B. für Velocity-Templates
	 */
	public String getDefaultEncoding() {
		String result = getParam(OctopusEnvironment.KEY_DEFAULT_ENCODING);
//		if (result == null || result.length() == 0)
//			result = octopusEnvironment.get(OctopusEnvironment.KEY_DEFAULT_ENCODING);
		if (result == null || result.length() == 0) {
			if (logger.isWarnEnabled())
				logger.warn("No default encoding for module \"" + name + "\" found. Setting encoding to UTF-8.");
			setParam(OctopusEnvironment.KEY_DEFAULT_ENCODING, result = "UTF-8");
		}
		return result;
	}

	/**
	 * Verzeichnis, indem die Templates für ein bestimmtes Modul liegen.
	 * 
	 * @return Verzeichnis
	 */
	public File getTemplateRootPath() {
		if (getParam(OctopusEnvironment.KEY_PATHS_TEMPLATE_ROOT) == null || getParam(OctopusEnvironment.KEY_PATHS_TEMPLATE_ROOT).length() == 0)
			return getRealPath();
		else
			return new File(getRealPath(), getParam(OctopusEnvironment.KEY_PATHS_TEMPLATE_ROOT));
	}
}
