/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.octopus.data;

import de.tarent.octopus.util.RootCauseException;

/**
 * Kann benutzt werden um Fehler, die wärend des Datenzugriffes auftreten weiter
 * zu geben.
 * 
 * @author <a href="mailto:mancke@mancke-software.de">Sebastian Mancke</a>,
 *         <b>tarent GmbH</b>
 */
public class DataAccessException extends Exception implements RootCauseException {
	/**
	 * serialVersionUID = 2926994428165573435L
	 */
	private static final long serialVersionUID = 2926994428165573435L;
	
	/**
	 * Expection, die der eigentliche Grund ist.
	 */
	Throwable rootCause;
	
	public DataAccessException(String message) {
		super(message);
	}
	
	public DataAccessException(String message, Throwable rootCause) {
		super(message, rootCause);
		this.rootCause = rootCause;
	}
	
	public Throwable getRootCause() {
		return rootCause;
	}
}
