/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.octopus.embedded;

import de.tarent.octopus.request.OctopusSession;
import java.util.Enumeration;
import java.util.Hashtable;

/**
 * Stellt ein Sessionobjekt eines direkten Aufrufes dar.
 * 
 * @author <a href="mailto:mancke@mancke-software.de">Sebastian Mancke</a>,
 *         <b>tarent GmbH</b>
 */
public class DirectCallSession implements OctopusSession {
	
	Hashtable attributes = new Hashtable();
	long creationTime;
	String id;
	
	public DirectCallSession() {
		this.creationTime = System.currentTimeMillis();
		this.id = (new StringBuffer(Long.toHexString(System.currentTimeMillis())).reverse()).toString();
	}
	
	public Object getAttribute(String name) {
		return attributes.get(name);
	}
	
	public Enumeration getAttributeNames() {
		return attributes.keys();
	}
	
	public void removeAttribute(String name) {
		attributes.remove(name);
	}
	
	public void setAttribute(String name, Object value) {
		attributes.put(name, value);
	}
	
	
	public long getCreationTime() {
		return creationTime;
	}
	
	public String getId() {
		return id;
	}
	
	public long getLastAccessedTime() {
		return -1;
	}
	
	public int getMaxInactiveInterval() {
		return -1;
	}
	
	public void invalidate() {
		// do nothing
	}
	
	public boolean isNew() {
		return false;
	}
	
	public void setMaxInactiveInterval(int interval) {
		// do nothing
	}
}
