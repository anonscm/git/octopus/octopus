/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.octopus.response.soap;

import java.util.Iterator;
import java.util.Map;

import javax.xml.namespace.QName;
import javax.xml.rpc.ParameterMode;

import org.apache.axis.Message;
import org.apache.axis.client.Call;
import org.apache.axis.client.Service;
import org.apache.axis.encoding.XMLType;
import org.apache.commons.logging.Log;

import de.tarent.octopus.config.CommonConfig;
import de.tarent.octopus.config.ModuleConfig;
import de.tarent.octopus.config.OctopusEnvironment;
import de.tarent.octopus.logging.LogFactory;
import de.tarent.octopus.request.OctopusResponse;
import de.tarent.octopus.resource.Resources;
import de.tarent.octopus.response.AbstractResponseEngine;
import de.tarent.octopus.response.ResponseDescription;
import de.tarent.octopus.response.ResponseEngine;
import de.tarent.octopus.response.ResponseProcessingException;
import de.tarent.octopus.server.OctopusContext;

/**
 * Diese Klasse gibt die ausgewählten Felder eines OctopusContent Objekte als
 * SOAP Nachricht zurück.
 * 
 * @author <a href="mailto:mancke@mancke-software.de">Sebastian Mancke</a>,
 *         <b>tarent GmbH</b>
 */
public class SoapResponseEngine extends AbstractResponseEngine implements ResponseEngine {
	/** Der Logger */
	private static Log logger = LogFactory.getLog(CommonConfig.class);
	
	private SoapEngine engine;
	
	public String getDefaultName() {
		return "soap";
	}

	public String getDefaultContentType() {
		// Disabled because browser does not like this and download response than.
//		return "application/soap+xml";
		return "text/xml";
	}
	
	public void init(ModuleConfig moduleConfig) {
		engine = new SoapEngine(moduleConfig);
	}
	
	public void sendEmptyResponse(ResponseDescription desc, OctopusContext octopusContext, OctopusResponse octopusResponse) throws ResponseProcessingException {
		String namespace = desc.getAsString("responseParams.Namespace");
		if (namespace == null)
			namespace = engine.getNamespaceByModuleName(octopusResponse.getModuleName());
		
		String callingMethodName = octopusResponse.getTaskName();
		SoapResponse rpcResponse = new SoapResponse(engine, namespace, callingMethodName);
		
		try {
			rpcResponse.addParam("Status", "ok");
			
			logger.debug("Gebe SOAP Message aus. Antwort auf Methode:" + callingMethodName);
			rpcResponse.writeTo(octopusResponse.getOutputStream());
			octopusResponse.close();
		} catch (Exception e) {
			logger.error("Versuche, eine SOAP-Fault auszugeben.", e);
			
			SoapException soapException;
			if (octopusContext.moduleConfig().getParam(OctopusEnvironment.KEY_RESPONSE_ERROR_LEVEL).equals(OctopusEnvironment.VALUE_RESPONSE_ERROR_LEVEL_DEVELOPMENT))
				soapException = new SoapException(e);
			else
				soapException = new SoapException(Resources.getInstance().get("ERROR_MESSAGE_GENERAL_ERROR"));
			
			try {
				soapException.writeTo(octopusResponse.getOutputStream());
				octopusResponse.close();
			} catch (Exception e2) {
				logger.error("Es konnte auch keine SOAP Fehlermeldung ausgegeben werden. Schmeiße jetzt einfach eine Exception.", e2);
				throw new ResponseProcessingException("Es ist Fehler bei der Formatierung der Ausgabe ausgetreten.", e);
			}
		}
		
	}
	
	public void sendNormalResponse(ResponseDescription desc, OctopusContext octopusContext, OctopusResponse octopusResponse) throws ResponseProcessingException {
		String namespace = desc.getAsString("responseParams.Namespace");
		if (namespace == null)
			namespace = engine.getNamespaceByModuleName(octopusResponse.getModuleName());
		
		String callingMethodName = octopusResponse.getTaskName();
		SoapResponse rpcResponse = new SoapResponse(engine, namespace, callingMethodName);
		
		try {
			Map outputFields = getOutputFields(desc, octopusContext, octopusResponse);
			
			if (outputFields != null) {
				for (Iterator iter = outputFields.entrySet().iterator(); iter.hasNext();) {
					Map.Entry entry = (Map.Entry) iter.next();
					rpcResponse.addParam((String) entry.getKey(), entry.getValue());
				}
			}
			
			logger.debug("Gebe SOAP Message aus. Antwort auf Methode:" + callingMethodName);
			rpcResponse.writeTo(octopusResponse.getOutputStream());
			octopusResponse.close();
		} catch (Exception e) {
			logger.error("Versuche, eine SOAP-Fault auszugeben.", e);
			
			String errorLevel = octopusContext.moduleConfig().getParam(OctopusEnvironment.KEY_RESPONSE_ERROR_LEVEL);
			SoapException soapException;
			if (errorLevel != null && errorLevel.equals(OctopusEnvironment.VALUE_RESPONSE_ERROR_LEVEL_DEVELOPMENT))
				soapException = new SoapException(e);
			else
				soapException = new SoapException(Resources.getInstance().get("ERROR_MESSAGE_GENERAL_ERROR"));
			
			try {
				soapException.writeTo(octopusResponse.getOutputStream());
				octopusResponse.close();
			} catch (Exception e2) {
				logger.error("Es konnte auch keine SOAP Fehlermeldung ausgegeben werden. Schmeiße jetzt einfach eine Exception.", e2);
				throw new ResponseProcessingException("Es ist Fehler bei der Formatierung der Ausgabe ausgetreten.", e);
			}
		}
	}
	
	public void sendErrorResponse(ResponseDescription desc, OctopusContext octopusContext, OctopusResponse octopusResponse, String message, Exception exception) throws ResponseProcessingException {
		String namespace = desc.getAsString("responseParams.Namespace");
		if (namespace == null)
			namespace = engine.getNamespaceByModuleName(octopusResponse.getModuleName());
		
		String callingMethodName = octopusResponse.getTaskName();
		SoapException soapException;
		if (exception != null)
			soapException = new SoapException(exception);
		else
			soapException = new SoapException(message);
		
		try {
			
			logger.debug("Gebe SOAP Error aus. Antwort auf Methode:" + callingMethodName);
			soapException.writeTo(octopusResponse.getOutputStream());
			octopusResponse.close();
		} catch (Exception e) {
			logger.error("Versuche, eine SOAP-Fault auszugeben.", e);
			
			String errorLevel = octopusContext.moduleConfig().getParam(OctopusEnvironment.KEY_RESPONSE_ERROR_LEVEL);
			if (errorLevel != null && errorLevel.equals(OctopusEnvironment.VALUE_RESPONSE_ERROR_LEVEL_DEVELOPMENT))
				soapException = new SoapException(e);
			else
				soapException = new SoapException(Resources.getInstance().get("ERROR_MESSAGE_GENERAL_ERROR"));
			
			try {
				soapException.writeTo(octopusResponse.getOutputStream());
				octopusResponse.close();
			} catch (Exception e2) {
				logger.error("Es konnte auch keine SOAP Fehlermeldung ausgegeben werden. Schmeiße jetzt einfach eine Exception.", e2);
				throw new ResponseProcessingException("Es ist Fehler bei der Formatierung der Ausgabe ausgetreten.", e);
			}
		}
	}
	
	public void sendEmptyCallback(ResponseDescription desc, OctopusContext octopusContext, OctopusResponse octopusResponse) throws ResponseProcessingException {
		sendEmptyResponse(desc, octopusContext, octopusResponse);
	}

	public void sendNormalCallback(ResponseDescription desc, OctopusContext octopusContext, OctopusResponse octopusResponse) throws ResponseProcessingException {
		Service webService = new Service();
		Call call = null;
		try {
			call = (Call) webService.createCall();
			// set the callback address
			call.setTargetEndpointAddress(octopusContext.getRequestObject().getHeaders().getReplyToAddress());
			call.setMaintainSession(false);
			call.setReturnType(XMLType.XSD_ANY);
			
			String namespace = octopusContext.getContentObject().getAsString("responseParams.Namespace");
			if (namespace == null)
				namespace = engine.getNamespaceByModuleName(octopusResponse.getModuleName());
			
			call.setOperationName(new QName(namespace, octopusResponse.getTaskName() + "Request"));
			
			Map outputFields = getOutputFields(desc, octopusContext, octopusResponse);
			Object[] parameters = new Object[outputFields.size()];
			int i = 0;
			for (Iterator iter = outputFields.keySet().iterator(); iter.hasNext();) {
				String fieldNameOutput = (String) iter.next();
				String fieldNameContent = (String) outputFields.get(fieldNameOutput);
				call.addParameter("id", XMLType.XSD_ANY, ParameterMode.IN);
				parameters[i] = octopusContext.getContentObject().getAsObject(fieldNameContent);
			}
			
			call.invoke(parameters);
		} catch (Exception e) {
			logger.error("Versuche, eine SOAP-Fault auszugeben.", e);
			
			SoapException soapException;
			if (octopusContext.moduleConfig().getParam(OctopusEnvironment.KEY_RESPONSE_ERROR_LEVEL).equals(OctopusEnvironment.VALUE_RESPONSE_ERROR_LEVEL_DEVELOPMENT))
				soapException = new SoapException(e);
			else
				soapException = new SoapException(Resources.getInstance().get("ERROR_MESSAGE_GENERAL_ERROR"));
			
			try {
				if (call != null)
					call.invoke(new Message(soapException.getAxisFault()));
				throw new NullPointerException("Call instance is null.");
			} catch (Exception e2) {
				logger.error("Es konnte auch keine SOAP Fehlermeldung ausgegeben werden. Schmeiße jetzt einfach eine Exception.", e2);
				throw new ResponseProcessingException("Es ist Fehler bei der Formatierung der Ausgabe ausgetreten.", e);
			}
		}
	}
	
	public void sendErrorCallback(ResponseDescription desc, OctopusContext octopusContext, OctopusResponse octopusResponse, String message, Exception exception) throws ResponseProcessingException {
		sendErrorResponse(desc, octopusContext, octopusResponse, message, exception);
	}
	
	/**
	 * Diese Methode gibt eine Ausnahme als SOAP-Fault zurück.
	 * 
	 * @param requestID
	 *            die ID der Anfrage
	 * @param e
	 *            eine Exception
	 */
	/*
	private void printSoapException(String requestID, Exception e) {
		SoapException soapException;
		
		if (e instanceof ResponseProcessingException && ((ResponseProcessingException) e).getRootCause() instanceof Exception)
			e = (Exception) ((ResponseProcessingException) e).getRootCause();
		else if (e instanceof ContentProzessException && ((ContentProzessException) e).getCause() instanceof Exception)
			e = (Exception) ((ContentProzessException) e).getCause();
		
		// SoapException and OctopusSecurityException may pass in each case
		// other exceptions should only come out, if we are not in release-mode
		if (e instanceof SoapException)
			soapException = (SoapException) e;
		else if (e instanceof OctopusSecurityException)
			soapException = new SoapException(e);
		else {
			if (isErrorLevelRelease())
				soapException = new SoapException(Resources.getInstance().get("ERROR_MESSAGE_GENERAL_ERROR"));
			else
				soapException = new SoapException(e);
		}
		
		response.setContentType(HttpHelper.CONTENT_TYPE_XML);
		
		try {
			soapException.writeTo(outputStream);
		} catch (Exception e2) {
			logger.error(Resources.getInstance().get("RESPONSE_LOG_FAULT_OUT_EXCEPTION", requestID), e2);
			printSimpleSOAPFault(requestID, e2);
		}
	}
	*/
	
	/**
	 * Diese Methode gibt eine einfache SOAP-Fehlermeldung zurück.
	 * 
	 * @param requestID
	 *            die ID der Anfrage
	 * @param e
	 *            eine Exception
	 */
	/*
	private void printSimpleSOAPFault(String requestID, Exception e) {
		response.setContentType(HttpHelper.CONTENT_TYPE_XML);
		
		if (isErrorLevelRelease()) {
			println(Resources.getInstance().get("RESPONSE_OUT_SOAPFAULT_HEAD", Xml.escape(Resources.getInstance().get("ERROR_MESSAGE_GENERAL_ERROR"))));
		} else {
			println(Resources.getInstance().get("RESPONSE_OUT_SOAPFAULT_HEAD", Xml.escape(e.toString())));
			e.printStackTrace(writer);
			if (e instanceof RootCauseException) {
				Throwable rootCause = ((RootCauseException) e).getRootCause();
				if (rootCause != null) {
					println(Resources.getInstance().get("RESPONSE_OUT_SOAPFAULT_ROOT", Xml.escape(rootCause.toString())));
					rootCause.printStackTrace(writer);
				}
			}
		}
		println(Resources.getInstance().get("RESPONSE_OUT_SOAPFAULT_TAIL", requestID));
	}
	*/
}
