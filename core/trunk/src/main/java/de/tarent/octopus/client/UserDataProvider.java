/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.octopus.client;

/**
 * Liefert auf Nachfrage die Benutzerdaten, die z.B. über einen Dialog
 * angefordert werden können.
 */
public interface UserDataProvider {
	

	/**
	 * Fordert die Benutzerdaten an (z.B. über einen Login-Dialog);
	 * 
	 * @param message
	 *            Nachricht, die einem Benutzer gezeigt werden kann.
	 * @param usernamePreselection
	 *            Vorbelegung des Benutzernamens, darf null sein.
	 * @return true, wenn die Daten vorliegen, false sonst (z.B. bei Abbruch des
	 *         Dialogs).
	 */
	public boolean requestUserData(String message, String usernamePreselection);
	
	/**
	 * @return Liefert den bereit gestellten Benutzernamen
	 */
	public String getUsername();
	
	/**
	 * @return Liefert das bereit gestellte Passwort
	 */
	public String getPassword();
	
}
