/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

/* $Id: OctopusInvoker.java,v 1.4 2007/06/11 13:05:38 christoph Exp $
 * tarent-octopus, Webservice Data Integrator and Applicationserver
 * Copyright (C) 2002 tarent GmbH
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus'
 * (which makes passes at compilers) written
 * by Sebastian Mancke and Michael Klink.
 * signature of Elmar Geese, 1 June 2002
 * Elmar Geese, CEO tarent GmbH
 */

package de.tarent.octopus.client;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import de.tarent.octopus.resource.Resources;

/**
 * This static helper class wraps octopus calling mechanismus.
 * 
 * @author Christoph Jerolimov
 */
public class OctopusInvoker {
	/**
	 * Private OctopusInvoker constructor.
	 */
	private OctopusInvoker() {
		// no instance of this class allowed yet.
	}
	
	/**
	 * This method invoke an octopus task in the local octopus classpath
	 * installation. Use the given parameter for select a module and task and
	 * deliver the parameters.
	 * 
	 * The result of the method is null if the task will return nothing. If only
	 * one result object is given it will be returned as unpacked instance. As
	 * map otherwise.
	 * 
	 * @param module
	 * @param task
	 * @param parameters
	 * @return null or octopus result
	 */
	public static Object invoke(String module, String task, Map parameters) {
		return invoke(module, task, (String[]) parameters.keySet().toArray(new String[parameters.size()]), parameters.values().toArray());
	}
	
	/**
	 * This method invoke an octopus task in the local octopus classpath
	 * installation. Use the given parameter for select a module and task and
	 * deliver the parameters.
	 * 
	 * The result of the method is null if the task will return nothing. If only
	 * one result object is given it will be returned as unpacked instance. As
	 * map otherwise.
	 * 
	 * @param module
	 * @param task
	 * @param keys
	 * @param values
	 * @return null or octopus result
	 */
	public static Object invoke(String module, String task, Collection keys, Collection values) {
		return invoke(module, task, (String[]) keys.toArray(new String[keys.size()]), values.toArray());
	}
	
	/**
	 * This method invoke an octopus task in the local octopus classpath
	 * installation. Use the given parameter for select a module and task and
	 * deliver the parameters.
	 * 
	 * The result of the method is null if the task will return nothing. If only
	 * one result object is given it will be returned as unpacked instance. As
	 * map otherwise.
	 * 
	 * @param module
	 * @param task
	 * @param keys
	 * @param values
	 * @return null or octopus result
	 */
	public static Object invoke(String module, String task, String keys[], Object values[]) {
		// MethodCall logging
		// MethodCall methodCall = ThreadLogger.logMethodCall();
		// methodCall.addParameter("module", module);
		// methodCall.addParameter("task", task);
		// methodCall.addParameter("parameterKeys", parameterKeys);
		// methodCall.addParameter("parameterValues", parameterValues);
		
		// Verify input parameters.
		List parameterKeys = keys != null ? Arrays.asList(keys) : Collections.EMPTY_LIST;
		List parameterValues = keys != null ? Arrays.asList(values) : Collections.EMPTY_LIST;
		if (parameterKeys.size() != parameterValues.size())
			throw new RuntimeException(Resources.getInstance().get("REQUESTPROXY_OUT_PROCESSING_EXCEPTION", module, task));
		
		// Set up octopus connection to use internal calls.
		// TODO Make this only one times or use a property?
		OctopusConnectionFactory ocf = OctopusConnectionFactory.getInstance();
		Map configuration = Collections.singletonMap(OctopusConnectionFactory.CONNECTION_TYPE_KEY, OctopusConnectionFactory.CONNECTION_TYPE_INTERNAL);
		ocf.setConfiguration(module, configuration);
		
		// Get octopus connection for module.
		OctopusConnection oc = ocf.getConnection(module);
		if (oc == null)
			throw new RuntimeException(Resources.getInstance().get("INTERNAL_CALL_NO_OCTOPUS_MODULE", module));
		
		// Get octopus task for taskname.
		OctopusTask ot = oc.getTask(task);
		if (ot == null)
			throw new RuntimeException(Resources.getInstance().get("INTERNAL_CALL_NO_OCTOPUS_TASK", module, task));
		
		// Set the parameters.
		// FIXME This module parameter is imo stupid because is already known by
		// ot?
		// Fix this in OctopusConnection/OctopusTask/... and remove it here.
		ot.add("module", module);
		
		Iterator itKeys = parameterKeys.iterator();
		Iterator itValues = parameterValues.iterator();
		
		while (itKeys.hasNext() && itValues.hasNext()) {
			Object key = itKeys.next();
			if (key != null)
				ot.add(key.toString(), itValues.next());
		}
		
		// Invoke octopus task.
		OctopusResult or = ot.invoke();
		if (or == null)
			throw new RuntimeException(Resources.getInstance().get("INTERNAL_CALL_NO_OCTOPUS_RESULT", module, task));
		
		// Transform data and make it more simple to use them.
		Map data = new LinkedHashMap();
		for (Iterator it = or.getDataKeys(); it.hasNext();) {
			String key = (String) it.next();
			data.put(key, or.getData(key));
		}
		
		// methodCall.addVariable("data", data);
		
		if (data.size() == 0)
			return null;
		else if (data.size() == 1)
			return data.values().toArray()[0];
		else
			return data;
	}
}
