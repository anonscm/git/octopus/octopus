/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.octopus.response;

import de.tarent.octopus.util.RootCauseException;

/**
 * Für Fehler, die wärend der Ausgabeverarbeitung auf treten.
 * 
 * @author <a href="mailto:H.Helwich@tarent.de">Hendrik Helwich</a>, tarent
 *         GmbH
 * @author <a href="mailto:mancke@mancke-software.de">Sebastian Mancke</a>,
 *         tarent GmbH
 */
public class ResponseProcessingException extends Exception implements RootCauseException {
	/**
	 * serialVersionUID = -1283546477850825557L
	 */
	private static final long serialVersionUID = -1283546477850825557L;
	
	/**
	 * Expection, die der eigentliche Grund ist.
	 */
	Throwable rootCause;
	
	public ResponseProcessingException(String message) {
		super(message);
	}
	
	public ResponseProcessingException(String message, Throwable rootCause) {
		super(message, rootCause);
		this.rootCause = rootCause;
	}
	
	public Throwable getRootCause() {
		return rootCause;
	}
}
