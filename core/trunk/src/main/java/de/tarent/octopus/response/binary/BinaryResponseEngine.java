/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.octopus.response.binary;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Reader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.apache.commons.logging.Log;

import de.tarent.octopus.config.CommonConfig;
import de.tarent.octopus.config.ModuleConfig;
import de.tarent.octopus.logging.LogFactory;
import de.tarent.octopus.request.OctopusRequest;
import de.tarent.octopus.request.OctopusResponse;
import de.tarent.octopus.resource.Resources;
import de.tarent.octopus.response.AbstractResponseEngine;
import de.tarent.octopus.response.ResponseDescription;
import de.tarent.octopus.response.ResponseEngine;
import de.tarent.octopus.response.ResponseProcessingException;
import de.tarent.octopus.server.OctopusContext;

/**
 * This class return a stream from the filesystem or the octopus context.
 * 
 * The parameter must be defined in an map whose key context-key is defined as
 * the name attribute of the repoonse tag.
 * 
 * The input type must be a {@link Map} (with the <code>PARAM_*</code> keys, a
 * {@link File}, a {@link String} (which point on a file), an
 * {@link InputStream} or a {@link Reader}.
 * 
 * @author <a href="mailto:c.jerolimov@tarent.de">Christoph Jerolimov</a>,
 *         tarent GmbH
 */
public class BinaryResponseEngine extends AbstractResponseEngine implements ResponseEngine {
	/**
	 * Param for a map response, define the response type, see
	 * {@link #BINARY_RESPONSE_TYPE_STREAM} and
	 * {@link #BINARY_RESPONSE_TYPE_LOCALFILE}.
	 */
	public static final String PARAM_TYPE = "type";
	/** Param for a map response, must contain the filename. */
	public static final String PARAM_FILENAME = "filename";
	/**
	 * Param for a map response, may cotain a last modified header date (as
	 * string).
	 */
	public static final String PARAM_FILEDATE = "date";
	/** Param for a map response, may contain the octopusContent-type. */
	public static final String PARAM_MIMETYPE = "mimetype";
	/**
	 * Param for a map response, must contain a readable stream if type is
	 * stream.
	 */
	public static final String PARAM_STREAM = "stream";
	/**
	 * Param for a map response, may contain <code>true</code> if this is a
	 * download.
	 */
	public static final String PARAM_IS_ATTACHMENT = "isAttachment";
	/**
	 * Param for a map response, may contain the name suggested for the
	 * downloaded file, if it is an attachment. As default, the local name of
	 * the PARAM_FILENAME will be used.
	 */
	public static final String PARAM_ATTACHMENT_FILENAME = "attachmentFilename";
	
	/**
	 * Param for a map response, may contain a Runnable, that is executed, when
	 * the BinaryResponse succeeds, meaning that writing data to the respone's
	 * outputstream finished without any IOExceptions.
	 */
	public static final String PARAM_RUNNABLE_SUCCEED = "succeededRunnable";
	
	/**
	 * Param for a map response, may contain a Runnable, that is executed, when
	 * the BinaryResponse fails. i.e. the user cancels the download, or other
	 * network problems occur and an IOException is thrown.
	 */
	public static final String PARAM_RUNNABLE_FAIL = "failedRunnable";
	
	/** Value for {@link #PARAM_TYPE}, will return a stream. */
	public static final String BINARY_RESPONSE_TYPE_STREAM = "stream";
	/** Value for {@link #PARAM_TYPE}, will return a local file. */
	public static final String BINARY_RESPONSE_TYPE_LOCALFILE = "localfile";
	
	/** HTTP header timestamp for the last modification date. */
	public static final DateFormat HTTP_DEFAULT_TIMESTAMP = new SimpleDateFormat("dd MMM yyyy HH:mm:ss");
	/** HTTP header octopusContent type for unknown octopusContent types. */
	public static final String HTTP_DETAULT_MIMETYPE = "application/octet-stream";
	
	/** java util logger */
	private Log logger = LogFactory.getLog(CommonConfig.class);
	
	public String getDefaultName() {
		return "binary";
	}

	public String getDefaultContentType() {
		return HTTP_DETAULT_MIMETYPE;
	}
	
	public void init(ModuleConfig moduleConfig) {
	}

	public void sendEmptyResponse(ResponseDescription desc, OctopusContext octopusContext, OctopusResponse octopusResponse) {
	}
	
	public void sendNormalResponse(ResponseDescription desc, OctopusContext octopusContext, OctopusResponse octopusResponse) throws ResponseProcessingException {
		try {
			Object data = null;
			String binarySourceName = desc.getAsString("binarySource");
			if (binarySourceName != null && !binarySourceName.trim().equals(""))
				data = octopusContext.contentAsObject(binarySourceName);
			
			if (data instanceof File) {
				File file = (File) data;
				octopusResponse.setContentType(getContentString(file.getName(), null));
				processFile(octopusContext.getRequestObject(), octopusResponse, file, file.getName(), null);
			} else if (data instanceof String) {
				File file = new File((String) data);
				octopusResponse.setContentType(getContentString(file.getName(), null));
				processFile(octopusContext.getRequestObject(), octopusResponse, file, file.getName(), null);
			} else if (data instanceof Map) {
				Map info = (Map) data;
				String type = (String) info.get(PARAM_TYPE);
				String filename = (String) info.get(PARAM_FILENAME);
				String filedate = (String) info.get(PARAM_FILEDATE);
				String mimetype = (String) info.get(PARAM_MIMETYPE);
				Runnable succeededRunnable = (Runnable) info.get(PARAM_RUNNABLE_SUCCEED);
				Runnable failedRunnable = (Runnable) info.get(PARAM_RUNNABLE_FAIL);
				
				String attachmentFilename = (String) info.get(PARAM_ATTACHMENT_FILENAME);
				if (attachmentFilename == null && filename != null) {
					attachmentFilename = new File(filename).getName();
				}
				
				if (info.get(PARAM_IS_ATTACHMENT) != null && Boolean.valueOf(info.get(PARAM_IS_ATTACHMENT).toString()).booleanValue()) {
					octopusResponse.setHeader("OctopusContent-Disposition", "attachment" + (attachmentFilename != null ? "; filename=\"" + attachmentFilename + "\"" : ""));
				}
				try {
					octopusResponse.setContentType(getContentString(attachmentFilename, mimetype));
					if (BINARY_RESPONSE_TYPE_STREAM.equals(type)) {
						processStream(octopusResponse, info.get(PARAM_STREAM));
					} else if (BINARY_RESPONSE_TYPE_LOCALFILE.equals(type)) {
						File file = new File(filename);
						processFile(octopusContext.getRequestObject(), octopusResponse, file, attachmentFilename, filedate);
					}
				} catch (IOException ioe) {
					// if an IOExcption occurs, the response failed.
					if (failedRunnable != null)
						failedRunnable.run();
					throw ioe;
				}
				// when arriving here, the response succeeded.
				if (succeededRunnable != null)
					succeededRunnable.run();
				
			} else {
				octopusResponse.setContentType(getContentString(null, null));
				processStream(octopusResponse, data);
			}
		} catch (Exception e) {
			logger.error(Resources.getInstance().get("BINARYRESPONSE_LOG_ERROR", octopusContext.getRequestObject().getRequestID()), e);
			throw new ResponseProcessingException(Resources.getInstance().get("BINARYRESPONSE_EXC_ERROR"), e);
		} finally {
			try {
				if (octopusResponse != null)
					octopusResponse.close();
			} catch (IOException e) {
				logger.error(Resources.getInstance().get("BINARYRESPONSE_LOG_ERROR", octopusContext.getRequestObject().getRequestID()), e);
			}
		}
	}

	public void sendErrorResponse(ResponseDescription desc, OctopusContext octopusContext, OctopusResponse octopusResponse, String message, Exception exception) throws ResponseProcessingException {
		// FIXME NICE TO HAVE send HTTP 4xx or 5xx state here
	}
	
	public void sendEmptyCallback(ResponseDescription desc, OctopusContext octopusContext, OctopusResponse octopusResponse) throws ResponseProcessingException {
		sendEmptyResponse(desc, octopusContext, octopusResponse);
	}
	
	public void sendNormalCallback(ResponseDescription desc, OctopusContext octopusContext, OctopusResponse octopusResponse) throws ResponseProcessingException {
		sendNormalResponse(desc, octopusContext, octopusResponse);
	}
	
	public void sendErrorCallback(ResponseDescription desc, OctopusContext octopusContext, OctopusResponse octopusResponse, String message, Exception exception) throws ResponseProcessingException {
		sendErrorResponse(desc, octopusContext, octopusResponse, message, exception);
	}
	
	/**
	 * Process a file stream.
	 * 
	 * @param octopusRequest
	 * @param octopusResponse
	 * @param file
	 * @param filename
	 * @param filedate
	 * @throws IOException
	 * @throws ParseException
	 */
	protected void processFile(OctopusRequest octopusRequest, OctopusResponse octopusResponse, File file, String filename, String filedate) throws IOException, ParseException {
		if (file.exists() && file.isFile()) {
			// send only if file was modified
			Date dateFileIsModified = new Date(file.lastModified() / 1000 * 1000);
			Date dateBrowserSend = (filedate == null ? null : HTTP_DEFAULT_TIMESTAMP.parse(filedate));
			String dateFileString = HTTP_DEFAULT_TIMESTAMP.format(dateFileIsModified);
			
			if (dateBrowserSend == null || dateFileIsModified.after(dateBrowserSend)) {
				// set header
				octopusResponse.setHeader("Pragma", null);
				octopusResponse.setHeader("Date", dateFileString);
				octopusResponse.setHeader("Last-Modified", dateFileString);
				octopusResponse.setHeader("OctopusContent-Length", String.valueOf(file.length()));
				octopusResponse.setContentType(getContentString(file.getName(), null));
				
				// return stream
				processStream(octopusResponse, new FileInputStream(file));
				logger.debug(Resources.getInstance().get("BINARYRESPONSE_LOG_FILE_HANDLED", octopusRequest.getRequestID(), filename));
			} else {
				// return file not modified
				octopusResponse.setHeader("Pragma", null);
				octopusResponse.setHeader("Date", dateFileString);
				octopusResponse.setHeader("Last-Modified", dateFileString);
				octopusResponse.setStatus(304);
				logger.debug(Resources.getInstance().get("BINARYRESPONSE_LOG_NOT_MODIFIED", octopusRequest.getRequestID(), filename));
			}
		} else {
			// if no file found return http status 404
			octopusResponse.setStatus(404);
			logger.warn(Resources.getInstance().get("BINARYRESPONSE_LOG_NOT_FOUND", octopusRequest.getRequestID(), filename));
		}
	}
	
	/**
	 * Pipe the real data stream to the reponse. At the moment it can handle
	 * {@link InputStream} and {@link Reader} instances.
	 * 
	 * @param octopusResponse
	 * @param input
	 * @throws IOException
	 * @throws NullPointerException
	 *             If input is null.
	 * @throws ClassCastException
	 *             If input class is not supported.
	 */
	protected void processStream(OctopusResponse octopusResponse, Object input) throws IOException {
		octopusResponse.flush();
		if (input instanceof InputStream) {
			InputStream is = (InputStream) input;
			OutputStream os = octopusResponse.getOutputStream();
			int size = 0;
			byte b[] = new byte[1024];
			while (size != -1) {
				size = is.read(b);
				if (size != -1)
					os.write(b, 0, size);
			}
			is.close();
			os.close();
		} else if (input instanceof Reader) {
			Reader r = (Reader) input;
			PrintWriter w = octopusResponse.getWriter();
			int size = 0;
			char c[] = new char[1024];
			while (size != -1) {
				size = r.read(c);
				if (size != -1)
					w.write(c, 0, size);
			}
			r.close();
			w.close();
		} else if (input == null) {
			throw new NullPointerException();
		} else {
			throw new ClassCastException();
		}
	}
	
	/**
	 * Create a http-like contenttype from the filename and mimetype.
	 * 
	 * format: mime/type mime/type; name="filename.txt"
	 * 
	 * default: {@link #HTTP_DETAULT_MIMETYPE} without filename
	 */
	private String getContentString(String filename, String mimetype) {
		if (filename == null && mimetype == null) {
			return HTTP_DETAULT_MIMETYPE;
		} else if (filename == null) {
			return mimetype;
		} else if (mimetype == null) {
			return getMimeType(filename) + "; name=\"" + filename + "\"";
		} else {
			return mimetype + "; name=\"" + filename + "\"";
		}
	}
	
	/**
	 * Create some standard file types.
	 * 
	 * @param filename
	 * @return
	 */
	protected String getMimeType(String filename) {
		if (filename.endsWith(".png")) {
			return "image/png";
		} else if (filename.endsWith(".gif")) {
			return "image/gif";
		} else if (filename.endsWith(".jpg")) {
			return "image/jpg";
		} else if (filename.endsWith(".txt")) {
			return "text/plain";
		} else if (filename.endsWith(".css")) {
			return "text/css";
		}
		return HTTP_DETAULT_MIMETYPE;
	}
}
