/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

/*
 * $Id: Threads.java,v 1.3 2007/06/11 13:05:39 christoph Exp $
 * 
 * Created on 04.07.2005
 */
package de.tarent.octopus.util;

/**
 * Diese Klasse stellt Hilfsmethoden für das Multi-Threading zur Verfügung.
 * 
 * @author mikel
 */
public class Threads {
	/**
	 * Diese statische Methode setzt den Kontext-{@link ClassLoader} des
	 * aktuellen {@link Thread}s und gibt den bisherigen zurück.
	 * 
	 * @param newLoader
	 *            der neue {@link ClassLoader}; darf nicht <code>null</code>
	 *            sein.
	 * @return der bisherige {@link ClassLoader}.
	 */
	public static ClassLoader setContextClassLoader(ClassLoader newLoader) {
		// if (newLoader == null)
		// throw new NullPointerException("ClassLoader instance is required.");
		Thread currentThread = Thread.currentThread();
		ClassLoader currentLoader = currentThread.getContextClassLoader();
		currentThread.setContextClassLoader(newLoader);
		return currentLoader;
	}
}
