/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */
package de.tarent.octopus.util;

import java.io.ByteArrayInputStream;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;

/**
 * Tests the Parsing of List Params with nested Maps
 */
public class XMLTest extends junit.framework.TestCase {
	
	/**
	 * Void Constructor for instantiation as worker
	 */
	public XMLTest() {
	}
	
	public XMLTest(String init) {
		super(init);
	}
	
	public void setUp() {
		
	}
	
	public void testNestedValues() throws Exception {
		
		String doc = "<param name=\"nix\" type=\"list\">\n" + "     <value>Hallo</value>\n" + "     <value>Ballo</value>\n" + "</param>\n";
		
		DocumentBuilder parser = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		Document document = parser.parse(new ByteArrayInputStream(doc.getBytes()));
		List l = (List) Xml.getParamValue(document.getDocumentElement());
		
		assertEquals("Size", 2, l.size());
		assertEquals("List Element", "Hallo", l.get(0));
		assertEquals("List Element", "Ballo", l.get(1));
	}
	
	public void testNestedParams() throws Exception {
		
		String doc = "<param name=\"nix\" type=\"list\">\n" + "     <param value=\"Hallo\"/>\n" + "     <param value=\"Ballo\"/>\n" + "     <param type=\"map\">\n" + "        <param name=\"vorname\" value=\"Felix\"/>\n"
				+ "        <param name=\"nachname\" value=\"Mancke\"/>\n" + "     </param>\n" + "</param>\n";
		
		DocumentBuilder parser = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		Document document = parser.parse(new ByteArrayInputStream(doc.getBytes()));
		List l = (List) Xml.getParamValue(document.getDocumentElement());
		
		assertEquals("Size", 3, l.size());
		assertEquals("List Element", "Hallo", l.get(0));
		assertEquals("List Element", "Ballo", l.get(1));
		Map map = (Map) l.get(2);
		assertEquals("Map Size", 2, map.size());
		assertEquals("Map Elements", "Felix", map.get("vorname"));
		assertEquals("Map Elements", "Mancke", map.get("nachname"));
	}
}
