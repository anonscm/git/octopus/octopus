package de.tarent.octopus.response.binary;

import de.tarent.octopus.response.AbstractResponseEngineTest;
import de.tarent.octopus.response.ResponseEngine;
import de.tarent.octopus.response.ResponseProcessingException;

public class BinaryResponseEngineTest extends AbstractResponseEngineTest {
	public ResponseEngine getResponseEngine() {
		return new BinaryResponseEngine();
	}

	public String getResponseEngineName() {
		return "binary";
	}

	public void testInit() throws ResponseProcessingException {
	}

	public void testSendEmptyResponse() throws ResponseProcessingException {
	}

	public void testSendNormalResponse() throws ResponseProcessingException {
	}

	public void testSendErrorResponse() throws ResponseProcessingException {
	}

	public void testSendEmptyCallback() throws ResponseProcessingException {
	}

	public void testSendNormalCallback() throws ResponseProcessingException {
	}

	public void testSendErrorCallback() throws ResponseProcessingException {
	}
}
