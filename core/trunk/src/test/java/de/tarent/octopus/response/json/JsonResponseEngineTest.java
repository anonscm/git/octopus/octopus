package de.tarent.octopus.response.json;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

import de.tarent.octopus.response.AbstractResponseEngine;
import de.tarent.octopus.response.AbstractResponseEngineTest;
import de.tarent.octopus.response.ResponseEngine;
import de.tarent.octopus.response.ResponseProcessingException;

public class JsonResponseEngineTest extends AbstractResponseEngineTest {
	public ResponseEngine getResponseEngine() {
		return new JsonResponseEngine();
	}

	public String getResponseEngineName() {
		return "json";
	}

	public void testInit() throws ResponseProcessingException {
		getResponseEngine().init(moduleConfig);
	}

	public void testSendEmptyResponse() throws ResponseProcessingException {
		assertEquals(
				"null",
				getEmptyResponseString(responseEngine, null, null));
		
		// EMPTY TESTS
		
		Map desc = new LinkedHashMap();
		Map content = new LinkedHashMap();
		assertEquals(
				"null",
				getEmptyResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", "test01");
		assertEquals(
				"null",
				getEmptyResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		// SINGLE OUTPUT TESTS
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", Collections.singleton("test02"));
		assertEquals(
				"null",
				getEmptyResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", Collections.singletonList("test03"));
		assertEquals(
				"null",
				getEmptyResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", Collections.singletonMap("test04a", "test04b"));
		assertEquals(
				"null",
				getEmptyResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", new String[] { "test05a", "test05b" });
		assertEquals(
				"null",
				getEmptyResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		// MULTI OUTPUT TESTS
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, new String[] { "c1", "c2" });
		content.put("c1", "test06a");
		content.put("c2", "test06b");
		assertEquals(
				"null",
				getEmptyResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, new String[] { "c1", "c2" });
		content.put("c1", Collections.singleton("id xyz"));
		content.put("c2", new Object[] {
				"Person",
				"Male",
				new Integer(22),
				new String[] { "2 arms", "2 legs" },
				Collections.singletonMap("key", "value"),
				Collections.singletonMap("key", "value"),
				Collections.singletonMap("key", "value")});
		assertEquals(
				"null",
				getEmptyResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
	}

	public void testSendNormalResponse() throws ResponseProcessingException {
		assertEquals(
				"null",
				getNormalResponseString(responseEngine, null, null));
		
		// EMPTY TESTS
		
		Map desc = new LinkedHashMap();
		Map content = new LinkedHashMap();
		assertEquals(
				"null",
				getNormalResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", "test01");
		assertEquals(
				"[\"test01\"]",
				getNormalResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		// SINGLE OUTPUT TESTS
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", Collections.singleton("test02"));
		assertEquals(
				"[\"test02\"]",
				getNormalResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", Collections.singletonList("test03"));
		assertEquals(
				"[\"test03\"]",
				getNormalResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", Collections.singletonMap("test04a", "test04b"));
		assertEquals(
				"{\"test04a\":\"test04b\"}",
				getNormalResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", new String[] { "test05a", "test05b" });
		assertEquals(
				"[\"test05a\",\"test05b\"]",
				getNormalResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		// MULTI OUTPUT TESTS
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, new String[] { "c1", "c2" });
		content.put("c1", "test06a");
		content.put("c2", "test06b");
		assertEquals(
				"{\"c2\":\"test06b\",\"c1\":\"test06a\"}",
				getNormalResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, new String[] { "c1", "c2" });
		content.put("c1", Collections.singleton("id xyz"));
		content.put("c2", new Object[] {
				"Person",
				"Male",
				new Integer(22),
				new String[] { "2 arms", "2 legs" },
				Collections.singletonMap("key", "value"),
				Collections.singletonMap("key", "value"),
				Collections.singletonMap("key", "value")});
		assertEquals(
				"{\"c2\":" +
				"[\"Person\"," +
				"\"Male\"," +
				"22," +
				"[\"2 arms\",\"2 legs\"]," +
				"{\"key\":\"value\"}," +
				"{\"key\":\"value\"}," +
				"{\"key\":\"value\"}]," +
				"\"c1\":" +
				"[\"id xyz\"]}",
				getNormalResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
	}

	public void testSendErrorResponse() throws ResponseProcessingException {
		assertEquals(
				"[\"error message\"]",
				getErrorResponseString(responseEngine, null, null));
		
		// EMPTY TESTS
		
		Map desc = new LinkedHashMap();
		Map content = new LinkedHashMap();
		assertEquals(
				"[\"error message\"]",
				getErrorResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", "test01");
		assertEquals(
				"[\"error message\"]",
				getErrorResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		// SINGLE OUTPUT TESTS
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", Collections.singleton("test02"));
		assertEquals(
				"[\"error message\"]",
				getErrorResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", Collections.singletonList("test03"));
		assertEquals(
				"[\"error message\"]",
				getErrorResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", Collections.singletonMap("test04a", "test04b"));
		assertEquals(
				"[\"error message\"]",
				getErrorResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", new String[] { "test05a", "test05b" });
		assertEquals(
				"[\"error message\"]",
				getErrorResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		// MULTI OUTPUT TESTS
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, new String[] { "c1", "c2" });
		content.put("c1", "test06a");
		content.put("c2", "test06b");
		assertEquals(
				"[\"error message\"]",
				getErrorResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, new String[] { "c1", "c2" });
		content.put("c1", Collections.singleton("id xyz"));
		content.put("c2", new Object[] {
				"Person",
				"Male",
				new Integer(22),
				new String[] { "2 arms", "2 legs" },
				Collections.singletonMap("key", "value"),
				Collections.singletonMap("key", "value"),
				Collections.singletonMap("key", "value")});
		assertEquals(
				"[\"error message\"]",
				getErrorResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
	}

	public void testSendEmptyCallback() throws ResponseProcessingException {
	}

	public void testSendNormalCallback() throws ResponseProcessingException {
	}

	public void testSendErrorCallback() throws ResponseProcessingException {
	}
}
