/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */
package de.tarent.octopus.config;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import junit.framework.TestCase;

/**
 * This is a test class for {@link OctopusEnvironment}
 * 
 * @author Alex Maier, tarent GmbH
 */
public class OctopusEnvironmentTest extends TestCase {
	/**
	 * Returns all default response types
	 * @throws OctopusConfigException 
	 */
	public void testGetDefaultResponseTypes() throws OctopusConfigException {
		OctopusEnvironment octopusEnvironment = new OctopusEnvironment();
		
		Map responseTypes = (Map) octopusEnvironment.getValueAsObject("responseTypes");
		
		assertNotNull(responseTypes);
		assertEquals(10, responseTypes.size());
		
		List names = new ArrayList(responseTypes.keySet());
		Collections.sort(names);
		
		assertEquals("csv", names.get(1));
		assertEquals("json", names.get(2));
		assertEquals("raw", names.get(3));
		assertEquals("rpc", names.get(4));
		assertEquals("simple", names.get(5));
		assertEquals("soap", names.get(6));
		assertEquals("velocity", names.get(7));
		assertEquals("xmlrpc", names.get(8));
		assertEquals("xslt", names.get(9));
	}
}
