package de.tarent.octopus.response;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Reader;
import java.util.LinkedHashMap;
import java.util.Map;

import de.tarent.octopus.request.OctopusResponse;

public class OctopusResponseTest implements OctopusResponse {
	private String moduleName = null;
	private String taskName = null;
	private String contentType = null;
	private Map header = null;
	private ByteArrayOutputStream baos = null;
	private PrintWriter writer = null;

	private boolean closed = true;

	public OctopusResponseTest() {
		resetAndInitTest();
	}

	public void resetAndInitTest() {
		moduleName = null;
		taskName = null;
		contentType = null;
		header = new LinkedHashMap();
		baos = new ByteArrayOutputStream();
		writer = new PrintWriter(baos);
		closed = false;
	}

	public String getResponseAsString() {
		writer.flush();
		return baos.toString();
	}

	public InputStream getResponseAsInputStream() {
		writer.flush();
		return new ByteArrayInputStream(baos.toByteArray());
	}

	public Reader getResponseAsReader() {
		writer.flush();
		return new InputStreamReader(getResponseAsInputStream());
	}

	public void addCookie(String name, String value, Map settings) {
		if (closed)
			throw new RuntimeException("OutputStream already closed.");
		throw new RuntimeException("Cookies not supported in test.");
	}

	public void addCookie(Object cookie) {
		if (closed)
			throw new RuntimeException("OutputStream already closed.");
		throw new RuntimeException("Cookies not supported in test.");
	}

	public void close() {
		if (closed)
			throw new RuntimeException("OutputStream already closed.");
		closed = true;
		writer.flush();
		writer.close();
	}

	public void flush() throws IOException {
		if (closed)
			throw new RuntimeException("OutputStream already closed.");
		baos.flush();
		writer.flush();
	}

	public int getCachingTime() {
		throw new RuntimeException("Caching not supported in test.");
	}

	public String getModuleName() {
		return moduleName;
	}

	public OutputStream getOutputStream() {
		return baos;
	}

	public String getTaskName() {
		return taskName;
	}

	public PrintWriter getWriter() {
		return writer;
	}

	public boolean isDirectCall() {
		return false;
	}

	public void print(String responseString) {
		if (closed)
			throw new RuntimeException("OutputStream already closed.");
		writer.print(responseString);
	}

	public void println(String responseString) {
		if (closed)
			throw new RuntimeException("OutputStream already closed.");
		writer.println(responseString);
	}

	public void setAuthorisationRequired(String authorisationAction) {
		if (closed)
			throw new RuntimeException("OutputStream already closed.");
		throw new RuntimeException("Authorisation not supported in test.");
	}

	public void setCachingTime(int millis) {
		if (closed)
			throw new RuntimeException("OutputStream already closed.");
		throw new RuntimeException("Caching not supported in test.");
	}

	public void setCachingTime(int millis, String param) {
		if (closed)
			throw new RuntimeException("OutputStream already closed.");
		throw new RuntimeException("Caching not supported in test.");
	}

	public void setContentType(String contentType) {
		if (closed)
			throw new RuntimeException("OutputStream already closed.");
		this.contentType = contentType;
	}

	public String getContentType() {
		return contentType;
	}

	public void setErrorLevel(String errorLevel) {
		if (closed)
			throw new RuntimeException("OutputStream already closed.");
		throw new RuntimeException("Error level not supported in test.");
	}

	public void setHeader(String key, String value) {
		if (closed)
			throw new RuntimeException("OutputStream already closed.");
		header.put(key, value);
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public void setStatus(int code) {
		if (closed)
			throw new RuntimeException("OutputStream already closed.");
		throw new RuntimeException("Status not supported in test.");
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}
}
