/*
 * tarent-octopus core,
 * an opensource webservice and webapplication framework (core)
 * Copyright (c) 2001-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus core'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */
package de.tarent.octopus.util;

import java.io.File;
import java.net.URL;
import java.util.prefs.Preferences;

import junit.framework.TestCase;

import org.w3c.dom.Document;

import de.tarent.octopus.config.ModuleConfig;
import de.tarent.octopus.resource.Resources;

/**
 * Tests the Parsing of List Params with nested Maps
 */
public class ConfigIncludeTest extends TestCase {
	public void testRecursiveIncludes() throws Exception {
		URL url = ConfigIncludeTest.class.getResource("config.xml");
		
		String resource = Resources.getInstance().get("REQUESTPROXY_URL_MODULE_CONFIG", url.getFile());
		
		Document document = Xml.getParsedDocument(resource);
		
		Preferences modulePreferences = Preferences.systemRoot().node("/de/tarent/octopus");
		
		ModuleConfig config = new ModuleConfig("Testname", new File(url.getFile()).getParentFile(), document, modulePreferences);
		
		assertEquals("Falsche Anzahl an eingelesenen Parametern: ", 20, config.getParams().size());
	}
}
