package de.tarent.octopus.response.csv;

import java.util.LinkedHashMap;
import java.util.Map;

import de.tarent.octopus.response.AbstractResponseEngine;
import de.tarent.octopus.response.AbstractResponseEngineTest;
import de.tarent.octopus.response.ResponseEngine;
import de.tarent.octopus.response.ResponseProcessingException;

public class CsvResponseEngineTest extends AbstractResponseEngineTest {
	public ResponseEngine getResponseEngine() {
		return new CsvResponseEngine();
	}

	public String getResponseEngineName() {
		return "csv";
	}

	public void testInit() throws ResponseProcessingException {
	}

	public void testSendEmptyResponse() throws ResponseProcessingException {
		assertEquals(
				"",
				getEmptyResponseString(responseEngine, null, null));
		
		Map desc = new LinkedHashMap();
		Map content = new LinkedHashMap();
		assertEquals(
				"",
				getEmptyResponseString(responseEngine, desc, content));
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", new String[][] {
				new String[] { "A1", "B1", "C1" },
				new String[] { "A2", "B2", "C2" },
				new String[] { "A3", "B3", "C3" } } );
		assertEquals(
				"",
				getEmptyResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, new String[] { "header", "content" } );
		content.put("header", new String[] {
				"column1", "column2", "column3" } );
		content.put("content", new String[][] {
				new String[] { "A1", "B1", "C1" },
				new String[] { "A2", "B2", "C2" },
				new String[] { "A3", "B3", "C3" } } );
		assertEquals(
				"",
				getEmptyResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
	}

	public void testSendNormalResponse() throws ResponseProcessingException {
		assertEquals(
				"null",
				getNormalResponseString(responseEngine, null, null));
		
		Map desc = new LinkedHashMap();
		Map content = new LinkedHashMap();
		assertEquals(
				"null",
				getNormalResponseString(responseEngine, desc, content));
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", new String[][] {
				new String[] { "A1", "B1", "C1" },
				new String[] { "A2", "B2", "C2" },
				new String[] { "A3", "B3", "C3" } } );
		assertEquals(
				"\"A1\";\"B1\";\"C1\"\r\n" +
				"\"A2\";\"B2\";\"C2\"\r\n" +
				"\"A3\";\"B3\";\"C3\"\r\n",
				getNormalResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, new String[] { "header", "content" } );
		content.put("header", new String[] {
				"column1", "column2", "column3" } );
		content.put("content", new String[][] {
				new String[] { "A1", "B1", "C1" },
				new String[] { "A2", "B2", "C2" },
				new String[] { "A3", "B3", "C3" } } );
		assertEquals(
				"\"column1\";\"column2\";\"column3\"\r\n" +
				"\"A1\";\"B1\";\"C1\"\r\n" +
				"\"A2\";\"B2\";\"C2\"\r\n" +
				"\"A3\";\"B3\";\"C3\"\r\n",
				getNormalResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
	}

	public void testSendErrorResponse() throws ResponseProcessingException {
		assertEquals(
				"error message",
				getErrorResponseString(responseEngine, null, null));
		
		Map desc = new LinkedHashMap();
		Map content = new LinkedHashMap();
		assertEquals(
				"error message",
				getErrorResponseString(responseEngine, desc, content));
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", new String[][] {
				new String[] { "A1", "B1", "C1" },
				new String[] { "A2", "B2", "C2" },
				new String[] { "A3", "B3", "C3" } } );
		assertEquals(
				"error message",
				getErrorResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, new String[] { "header", "content" } );
		content.put("header", new String[] {
				"column1", "column2", "column3" } );
		content.put("content", new String[][] {
				new String[] { "A1", "B1", "C1" },
				new String[] { "A2", "B2", "C2" },
				new String[] { "A3", "B3", "C3" } } );
		assertEquals(
				"error message",
				getErrorResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
	}

	public void testSendEmptyCallback() throws ResponseProcessingException {
	}

	public void testSendNormalCallback() throws ResponseProcessingException {
	}

	public void testSendErrorCallback() throws ResponseProcessingException {
	}
}
