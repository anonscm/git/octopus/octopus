package de.tarent.octopus.response.soap;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

import de.tarent.octopus.response.AbstractResponseEngine;
import de.tarent.octopus.response.AbstractResponseEngineTest;
import de.tarent.octopus.response.ResponseEngine;
import de.tarent.octopus.response.ResponseProcessingException;

public class SoapResponseEngineTest extends AbstractResponseEngineTest {
	public ResponseEngine getResponseEngine() {
		return new SoapResponseEngine();
	}

	public String getResponseEngineName() {
		return "soap";
	}

	public void testInit() throws ResponseProcessingException {
		getResponseEngine().init(moduleConfig);
	}

	public void testSendEmptyResponse() throws ResponseProcessingException {
		assertEquals(
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?><soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n" +
				" <soapenv:Body>\n" +
				"  <ns1:testResponse xmlns:ns1=\"http://schemas.tarent.de/test\">\n" +
				"   <Status xsi:type=\"soapenc:string\" xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\">ok</Status>\n" +
				"  </ns1:testResponse>\n" +
				" </soapenv:Body>\n" +
				"</soapenv:Envelope>",
				getEmptyResponseString(responseEngine, null, null));
		
		// EMPTY TESTS
		
		Map desc = new LinkedHashMap();
		Map content = new LinkedHashMap();
		assertEquals(
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?><soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n" +
				" <soapenv:Body>\n" +
				"  <ns1:testResponse xmlns:ns1=\"http://schemas.tarent.de/test\">\n" +
				"   <Status xsi:type=\"soapenc:string\" xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\">ok</Status>\n" +
				"  </ns1:testResponse>\n" +
				" </soapenv:Body>\n" +
				"</soapenv:Envelope>",
				getEmptyResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", "test01");
		assertEquals(
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?><soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n" +
				" <soapenv:Body>\n" +
				"  <ns1:testResponse xmlns:ns1=\"http://schemas.tarent.de/test\">\n" +
				"   <Status xsi:type=\"soapenc:string\" xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\">ok</Status>\n" +
				"  </ns1:testResponse>\n" +
				" </soapenv:Body>\n" +
				"</soapenv:Envelope>",
				getEmptyResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		// SINGLE OUTPUT TESTS
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", Collections.singleton("test02"));
		assertEquals(
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?><soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n" +
				" <soapenv:Body>\n" +
				"  <ns1:testResponse xmlns:ns1=\"http://schemas.tarent.de/test\">\n" +
				"   <Status xsi:type=\"soapenc:string\" xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\">ok</Status>\n" +
				"  </ns1:testResponse>\n" +
				" </soapenv:Body>\n" +
				"</soapenv:Envelope>",
				getEmptyResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", Collections.singletonList("test03"));
		assertEquals(
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?><soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n" +
				" <soapenv:Body>\n" +
				"  <ns1:testResponse xmlns:ns1=\"http://schemas.tarent.de/test\">\n" +
				"   <Status xsi:type=\"soapenc:string\" xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\">ok</Status>\n" +
				"  </ns1:testResponse>\n" +
				" </soapenv:Body>\n" +
				"</soapenv:Envelope>",
				getEmptyResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", Collections.singletonMap("test04a", "test04b"));
		assertEquals(
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?><soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n" +
				" <soapenv:Body>\n" +
				"  <ns1:testResponse xmlns:ns1=\"http://schemas.tarent.de/test\">\n" +
				"   <Status xsi:type=\"soapenc:string\" xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\">ok</Status>\n" +
				"  </ns1:testResponse>\n" +
				" </soapenv:Body>\n" +
				"</soapenv:Envelope>",
				getEmptyResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", new String[] { "test05a", "test05b" });
		assertEquals(
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?><soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n" +
				" <soapenv:Body>\n" +
				"  <ns1:testResponse xmlns:ns1=\"http://schemas.tarent.de/test\">\n" +
				"   <Status xsi:type=\"soapenc:string\" xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\">ok</Status>\n" +
				"  </ns1:testResponse>\n" +
				" </soapenv:Body>\n" +
				"</soapenv:Envelope>",
				getEmptyResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		// MULTI OUTPUT TESTS
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, new String[] { "c1", "c2" });
		content.put("c1", "test06a");
		content.put("c2", "test06b");
		assertEquals(
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?><soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n" +
				" <soapenv:Body>\n" +
				"  <ns1:testResponse xmlns:ns1=\"http://schemas.tarent.de/test\">\n" +
				"   <Status xsi:type=\"soapenc:string\" xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\">ok</Status>\n" +
				"  </ns1:testResponse>\n" +
				" </soapenv:Body>\n" +
				"</soapenv:Envelope>",
				getEmptyResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, new String[] { "c1", "c2" });
		content.put("c1", Collections.singleton("id xyz"));
		content.put("c2", new Object[] {
				"Person",
				"Male",
				new Integer(22),
				new String[] { "2 arms", "2 legs" },
				Collections.singletonMap("key", "value"),
				Collections.singletonMap("key", "value"),
				Collections.singletonMap("key", "value")});
		assertEquals(
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?><soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n" +
				" <soapenv:Body>\n" +
				"  <ns1:testResponse xmlns:ns1=\"http://schemas.tarent.de/test\">\n" +
				"   <Status xsi:type=\"soapenc:string\" xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\">ok</Status>\n" +
				"  </ns1:testResponse>\n" +
				" </soapenv:Body>\n" +
				"</soapenv:Envelope>",
				getEmptyResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
	}

	public void testSendNormalResponse() throws ResponseProcessingException {
		assertEquals(
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?><soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n" +
				" <soapenv:Body>\n" +
				"  <ns1:testResponse xmlns:ns1=\"http://schemas.tarent.de/test\"/>\n" +
				" </soapenv:Body>\n" +
				"</soapenv:Envelope>",
				getNormalResponseString(responseEngine, null, null));
		
		// EMPTY TESTS
		
		Map desc = new LinkedHashMap();
		Map content = new LinkedHashMap();
		assertEquals(
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?><soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n" +
				" <soapenv:Body>\n" +
				"  <ns1:testResponse xmlns:ns1=\"http://schemas.tarent.de/test\"/>\n" +
				" </soapenv:Body>\n" +
				"</soapenv:Envelope>",
				getNormalResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", "test01");
		assertEquals(
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?><soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n" +
				" <soapenv:Body>\n" +
				"  <ns1:testResponse xmlns:ns1=\"http://schemas.tarent.de/test\">\n" +
				"   <content xsi:type=\"soapenc:string\" xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\">test01</content>\n" +
				"  </ns1:testResponse>\n" +
				" </soapenv:Body>\n" +
				"</soapenv:Envelope>",
				getNormalResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		// SINGLE OUTPUT TESTS
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", Collections.singleton("test02"));
		assertEquals(
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?><soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n" +
				" <soapenv:Body>\n" +
				"  <ns1:testResponse xmlns:ns1=\"http://schemas.tarent.de/test\">\n" +
				"   <content soapenc:arrayType=\"xsd:anyType[1]\" xsi:type=\"soapenc:Array\" xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\">\n" +
				"    <content xsi:type=\"soapenc:string\">test02</content>\n" +
				"   </content>\n" +
				"  </ns1:testResponse>\n" +
				" </soapenv:Body>\n" +
				"</soapenv:Envelope>",
				getNormalResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", Collections.singletonList("test03"));
		assertEquals(
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?><soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n" +
				" <soapenv:Body>\n" +
				"  <ns1:testResponse xmlns:ns1=\"http://schemas.tarent.de/test\">\n" +
				"   <content soapenc:arrayType=\"xsd:anyType[1]\" xsi:type=\"soapenc:Array\" xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\">\n" +
				"    <content xsi:type=\"soapenc:string\">test03</content>\n" +
				"   </content>\n" +
				"  </ns1:testResponse>\n" +
				" </soapenv:Body>\n" +
				"</soapenv:Envelope>",
				getNormalResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", Collections.singletonMap("test04a", "test04b"));
		assertEquals(
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?><soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n" +
				" <soapenv:Body>\n" +
				"  <ns1:testResponse xmlns:ns1=\"http://schemas.tarent.de/test\">\n" +
				"   <content xsi:type=\"ns2:Map\" xmlns:ns2=\"http://xml.apache.org/xml-soap\">\n" +
				"    <item xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\">\n" +
				"     <key xsi:type=\"soapenc:string\">test04a</key>\n" +
				"     <value xsi:type=\"soapenc:string\">test04b</value>\n" +
				"    </item>\n" +
				"   </content>\n" +
				"  </ns1:testResponse>\n" +
				" </soapenv:Body>\n" +
				"</soapenv:Envelope>",
				getNormalResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", new String[] { "test05a", "test05b" });
		assertEquals(
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?><soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n" +
				" <soapenv:Body>\n" +
				"  <ns1:testResponse xmlns:ns1=\"http://schemas.tarent.de/test\">\n" +
				"   <content soapenc:arrayType=\"xsd:string[2]\" xsi:type=\"soapenc:Array\" xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\">\n" +
				"    <content xsi:type=\"xsd:string\">test05a</content>\n" +
				"    <content xsi:type=\"xsd:string\">test05b</content>\n" +
				"   </content>\n" +
				"  </ns1:testResponse>\n" +
				" </soapenv:Body>\n" +
				"</soapenv:Envelope>",
				getNormalResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		// MULTI OUTPUT TESTS
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, new String[] { "c1", "c2" });
		content.put("c1", "test06a");
		content.put("c2", "test06b");
		assertEquals(
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?><soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n" +
				" <soapenv:Body>\n" +
				"  <ns1:testResponse xmlns:ns1=\"http://schemas.tarent.de/test\">\n" +
				"   <c1 xsi:type=\"soapenc:string\" xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\">test06a</c1>\n" +
				"   <c2 xsi:type=\"soapenc:string\" xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\">test06b</c2>\n" +
				"  </ns1:testResponse>\n" +
				" </soapenv:Body>\n" +
				"</soapenv:Envelope>",
				getNormalResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, new String[] { "c1", "c2" });
		content.put("c1", Collections.singleton("id xyz"));
		content.put("c2", new Object[] {
				"Person",
				"Male",
				new Integer(22),
				new String[] { "2 arms", "2 legs" },
				Collections.singletonMap("key", "value"),
				Collections.singletonMap("key", "value"),
				Collections.singletonMap("key", "value")});
		assertEquals(
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?><soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n" +
				" <soapenv:Body>\n" +
				"  <ns1:testResponse xmlns:ns1=\"http://schemas.tarent.de/test\">\n" +
				"   <c1 soapenc:arrayType=\"xsd:anyType[1]\" xsi:type=\"soapenc:Array\" xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\">\n" +
				"    <c1 xsi:type=\"soapenc:string\">id xyz</c1>\n" +
				"   </c1>\n" +
				"   <c2 soapenc:arrayType=\"xsd:anyType[7]\" xsi:type=\"soapenc:Array\" xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\">\n" +
				"    <c2 xsi:type=\"soapenc:string\">Person</c2>\n" +
				"    <c2 xsi:type=\"soapenc:string\">Male</c2>\n" +
				"    <c2 xsi:type=\"soapenc:int\">22</c2>\n" +
				"    <c2 soapenc:arrayType=\"xsd:string[2]\" xsi:type=\"soapenc:Array\">\n" +
				"     <c2 xsi:type=\"xsd:string\">2 arms</c2>\n" +
				"     <c2 xsi:type=\"xsd:string\">2 legs</c2>\n" +
				"    </c2>\n" +
				"    <c2 xsi:type=\"ns2:Map\" xmlns:ns2=\"http://xml.apache.org/xml-soap\">\n" +
				"     <item>\n" +
				"      <key xsi:type=\"soapenc:string\">key</key>\n" +
				"      <value xsi:type=\"soapenc:string\">value</value>\n" +
				"     </item>\n" +
				"    </c2>\n" +
				"    <c2 xsi:type=\"ns3:Map\" xmlns:ns3=\"http://xml.apache.org/xml-soap\">\n" +
				"     <item>\n" +
				"      <key xsi:type=\"soapenc:string\">key</key>\n" +
				"      <value xsi:type=\"soapenc:string\">value</value>\n" +
				"     </item>\n" +
				"    </c2>\n" +
				"    <c2 xsi:type=\"ns4:Map\" xmlns:ns4=\"http://xml.apache.org/xml-soap\">\n" +
				"     <item>\n" +
				"      <key xsi:type=\"soapenc:string\">key</key>\n" +
				"      <value xsi:type=\"soapenc:string\">value</value>\n" +
				"     </item>\n" +
				"    </c2>\n" +
				"   </c2>\n" +
				"  </ns1:testResponse>\n" +
				" </soapenv:Body>\n" +
				"</soapenv:Envelope>",
				getNormalResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
	}

	public void testSendErrorResponse() throws ResponseProcessingException {
		assertEquals(
				"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">" +
				"<soapenv:Body>" +
				"<soapenv:Fault>" +
				"<faultcode>soapenv:Server.generalException</faultcode>" +
				"<faultstring>error-message</faultstring>" +
				"</soapenv:Fault>" +
				"</soapenv:Body>" +
				"</soapenv:Envelope>",
				getErrorResponseString(responseEngine, null, null, "error-message", null));
		
		// EMPTY TESTS
		
		Map desc = new LinkedHashMap();
		Map content = new LinkedHashMap();
		assertEquals(
				"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">" +
				"<soapenv:Body>" +
				"<soapenv:Fault>" +
				"<faultcode>soapenv:Server.generalException</faultcode>" +
				"<faultstring>error-message</faultstring>" +
				"</soapenv:Fault>" +
				"</soapenv:Body>" +
				"</soapenv:Envelope>",
				getErrorResponseString(responseEngine, desc, content, "error-message", null));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", "test01");
		assertEquals(
				"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">" +
				"<soapenv:Body>" +
				"<soapenv:Fault>" +
				"<faultcode>soapenv:Server.generalException</faultcode>" +
				"<faultstring>error-message</faultstring>" +
				"</soapenv:Fault>" +
				"</soapenv:Body>" +
				"</soapenv:Envelope>",
				getErrorResponseString(responseEngine, desc, content, "error-message", null));
		desc.clear();
		content.clear();
		
		// SINGLE OUTPUT TESTS
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", Collections.singleton("test02"));
		assertEquals(
				"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">" +
				"<soapenv:Body>" +
				"<soapenv:Fault>" +
				"<faultcode>soapenv:Server.generalException</faultcode>" +
				"<faultstring>error-message</faultstring>" +
				"</soapenv:Fault>" +
				"</soapenv:Body>" +
				"</soapenv:Envelope>",
				getErrorResponseString(responseEngine, desc, content, "error-message", null));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", Collections.singletonList("test03"));
		assertEquals(
				"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">" +
				"<soapenv:Body>" +
				"<soapenv:Fault>" +
				"<faultcode>soapenv:Server.generalException</faultcode>" +
				"<faultstring>error-message</faultstring>" +
				"</soapenv:Fault>" +
				"</soapenv:Body>" +
				"</soapenv:Envelope>",
				getErrorResponseString(responseEngine, desc, content, "error-message", null));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", Collections.singletonMap("test04a", "test04b"));
		assertEquals(
				"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">" +
				"<soapenv:Body>" +
				"<soapenv:Fault>" +
				"<faultcode>soapenv:Server.generalException</faultcode>" +
				"<faultstring>error-message</faultstring>" +
				"</soapenv:Fault>" +
				"</soapenv:Body>" +
				"</soapenv:Envelope>",
				getErrorResponseString(responseEngine, desc, content, "error-message", null));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", new String[] { "test05a", "test05b" });
		assertEquals(
				"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">" +
				"<soapenv:Body>" +
				"<soapenv:Fault>" +
				"<faultcode>soapenv:Server.generalException</faultcode>" +
				"<faultstring>error-message</faultstring>" +
				"</soapenv:Fault>" +
				"</soapenv:Body>" +
				"</soapenv:Envelope>",
				getErrorResponseString(responseEngine, desc, content, "error-message", null));
		desc.clear();
		content.clear();
		
		// MULTI OUTPUT TESTS
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, new String[] { "c1", "c2" });
		content.put("c1", "test06a");
		content.put("c2", "test06b");
		assertEquals(
				"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">" +
				"<soapenv:Body>" +
				"<soapenv:Fault>" +
				"<faultcode>soapenv:Server.generalException</faultcode>" +
				"<faultstring>error-message</faultstring>" +
				"</soapenv:Fault>" +
				"</soapenv:Body>" +
				"</soapenv:Envelope>",
				getErrorResponseString(responseEngine, desc, content, "error-message", null));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, new String[] { "c1", "c2" });
		content.put("c1", Collections.singleton("id xyz"));
		content.put("c2", new Object[] {
				"Person",
				"Male",
				new Integer(22),
				new String[] { "2 arms", "2 legs" },
				Collections.singletonMap("key", "value"),
				Collections.singletonMap("key", "value"),
				Collections.singletonMap("key", "value")});
		assertEquals(
				"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">" +
				"<soapenv:Body>" +
				"<soapenv:Fault>" +
				"<faultcode>soapenv:Server.generalException</faultcode>" +
				"<faultstring>error-message</faultstring>" +
				"</soapenv:Fault>" +
				"</soapenv:Body>" +
				"</soapenv:Envelope>",
				getErrorResponseString(responseEngine, desc, content, "error-message", null));
		desc.clear();
		content.clear();
	}

	public void testSendEmptyCallback() throws ResponseProcessingException {
	}

	public void testSendNormalCallback() throws ResponseProcessingException {
	}

	public void testSendErrorCallback() throws ResponseProcessingException {
	}
}
