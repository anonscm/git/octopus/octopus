package de.tarent.octopus.response;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import de.tarent.octopus.config.CommonConfig;
import de.tarent.octopus.config.ModuleConfig;
import de.tarent.octopus.config.PersonalConfigImpl;
import de.tarent.octopus.content.OctopusContent;
import de.tarent.octopus.request.OctopusRequest;
import de.tarent.octopus.server.OctopusContext;
import de.tarent.octopus.server.OctopusContextImpl;
import de.tarent.octopus.server.PersonalConfig;
import junit.framework.TestCase;

public abstract class AbstractResponseEngineTest extends TestCase  {
	private final int PARALLEL_LOOPS = 10;

	public abstract ResponseEngine getResponseEngine();

	public ResponseEngine getInitializedResponseEngine() {
		ResponseEngine responseEngine = getResponseEngine();
		responseEngine.init(moduleConfig);
		return responseEngine;
	}

	public abstract String getResponseEngineName();

	protected PersonalConfig personalConfig;
	protected CommonConfig commonConfig;
	protected ModuleConfig moduleConfig;

	protected ResponseEngine responseEngine;

	protected void setUp() throws Exception {
		super.setUp();
		
		personalConfig = new PersonalConfigImpl();
		commonConfig = new CommonConfig(null, null);
		moduleConfig = new ModuleConfig("src/test", new LinkedHashMap());
		
		responseEngine = getInitializedResponseEngine();
	}

	protected void tearDown() throws Exception {
		responseEngine = null;
		
		moduleConfig = null;
		commonConfig = null;
		personalConfig = null;
		
		super.tearDown();
	}

	public void testResponseEngineNotNull() {
		assertNotNull(responseEngine);
	}

	public void testResponseEngineName() {
		assertNotNull(getResponseEngineName());
		assertEquals(getResponseEngineName(), responseEngine.getDefaultName());
		
		String className =
				"de.tarent.octopus.response." +
				StringUtils.lowerCase(responseEngine.getDefaultName()) +
				"." +
				StringUtils.capitalize(responseEngine.getDefaultName() +
				"ResponseEngine");
		assertEquals(className, responseEngine.getClass().getName());
	}

	public abstract void testInit() throws ResponseProcessingException;

	public abstract void testSendEmptyResponse() throws ResponseProcessingException;

	public abstract void testSendNormalResponse() throws ResponseProcessingException;

	public abstract void testSendErrorResponse() throws ResponseProcessingException;

	public abstract void testSendEmptyCallback() throws ResponseProcessingException;

	public abstract void testSendNormalCallback() throws ResponseProcessingException;

	public abstract void testSendErrorCallback() throws ResponseProcessingException;

	public void testSendEmptyResponseParallel() throws ResponseProcessingException, InterruptedException {
		final ExceptionPointer exceptionPointer = new ExceptionPointer();
		
		List threads = new LinkedList();
		for (int i = 0; i < PARALLEL_LOOPS; i++) {
			threads.add(new Thread(new Runnable() {
				public void run() {
					try {
						testSendEmptyResponse();
					} catch (ResponseProcessingException e) {
						exceptionPointer.exception = e;
					} catch (RuntimeException e) {
						exceptionPointer.runtimeException = e;
					} catch (Error e) {
						exceptionPointer.error = e;
					}
				}
			}));
		}
		for (Iterator it = threads.iterator(); it.hasNext(); ) {
			((Thread) it.next()).start();
		}
		for (Iterator it = threads.iterator(); it.hasNext(); ) {
			((Thread) it.next()).join(10000);
		}
		
		if (exceptionPointer.exception != null)
			throw exceptionPointer.exception;
		else if (exceptionPointer.runtimeException != null)
			throw exceptionPointer.runtimeException;
		else if (exceptionPointer.error != null)
			throw exceptionPointer.error;
	}

	public void testSendResponseParallel() throws ResponseProcessingException, InterruptedException {
		final ExceptionPointer exceptionPointer = new ExceptionPointer();
		
		List threads = new LinkedList();
		for (int i = 0; i < PARALLEL_LOOPS; i++) {
			threads.add(new Thread(new Runnable() {
				public void run() {
					try {
						testSendNormalResponse();
					} catch (ResponseProcessingException e) {
						exceptionPointer.exception = e;
					} catch (RuntimeException e) {
						exceptionPointer.runtimeException = e;
					} catch (Error e) {
						exceptionPointer.error = e;
					}
				}
			}));
		}
		for (Iterator it = threads.iterator(); it.hasNext(); ) {
			((Thread) it.next()).start();
		}
		for (Iterator it = threads.iterator(); it.hasNext(); ) {
			((Thread) it.next()).join(10000);
		}
		
		if (exceptionPointer.exception != null)
			throw exceptionPointer.exception;
		else if (exceptionPointer.runtimeException != null)
			throw exceptionPointer.runtimeException;
		else if (exceptionPointer.error != null)
			throw exceptionPointer.error;
	}

	public void testSendErrorParallel() throws ResponseProcessingException, InterruptedException {
		final ExceptionPointer exceptionPointer = new ExceptionPointer();
		
		List threads = new LinkedList();
		for (int i = 0; i < PARALLEL_LOOPS; i++) {
			threads.add(new Thread(new Runnable() {
				public void run() {
					try {
						testSendErrorResponse();
					} catch (ResponseProcessingException e) {
						exceptionPointer.exception = e;
					} catch (RuntimeException e) {
						exceptionPointer.runtimeException = e;
					} catch (Error e) {
						exceptionPointer.error = e;
					}
				}
			}));
		}
		for (Iterator it = threads.iterator(); it.hasNext(); ) {
			((Thread) it.next()).start();
		}
		for (Iterator it = threads.iterator(); it.hasNext(); ) {
			((Thread) it.next()).join(10000);
		}
		
		if (exceptionPointer.exception != null)
			throw exceptionPointer.exception;
		else if (exceptionPointer.runtimeException != null)
			throw exceptionPointer.runtimeException;
		else if (exceptionPointer.error != null)
			throw exceptionPointer.error;
	}

	public void testSendCallBackResponseParallel() throws ResponseProcessingException, InterruptedException {
		final ExceptionPointer exceptionPointer = new ExceptionPointer();
		
		List threads = new LinkedList();
		for (int i = 0; i < PARALLEL_LOOPS; i++) {
			threads.add(new Thread(new Runnable() {
				public void run() {
					try {
						testSendNormalCallback();
					} catch (ResponseProcessingException e) {
						exceptionPointer.exception = e;
					} catch (RuntimeException e) {
						exceptionPointer.runtimeException = e;
					} catch (Error e) {
						exceptionPointer.error = e;
					}
				}
			}));
		}
		for (Iterator it = threads.iterator(); it.hasNext(); ) {
			((Thread) it.next()).start();
		}
		for (Iterator it = threads.iterator(); it.hasNext(); ) {
			((Thread) it.next()).join(10000);
		}
		
		if (exceptionPointer.exception != null)
			throw exceptionPointer.exception;
		else if (exceptionPointer.runtimeException != null)
			throw exceptionPointer.runtimeException;
		else if (exceptionPointer.error != null)
			throw exceptionPointer.error;
	}

	public void testSendCallBackErrorParallel() throws ResponseProcessingException, InterruptedException {
		final ExceptionPointer exceptionPointer = new ExceptionPointer();
		
		List threads = new LinkedList();
		for (int i = 0; i < PARALLEL_LOOPS; i++) {
			threads.add(new Thread(new Runnable() {
				public void run() {
					try {
						testSendErrorCallback();
					} catch (ResponseProcessingException e) {
						exceptionPointer.exception = e;
					} catch (RuntimeException e) {
						exceptionPointer.runtimeException = e;
					} catch (Error e) {
						exceptionPointer.error = e;
					}
				}
			}));
		}
		for (Iterator it = threads.iterator(); it.hasNext(); ) {
			((Thread) it.next()).start();
		}
		for (Iterator it = threads.iterator(); it.hasNext(); ) {
			((Thread) it.next()).join(10000);
		}
		
		if (exceptionPointer.exception != null)
			throw exceptionPointer.exception;
		else if (exceptionPointer.runtimeException != null)
			throw exceptionPointer.runtimeException;
		else if (exceptionPointer.error != null)
			throw exceptionPointer.error;
	}
	
	private ResponseDescription getResponseDescription(
			ResponseEngine responseEngine,
			Map descriptionParameter) throws ResponseProcessingException {
		
		return new ResponseDescription(
				responseEngine.getDefaultName(),
				descriptionParameter);
	}
	
	private OctopusContext getOctopusContext(
			Map contentParameter) {
		
		OctopusContext octopusContext = new OctopusContextImpl(
				new OctopusRequest(),
				new OctopusContent(),
				personalConfig,
				commonConfig,
				moduleConfig);
		
		if (contentParameter != null) {
			for (Iterator it = contentParameter.entrySet().iterator(); it.hasNext(); ) {
				Map.Entry entry = (Map.Entry) it.next();
				octopusContext.setContent((String) entry.getKey(), entry.getValue());
			}
		}
		
		return octopusContext;
	}

	protected String getEmptyResponseString(
			ResponseEngine responseEngine,
			Map descriptionParameter,
			Map contentParameter) throws ResponseProcessingException {
		
		OctopusResponseTest octopusResponseTest = new OctopusResponseTest();
		octopusResponseTest.setModuleName("test");
		octopusResponseTest.setTaskName("test");
		
		responseEngine.sendEmptyResponse(
				getResponseDescription(responseEngine, descriptionParameter),
				getOctopusContext(contentParameter),
				octopusResponseTest);
		
		return octopusResponseTest.getResponseAsString();
	}

	protected String getNormalResponseString(
			ResponseEngine responseEngine,
			Map descriptionParameter,
			Map contentParameter) throws ResponseProcessingException {
		
		OctopusResponseTest octopusResponseTest = new OctopusResponseTest();
		octopusResponseTest.setModuleName("test");
		octopusResponseTest.setTaskName("test");
		
		responseEngine.sendNormalResponse(
				getResponseDescription(responseEngine, descriptionParameter),
				getOctopusContext(contentParameter),
				octopusResponseTest);
		
		return octopusResponseTest.getResponseAsString();
	}

	protected String getErrorResponseString(
			ResponseEngine responseEngine,
			Map descriptionParameter,
			Map contentParameter) throws ResponseProcessingException {
		return getErrorCallbackString(
				responseEngine, descriptionParameter, contentParameter,
				"error message", new Exception());
	}

	protected String getErrorResponseString(
			ResponseEngine responseEngine,
			Map descriptionParameter,
			Map contentParameter,
			String message,
			Exception exception) throws ResponseProcessingException {
		
		OctopusResponseTest octopusResponseTest = new OctopusResponseTest();
		octopusResponseTest.setModuleName("test");
		octopusResponseTest.setTaskName("test");
		
		responseEngine.sendErrorResponse(
				getResponseDescription(responseEngine, descriptionParameter),
				getOctopusContext(contentParameter),
				octopusResponseTest,
				message,
				exception);
		
		return octopusResponseTest.getResponseAsString();
	}

	protected String getEmptyCallbackString(
			ResponseEngine responseEngine,
			Map descriptionParameter,
			Map contentParameter) throws ResponseProcessingException {
		
		OctopusResponseTest octopusResponseTest = new OctopusResponseTest();
		octopusResponseTest.setModuleName("test");
		octopusResponseTest.setTaskName("test");
		
		responseEngine.sendEmptyCallback(
				getResponseDescription(responseEngine, descriptionParameter),
				getOctopusContext(contentParameter),
				octopusResponseTest);
		
		return octopusResponseTest.getResponseAsString();
	}

	protected String getNormalCallbackString(
			ResponseEngine responseEngine,
			Map descriptionParameter,
			Map contentParameter) throws ResponseProcessingException {
		
		OctopusResponseTest octopusResponseTest = new OctopusResponseTest();
		octopusResponseTest.setModuleName("test");
		octopusResponseTest.setTaskName("test");
		
		responseEngine.sendNormalCallback(
				getResponseDescription(responseEngine, descriptionParameter),
				getOctopusContext(contentParameter),
				octopusResponseTest);
		
		return octopusResponseTest.getResponseAsString();
	}

	protected String getErrorCallbackString(
			ResponseEngine responseEngine,
			Map descriptionParameter,
			Map contentParameter) throws ResponseProcessingException {
		return getErrorCallbackString(
				responseEngine, descriptionParameter, contentParameter,
				"error message", new Exception());
	}

	protected String getErrorCallbackString(
			ResponseEngine responseEngine,
			Map descriptionParameter,
			Map contentParameter,
			String message,
			Exception exception) throws ResponseProcessingException {
		
		OctopusResponseTest octopusResponseTest = new OctopusResponseTest();
		octopusResponseTest.setModuleName("test");
		octopusResponseTest.setTaskName("test");
		
		responseEngine.sendErrorCallback(
				getResponseDescription(responseEngine, descriptionParameter),
				getOctopusContext(contentParameter),
				octopusResponseTest,
				message,
				exception);
		
		return octopusResponseTest.getResponseAsString();
	}

	private static class ExceptionPointer {
		private ResponseProcessingException exception;
		private RuntimeException runtimeException;
		private Error error;
	}
}
