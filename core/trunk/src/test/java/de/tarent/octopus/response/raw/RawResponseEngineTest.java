package de.tarent.octopus.response.raw;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

import de.tarent.octopus.response.AbstractResponseEngine;
import de.tarent.octopus.response.AbstractResponseEngineTest;
import de.tarent.octopus.response.ResponseEngine;
import de.tarent.octopus.response.ResponseProcessingException;
import de.tarent.octopus.response.raw.RawResponseEngine;

public class RawResponseEngineTest extends AbstractResponseEngineTest {
	public ResponseEngine getResponseEngine() {
		return new RawResponseEngine();
	}

	public String getResponseEngineName() {
		return "raw";
	}

	public void testInit() throws ResponseProcessingException {
		getResponseEngine().init(moduleConfig);
	}

	public void testSendEmptyResponse() throws ResponseProcessingException {
		assertEquals(
				"ok",
				getEmptyResponseString(responseEngine, null, null));
		
		// EMPTY TESTS
		
		Map desc = new LinkedHashMap();
		Map content = new LinkedHashMap();
		assertEquals(
				"ok",
				getEmptyResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", "test01");
		assertEquals(
				"ok",
				getEmptyResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		// SINGLE OUTPUT TESTS
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", Collections.singleton("test02"));
		assertEquals(
				"ok",
				getEmptyResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", Collections.singletonList("test03"));
		assertEquals(
				"ok",
				getEmptyResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", Collections.singletonMap("test04a", "test04b"));
		assertEquals(
				"ok",
				getEmptyResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", new String[] { "test05a", "test05b" });
		assertEquals(
				"ok",
				getEmptyResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		// MULTI OUTPUT TESTS
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, new String[] { "c1", "c2" });
		content.put("c1", "test06a");
		content.put("c2", "test06b");
		assertEquals(
				"ok",
				getEmptyResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, new String[] { "c1", "c2" });
		content.put("c1", Collections.singleton("id xyz"));
		content.put("c2", new Object[] {
				"Person",
				"Male",
				new Integer(22),
				new String[] { "2 arms", "2 legs" },
				Collections.singletonMap("key", "value"),
				Collections.singletonMap("key", "value"),
				Collections.singletonMap("key", "value")});
		assertEquals(
				"ok",
				getEmptyResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
	}

	public void testSendNormalResponse() throws ResponseProcessingException {
		assertEquals(
				"\n",
				getNormalResponseString(responseEngine, null, null));
		
		// EMPTY TESTS
		
		Map desc = new LinkedHashMap();
		Map content = new LinkedHashMap();
		assertEquals(
				"\n",
				getNormalResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", "test01");
		assertEquals(
				"test01\n",
				getNormalResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		// SINGLE OUTPUT TESTS
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", Collections.singleton("test02"));
		assertEquals(
				"[test02]\n",
				getNormalResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", Collections.singletonList("test03"));
		assertEquals(
				"[test03]\n",
				getNormalResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", Collections.singletonMap("test04a", "test04b"));
		assertEquals(
				"{test04a=test04b}\n",
				getNormalResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		// MULTI OUTPUT TESTS
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, new String[] { "c1", "c2" });
		content.put("c1", "test06a");
		content.put("c2", "test06b");
		assertEquals(
				"test06a\ntest06b\n",
				getNormalResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, new String[] { "c1", "c2" });
		content.put("c1", Collections.singleton("id xyz"));
		content.put("c2", Arrays.asList(new Object[] {
				"Person",
				"Male",
				new Integer(22),
				Collections.singletonMap("key", "value"),
				Collections.singletonMap("key", "value"),
				Collections.singletonMap("key", "value")}));
		assertEquals(
				"[id xyz]\n" +
				"[Person, Male, 22, {key=value}, {key=value}, {key=value}]\n",
				getNormalResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
	}

	public void testSendErrorResponse() throws ResponseProcessingException {
		assertEquals(
				"error message",
				getErrorResponseString(responseEngine, null, null));
		
		// EMPTY TESTS
		
		Map desc = new LinkedHashMap();
		Map content = new LinkedHashMap();
		assertEquals(
				"error message",
				getErrorResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", "test01");
		assertEquals(
				"error message",
				getErrorResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		// SINGLE OUTPUT TESTS
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", Collections.singleton("test02"));
		assertEquals(
				"error message",
				getErrorResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", Collections.singletonList("test03"));
		assertEquals(
				"error message",
				getErrorResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", Collections.singletonMap("test04a", "test04b"));
		assertEquals(
				"error message",
				getErrorResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", new String[] { "test05a", "test05b" });
		assertEquals(
				"error message",
				getErrorResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		// MULTI OUTPUT TESTS
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, new String[] { "c1", "c2" });
		content.put("c1", "test06a");
		content.put("c2", "test06b");
		assertEquals(
				"error message",
				getErrorResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, new String[] { "c1", "c2" });
		content.put("c1", Collections.singleton("id xyz"));
		content.put("c2", new Object[] {
				"Person",
				"Male",
				new Integer(22),
				new String[] { "2 arms", "2 legs" },
				Collections.singletonMap("key", "value"),
				Collections.singletonMap("key", "value"),
				Collections.singletonMap("key", "value")});
		assertEquals(
				"error message",
				getErrorResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
	}

	public void testSendEmptyCallback() throws ResponseProcessingException {
	}

	public void testSendNormalCallback() throws ResponseProcessingException {
	}

	public void testSendErrorCallback() throws ResponseProcessingException {
	}
}
