package de.tarent.octopus.response.json;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import junit.framework.TestCase;
import net.sf.json.JSONObject;

public class JsonTest extends TestCase {
	public void testJsonLibrary() {
		Map map = new HashMap();
		map.put("name", "json");
		map.put("bool", Boolean.TRUE);
		map.put("int", new Integer(1));
		map.put("arr", new String[] { "a", "b" });
		map.put("func", "function(i){ return this.arr[i]; }");
		
		JSONObject json = JSONObject.fromObject(map);
		System.out.println(json);
		
		StringWriter writer = new StringWriter();
		json.write(writer);
		System.out.println(writer);
		
		// prints
		// ["name":"json","bool":true,"int":1,"arr":["a","b"],"func":function(i){
		// return this.arr[i]; }]
		
	}
}
