package de.tarent.octopus.response.xmlrpc;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

import de.tarent.octopus.response.AbstractResponseEngine;
import de.tarent.octopus.response.AbstractResponseEngineTest;
import de.tarent.octopus.response.ResponseEngine;
import de.tarent.octopus.response.ResponseProcessingException;

public class XmlRpcResponseEngineTest extends AbstractResponseEngineTest {
	public ResponseEngine getResponseEngine() {
		return new XmlRpcResponseEngine();
	}

	public String getResponseEngineName() {
		return "xmlRpc";
	}

	public void testInit() throws ResponseProcessingException {
		getResponseEngine().init(moduleConfig);
	}

	public void testSendEmptyResponse() throws ResponseProcessingException {
		assertEquals(
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
				"<methodResponse>" +
				"</methodResponse>",
				getEmptyResponseString(responseEngine, null, null));
		
		// EMPTY TESTS
		
		Map desc = new LinkedHashMap();
		Map content = new LinkedHashMap();
		assertEquals(
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
				"<methodResponse>" +
				"</methodResponse>",
				getEmptyResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", "test01");
		assertEquals(
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
				"<methodResponse>" +
				"</methodResponse>",
				getEmptyResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		// SINGLE OUTPUT TESTS
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", Collections.singleton("test02"));
		assertEquals(
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
				"<methodResponse>" +
				"</methodResponse>",
				getEmptyResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", Collections.singletonList("test03"));
		assertEquals(
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
				"<methodResponse>" +
				"</methodResponse>",
				getEmptyResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", Collections.singletonMap("test04a", "test04b"));
		assertEquals(
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
				"<methodResponse>" +
				"</methodResponse>",
				getEmptyResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", new String[] { "test05a", "test05b" });
		assertEquals(
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
				"<methodResponse>" +
				"</methodResponse>",
				getEmptyResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		// MULTI OUTPUT TESTS
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, new String[] { "c1", "c2" });
		content.put("c1", "test06a");
		content.put("c2", "test06b");
		assertEquals(
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
				"<methodResponse>" +
				"</methodResponse>",
				getEmptyResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, new String[] { "c1", "c2" });
		content.put("c1", Collections.singleton("id xyz"));
		content.put("c2", new Object[] {
				"Person",
				"Male",
				new Integer(22),
				new String[] { "2 arms", "2 legs" },
				Collections.singletonMap("key", "value"),
				Collections.singletonMap("key", "value"),
				Collections.singletonMap("key", "value")});
		assertEquals(
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
				"<methodResponse>" +
				"</methodResponse>",
				getEmptyResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
	}

	public void testSendNormalResponse() throws ResponseProcessingException {
		assertEquals(
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
				"<methodResponse>" +
				"</methodResponse>",
				getNormalResponseString(responseEngine, null, null));
		
		// EMPTY TESTS
		
		Map desc = new LinkedHashMap();
		Map content = new LinkedHashMap();
		assertEquals(
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
				"<methodResponse>" +
				"</methodResponse>",
				getNormalResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", "test01");
		assertEquals(
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
				"<methodResponse><params>" +
				"<param><value>test01</value></param>" +
				"</params></methodResponse>",
				getNormalResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		// SINGLE OUTPUT TESTS
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", Collections.singleton("test02"));
		assertEquals(
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
				"<methodResponse><params>" +
				"<param><value><array>" +
				"<data><value>test02</value></data>" +
				"</array></value></param>" +
				"</params></methodResponse>",
				getNormalResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", Collections.singletonList("test03"));
		assertEquals(
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
				"<methodResponse><params>" +
				"<param><value><array>" +
				"<data><value>test03</value></data>" +
				"</array></value></param>" +
				"</params></methodResponse>",
				getNormalResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", Collections.singletonMap("test04a", "test04b"));
		assertEquals(
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
				"<methodResponse><params>" +
				"<param><value><struct>" +
				"<member><name>test04a</name><value>test04b</value></member>" +
				"</struct></value></param>" +
				"</params></methodResponse>",
				getNormalResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", new String[] { "test05a", "test05b" });
		assertEquals(
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
				"<methodResponse><params>" +
				"<param><value><array>" +
				"<data><value>test05a</value>" +
				"<value>test05b</value></data>" +
				"</array></value></param>" +
				"</params></methodResponse>",
				getNormalResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		// MULTI OUTPUT TESTS
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, new String[] { "c1", "c2" });
		content.put("c1", "test06a");
		content.put("c2", "test06b");
		assertEquals(
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?><methodResponse>" +
				"<params><param><value>" +
				"<struct>" +
				"<member><name>c1</name><value>test06a</value></member>" +
				"<member><name>c2</name><value>test06b</value></member>" +
				"</struct></value></param>" +
				"</params></methodResponse>",
				getNormalResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, new String[] { "c1", "c2" });
		content.put("c1", Collections.singleton("id xyz"));
		content.put("c2", new Object[] {
				"Person",
				"Male",
				new Integer(22),
				new String[] { "2 arms", "2 legs" },
				Collections.singletonMap("key", "value"),
				Collections.singletonMap("key", "value"),
				Collections.singletonMap("key", "value")});
		assertEquals(
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?><methodResponse>" +
				"<params><param><value>" +
				"<struct>" +
				"<member><name>c1</name><value><array>" +
				"<data><value>id xyz</value></data></array></value></member>" +
				"<member><name>c2</name><value><array>" +
				"<data>" +
				"<value>Person</value>" +
				"<value>Male</value>" +
				"<value><int>22</int></value>" +
				"<value><array><data><value>2 arms</value>" +
				"<value>2 legs</value></data></array></value><value><struct>" +
				"<member><name>key</name><value>value</value></member></struct>" +
				"</value><value><struct><member><name>key</name>" +
				"<value>value</value></member></struct></value><value><struct>" +
				"<member><name>key</name><value>value</value></member></struct>" +
				"</value></data></array></value></member></struct></value>" +
				"</param>" +
				"</params></methodResponse>",
				getNormalResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
	}

	public void testSendErrorResponse() throws ResponseProcessingException {
		assertEquals(
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
				"<methodResponse><fault>" +
				"<value><struct>" +
				"<member><name>faultString</name><value>error message</value></member>" +
				"<member><name>faultCode</name><value><int>0</int></value></member>" +
				"</struct></value>" +
				"</fault></methodResponse>",
				getErrorResponseString(responseEngine, null, null));
		
		// EMPTY TESTS
		
		Map desc = new LinkedHashMap();
		Map content = new LinkedHashMap();
		assertEquals(
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
				"<methodResponse><fault>" +
				"<value><struct>" +
				"<member><name>faultString</name><value>error message</value></member>" +
				"<member><name>faultCode</name><value><int>0</int></value></member>" +
				"</struct></value>" +
				"</fault></methodResponse>",
				getErrorResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", "test01");
		assertEquals(
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
				"<methodResponse><fault>" +
				"<value><struct>" +
				"<member><name>faultString</name><value>error message</value></member>" +
				"<member><name>faultCode</name><value><int>0</int></value></member>" +
				"</struct></value>" +
				"</fault></methodResponse>",
				getErrorResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		// SINGLE OUTPUT TESTS
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", Collections.singleton("test02"));
		assertEquals(
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
				"<methodResponse><fault>" +
				"<value><struct>" +
				"<member><name>faultString</name><value>error message</value></member>" +
				"<member><name>faultCode</name><value><int>0</int></value></member>" +
				"</struct></value>" +
				"</fault></methodResponse>",
				getErrorResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", Collections.singletonList("test03"));
		assertEquals(
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
				"<methodResponse><fault>" +
				"<value><struct>" +
				"<member><name>faultString</name><value>error message</value></member>" +
				"<member><name>faultCode</name><value><int>0</int></value></member>" +
				"</struct></value>" +
				"</fault></methodResponse>",
				getErrorResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", Collections.singletonMap("test04a", "test04b"));
		assertEquals(
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
				"<methodResponse><fault>" +
				"<value><struct>" +
				"<member><name>faultString</name><value>error message</value></member>" +
				"<member><name>faultCode</name><value><int>0</int></value></member>" +
				"</struct></value>" +
				"</fault></methodResponse>",
				getErrorResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", new String[] { "test05a", "test05b" });
		assertEquals(
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
				"<methodResponse><fault>" +
				"<value><struct>" +
				"<member><name>faultString</name><value>error message</value></member>" +
				"<member><name>faultCode</name><value><int>0</int></value></member>" +
				"</struct></value>" +
				"</fault></methodResponse>",
				getErrorResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		// MULTI OUTPUT TESTS
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, new String[] { "c1", "c2" });
		content.put("c1", "test06a");
		content.put("c2", "test06b");
		assertEquals(
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
				"<methodResponse><fault>" +
				"<value><struct>" +
				"<member><name>faultString</name><value>error message</value></member>" +
				"<member><name>faultCode</name><value><int>0</int></value></member>" +
				"</struct></value>" +
				"</fault></methodResponse>",
				getErrorResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, new String[] { "c1", "c2" });
		content.put("c1", Collections.singleton("id xyz"));
		content.put("c2", new Object[] {
				"Person",
				"Male",
				new Integer(22),
				new String[] { "2 arms", "2 legs" },
				Collections.singletonMap("key", "value"),
				Collections.singletonMap("key", "value"),
				Collections.singletonMap("key", "value")});
		assertEquals(
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
				"<methodResponse><fault>" +
				"<value><struct>" +
				"<member><name>faultString</name><value>error message</value></member>" +
				"<member><name>faultCode</name><value><int>0</int></value></member>" +
				"</struct></value>" +
				"</fault></methodResponse>",
				getErrorResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
	}

	public void testSendEmptyCallback() throws ResponseProcessingException {
	}

	public void testSendNormalCallback() throws ResponseProcessingException {
	}

	public void testSendErrorCallback() throws ResponseProcessingException {
	}
}
