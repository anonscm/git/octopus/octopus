package de.tarent.octopus.response.xslt;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

import de.tarent.octopus.response.AbstractResponseEngine;
import de.tarent.octopus.response.AbstractResponseEngineTest;
import de.tarent.octopus.response.ResponseEngine;
import de.tarent.octopus.response.ResponseProcessingException;

public class XsltResponseEngineTest extends AbstractResponseEngineTest {
	public ResponseEngine getResponseEngine() {
		return new XsltResponseEngine();
	}

	public String getResponseEngineName() {
		return "xslt";
	}

	public void testInit() throws ResponseProcessingException {
		getResponseEngine().init(moduleConfig);
	}

	public void testSendEmptyResponse() throws ResponseProcessingException {
		assertEquals(
				"<empty>ok</empty>",
				getEmptyResponseString(responseEngine, null, null));
		
		// EMPTY TESTS
		
		Map desc = new LinkedHashMap();
		Map content = new LinkedHashMap();
		assertEquals(
				"<empty>ok</empty>",
				getEmptyResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", "test01");
		assertEquals(
				"<empty>ok</empty>",
				getEmptyResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		// SINGLE OUTPUT TESTS
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", Collections.singleton("test02"));
		assertEquals(
				"<empty>ok</empty>",
				getEmptyResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", Collections.singletonList("test03"));
		assertEquals(
				"<empty>ok</empty>",
				getEmptyResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", Collections.singletonMap("test04a", "test04b"));
		assertEquals(
				"<empty>ok</empty>",
				getEmptyResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", new String[] { "test05a", "test05b" });
		assertEquals(
				"<empty>ok</empty>",
				getEmptyResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		// MULTI OUTPUT TESTS
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, new String[] { "c1", "c2" });
		content.put("c1", "test06a");
		content.put("c2", "test06b");
		assertEquals(
				"<empty>ok</empty>",
				getEmptyResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, new String[] { "c1", "c2" });
		content.put("c1", Collections.singleton("id xyz"));
		content.put("c2", new Object[] {
				"Person",
				"Male",
				new Integer(22),
				new String[] { "2 arms", "2 legs" },
				Collections.singletonMap("key", "value"),
				Collections.singletonMap("key", "value"),
				Collections.singletonMap("key", "value")});
		assertEquals(
				"<empty>ok</empty>",
				getEmptyResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
	}

	public void testSendNormalResponse() throws ResponseProcessingException {
		
		// Add tests for simple maps and lists.
		
//		assertEquals(
//				"<empty>ok</empty>",
//				getNormalResponseString(responseEngine, null, null));
//		
//		// EMPTY TESTS
//		
//		Map desc = new LinkedHashMap();
//		Map content = new LinkedHashMap();
//		assertEquals(
//				"null",
//				getNormalResponseString(responseEngine, desc, content));
//		desc.clear();
//		content.clear();
//		
//		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
//		content.put("content", "test01");
//		assertEquals(
//				"[\"test01\"]",
//				getNormalResponseString(responseEngine, desc, content));
//		desc.clear();
//		content.clear();
//		
//		// SINGLE OUTPUT TESTS
//		
//		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
//		content.put("content", Collections.singleton("test02"));
//		assertEquals(
//				"[\"test02\"]",
//				getNormalResponseString(responseEngine, desc, content));
//		desc.clear();
//		content.clear();
//		
//		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
//		content.put("content", Collections.singletonList("test03"));
//		assertEquals(
//				"[\"test03\"]",
//				getNormalResponseString(responseEngine, desc, content));
//		desc.clear();
//		content.clear();
//		
//		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
//		content.put("content", Collections.singletonMap("test04a", "test04b"));
//		assertEquals(
//				"{\"test04a\":\"test04b\"}",
//				getNormalResponseString(responseEngine, desc, content));
//		desc.clear();
//		content.clear();
//		
//		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
//		content.put("content", new String[] { "test05a", "test05b" });
//		assertEquals(
//				"[\"test05a\",\"test05b\"]",
//				getNormalResponseString(responseEngine, desc, content));
//		desc.clear();
//		content.clear();
//		
//		// MULTI OUTPUT TESTS
//		
//		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, new String[] { "c1", "c2" });
//		content.put("c1", "test06a");
//		content.put("c2", "test06b");
//		assertEquals(
//				"{\"c2\":\"test06b\",\"c1\":\"test06a\"}",
//				getNormalResponseString(responseEngine, desc, content));
//		desc.clear();
//		content.clear();
//		
//		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, new String[] { "c1", "c2" });
//		content.put("c1", Collections.singleton("id xyz"));
//		content.put("c2", new Object[] {
//				"Person",
//				"Male",
//				new Integer(22),
//				new String[] { "2 arms", "2 legs" },
//				Collections.singletonMap("key", "value"),
//				Collections.singletonMap("key", "value"),
//				Collections.singletonMap("key", "value")});
//		assertEquals(
//				"{\"c2\":" +
//				"[\"Person\"," +
//				"\"Male\"," +
//				"22," +
//				"[\"2 arms\",\"2 legs\"]," +
//				"{\"key\":\"value\"}," +
//				"{\"key\":\"value\"}," +
//				"{\"key\":\"value\"}]," +
//				"\"c1\":" +
//				"[\"id xyz\"]}",
//				getNormalResponseString(responseEngine, desc, content));
//		desc.clear();
//		content.clear();
		
		String xml =
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
				"<response>" +
				"<message>hello world</message>" +
				"</response>";
		
		Map desc = new LinkedHashMap();
		Map content = new LinkedHashMap();
		desc.put("xmlSource", "content");
		content.put("content", xml);
		assertEquals(
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
				"<response>\n" +
				"<message>hello world</message>\n" +
				"</response>\n",
				getNormalResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
	}

	public void testSendErrorResponse() throws ResponseProcessingException {
		assertEquals(
				"<error>error message</error>",
				getErrorResponseString(responseEngine, null, null));
		
		// EMPTY TESTS
		
		Map desc = new LinkedHashMap();
		Map content = new LinkedHashMap();
		assertEquals(
				"<error>error message</error>",
				getErrorResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", "test01");
		assertEquals(
				"<error>error message</error>",
				getErrorResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		// SINGLE OUTPUT TESTS
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", Collections.singleton("test02"));
		assertEquals(
				"<error>error message</error>",
				getErrorResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", Collections.singletonList("test03"));
		assertEquals(
				"<error>error message</error>",
				getErrorResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", Collections.singletonMap("test04a", "test04b"));
		assertEquals(
				"<error>error message</error>",
				getErrorResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, "content");
		content.put("content", new String[] { "test05a", "test05b" });
		assertEquals(
				"<error>error message</error>",
				getErrorResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		// MULTI OUTPUT TESTS
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, new String[] { "c1", "c2" });
		content.put("c1", "test06a");
		content.put("c2", "test06b");
		assertEquals(
				"<error>error message</error>",
				getErrorResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
		
		desc.put(AbstractResponseEngine.PARAM_OUTPUT_FALLBACK, new String[] { "c1", "c2" });
		content.put("c1", Collections.singleton("id xyz"));
		content.put("c2", new Object[] {
				"Person",
				"Male",
				new Integer(22),
				new String[] { "2 arms", "2 legs" },
				Collections.singletonMap("key", "value"),
				Collections.singletonMap("key", "value"),
				Collections.singletonMap("key", "value")});
		assertEquals(
				"<error>error message</error>",
				getErrorResponseString(responseEngine, desc, content));
		desc.clear();
		content.clear();
	}

	public void testSendEmptyCallback() throws ResponseProcessingException {
	}

	public void testSendNormalCallback() throws ResponseProcessingException {
	}

	public void testSendErrorCallback() throws ResponseProcessingException {
	}
}
