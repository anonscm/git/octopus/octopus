
Howto: Update octopus 1.x to octopus 2.0

* List of refactoring steps

  Many classes of tarent-octopus core would be refactored from version 1.x to
  version 2.0 so you must all your imports. Below you find an automatically way
  for doing these with Eclipse.

  The following list shows all renamed classes:

    * de.tarent.octopus.config.TcCommonConfig -> CommonConfig

    * de.tarent.octopus.config.TcConfig -> OctopusConfig

    * de.tarent.octopus.config.TcConfigException -> OctopusConfigException

    * de.tarent.octopus.config.TcModuleConfig -> ModuleConfig

    * de.tarent.octopus.config.TcPersonalConfig -> PersonalConfig

    * de.tarent.octopus.config.TvEnv -> OctopusEnvironment

    * de.tarent.octopus.content.Tc* -> remove Tc Prefix

    * de.tarent.octopus.data.Tc* -> remove Tc Prefix

    * de.tarent.octopus.data.Tar* -> remove Tar Prefix

    * de.tarent.octopus.request.Tc* -> remove Tc Prefix

    * de.tarent.octopus.request.directCall.Tc* -> remove Tc Prefix

    * de.tarent.octopus.request.servlet.Tc* -> remove Tc Prefix

    * de.tarent.octopus.response.Tc* -> remove Tc Prefix

    * de.tarent.octopus.security.Tc* -> remove Tc Prefix

    * de.tarent.octopus.security.ldap.Tc* -> remove Tc Prefix

    * de.tarent.octopus.soap.ldap.Tc* -> remove Tc Prefix

  The following class was moved:

    * de.tarent.octopus.content.All -> de.tarent.octopus.server.OctopusContextImpl.

  The following packages was moved:

    * de.tarent.octopus.soap -> de.tarent.octopus.response.soap

    * de.tarent.octopus.xmlrpc -> de.tarent.octopus.response.xmlrpc

    * de.tarent.octopus.request.Task & co. -> de.tarent.octopus.config

  The following package was deleted:

    * de.tarent.octopus.data\
      \
      It was replaced by tarent-database which was also published on
      evolvis.org.


* List of config changes

  (de) Zusätzlich finde ich sollten direkt noch die "erwarteten" Klassennamen
  für die Responseengines entfernt werden. Diese stattdessen lieber durch einen
  FQCN auf einen internen Namen (z.B. soap) mappen und in der Octopus
  Konfiguration festhalten. Ggf. direkt noch den Modulen ermöglichen ebenfalls
  Responseengines zu registieren bzw. zu überschreiben.


* Step by step migration howto

  Eclipse IDE required. 

	* First download the refactoring script {{update_octopus_1.x_to_octopus_2.0_eclipse_refactoring.xml}}

	* Checkout the old OCTOPUS version (CVS Tag PRE_OCTOPUS2_REFACTORING) in your local workspace (IMPORTANT give the name for the Project "PRE_OCTOPUS2_REFACTORING")
    
	* Checkout your java project, that you will migrate	  
	  	
	* Open the properties of your project and make following changes:	  
	  	
    	* go to "Libraries"-Tab and remove the library octopus-core-X.X.X.jar 	  
	  	
    	* go to "Projects"-Tab and add the Project "PRE_OCTOPUS2_REFACTORING"	  
	  	
    	* confirm changes (click ok)	  
	  	
	* Go to menue "Refactor" and select the item "Apply Script..."
	
    	* choose the downloaded refactoring script click "Finish"
    	
	* Open the properties of your project and make following changes:
	     		
    	* go to "Projects"-Tab and remove the Project "PRE_OCTOPUS2_REFACTORING"
    	
	* Edit the pom.xml of your project:
	
    	* set the Octopus version to 1.90.0-SNAPSHOT
    	
	* Start the console and do following:
	
    	* navigate to your project directory cd /home/username/workspace/yourProject
    	
    	* execute mvn -U eclipse:eclipse    	  	

	* Refresh your project view in eclipse     		    	
		    	
	* At last delete the Project "PRE_OCTOPUS2_REFACTORING" from your workspace     		    	