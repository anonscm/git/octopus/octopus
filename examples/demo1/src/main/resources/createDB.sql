CREATE TABLE addresses (
    pk INTEGER,
    salutation VARCHAR(20), 
    lastname VARCHAR(50), 
    firstname VARCHAR(50),
    age INTEGER
    );

INSERT INTO addresses (pk, salutation, lastname, firstname, age) VALUES (1, 'Herr', 'Meyer', 'Michael', 42);
INSERT INTO addresses (pk, salutation, lastname, firstname, age) VALUES (2, 'Frau', 'Mueller', 'Marianne', 24);
INSERT INTO addresses (pk, salutation, lastname, firstname, age) VALUES (3, 'Herr', 'Kohl', 'Rudolf', 6);
INSERT INTO addresses (pk, salutation, lastname, firstname, age) VALUES (4, 'Frau', 'Maus', 'Mini', 43);
INSERT INTO addresses (pk, salutation, lastname, firstname, age) VALUES (5, 'Herr', 'Maus', 'Maxi', 12);
INSERT INTO addresses (pk, salutation, lastname, firstname, age) VALUES (6, 'Herr', 'Anstedt', 'Karsten', 21);
INSERT INTO addresses (pk, salutation, lastname, firstname, age) VALUES (7, 'Herr', 'Willhelms', 'Michael', 44);
