<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" indent="yes"/>
<xsl:template match="addresses">

<html>
 <head>
 <!-- <meta http-equiv="content-type" content="text/html; charset=UTF-8"/> -->
  <title>Octopus DEMO Applikation (XSLT)</title>
    <style>
body {
	font-size: 11px;
	font-family: sans-serif;
    background-color: #a7b1ca;
}

table {
	border: 1px solid #000000;
	margin: 5px 5px 5px 5px;
	empty-cells: show;
    width: 99.5%;
}
th {
	border: 1px solid #000000;
	font-size: 11px;
	font-family: sans-serif;
	text-align: left;
	vertical-align: middle;
    background-color: #d6bcbc;
}
td {
	border: 1px solid #000000;
	font-size: 11px;
	font-family: sans-serif;
	text-align: left;
	vertical-align: top;
    background-color: #ffffff;
}

td.error {
	border: 4px solid #ff0000;
	font-size: 13px;
	font-family: sans-serif;
	text-align: left;
	vertical-align: top;
    font-color: #ff0000;
}
img {
	border: 0px;
}
p, div, span, font, pre {
	font-size: 11px;
	font-family: sans-serif;
	color: #000000;
	background-color: transparent;
	margin: 0px;
	padding: 0px;
}
form {
	margin: 0px;
	padding: 0px;
}

div.err {
	font-size: 14px;
	font-family: sans-serif;
	margin: 5px 5px 5px 5px;
	padding: 5px 5px 5px 5px;
	background-color: #ffffff;
	border: 4px solid #ff0000;
}

div.cont {
	font-size: 14px;
	font-family: sans-serif;
	margin: 5px 5px 5px 5px;
	padding: 5px 5px 5px 5px;
	background-color: #f3f8ff;
	border: 2px solid #000000;
}


</style>
 </head>
 <body>
  <h3>Octopus DEMO Applikation (XSLT)</h3>
  <table border="1" cellpadding="3">
    <tr><th colspan="4">Liste von Adressen</th></tr>
    <tr>
     <td><b>Anrede</b></td>
     <td><b>Name</b></td>
     <td><b>Vorname</b></td>
     <td><b><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text></b></td>
    </tr>
	<xsl:apply-templates/>
    </table>
 </body>
</html>
</xsl:template>

<xsl:template match="address">
	<tr>
    	<xsl:apply-templates select="salutation"/>
	<xsl:apply-templates select="lastname"/>
	<xsl:apply-templates select="firstname"/>
	<xsl:apply-templates select="pk"/>
	</tr>
</xsl:template>
<xsl:template match="salutation">
	<td>
		<xsl:value-of select="."/><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
	</td>
</xsl:template>
<xsl:template match="lastname">
	<td>
		<xsl:value-of select="."/><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
	</td>
</xsl:template>
<xsl:template match="firstname">
	<td>
		<xsl:value-of select="."/><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
	</td>
</xsl:template>
<xsl:template match="pk">
    <xsl:variable name="pk"><xsl:value-of select="."/></xsl:variable>  
	<td>
		<a href="viewPerson?id={$pk}">Detail</a>
	</td>
</xsl:template>
</xsl:stylesheet>