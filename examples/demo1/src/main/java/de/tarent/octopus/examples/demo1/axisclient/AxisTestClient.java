package de.tarent.octopus.examples.demo1.axisclient;

import java.rmi.RemoteException;

import javax.xml.rpc.ParameterMode;
import javax.xml.rpc.ServiceException;
import javax.xml.soap.SOAPException;

import org.apache.axis.Constants;
import org.apache.axis.client.Call;
import org.apache.axis.client.Service;
import org.apache.axis.message.SOAPHeaderElement;

/**
 * @author Alex Maier, tarent GmbH
 */
public class AxisTestClient {

	public static Object viewPerson() throws RemoteException, ServiceException,
			SOAPException {
		Service webService = new Service();
		Call call = (Call) webService.createCall();

		call
				.setTargetEndpointAddress("http://localhost:8080/octopus-demo/soap/demo/");
		call.setMaintainSession(false);
		SOAPHeaderElement addressElem = new SOAPHeaderElement(
				"http://schemas.xmlsoap.org/ws/2003/03/addressing", "Address");
		addressElem.setValue("http://localhost:8080/");
		SOAPHeaderElement replyTo = new SOAPHeaderElement(
				"http://schemas.xmlsoap.org/ws/2003/03/addressing", "ReplyTo");
		replyTo.addChildElement(addressElem);

		call.addHeader(replyTo);
		call.setOperationName("viewPerson");
		call.addParameter("id", Constants.XSD_STRING, ParameterMode.IN);
		call.setReturnType(Constants.XSD_ANY);

		Object[] parameters = new Object[] { "2" };
		Object result = call.invoke(parameters);

		return result;
	}

	public static Object listAddresses() throws RemoteException,
			ServiceException, SOAPException {
		Service webService = new Service();
		Call call = (Call) webService.createCall();

		call
				.setTargetEndpointAddress("http://localhost:8080/octopus-demo/soap/demo/");
		call.setMaintainSession(false);
		SOAPHeaderElement addressElem = new SOAPHeaderElement(
				"http://schemas.xmlsoap.org/ws/2003/03/addressing", "Address");
		addressElem.setValue("http://localhost:8080");
		SOAPHeaderElement replyTo = new SOAPHeaderElement(
				"http://schemas.xmlsoap.org/ws/2003/03/addressing", "ReplyTo");
		replyTo.addChildElement(addressElem);

		call.addHeader(replyTo);
		call.setOperationName("listAddresses");
		call.setReturnType(Constants.XSD_ANY);

		Object result = call.invoke(new Object[] {});
		return result;
	}

	public static void main(String args[]) throws Exception {
		System.out.println("listAddresses");
		System.out.println(listAddresses());
		System.out.println("viewPerson [id=2]");
		System.out.println(viewPerson());

	}

}
