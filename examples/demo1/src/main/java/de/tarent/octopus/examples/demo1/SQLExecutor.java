package de.tarent.octopus.examples.demo1;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import de.tarent.octopus.config.ModuleConfig;

/**
 * Worker zum Ausführen von SQL und Abholen von Resultsets
 * 
 * In diesem Demo baut der Worker seine JDBC Connection selbst auf. Für eine
 * Produktive Anwendung sollte Connection-Pooling mit geteilten Verbindungen für
 * unterschiedliche Worker und Wiederaufnahme der Verbindungen eingesetzt
 * werden.
 * 
 * 
 * @author Sebastian Mancke, tarent GmbH
 * @author Alex Maier, tarent GmbH
 */
public class SQLExecutor {

	private final static String CREATE_TABLE = "CREATE TABLE";
	private final static String SELECT = "SELECT";
	private final static String FROM = "FROM";
	private final static String ITEM_SEPARATOR = " ";
	private final static String SELECT_TABLE_EXISTS = SELECT + ITEM_SEPARATOR
			+ "*" + ITEM_SEPARATOR + FROM + ITEM_SEPARATOR;

	/** Feld im Content für den auszuführenden SQL String */
	public static final String CP_SQL_STATEMENT = "sqlStatement";

	/** Feld im Content für das Ergebnis der Anfrage */
	public static final String CP_RESULT_SET = "resultSet";

	/** Feld im Content für den Ersten Datensatz einer Anfrage */
	public static final String CP_FIRST_ROW = "firstRow";

	// Konstanten für die Konfigurationsparameter der JDBC Verbindung
	public static final String CFG_JDBC_DRIVER = "jdbcDriver";
	public static final String CFG_JDBC_CONNECTION_STRING = "jdbcConnectionString";
	public static final String CFG_JDBC_USERNAME = "jdbcUsername";
	public static final String CFG_JDBC_PASSWORD = "jdbcPassword";

	/**
	 * JDBC Verbindung des Workers
	 */
	protected Connection connection = null;

	/**
	 * Privater Logger für diese Klasse
	 */
	private static final Logger logger = Logger.getLogger(SQLExecutor.class
			.getName());

	private String driver = null;
	private String con = null;
	private String user = null;
	private String pwd = null;

	/**
	 * Initialisierungsmethode die automatisch vom Octopus aufgerufen wird. Hier
	 * wird die DB-Verbindung aufgebaut Anschliessend wir eine Tabelle in der
	 * datenbank angelegt mit initialen Daten.
	 * 
	 * @throws Exception
	 */
	public void init(ModuleConfig config) throws Exception {

		driver = config.getParam(CFG_JDBC_DRIVER);
		con = config.getParam(CFG_JDBC_CONNECTION_STRING);
		user = config.getParam(CFG_JDBC_USERNAME);
		pwd = config.getParam(CFG_JDBC_PASSWORD);
		if (driver == null || con == null || user == null || pwd == null)
			throw new RuntimeException(
					"Es sind nicht alle JDBC Konfigurationsparameter vorhanden.");

		try {
			Class.forName(driver);
			connection = DriverManager.getConnection(con, user, pwd);
			runSQLScriptFromResouces("createDB.sql");
		} catch (ClassNotFoundException e) {
			System.out
					.println("Fehler beim Erstellen der Datenbankverbindung: JDBC Treiber konnte nicht geladen werden ("
							+ driver + ")" + e.getMessage());
			logger
					.log(
							Level.SEVERE,
							"Fehler beim Erstellen der Datenbankverbindung: JDBC Treiber konnte nicht geladen werden ("
									+ driver + ")", e);
			throw new RuntimeException(
					"Fehler beim Erstellen der Datenbankverbindung: JDBC Treiber konnte nicht geladen werden ("
							+ driver + ")");
		} catch (SQLException e) {
			logger.log(Level.SEVERE,
					"Fehler beim Erstellen der Datenbankverbindung", e);
			throw (e);
		}
	}

	public static String[] INPUT_fetchResultSet = { CP_SQL_STATEMENT };
	public static String OUTPUT_fetchResultSet = CP_RESULT_SET;

	/**
	 * Führt eine SQL Anfrage aus und liefert das ResultSet als Liste von
	 * Java-Maps zurück Dies ist eine einfache Demo Implementierung. Im
	 * Produktiven Einsatz verwenden wird z.B. Wrapper über dem Resultset, die
	 * sich als Listen und Maps darstellen, ohne alle Daten vom Server holen zu
	 * müssen.
	 * 
	 * @param sql
	 *            SQL Anfrage, die ausgeführt werden soll
	 * @return Eine Liste aller Datensätze als java.util.Map
	 */
	public List fetchResultSet(String sql) throws SQLException {

		ResultSet rs = null;
		Statement stmt = null;
		try {
			logger.info(sql);
			// connection = DriverManager.getConnection(con, user, pwd);
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			ResultSetMetaData rsmd = rs.getMetaData();
			int numberOfColumns = rsmd.getColumnCount();
			List resultList = new LinkedList();
			while (rs.next()) {
				Map map = new HashMap();
				resultList.add(map);
				for (int i = 1; i <= numberOfColumns; i++) {
					map.put(rsmd.getColumnName(i).toLowerCase(), rs
							.getObject(i));
				}
			}
			return resultList;
		} catch (SQLException e) {
			logger.log(Level.SEVERE, "Fehler beim Ausführen einer SQL Anfrage",
					e);
			throw (e);
		} finally {
			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception e) {
					// Do nothing here
				}
		}
	}

	public static String[] INPUT_mapToXML = { "map" };
	public boolean[] MANDATORY_mapToXML = { false };
	public static String OUTPUT_mapToXML = "addressAsXML";

	public String mapToXML(Map map) {
		StringBuffer sb = new StringBuffer(
				"<?xml version='1.0' encoding='UTF-8'?>");
		sb.append("<address>");
		if (map != null)
			sb.append(map2XML(map));
		sb.append("</address>");
		return sb.toString();
	}

	public static String[] INPUT_listToXML = { "list" };
	public boolean[] MANDATORY_listToXML = { false };
	public static String OUTPUT_listToXML = "addressesAsXML";

	public String listToXML(List list) {
		StringBuffer sb = new StringBuffer(
				"<?xml version='1.0' encoding='UTF-8'?>");
		sb.append("<addresses>");
		if (list != null)
			sb.append(list2XML(list));
		sb.append("</addresses>");
		return sb.toString();
	}

	public static String[] INPUT_fetchFirstRow = { CP_SQL_STATEMENT };
	public static String OUTPUT_fetchFirstRow = CP_FIRST_ROW;

	/**
	 * Führt eine SQL Anfrage aus und liefert des ersten Datensatz des
	 * ResultSets zurück
	 * 
	 * @param sql
	 *            SQL Anfrage, die ausgeführt werden soll
	 * @return Eine Map mit dem Datensatz
	 */
	public Map fetchFirstRow(String sql) throws SQLException {

		ResultSet rs = null;
		Statement stmt = null;
		try {
			logger.info(sql);
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			ResultSetMetaData rsmd = rs.getMetaData();
			int numberOfColumns = rsmd.getColumnCount();
			if (rs.next()) {
				Map map = new HashMap();
				for (int i = 1; i <= numberOfColumns; i++) {
					map.put(rsmd.getColumnName(i).toLowerCase(), rs
							.getObject(i));
				}
				return map;
			}
			return null;
		} catch (SQLException e) {
			logger.log(Level.SEVERE, "Fehler beim Ausführen einer SQL Anfrage",
					e);
			throw (e);
		} finally {
			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception e) {
					logger.log(Level.SEVERE, "Fehler beim Schliessen der ", e);
				}
		}
	}

	public static String[] INPUT_cleanup = {};

	public void cleanup() {
		try {
			connection.close();
		} catch (SQLException e) {
			logger.log(Level.SEVERE, "Fehler beim Schließen der DB Verbindung",
					e);
		}
	}

	public void runSQLScriptFromResouces(String Filename) throws Exception {

		String sqlscript = null;
		BufferedInputStream inputstream = null;
		ByteArrayOutputStream outstream = null;
		try {
			inputstream = new BufferedInputStream(SQLExecutor.class
					.getResourceAsStream("/" + Filename));
			outstream = new ByteArrayOutputStream(1024);
			byte[] buffer = new byte[4096];
			int len;

			if (inputstream == null)
				throw new Exception("Could not open file!");

			while ((len = inputstream.read(buffer)) > 0) {
				outstream.write(buffer, 0, len);
			}
			outstream.close();
			inputstream.close();
			sqlscript = outstream.toString();
		} finally {
			try {
				outstream.close();
			} catch (Exception e) {
				logger.log(Level.SEVERE,
						"Fehler beim Schliessen des Output streams", e);
			}
			try {
				inputstream.close();
			} catch (Exception e) {
				logger.log(Level.SEVERE,
						"Fehler beim Schliessen des Input streams", e);
			}
		}
		String[] updatequerys = sqlscript.split(";");

		for (int i = 0; i < updatequerys.length; i++) {
			if (!updatequerys[i].trim().equals("")) {

				String updateQuery = updatequerys[i].trim();
				if (updateQuery.toUpperCase().startsWith(CREATE_TABLE)) {
					if (!tableExists(updateQuery)) {// check that the table does
													// not exist
						try {
							Statement statement = connection.createStatement();
							statement.executeUpdate(updateQuery);
							statement.close();
						} catch (SQLException sqle) {
							logger.log(Level.SEVERE,
									"Fehler beim Anlegen der Tabelle", sqle);
						}
					}
				} else {
					try {
						Statement statement = connection.createStatement();
						statement.executeUpdate(updateQuery);
						statement.close();
					} catch (SQLException sqle) {
						logger.log(Level.SEVERE,
								"Fehler beim Anlegen der Tabelle", sqle);
					}
				}
			}
		}
	}

	/**
	 * Check whether the table already exists in the database.
	 * 
	 * @param query
	 *            the query string which contains the table name
	 * @throws ClassNotFoundException
	 */
	private boolean tableExists(String query) throws ClassNotFoundException {
		String tableName = null;
		Statement stmt = null;
		ResultSet rs = null;
		if (query == null)
			return false;
		tableName = query.substring((CREATE_TABLE + ITEM_SEPARATOR).length(),
				query.indexOf(ITEM_SEPARATOR + "("));
		if (tableName == null)
			return false;
		try {
			stmt = connection.createStatement();
			rs = stmt.executeQuery(SELECT_TABLE_EXISTS + tableName);
			rs.next();
			logger.info("Trying to create the: " + tableName
					+ ". But this table already exists in the database.");
			return true; // the table already exists
		} catch (SQLException sqle) {
			// the table does not exist
			logger.info("Trying to create the: " + tableName
					+ ". The database does not contain this table.");
			return false;
		}
	}

	private String map2XML(Map result) {
		StringBuffer sb = new StringBuffer("");
		if (result.size() == 0)
			sb.toString();
		if (result.size() == 1) {
			Object key = result.keySet().toArray()[0];
			if (key instanceof String) {
				sb.append("<" + (String) key + ">");
				Object value = result.get(key);
				if (value instanceof Map)
					sb.append(map2XML((Map) value));
				if (value instanceof List)
					sb.append(list2XML((List) value));
				if (value instanceof String)
					sb.append(value);
				else
					sb.append(value.toString());
				sb.append("</" + (String) key + ">");
			}
		}
		if (result.size() > 1) {
			Iterator iter = result.keySet().iterator();
			while (iter.hasNext()) {
				Object key = iter.next();
				if (key instanceof String) {
					sb.append("<" + (String) key + ">");
					Object value = result.get(key);
					if (value instanceof Map)
						sb.append(map2XML((Map) value));
					if (value instanceof List)
						sb.append(list2XML((List) value));
					if (value instanceof String)
						sb.append(value);
					else
						sb.append(value.toString());
					sb.append("</" + (String) key + ">");
				}
			}

		}
		return sb.toString();
	}

	private String list2XML(List result) {
		StringBuffer sb = new StringBuffer("");
		if (result == null || result.size() == 0)
			return sb.toString();

		for (int i = 0; i < result.size(); i++) {
			if (result.get(i) instanceof Map) {
				sb.append("<address>");
				sb.append(map2XML((Map) result.get(i)));
				sb.append("</address>");
			}
		}
		return sb.toString();

	}
}