package de.tarent.octopus.examples.demo1;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import de.tarent.octopus.server.Closeable;
import de.tarent.octopus.server.OctopusContext;

/**
 * Worker for Testing the cleanup-Feature from octopus
 * 
 * @author Sebastian Mancke, tarent GmbH
 */
public class CleanupTest {

	private static final Logger logger = Logger.getLogger(CleanupTest.class
			.getName());

	public static String[] INPUT_appendCleanupObjects = {};

	/**
	 * Appends an Object of type Closeable
	 */
	public void appendCleanupObjects(OctopusContext cntx) {
		final CallingTester callingTester = new CallingTester();

		final Runnable r = new Runnable() {
			public void run() {
				callingTester.removeCleanupObject(this);
				logger.info("Doing r1.close()");
			}
		};
		cntx.addCleanupCode(r);
		callingTester.addCleanupObject(r);

		final Closeable c1 = new Closeable() {
			public void close() {
				callingTester.removeCleanupObject(this);
				logger.info("Doing c1.run()");
				// doing some ugly stuff
				throw new RuntimeException("Test Exception");
			}
		};
		cntx.addCleanupCode(c1);
		callingTester.addCleanupObject(c1);

		final Closeable c2 = new Closeable() {
			public void close() {
				callingTester.removeCleanupObject(this);
				logger.info("Doing c2.run()");
			}
		};
		cntx.addCleanupCode(c2);
		callingTester.addCleanupObject(c2);

		callingTester.start();
	}

	public class CallingTester extends Thread {

		List cleanUpObjects = new ArrayList();

		public void addCleanupObject(Object o) {
			cleanUpObjects.add(o);
		}

		public void removeCleanupObject(Object o) {
			cleanUpObjects.remove(o);
		}

		@Override
		public void run() {
			try {
				Thread.sleep(1000);
				if (!cleanUpObjects.isEmpty()) {
					logger
							.severe("Folgende Cleanup Objects wurden nicht ausgefürt: "
									+ cleanUpObjects);
				} else
					logger.info("Alle Cleanup Objects wurden ausgefürt.");
			} catch (InterruptedException e) {

			}
		}
	}

}