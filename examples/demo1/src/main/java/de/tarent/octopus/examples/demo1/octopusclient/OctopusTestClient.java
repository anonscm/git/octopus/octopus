package de.tarent.octopus.examples.demo1.octopusclient;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import de.tarent.octopus.client.OctopusConnection;
import de.tarent.octopus.client.OctopusConnectionFactory;
import de.tarent.octopus.client.OctopusResult;
import de.tarent.octopus.client.OctopusTask;

/**
 * @author Alex Maier, tarent GmbH
 */
public class OctopusTestClient {
	private static OctopusConnection connection;
	private static final Logger logger = Logger
			.getLogger(OctopusTestClient.class.getName());

	private static void setUpConnection() {
		if (connection == null) {
			OctopusConnectionFactory factory = OctopusConnectionFactory
					.getInstance();

			Map properties = new HashMap();
			properties.put("serviceURL",
					"http://localhost:8080/octopus-demo/soap/demo/");
			properties.put("username", "test");
			properties.put("password", "tester");
			properties.put("type", "remote");
			properties.put("module", "demo");
			// properties.put("authType", "callParam");
			properties.put("autoLogin", "true");
			properties.put("keepSessionAlive", "0");
			properties.put("connectionTracking", "false");
			properties.put("useSessionCookie", "false");

			factory.setConfiguration("demo", properties);
			connection = factory.getConnection("demo");
		}
	}

	public static OctopusResult listAddresses() {
		setUpConnection();
		OctopusTask task = connection.getTask("listAddresses");
		return task.invoke();
	}

	public static OctopusResult viewPerson() {
		setUpConnection();
		OctopusTask task = connection.getTask("viewPerson");
		task.add("id", "2");
		return task.invoke();
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		System.out.println(listAddresses().getData("addresses"));
		System.out.println(viewPerson().getData("address"));
	}
}
