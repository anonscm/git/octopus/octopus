package de.tarent.octopus.examples.demo1;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import de.tarent.octopus.response.binary.BinaryResponseEngine;
import de.tarent.octopus.server.OctopusContext;

/**
 * Einfacher Testworker, der ein Bisschen SQL zur Ausführung bereit stellt.
 * 
 * @author Sebastian Mancke, tarent GmbH
 */
public class DemoWorker {

	/**
	 * Privater Logger für diese Klasse
	 */
	private static final Logger logger = Logger.getLogger(DemoWorker.class
			.getName());

	public static String[] INPUT_createPersonSelect = { "id" };
	public static String OUTPUT_createPersonSelect = SQLExecutor.CP_SQL_STATEMENT;

	/**
	 * Liefert den Select String für die Selektion einer Person
	 */
	public String createPersonSelect(Integer id) {
		return "SELECT * FROM addresses WHERE pk = " + id;
	}

	public static String[] INPUT_createImageForPerson = { SQLExecutor.CP_FIRST_ROW };
	public static String OUTPUT_createImageForPerson = "stream";

	/**
	 * Generates an image with the person data.
	 * 
	 * @param oct
	 *            {@link OctopusContext}
	 * @param firstRow
	 *            the Map with the person data
	 * @return Map with the parameters
	 * @throws FileNotFoundException
	 */
	public Map createImageForPerson(OctopusContext oct, Map firstRow) {
		byte[] imageByte = new byte[0];
		java.io.ByteArrayOutputStream out = null;
		BufferedImage bim = null;
		Graphics g = null;
		bim = new BufferedImage(200, 100, BufferedImage.TYPE_INT_RGB);
		g = bim.getGraphics();
		g.setColor(Color.RED);

		g.drawString("Anrede:", 5, 20);
		g.drawString((String) firstRow.get("salutation"), 100, 20);
		g.drawString("Name:", 5, 35);
		g.drawString((String) firstRow.get("lastname"), 100, 35);
		g.drawString("Vorname:", 5, 50);
		g.drawString((String) firstRow.get("firstname"), 100, 50);
		g.drawString("Alter:", 5, 65);
		g.drawString(String.valueOf(firstRow.get("age")), 100, 65);
		g.dispose();

		try {
			out = new java.io.ByteArrayOutputStream();
			javax.imageio.ImageIO.write(bim, "png", out);
			imageByte = out.toByteArray();
			out.flush();
			out.close();
		} catch (IllegalArgumentException e) {
			logger.log(Level.SEVERE, "Fehler beim generieren eines Images", e);
		} catch (java.io.IOException e) {
			logger.log(Level.SEVERE, "Fehler beim generieren eines Images", e);
		}

		Map stream = new HashMap();
		stream.put(BinaryResponseEngine.PARAM_TYPE,
				BinaryResponseEngine.BINARY_RESPONSE_TYPE_STREAM);
		stream.put(BinaryResponseEngine.PARAM_FILENAME, "Person_"
				+ String.valueOf(firstRow.get("pk")) + ".png");
		stream.put(BinaryResponseEngine.PARAM_MIMETYPE, "image/png");
		stream.put(BinaryResponseEngine.PARAM_STREAM, new ByteArrayInputStream(
				imageByte));
		stream.put(BinaryResponseEngine.PARAM_IS_ATTACHMENT, Boolean.TRUE);
		return stream;
	}

	public static String[] INPUT_createImageForAddresses = { SQLExecutor.CP_RESULT_SET };
	public static String OUTPUT_createImageForAddresses = "stream";

	/**
	 * Generates an image with addresses.
	 * 
	 * @param oct
	 *            {@link OctopusContext}
	 * @param firstRow
	 *            the List with addresses
	 * @return Map with the parameters
	 * @throws FileNotFoundException
	 */
	public Map createImageForAddresses(OctopusContext oct, List resultSet) {
		byte[] imageByte = new byte[0];
		java.io.ByteArrayOutputStream out = null;
		BufferedImage bim = null;
		Graphics g = null;
		int imageHeight = 50 + resultSet.size() * 15;
		bim = new BufferedImage(200, imageHeight, BufferedImage.TYPE_INT_RGB);
		g = bim.getGraphics();
		g.setColor(Color.RED);
		int nextLine = 20;
		// draw Header
		g.drawString("Anrede:", 5, nextLine);
		g.drawString("Name:", 70, nextLine);
		g.drawString("Vorname:", 135, nextLine);
		nextLine = nextLine + 15;
		// draw Line
		g.drawLine(5, nextLine, 195, nextLine);
		nextLine = nextLine + 15;
		// draw persons
		for (int i = 0; i < resultSet.size(); i++) {
			Map personMap = (Map) resultSet.get(i);
			g.drawString((String) personMap.get("salutation"), 5, nextLine);
			g.drawString((String) personMap.get("lastname"), 70, nextLine);
			g.drawString((String) personMap.get("firstname"), 135, nextLine);
			nextLine = nextLine + 15;
		}
		g.dispose();

		try {
			out = new java.io.ByteArrayOutputStream();
			javax.imageio.ImageIO.write(bim, "png", out);
			imageByte = out.toByteArray();
			out.flush();
			out.close();
		} catch (IllegalArgumentException e) {
			logger.log(Level.SEVERE, "Fehler beim generieren eines Images", e);
		} catch (java.io.IOException e) {
			logger.log(Level.SEVERE, "Fehler beim generieren eines Images", e);
		}

		Map stream = new HashMap();
		stream.put(BinaryResponseEngine.PARAM_TYPE,
				BinaryResponseEngine.BINARY_RESPONSE_TYPE_STREAM);
		stream.put(BinaryResponseEngine.PARAM_FILENAME, "Addresses.png");
		stream.put(BinaryResponseEngine.PARAM_MIMETYPE, "image/png");
		stream.put(BinaryResponseEngine.PARAM_STREAM, new ByteArrayInputStream(
				imageByte));
		stream.put(BinaryResponseEngine.PARAM_IS_ATTACHMENT, Boolean.TRUE);
		return stream;
	}
}