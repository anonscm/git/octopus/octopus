/**
 * 
 */
package org.evolvis.octopus.filter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

/**
 * @author Christoph Kunecki (<a href="mailto:c.kunecki@tarent.de">c.kunecki@tarent.de</a>) tarent GmbH
 */
public final class RequestWrapper extends HttpServletRequestWrapper {

	/**
	 * @param request
	 */
	public RequestWrapper(HttpServletRequest request) {
		super(request);
	}

	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServletRequestWrapper#getHeader(java.lang.String)
	 */
	@Override
	public String getHeader(String name) {
		String value = super.getHeader(name);
		if (value == null)
			return null;
		return cleanXSS(value); 
	}

	/* (non-Javadoc)
	 * @see javax.servlet.ServletRequestWrapper#getParameter(java.lang.String)
	 */
	@Override
	public String getParameter(String name) {
		String value = super.getParameter(name);
		if (value == null)
			return null;
		return cleanXSS(value); 
	}

	/* (non-Javadoc)
	 * @see javax.servlet.ServletRequestWrapper#getParameterValues(java.lang.String)
	 */
	@Override
	public String[] getParameterValues(String name) {
		String[] values = super.getParameterValues(name);
		if (values==null)
			return null;
		int count = values.length;
		String[] encodedValues = new String[count];
		for (int i = 0; i < count; i++){
			encodedValues[i] = cleanXSS(values[i]);
		}
		return encodedValues;   
	}
	
	private String cleanXSS(String value) {
		value = value.replace("\"", "&quot;").replaceAll("<", "").replaceAll(">", "");
		return value;
	}
}
