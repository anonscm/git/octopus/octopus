package org.evolvis.octopus.configuration;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.RandomStringUtils;

public class Utils {
	public static void main(String[] args) {
		System.out.println(getWorkDirectory());
	}
	
	public static File getWorkDirectory() {
		return getWorkDirectory(false);
	}

	public static File getWorkDirectory(boolean deleteOnExit) {
		StringBuffer pathname = new StringBuffer();
		pathname.append(System.getProperty("java.io.tmpdir"));
		pathname.append(File.separator);
		pathname.append("octopus");
		pathname.append(File.separator);
		pathname.append("work");
		pathname.append(File.separator);
		pathname.append(new SimpleDateFormat("yyyy-MM-dd_HH-mm_").format(new Date()));
		pathname.append(RandomStringUtils.randomAlphanumeric(8));
		pathname.append(File.separator);
		
		File workdir = new File(pathname.toString()).getAbsoluteFile();
		if (workdir.exists())
			return getWorkDirectory();
		workdir.mkdirs();
		if (deleteOnExit)
			workdir.deleteOnExit();
		return workdir;
	}
}
