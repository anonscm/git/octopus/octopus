package org.evolvis.octopus.configuration;

import org.apache.commons.jxpath.JXPathContext;
import org.evolvis.octopus.management.modules.Module;
import org.evolvis.octopus.management.modules.ModuleState;
import org.evolvis.octopus.management.modules.Operation;
import org.evolvis.octopus.management.modules.Service;

public class JXPathTest {
	public static void main(String[] args) throws Exception {
		
		Module module = new Module();
		module.setId("asda8934");
		module.setName("sample-01");
		module.setVersion("1.0.0");
		module.setState(ModuleState.RUNNING);
		
		module.getServices().add(new Service());
		module.getServices().add(new Service());
		
		Operation operation = new Operation();
		operation.setId("asd");
		operation.setName("asd2");
		
		Service service = new Service();
		service.getOperations().add(operation);
		module.getServices().add(service);
		
		System.out.println(module);
		
		JXPathContext context = JXPathContext.newContext(module);
		
		System.out.println(context.getPointer("/").getNode());
		System.out.println(context.getPointer("/").getRootNode());
		System.out.println(context.getPointer("/").getValue());
		
		System.out.println();
		
		System.out.println(context.getPointer("/[services/operations/id=asd]").getNode());
		System.out.println(context.getPointer("/services/operations").getRootNode());
		System.out.println(context.getPointer("/services/operations").getValue());

		System.out.println();
		
		System.out.println(context.getPointer("/services").getNode());
		System.out.println(context.getPointer("/services").getRootNode());
		System.out.println(context.getPointer("/services").getValue());
	}
}
