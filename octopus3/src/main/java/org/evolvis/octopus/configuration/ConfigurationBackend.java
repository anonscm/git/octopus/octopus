package org.evolvis.octopus.configuration;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.SchemaOutputResolver;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.Result;
import javax.xml.transform.stream.StreamResult;

import org.evolvis.octopus.management.modules.Module;
import org.evolvis.octopus.management.modules.ModuleState;
import org.evolvis.octopus.management.modules.Operation;
import org.evolvis.octopus.management.modules.Service;

public class ConfigurationBackend {
	public static void main(String[] args) throws Exception {
		
		JAXBContext context = JAXBContext.newInstance(OctopusConfigurationEntry.class, Module.class);
		
		if (true) {
			final OutputStreamWriter schemaWriter = new OutputStreamWriter(System.out) {
				@Override
				public void write(char[] cbuf, int off, int len) throws IOException {
					super.write(new String(cbuf, off, len).replaceAll("  ", "\t"));
				}
				
				@Override
				public void close() throws IOException {
					flush();
				}
			};
			
			context.generateSchema(new SchemaOutputResolver() {
				@Override
				public Result createOutput(String namespaceUri, String suggestedFileName) throws IOException {
					StreamResult streamResult = new StreamResult();
					streamResult.setSystemId("system.out");
					streamResult.setWriter(schemaWriter);
					return streamResult;
				}
			});
			schemaWriter.flush();
		}
		
		Marshaller marshaller = context.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		marshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, "asdasd.dtd");
		//marshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);
		
		Unmarshaller unmarshaller = context.createUnmarshaller();
		//unmarshaller.setProperty(Unmarshaller., value)
		
		Module module = new Module();
		module.setId("asda8934");
		module.setName("sample-01");
		module.setVersion("1.0.0");
		module.setState(ModuleState.RUNNING);
		
		module.getServices().add(new Service());
		module.getServices().add(new Service());
		
		Operation operation = new Operation();
		operation.setId("asd");
		operation.setName("asd2");
		
		Service service = new Service();
		service.getOperations().add(operation);
		module.getServices().add(service);

		StringWriter writer = new StringWriter();
		
		marshaller.marshal(new OctopusConfigurationEntry(module), writer);
		
		writer.close();
		
		System.out.println(writer);
		
		System.out.println(module);
		System.out.println(module.clone());
		
		StringReader stringReader = new StringReader(writer.toString());
		XMLEventReader eventReader = XMLInputFactory.newInstance().createXMLEventReader(new StringReader(writer.toString()));
		XMLStreamReader streamReader = XMLInputFactory.newInstance().createXMLStreamReader(new StringReader(writer.toString()));
		
		try {
			OctopusConfigurationEntry reference = (OctopusConfigurationEntry) unmarshaller.unmarshal(stringReader);
			System.out.println(reference);
			System.out.println(reference.getValue());
		} catch (Exception e) {
			System.err.println(e);
			//e.printStackTrace();
		}
		try {
			OctopusConfigurationEntry reference = (OctopusConfigurationEntry) unmarshaller.unmarshal(eventReader);
			System.out.println(reference);
			System.out.println(reference.getValue());
		} catch (Exception e) {
			System.err.println(e);
			//e.printStackTrace();
		}
		try {
			OctopusConfigurationEntry reference = (OctopusConfigurationEntry) unmarshaller.unmarshal(streamReader);
			System.out.println(reference);
			System.out.println(reference.getValue());
		} catch (Exception e) {
			System.err.println(e);
			//e.printStackTrace();
		}
	}
}
