package org.evolvis.octopus.configuration;

import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class OctopusConfigurationEntry {
	private Object value;

	public OctopusConfigurationEntry() {
		this.value = null;
	}
	
	public OctopusConfigurationEntry(Object value) {
		this.value = value;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	@XmlElement
	public String getType() {
		return value != null ? value.getClass().getName() : "null";
	}

	@XmlElement
	public String getHashCode() {
		return Integer.toBinaryString(value.hashCode());
	}
	
	@XmlElement
	public Date getLastModification() {
		return new Date();
	}
}
