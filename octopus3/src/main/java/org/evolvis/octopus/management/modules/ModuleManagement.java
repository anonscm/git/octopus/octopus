package org.evolvis.octopus.management.modules;

import java.util.Set;

import org.evolvis.octopus.management.application.Stage;

public interface ModuleManagement {
	public Set<Module> getModuleList(Stage stage);
	
	public Module getModule(Stage stage, Module module);
	
	public Module setModule(Stage stage, Module module);
}
