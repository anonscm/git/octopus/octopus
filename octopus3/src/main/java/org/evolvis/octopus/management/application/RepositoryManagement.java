package org.evolvis.octopus.management.application;

import java.io.File;

import org.evolvis.octopus.management.modules.Module;

public interface RepositoryManagement {
	public void installModule(File path);
	public void uninstallModule(File path);
	public void uninstallModule(Module module);
}
