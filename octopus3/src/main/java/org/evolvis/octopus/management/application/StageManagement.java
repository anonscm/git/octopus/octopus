package org.evolvis.octopus.management.application;

import java.util.Set;

public interface StageManagement {
	public Set<Stage> getStageList();
	
	public Stage getStage(Stage stage);
	
	public Stage setStage(Stage stage);
}
