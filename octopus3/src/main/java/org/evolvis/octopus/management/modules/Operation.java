package org.evolvis.octopus.management.modules;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlID;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class Operation {
	private String id;
	private String name;
	private Object source;
	private List<Parameter> inputParameters = new LinkedList<Parameter>();
	private List<Parameter> outputParameters = new LinkedList<Parameter>();

	public Operation() {
		// Nothing...
	}
	
	public Operation(Service parent) {
		parent.addOperation(this);
	}
	
	@XmlID
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Object getSource() {
		return source;
	}

	public void setSource(Object source) {
		this.source = source;
	}

	@XmlElementWrapper(name="inputParameters")
	@XmlElement(name="parameter")
	public List<Parameter> getInputParameters() {
		return inputParameters;
	}

	public void setInputParameters(List<Parameter> inputParameters) {
		this.inputParameters = inputParameters;
	}

	public void addInputParameter(Parameter parameter) {
		if (inputParameters == null)
			inputParameters = new ArrayList<Parameter>();
		inputParameters.add(parameter);
	}
	
	@XmlElementWrapper(name="outputParameters")
	@XmlElement(name="parameter")
	public List<Parameter> getOutputParameters() {
		return outputParameters;
	}

	public void setOutputParameters(List<Parameter> outputParameters) {
		this.outputParameters = outputParameters;
	}

	public void addOutputParameter(Parameter parameter) {
		if (outputParameters == null)
			outputParameters = new ArrayList<Parameter>();
		outputParameters.add(parameter);
	}
	
	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
