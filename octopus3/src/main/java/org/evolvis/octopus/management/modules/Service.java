package org.evolvis.octopus.management.modules;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class Service {
	private String id; // unique id of the service
	private String name; // human readable name of the service
	private Object source; // fully qualified class name of ws implementation
	private List<Operation> operations = new LinkedList<Operation>();
	private Boolean dynamicService;
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Object getSource() {
		return source;
	}
	
	public void setSource(Object source) {
		this.source = source;
	}
	
	@XmlElementWrapper(name = "operations")
	@XmlElement(name = "operation")
	public List<Operation> getOperations() {
		return operations;
	}
	
	public void setOperations(List<Operation> operations) {
		this.operations = operations;
	}
	
	public void addOperation(Operation operation) {
		if (operations == null)
			operations = new ArrayList<Operation>();
		operations.add(operation);
	}
	
	public Boolean getDynamicService() {
		return dynamicService;
	}
	
	public void setDynamicService(Boolean dynamicService) {
		this.dynamicService = dynamicService;
	}
	
	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
