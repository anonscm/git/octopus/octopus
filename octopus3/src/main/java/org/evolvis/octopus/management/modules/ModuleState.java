package org.evolvis.octopus.management.modules;

/**
 * The module state defines if an module is currently stopped, starting,
 * running or stopping.
 */
public enum ModuleState {
	STOPPED,
	STARTING,
	RUNNING,
	STOPPING
}
