package org.evolvis.octopus.management.backends;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class MavenPomLoader {
	public static void main(String[] args) {
		try {
			String home = System.getProperty("user.home");
			String repo = home + "/.m2/repository";
			System.out.println("[    OK    ] Home directory: " + home);
			System.out.println("[    OK    ] Maven repository: " + repo);
			
			Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(home + "/workspace/octopus3/pom.xml");
			XPath path = XPathFactory.newInstance().newXPath();
			Map<String, String> replacements = new LinkedHashMap<String, String>();
			
			NodeList properties = (NodeList) path.evaluate("//project/properties/*", document, XPathConstants.NODESET);
			for (int i = 0; i < properties.getLength(); i++) {
				String key = properties.item(i).getNodeName();
				String value = properties.item(i).getTextContent();
				System.out.println("[    OK    ] Found property: " + key + " = " + value);
				key = "\\$\\{" + key.replaceAll("\\.", "\\\\.") + "\\}";
				replacements.put(key, value);
			}
			
			NodeList dependencies = (NodeList) path.evaluate("//project/dependencies/dependency", document, XPathConstants.NODESET);
			for (int i = 0; i < dependencies.getLength(); i++) {
				Node groupIdNode = (Node) path.evaluate("groupId", dependencies.item(i), XPathConstants.NODE);
				Node artifactIdNode = (Node) path.evaluate("artifactId", dependencies.item(i), XPathConstants.NODE);
				Node versionNode = (Node) path.evaluate("version", dependencies.item(i), XPathConstants.NODE);
				Node scopeNode = (Node) path.evaluate("scope", dependencies.item(i), XPathConstants.NODE);
				
				String groupId = groupIdNode != null ? groupIdNode.getTextContent() : null;
				String artifactId = artifactIdNode != null ? artifactIdNode.getTextContent() : null;
				String version = versionNode != null ? versionNode.getTextContent() : null;
				String scope = scopeNode != null ? scopeNode.getTextContent() : null;
				
				if (groupId == null || artifactId == null || version == null)
					continue;
				if (!(scope == null || scope.equals("") || scope.equals("package")))
					continue;
				
				for (Map.Entry<String, String> entry : replacements.entrySet()) {
					groupId = groupId.replaceAll(entry.getKey(), entry.getValue());
					artifactId = artifactId.replaceAll(entry.getKey(), entry.getValue());
					version = version.replaceAll(entry.getKey(), entry.getValue());
				}
				
				String file = repo + "/" + groupId.replaceAll("\\.", "/") + "/" + artifactId + "/" + version + "/" + artifactId + "-" + version + ".jar";
				if (new File(file).exists()) {
					System.out.println("[    OK    ] " + file);
					continue;
				}
				
				String file2 = repo + "/" + groupId + "/" + artifactId + "/" + version + "/" + artifactId + "-" + version + ".jar";
				if (new File(file2).exists()) {
					System.out.println("[    OK    ] " + file2);
					continue;
				}
				
				System.out.println("[NOT FOUND!] " + file);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
