package org.evolvis.octopus.test;

import java.io.IOException;
import java.io.StringWriter;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.SchemaOutputResolver;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.transform.Result;
import javax.xml.transform.stream.StreamResult;

@XmlRootElement
public class OctopusXMLTest {
	public static void main(String[] args) {
		OctopusXMLTest test = new OctopusXMLTest();
	    for (Map.Entry<String, String> entry : test.getCompleteTestResult().entrySet()) {
	    	System.out.println(entry.getKey() + "\t = " + entry.getValue());
	    }
    }
	
	public Map<String, String> getCompleteTestResult() {
		Map<String, String> testResult = new LinkedHashMap<String, String>();
		
		ClassLoader original = getClass().getClassLoader();
		
		System.out.println(original);
		System.out.println(original instanceof URLClassLoader);
		
		URL[] urls = ((URLClassLoader) original).getURLs();
		
		try {
			String className = getClass().getName();
			for (Method method : getClass().getMethods()) {
				String methodName = method.getName();
				if (!methodName.startsWith("test"))
					continue;
				
				try {
					ClassLoader cl = new URLClassLoader(urls);
					Class testClass = cl.loadClass(getClass().getName());
					Object testInstance = testClass.newInstance();
					Method testMethod = testClass.getMethod(methodName, new Class[0]);
					
					testResult.put(className + "#" + methodName, (String) testMethod.invoke(testInstance, new Object[0]));
					
				} catch (Throwable t) {
					testResult.put(className + "#" + methodName, t.toString());
				}
			}
		} catch (Throwable t) {
			testResult.put(getClass().getName(), t.toString());
		}
		
		return testResult;
	}
	
	public String testJAXBContext() {
		try {
			JAXBContext.newInstance(OctopusXMLTest.class);
			return null;
		} catch (Throwable t) {
			return t.toString();
		}
	}

	public String testJAXBSchema() {
		try {
			final StringWriter writer = new StringWriter();
			JAXBContext.newInstance(OctopusXMLTest.class).generateSchema(new SchemaOutputResolver() {
				@Override
				public Result createOutput(String namespaceUri, String suggestedFileName) throws IOException {
					StreamResult streamResult = new StreamResult();
					streamResult.setSystemId(getClass().getSimpleName());
					streamResult.setWriter(writer);
					return streamResult;
				}
			});
			return null;
		} catch (Throwable t) {
			return t.toString();
		}
	}
	
	public String testXMLInputFactory() {
		try {
			XMLInputFactory factory = XMLInputFactory.newInstance();
			factory.setProperty(XMLInputFactory.SUPPORT_DTD, false);
			factory.setProperty(XMLInputFactory.IS_VALIDATING, false);
			return null;
		} catch (Throwable t) {
			return t.toString();
		}
	}
	
	public String testXMLOutputFactory() {
		try {
			XMLOutputFactory.newInstance();
			return null;
		} catch (Throwable e) {
			return null;
		}
	}
	
	public String testJAXBMarshaller() {
		try {
			StringWriter stream = new StringWriter();
			Marshaller marshaller = JAXBContext.newInstance(OctopusXMLTest.class).createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, false);
			marshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);
			marshaller.marshal(this, stream);
			return null;
		} catch (Throwable t) {
			return t.toString();
		}
	}

	public String testJAXBUnmarshaller() {
		try {
			StringWriter stream = new StringWriter();
			Marshaller marshaller = JAXBContext.newInstance(OctopusXMLTest.class).createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, false);
			marshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);
			marshaller.marshal(this, stream);
			return null;
		} catch (Throwable t) {
			return t.toString();
		}
	}

	public String testXMLStreamWriter() {
		try {
			StringWriter stream = new StringWriter();
			XMLStreamWriter writer = XMLOutputFactory.newInstance().createXMLStreamWriter(stream);
			Marshaller marshaller = JAXBContext.newInstance(OctopusXMLTest.class).createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, false);
			marshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);
			marshaller.marshal(this, writer);
			writer.close();
			return null;
		} catch (Throwable t) {
			return t.toString();
		}
	}
	
	public String testXMLEventWriter() {
		try {
			StringWriter stream = new StringWriter();
			XMLEventWriter writer = XMLOutputFactory.newInstance().createXMLEventWriter(stream);
			Marshaller marshaller = JAXBContext.newInstance(OctopusXMLTest.class).createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, false);
			marshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);
			marshaller.marshal(this, writer);
			writer.close();
			return null;
		} catch (Throwable t) {
			return t.toString();
		}
	}
}
