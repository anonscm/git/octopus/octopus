package org.evolvis.octopus.backend.scio;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.evolvis.octopus.backend.http.HTTPMapping;
import org.evolvis.octopus.backend.http.HTTPServer;
import org.evolvis.octopus.management.modules.Operation;
import org.evolvis.octopus.management.modules.Parameter;
import org.evolvis.octopus.management.modules.Service;

public class Testrun {
	public static void main(String[] args) throws Exception {
	    HTTPServer server = new HTTPServer();
	    server.openSocket(8888);
	    server.addChildren("/bla", new HTTPMapping() {
	    	@Override
	    	public boolean invoke(String urlPart, InputStream is, OutputStream os) throws IOException {
	    	    return super.invoke(urlPart, is, os);
	    	}
	    });
	    
	    ScioBackend backend = new ScioBackend();
	    
	    Service service = new Service();
	    service.setId("1");
	    service.setName(Testservice.class.getSimpleName());
	    service.setSource(Testservice.class);
	    
	    Operation operation = new Operation(service);
	    operation.setId("1");
	    operation.setName("sum");
	    operation.setSource(Testservice.class.getMethod("sum", Integer.class, Integer.class));
	    operation.addInputParameter(new Parameter("a", Integer.class));
	    operation.addInputParameter(new Parameter("b", Integer.class));
	    operation.addOutputParameter(new Parameter("result", Integer.class));
	    
	    System.out.println("  service: " + service);
		System.out.println("    operation: " + operation);
		
		backend.deployService(service, null);
    }
}
