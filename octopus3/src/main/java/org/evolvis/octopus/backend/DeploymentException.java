package org.evolvis.octopus.backend;

/** This exception is thrown when something goes wrong
 * while deploying or undeploying a service.
 * 
 * @author Martin Pelzer, tarent GmbH
 *
 */
public class DeploymentException extends Exception {

	
	public DeploymentException(String msg) {
		super(msg);
	}
	
	
	public DeploymentException(String msg, Throwable cause) {
		super(msg, cause);
	}
	
}
