package org.evolvis.octopus.backend.scio;

import java.io.IOException;
import java.io.Writer;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;

import org.evolvis.octopus.backend.BackendDeployable;
import org.evolvis.octopus.backend.DeploymentException;
import org.evolvis.octopus.backend.InitializationException;
import org.evolvis.octopus.core.impl.TreeInvocationHandler;
import org.evolvis.octopus.management.modules.Service;
import org.evolvis.scio.bind.BindContext;

public class ScioBackend extends TreeInvocationHandler implements BackendDeployable {
	private List<Service> services = new LinkedList<Service>();
	
	@Override
	public void initBackend() throws InitializationException {
	}
	
	@Override
	public void deployService(Service service, ServletConfig servletConfig) throws DeploymentException {
		services.add(service);
	}
	
	@Override
	public void undeployService(Service service) throws DeploymentException {
		services.remove(service);
	}
	
	@Override
	public boolean invoke(String urlPart, HttpServletRequest request, HttpServletResponse response) throws IOException {
		System.out.println("ScioBackend invoke: \"" + urlPart + "\"");
		if (urlPart.startsWith("/"))
			urlPart = urlPart.substring(1);
		
		if (urlPart.equals("")) {
			showServiceOverview(response);
			return true;
		} else if (!urlPart.contains("/")) {
			showOperationOverview(urlPart, response);
			return true;
		} else {
			String service = urlPart.substring(0, urlPart.indexOf("/"));
			String operation = urlPart.substring(urlPart.indexOf("/"));
			callServiceOperation(service, operation);
			return true;
		}
	    //return super.invoke(urlPart, is, os);
	}
	
	public void showServiceOverview(HttpServletResponse response) throws IOException {
		Writer writer = response.getWriter();
		writer.write("<html><body>");
		writer.write("<h1>ShowServiceOverview</h1>");
		writer.write("<ul>");
		for (Service service : services) {
			writer.write("<li>");
			writer.write("<a href=\"");
			writer.write(service.getName());
			writer.write("\">");
			writer.write(service.getName());
			writer.write("</a>");
		}
		writer.write("</li>");
		writer.write("</ul>");
		writer.write("</body></html>");
		writer.close();
	}
	
	public void showOperationOverview(String service, HttpServletResponse response) throws IOException {
		Writer writer = response.getWriter();
		writer.write("<html><body>");
		writer.write("<h1>ShowOperationOverview</h1>");
		writer.write("<ul>");
		writer.write("<li>TODO</li>");
		/*
		for (Service service : services) {
			writer.write("<li>");
			writer.write("<a href=\"");
			writer.write(service.getName());
			writer.write("\">");
			writer.write(service.getName());
			writer.write("</a>");
		}
		*/
		writer.write("</li>");
		writer.write("</ul>");
		writer.write("</body></html>");
		writer.close();
	}
	
	public void callServiceOperation(String service, String operation) {
		try {
			XMLStreamReader reader = null;
			XMLStreamWriter writer = null;
			
			BindContext bindContext = BindContext.newInstance(String.class);
			
			Object input = bindContext.createUnarshaller().unmarshal(reader);
			Object output = input;
			bindContext.createMarshaller().marshal(output, writer);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
