package org.evolvis.octopus.backend;

/** This exception is thrown if something goes wrong
 * while initializing a backend.
 * 
 * @author Martin Pelzer, tarent GmbH
 *
 */
public class InitializationException extends Exception {

	
	public InitializationException(String msg) {
		super(msg);
	}
	
	
	public InitializationException(String msg, Throwable cause) {
		super(msg, cause);
	}
	
}
