package org.evolvis.octopus.backend;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.ServletConfig;

import org.evolvis.octopus.backend.cxf.CXFBackend;
import org.evolvis.octopus.management.modules.Service;

/** takes a service to deploy in the backend, decides where
 * exactly to deploy it (CXF, scio, ...) and forwards it
 * to the appropriate class
 * 
 * @author Martin Pelzer, tarent GmbH
 *
 */
public class BackendManager {

	private List<BackendDeployable> backends;
	
	
	public void init() {
		// FIXME For now we just register the existing backends here.
		// This should be done in some nice dynamic way.
		this.backends = new LinkedList<BackendDeployable>();
		this.backends.add(new CXFBackend()); // for now we just know CXF and nothing else
	}
	
	
	/** deploys a service to the appropriate backend
	 * 
	 */
	public void deployService(Service service, ServletConfig config) {
		// we could use something like this if we do not want to deploy
		// a service automatically to all backends
		//BackendFactory.getBackendForService(service).deployService(service);
		
		Iterator<BackendDeployable> iter = this.backends.iterator();
		while (iter.hasNext()) {
			try {
				iter.next().deployService(service, config);
			} catch (DeploymentException e) {
				// TODO wat nu?
				e.printStackTrace();
			}
		}
	}

	
	/** undeploys a service from the appropriate backend
	 * 
	 */
	public void undeployService(Service service) {
		//BackendFactory.getBackendForService(service).undeployService(service);
		
		Iterator<BackendDeployable> iter = this.backends.iterator();
		while (iter.hasNext()) {
			try {
				iter.next().undeployService(service);
			} catch (DeploymentException e) {
				// TODO wat nu?
				e.printStackTrace();
			}
		}
	}

}
