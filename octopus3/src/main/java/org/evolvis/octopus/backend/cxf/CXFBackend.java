package org.evolvis.octopus.backend.cxf;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.xml.ws.Endpoint;

import org.apache.cxf.BusFactory;
import org.evolvis.octopus.backend.BackendDeployable;
import org.evolvis.octopus.backend.DeploymentException;
import org.evolvis.octopus.backend.InitializationException;
import org.evolvis.octopus.management.modules.Service;

/** backend implementation for Apache CXF
 * 
 * @author Martin Pelzer, tarent GmbH
 *
 */
public class CXFBackend implements BackendDeployable {
	
	//private ServletConfig servletConfig;
	private CXFBackendServlet cxfServlet;
	
	public void initBackend() throws InitializationException {
		/*try {
			this.init();
		} catch (ServletException e) {
			// TODO log
			throw new InitializationException("error while initializing CXF backend", e);
		}*/
		this.cxfServlet = new CXFBackendServlet();
		try {
			this.cxfServlet.init();
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			throw new InitializationException("cxf servlet could not be initialized", e);
		}
	}
	
	
    public void deployService(Service service, ServletConfig config) throws DeploymentException {
    	// create an instance of the implementation class
    	Object impl;
		try {
			impl = ((Class) service.getSource()).newInstance();
		} catch (Exception e) {
			// TODO log
			throw new DeploymentException("error while instantiating service implementation class: " + service.getSource(), e); 
		}
		
    	// deploy service to the CXF backend
		//Bus bus = this.getBus();
		BusFactory.setDefaultBus(this.cxfServlet.getBus()); 
		
		
		
		// FIXME das startet einen neuen Jetty-Server
		// Dumme Sache, wenn ich fuer jeden Service einen neuen Server starte...
		//Endpoint.publish("http://localhost:8081/" + service.getName(), impl);
		
		Endpoint endpoint = Endpoint.create("/" + service.getName(), impl);
		endpoint.publish(this.cxfServlet.getServletConfig());
		endpoint.publish("/" + service.getName());
		
		
		/*EndpointInfo ei = new EndpointInfo();
	    ei.setAddress(serviceFactory.getAddress());
	    Destination destination = df.getDestination(ei);
	    JettyHTTPDestination jettyDestination = (JettyHTTPDestination) destination;
	    ServerEngine engine = jettyDestination.getEngine();
	    Handler handler = engine.getServant(new URL(serviceFactory.getAddress()));
	    org.mortbay.jetty.Server server = handler.getServer(); // The Server
		*/
		
		/*ServerFactoryBean serverFactoryBean = new ServerFactoryBean();
		serverFactoryBean.setBus(bus);
		serverFactoryBean.setServiceFactory(new OctopusServiceFactory());
		serverFactoryBean.setServiceClass(this.getClass());
		serverFactoryBean.setAddress("/" + service.getName());
		serverFactoryBean.setStart(true);
		serverFactoryBean.create();*/
		
		/*JaxWsImplementorInfo info = new JaxWsImplementorInfo(impl.getClass());
		
		JaxWsServiceFactoryBean sf = new JaxWsServiceFactoryBean();
		sf.setBus(getBus());
		//sf.setJaxWsImplementorInfo(info);
		sf.setServiceClass(impl.getClass());
		//sf.setDataBinding(dataBinding);
		//sf.setEndpointName(new QName("/" + service.getName()));
		sf.setWrapped(true);
		
		org.apache.cxf.service.Service s = sf.create();
		s.setInvoker(new BeanInvoker(impl));*/

//		JettyHTTPServerEngineFactory jettyFactory = new JettyHTTPServerEngineFactory();
//		JettyHTTPServerEngine jettyEngine = jettyFactory.retrieveJettyHTTPServerEngine(8080);
//		Server server = jettyEngine.getServer();
		
		
		/*JaxWsServerFactoryBean srvFactory = new JaxWsServerFactoryBean();
		srvFactory.setAddress("/" + service.getName());
		srvFactory.setServiceClass(impl.getClass());
		srvFactory.setServer(server); // TODO wo kriege ich den jetzt her?
		srvFactory.create();
		*/
		/*String interfaceName = impl.getClass().getAnnotation(WebService.class).endpointInterface();
		Class interfaceClass = Class.forName(interfaceName);
		
		JaxWsServerFactoryBean sf = new JaxWsServerFactoryBean();
		sf.setServiceClass(interfaceClass);
		sf.getServiceFactory().setWrapped(true);
		sf.setBindingId(HttpBindingFactory.HTTP_BINDING_ID);
		sf.setAddress("http://localhost:8080/");

		PeopleService peopleService = new PeopleServiceImpl();
		sf.getServiceFactory().setInvoker(new BeanInvoker(peopleService));

		Server svr = sf.create();*/
    }
    
    
    public void undeployService(Service service) {
    	// TODO remove service from the CXF backend
    	
    }
    
}
