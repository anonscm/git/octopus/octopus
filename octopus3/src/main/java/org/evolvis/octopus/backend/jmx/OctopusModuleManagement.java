/*
 * tarent-octopus jmx extension,
 * an opensource webservice and webapplication framework (jmx part)
 * Copyright (c) 2006-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent-octopus jmx extension'
 * Signature of Elmar Geese, 11 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

/*
 * Copyright (c) tarent GmbH
 * Bahnhofstrasse 13 . 53123 Bonn
 * www.tarent.de . info@tarent.de
 *
 * Created on 19.06.2006
 */

package org.evolvis.octopus.backend.jmx;

public class OctopusModuleManagement /*implements DynamicMBean*/
{
	/*
    private ObjectName jmxName = null;
    private MBeanInfo octopusMBeanInfo = null;
    private MBeanServer mbs = null;
    private HashMap octopusOperationMap = null;
    
    private static Logger logger = Logger.getLogger(OctopusModuleManagement.class.getName());

    public OctopusModuleManagement() throws MalformedObjectNameException, NullPointerException
    {
        octopusOperationMap = new HashMap();
        
        jmxName = new ObjectName("de.tarent.octopus.jmx:type=" + module);
        
        buildDynamicMBeanInfo(module);
    }

    public void start() throws Exception 
    {
        MBeanServer mbs = ManagementFactory.getPlatformMBeanServer(); 
           
        mbs.registerMBean(this, jmxName);  
    }

    public void stop() throws Exception
    {
        mbs.unregisterMBean(jmxName);
    }
    
    public Object getAttribute(String attribute) throws AttributeNotFoundException, MBeanException, ReflectionException
    {
        // Check whether used attribute name is null
        if (attribute==null) 
        {
            throw new RuntimeOperationsException(
                  new IllegalArgumentException("Attribute name cannot be null"),
                  "Cannot invoke a getter with null attribute name");
        }

        if (octopusAttributeAvailable(attribute))    
            return getOctopusAttribute(attribute);
        else
            // attribute has not been recognized: throw an AttributeNotFoundException
            throw new AttributeNotFoundException("Cannot find " + attribute + " attribute.");
    }

    public void setAttribute(Attribute attribute) throws AttributeNotFoundException, InvalidAttributeValueException, MBeanException, ReflectionException
    {
        // Check whether used attribute name is null
        if (attribute==null) 
        {
            throw new RuntimeOperationsException(
                  new IllegalArgumentException("Attribute name cannot be null"),
                  "Cannot invoke a setter with null attribute name");
        }
        
        if (octopusAttributeAvailable(attribute.getName()))    
            setOctopusAttribute(attribute.getName(), attribute.getValue());
        else
            // attribute has not been recognized: throw an AttributeNotFoundException
            throw new AttributeNotFoundException("Cannot find " + attribute + " attribute.");
    }

    public AttributeList getAttributes(String[] attributeNames)
    {
        // Check whether used attribute name is null
        if (attributeNames==null) 
        {
            throw new RuntimeOperationsException(
                  new IllegalArgumentException("Attribute name cannot be null"),
                  "Cannot invoke a setter with null attribute name");
        }
        
        AttributeList resultList = new AttributeList();
        
        // An empty list returns an empty result list
        if (attributeNames.length==0)
            return resultList;

        // Build result list
        for (int i=0;i<attributeNames.length;i++) 
        {
            try 
            {        
                Object value = getAttribute(attributeNames[i]);     
                resultList.add(new Attribute(attributeNames[i],value));
            } 
            catch (Exception e) 
            {
                throw new RuntimeOperationsException(
                        new IllegalArgumentException("Error getting value for " + attributeNames[i] + " attribute."));
            }
        }
        
        return resultList;
    }

    public AttributeList setAttributes(AttributeList attributeNames)
    {
        // Check whether used attribute name is null
        if (attributeNames==null) 
        {
            throw new RuntimeOperationsException(
                  new IllegalArgumentException("Attribute name cannot be null"),
                  "Cannot invoke a setter with null attribute name");
        }
        
        AttributeList resultList = new AttributeList();
        
        // An empty list returns an empty result list
        if (attributeNames.size()==0)
            return resultList;

        // Build result list
        Iterator i = attributeNames.iterator();
        while (i.hasNext()) 
        {
            Attribute thisAttribute = (Attribute)i.next();
            try 
            {
                setAttribute(thisAttribute);
                String name = thisAttribute.getName();
                Object value = getAttribute(name); 
                resultList.add(new Attribute(name,value));
            } 
            catch(Exception e) 
            {
                e.printStackTrace();
            }
        }
        
        return resultList;
    }

    public Object invoke(String actionName, Object[] params, String[] signature) throws MBeanException, ReflectionException
    {
        // check whether the operation name is null
        if (actionName==null) 
        {
            throw new RuntimeOperationsException(
                 new IllegalArgumentException("Operation name cannot be null"), 
                 "Cannot invoke a null operation.");
        }

        if (octopusTaskAvailable(actionName))    
            return callOctopusTask(actionName, params);
        else
            // Unknown operation
            throw new ReflectionException(
                    new NoSuchMethodException(actionName), 
                        "Cannot find the operation " + actionName);
    }

    public MBeanInfo getMBeanInfo()
    {
        return octopusMBeanInfo;
    }

    private boolean octopusTaskAvailable(String taskName)
    {
        for (int i=0; i<octopusMBeanInfo.getOperations().length;i++)
            if (octopusMBeanInfo.getOperations()[i].getName().equals(taskName))
                return true;
 
        return false;
    }
    
    private boolean octopusAttributeAvailable(String attributeName)
    {
        for (int i=0; i<octopusMBeanInfo.getAttributes().length;i++)
            if (octopusMBeanInfo.getAttributes()[i].getName().equals(attributeName))
                return true;
 
        return false;
    }
    
    private Object getOctopusAttribute(String attributeName) throws AttributeNotFoundException
    {
        if (!octopusAttributeAvailable(attributeName))
            throw new AttributeNotFoundException("Cannot find " + attributeName + " attribute.");
        
        return octopusConfig.getConfigData(attributeName);
    }

    private void setOctopusAttribute(String attributeName, Object attributeValue) throws AttributeNotFoundException
    {
        if (!octopusAttributeAvailable(attributeName))
            throw new AttributeNotFoundException("Cannot find " + attributeName + " attribute.");

        // TODO: implement
    }
    
    private Object callOctopusTask(String task, Object[] params) throws ReflectionException, MBeanException
    {
        // allow only available tasks
        if (!octopusTaskAvailable(task))
            throw new ReflectionException(
                    new NoSuchMethodException(task), 
                        "Cannot find the operation " + task);        

        // retrieve parameter names and other info
        MBeanParameterInfo[] parameterInfo = (MBeanParameterInfo[])octopusOperationMap.get(task);

        // build the request
        OctopusRequest request = new OctopusRequest();
        request.setRequestParameters(new HashMap());
        request.setParam("module", module);
        request.setParam("task", task);
        request.setParam("context", "Hallo");
        for (int i=0; i<params.length; i++)
        {
            String key = parameterInfo[i].getName();
            Object value = params[i];
            
            request.setParam(key, value);
        }
        
        // issue call
        DirectCallResponse response = new DirectCallResponse();
        try
        {
            octopus.dispatch(request, response, new DirectCallSession());
        }
        catch (ResponseProcessingException e)
        {
            logger.log(Level.SEVERE, "Error calling Octopus task " + task + " from JMX subsystem.", e);
        }

        // catch errors
        if (response.errorWhileProcessing())
        {
            throw new MBeanException(response.getErrorException(), response.getErrorMessage()); 
        }
        
        // retrieve output values
        Map result = new HashMap();
        Iterator iter = response.getResponseObjectKeys();
        while (iter.hasNext())
        {
            String key = (String)iter.next();
            Object value = response.getResponseObject(key);
            
            result.put(key, value);
        }
        
        return result;        
    }

    private void buildDynamicMBeanInfo(String module)
    {
        // construct Octopus core attribute descriptions
        List temp = new ArrayList();
        Iterator iter = null;
        Object tempA[] = null;
        MBeanAttributeInfo[] octopusAttributes = null;
        if ("octopus".equals(module))
        {
            iter = octopusConfig.getConfigKeys();
            while (iter.hasNext())
            {
                temp.add(new MBeanAttributeInfo((String)iter.next(),
                        "java.lang.String",
                        "Generic Octopus configuration parameter.",
                        true,
                        false,
                        false));
            }        
        
            tempA = temp.toArray();
            octopusAttributes = new MBeanAttributeInfo[tempA.length];
            System.arraycopy(tempA, 0, octopusAttributes, 0, tempA.length);
        }
        
        // construct Octopus task descriptions
        temp = new ArrayList();
        TaskList taskList = null;
        if (!"octopus".equals(module))
            taskList = octopusConfig.getTaskList(module);
        MBeanOperationInfo[] octopusOperations = null;
        if (taskList!=null)
        {
            iter = taskList.getTasksKeys();
            while (iter.hasNext())
            {
                String thisTaskName = (String)iter.next();
                Task thisTask = octopusConfig.getTaskList(module).getTask(thisTaskName);
                MBeanParameterInfo[] thisParameters = parseParameters(thisTask.getInputMessage());
    
                MBeanOperationInfo thisOperation = new MBeanOperationInfo(
                        thisTaskName,
                        thisTask.getDescription(),
                        thisParameters,
                        "java.util.Map", 
                        MBeanOperationInfo.ACTION);
                temp.add(thisOperation);
                octopusOperationMap.put(thisTaskName, thisParameters);
            }            
            
            tempA = temp.toArray();
            octopusOperations = new MBeanOperationInfo[tempA.length];
            System.arraycopy(tempA, 0, octopusOperations, 0, tempA.length);
        }
        
        // construct Octopus notification descriptions
        MBeanNotificationInfo[] octopusNotifications = new MBeanNotificationInfo[] {};
        
        octopusMBeanInfo = new MBeanInfo(module + "OctopusModuleManagement",
                                   "tarent Octopus JMX MBean for module " + module,
                                   octopusAttributes,
                                   new MBeanConstructorInfo[] {},
                                   octopusOperations,
                                   octopusNotifications);
    }

    private MBeanParameterInfo[] parseParameters(MessageDefinition inputMessage)
    {
        List result = new ArrayList();
        
        Iterator iter = inputMessage.getParts().iterator();
        while (iter.hasNext())
        {
            MessageDefinitionPart thisPart = (MessageDefinitionPart)iter.next();
            String thisPartName = thisPart.getName().replaceFirst(".*:","");
            
            String thisPartType = thisPart.getPartDataType();
            String thisPartDescription = thisPart.getDescription();
            
            MBeanParameterInfo thisInfo = new MBeanParameterInfo(thisPartName, thisPartType, thisPartDescription);
            result.add(thisInfo);
        }

        Object temp[] = result.toArray();
        MBeanParameterInfo[] octopusParameters = new MBeanParameterInfo[temp.length];
        System.arraycopy(temp, 0, octopusParameters, 0, temp.length);

        return octopusParameters;
    }
    */
}
