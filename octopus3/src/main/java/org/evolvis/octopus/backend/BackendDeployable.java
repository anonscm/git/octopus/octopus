package org.evolvis.octopus.backend;

import javax.servlet.ServletConfig;

import org.evolvis.octopus.management.modules.Service;

/** Interface for accessing an octopus backend from the octopus core.
 * This interface has to be implemented by all octopus backends.
 * 
 * @author Martin Pelzer, tarent GmbH
 *
 */
public interface BackendDeployable {
	
	/** initialize the backend (what happens in here 
	 * depends on the type of the backend)
	 * 
	 * @throws InitializationException is thrown when something goes wrong while initializing so that the backend can not be used
	 */
	public void initBackend() throws InitializationException;

	
	/** deploys a new service to the backend 
	 * 
	 * @param service the service to deploy
	 * @throws DeploymentException is thrown if the service can not be deployed in the backend
	 */
	public void deployService(Service service, ServletConfig servletConfig) throws DeploymentException;
	
	
	/** undeploys a service from a backend
	 * 
	 * @param service the name of the service to undeploy
	 * @throws DeploymentException is thrown if the service could not be undeployed
	 */
	public void undeployService(Service service) throws DeploymentException;
	
}
