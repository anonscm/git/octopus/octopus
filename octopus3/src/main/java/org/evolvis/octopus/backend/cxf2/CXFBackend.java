package org.evolvis.octopus.backend.cxf2;

import javax.servlet.ServletConfig;

import org.evolvis.octopus.backend.BackendDeployable;
import org.evolvis.octopus.backend.DeploymentException;
import org.evolvis.octopus.backend.InitializationException;
import org.evolvis.octopus.core.impl.TreeInvocationHandler;
import org.evolvis.octopus.management.modules.Service;

public class CXFBackend extends TreeInvocationHandler implements BackendDeployable {
	@Override
	public void initBackend() throws InitializationException {
	}
	
	@Override
	public void deployService(Service service, ServletConfig servletConfig) throws DeploymentException {
	}
	
	@Override
	public void undeployService(Service service) throws DeploymentException {
	}
}
