package org.evolvis.octopus.backend.http;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.LinkedHashMap;
import java.util.Map;

public class HTTPMapping implements HTTPHandle {
	protected Map<String, HTTPMapping> mapping;
	
	public boolean invoke(String urlPart, InputStream is, OutputStream os) throws IOException {
		if (mapping != null) {
			for (Map.Entry<String, HTTPMapping> entry : mapping.entrySet()) {
				if (urlPart.startsWith(entry.getKey())) {
					urlPart = urlPart.substring(entry.getKey().length());
					entry.getValue().invoke(urlPart, is, os);
					return true;
				}
			}
		}
		return false;
	}
	
	public void addChildren(String startWith, HTTPMapping child) {
		if (mapping == null)
			mapping = new LinkedHashMap<String, HTTPMapping>();
		mapping.put(startWith, child);
	}
	
	public void removeChildren(String startWith) {
		if (mapping != null)
			mapping.remove(startWith);
	}
}
