package org.evolvis.octopus.backend.http;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

public class HTTPServer extends HTTPMapping {
	private ServerSocket serverSocket;
	private Thread serverThread;
	private long handleCount = 0L;

	public void openSocket(int port) throws IOException {
		if (serverThread != null)
			throw new IllegalStateException("Server thread already started.");
		
		serverSocket = new ServerSocket(port);
		serverThread = new Thread(new Runnable() {
			public void run() {
				while (!Thread.interrupted()) {
					try {
						if (serverSocket == null || serverSocket.isClosed())
							return;
						
						final Socket socket = serverSocket.accept();
						Thread thread = new Thread(new Runnable() {
							public void run() {
								try {
									handle(socket.getInputStream(), socket.getOutputStream());
								} catch (IOException e) {
									e.printStackTrace();
								} finally {
									try {
										socket.close();
									} catch (IOException e) {
										e.printStackTrace();
									}
								}
							}
						});
						thread.setName("octopus-scio-HTTP-handle-" + handleCount++);
						thread.start();
					} catch (SocketException e) {
						// Nothing
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		});
		serverThread.setName("octopus-scio-HTTP-socket-" + serverSocket.getLocalPort());
		serverThread.start();
	}

	public void closeSocket() {
		if (serverThread != null) {
			serverThread.interrupt();
			serverThread = null;
		}
	}

	protected void handle(InputStream is, OutputStream os) throws IOException {
		byte buffer[] = new byte[1024 * 10];
		int length = is.read(buffer);
		
		ByteArrayInputStream baos = new ByteArrayInputStream(buffer, 0, length);
		BufferedReader reader = new BufferedReader(new InputStreamReader(baos));
		String line = null;
		
		String method = null;
		String url = null;
		String version = null;
		if ((line = reader.readLine()) != null) {
			if (line.startsWith("GET "))
				method = "GET";
			
			if (line.endsWith(" HTTP/1.0"))
				version = "HTTP/1.0";
			else if (line.endsWith(" HTTP/1.1"))
				version = "HTTP/1.1";
			
			if (method != null && version != null)
				url = line.substring(method.length() + 1, line.length() - version.length() - 1);
			else if (line.contains(" "))
				throw new IOException("Unsupported operation: " + line.substring(0, line.indexOf(" ")));
			else
				throw new IOException("Stream contains no valid HTTP header.");
		}
		
		Map<String, String> header = new LinkedHashMap<String, String>();
		while ((line = reader.readLine()) != null) {
			if (line.length() == 0) {
				break;
			}
			if (line.contains(":")) {
				String key = line.substring(0, line.indexOf(":")).trim();
				String value = line.substring(line.indexOf(":") + 1).trim();
				header.put(key, value);
			} else {
				header.put(line.trim(), null);
			}
		}
		header = Collections.unmodifiableMap(header);
		
		assert url != null;
		
		System.out.println("METHOD: " + method + ";");
		System.out.println("URL: " + url + ";");
		System.out.println("VERSION: " + version  + ";");
		System.out.println("HEADER: " + header + ";");
		
		boolean found = false;
		for (Map.Entry<String, HTTPMapping> entry : mapping.entrySet()) {
			if (url.startsWith(entry.getKey())) {
				os.write((version + " " + HttpServletResponse.SC_OK + " OK\n").getBytes("US-ASCII"));
				os.write("Connection: close\n".getBytes("US-ASCII"));
				os.write("\n".getBytes("US-ASCII"));
				
				entry.getValue().invoke(url, is, os);
				
				found = true;
				break;
			}
		}
		
		if (!found) {
			os.write((version + " " + HttpServletResponse.SC_NOT_FOUND + " Not Found\n").getBytes("US-ASCII"));
			os.write("Connection: close\n".getBytes("US-ASCII"));
			os.write("\n".getBytes("US-ASCII"));
			os.write(("URL not found: " + url).getBytes("UTF-8"));
		}
		
		// TODO muss angerechnet werden auf den inputstream for post nachrichten
		System.out.println(baos.available());
		int skipped = (int) reader.skip(buffer.length);
		System.out.println("skipped: " + skipped);
	}
}
