package org.evolvis.octopus.backend.json;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URL;
import java.net.URLConnection;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stax.StAXResult;
import javax.xml.transform.stax.StAXSource;

import org.apache.cxf.helpers.IOUtils;
import org.codehaus.jettison.mapped.Configuration;
import org.codehaus.jettison.mapped.MappedNamespaceConvention;

public class XMLToJSONServlet extends HttpServlet {
	private static final long serialVersionUID = 7923885133907364879L;
	
	@SuppressWarnings("unused")
	private ServletContext servletContext;
	
	@Override
	public void init(ServletConfig servletConfig) throws ServletException {
		super.init(servletConfig);
		servletContext = servletConfig.getServletContext();
	}
	
	@Override
	public void destroy() {
		servletContext = null;
		super.destroy();
	}
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		try {
		    String url = request.getRequestURL().toString();
		    url = url.replaceAll("\\/json\\/", "/services/");
		    if (request.getQueryString() != null && request.getQueryString().length() != 0)
		    	url = url + "?" + request.getQueryString();
		    
		    System.out.println("send request to " + url);
		    
		    URLConnection connection = new URL(url).openConnection();
		    connection.connect();
		    
		    // Send XML input from URL as JSON servlet response.
		    String xml = IOUtils.toString(new InputStreamReader(connection.getInputStream(), "UTF-8"));
		    
		    System.out.println("XML: " + xml);
			
		    Configuration config = new Configuration();
		    config.getXmlToJsonNamespaces().put("http://java.duke.org", "");
		    config.getXmlToJsonNamespaces().put("ns:", "");
		    config.getXmlToJsonNamespaces().put("", "");
			MappedNamespaceConvention con = new MappedNamespaceConvention(config);
			XMLStreamWriter streamWriter = new ExtendedJettisonXMLStreamWriter(con, response.getWriter());
			
			Source source = new StAXSource(XMLInputFactory.newInstance().createXMLStreamReader(new StringReader(xml)));
			Result output = new StAXResult(streamWriter);
			
			TransformerFactory.newInstance().newTransformer().transform(source, output);
			
		} catch (Exception e) {
			throw new IOException(e);
		}
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	    System.err.println("[" + getClass().getSimpleName() + "] *** POST REQUEST TO JSON SERVLET ARE NOT IMPLEMENETED YET *** ");
	    super.doPost(req, resp);
	}
}
