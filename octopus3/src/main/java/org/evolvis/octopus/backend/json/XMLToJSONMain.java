package org.evolvis.octopus.backend.json;

import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringReader;
import java.io.Writer;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stax.StAXResult;
import javax.xml.transform.stax.StAXSource;
import javax.xml.transform.stream.StreamSource;

import org.apache.cxf.helpers.IOUtils;
import org.codehaus.jettison.json.JSONObject;
import org.codehaus.jettison.mapped.MappedNamespaceConvention;

public class XMLToJSONMain {
	public static void main(String[] args) {
	    System.out.println("xml to json");
		try {
			
			String xml = "<root><child>test</child><child>test</child></root>";
			
			Reader reader = new StringReader(xml);
			Writer writer = new PrintWriter(System.out);
			
			MappedNamespaceConvention con = new MappedNamespaceConvention();
			XMLStreamWriter streamWriter = new ExtendedJettisonXMLStreamWriter(con, writer);
			
			Source source = new StreamSource(reader);
			Result output = new StAXResult(streamWriter);
			
			TransformerFactory.newInstance().newTransformer().transform(source, output);
			
			streamWriter.flush();
			writer.write("\n\n");
			writer.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	    System.out.println("json to xml");
		try {
			String json = "{\"root\":{\"child\":[\"tsest\",\"test\"]}}";
			
			Reader reader = new StringReader(json);
			Writer writer = new PrintWriter(System.out);
			
			JSONObject object = new JSONObject(IOUtils.toString(reader));
			MappedNamespaceConvention con = new MappedNamespaceConvention();
			XMLStreamReader streamReader = new ExtendedJettisonXMLStreamReader(object, con);
			
			Source source = new StAXSource(streamReader);
			Result output = new StAXResult(XMLOutputFactory.newInstance().createXMLStreamWriter(writer));
			
			TransformerFactory.newInstance().newTransformer().transform(source, output);
			
			writer.flush();
			writer.write("\n\n");
			writer.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
}
