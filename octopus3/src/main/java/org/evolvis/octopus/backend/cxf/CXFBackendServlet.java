package org.evolvis.octopus.backend.cxf;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.xml.ws.Endpoint;

import org.apache.cxf.BusFactory;

import de.tarent.octopus.prototype.JAXWSSimpleDemoImpl;

/**
 * 
 * @author Martin Pelzer, tarent GmbH
 *
 */
public class CXFBackendServlet extends CXFServlet {
    private static final long serialVersionUID = -945081338369165250L;

	@Override
    public void init(ServletConfig servletConfig) throws ServletException {
    	BusFactory.setDefaultBus(getBus());
    	
    	super.init(servletConfig);
    	
    	Endpoint.publish("/huibuh", new JAXWSSimpleDemoImpl());
    }
	
	
	
}
