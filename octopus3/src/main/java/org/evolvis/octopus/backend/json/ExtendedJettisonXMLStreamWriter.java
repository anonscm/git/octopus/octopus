package org.evolvis.octopus.backend.json;

import java.io.Writer;

import javax.xml.stream.XMLStreamException;

import org.codehaus.jettison.mapped.MappedNamespaceConvention;
import org.codehaus.jettison.mapped.MappedXMLStreamWriter;

public class ExtendedJettisonXMLStreamWriter extends MappedXMLStreamWriter {
	public ExtendedJettisonXMLStreamWriter(MappedNamespaceConvention convention, Writer writer) {
	    super(convention, writer);
    }
	
	@Override
	public void writeStartElement(String local) throws XMLStreamException {
		if (local.contains(":"))
			local = local.substring(local.indexOf(":") + 1);
	    super.writeStartElement(local);
	}
	
	@Override
	public void writeStartElement(String ns, String local) throws XMLStreamException {
		if (local.contains(":"))
			local = local.substring(local.indexOf(":") + 1);
	    super.writeStartElement(ns, local);
	}
	
	@Override
	public void writeStartElement(String prefix, String local, String ns) throws XMLStreamException {
		if (local.contains(":"))
			local = local.substring(local.indexOf(":") + 1);
	    super.writeStartElement(prefix, local, ns);
	}
}
