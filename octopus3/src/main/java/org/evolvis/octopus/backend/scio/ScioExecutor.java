package org.evolvis.octopus.backend.scio;

import java.util.HashSet;
import java.util.Set;

import org.evolvis.octopus.management.modules.Operation;
import org.evolvis.octopus.management.modules.Parameter;
import org.evolvis.octopus.management.modules.Service;

public class ScioExecutor {
	public void invoke(Service service, Operation operation) {
		
		Set<Class> classes = new HashSet<Class>();
		classes.add(service.getSource().getClass());
		classes.add(operation.getSource().getClass());
		for (Parameter parameter : operation.getInputParameters())
			classes.add(parameter.getType());
		for (Parameter parameter : operation.getOutputParameters())
			classes.add(parameter.getType());
		
		System.out.println("classes: " + classes);
		
	}
}
