package org.evolvis.octopus.backend.json;

import javax.xml.stream.XMLStreamException;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.codehaus.jettison.mapped.MappedNamespaceConvention;
import org.codehaus.jettison.mapped.MappedXMLStreamReader;

public class ExtendedJettisonXMLStreamReader extends MappedXMLStreamReader {
	public ExtendedJettisonXMLStreamReader(JSONObject obj) throws JSONException, XMLStreamException {
	    super(obj);
    }
	
	public ExtendedJettisonXMLStreamReader(JSONObject obj, MappedNamespaceConvention con) throws JSONException, XMLStreamException {
	    super(obj, con);
    }
	
	@Override
	public int getTextCharacters(int sourceStart, char[] target, int targetStart, int length) throws XMLStreamException {
		getText().getChars(sourceStart,sourceStart+length,target,targetStart);
        return length;
	}
}
