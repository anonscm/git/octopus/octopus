package org.evolvis.octopus.backend.http;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public interface HTTPHandle {
	public boolean invoke(String urlPart, InputStream is, OutputStream os) throws IOException;
}
