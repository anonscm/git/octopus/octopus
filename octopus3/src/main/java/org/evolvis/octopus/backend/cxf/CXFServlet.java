package org.evolvis.octopus.backend.cxf;

import java.io.IOException;
import java.io.InputStream;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;


import org.apache.cxf.bus.spring.SpringBusFactory;
import org.apache.cxf.common.classloader.ClassLoaderUtils;
import org.apache.cxf.common.logging.LogUtils;
import org.apache.cxf.resource.ResourceManager;
import org.apache.cxf.resource.URIResolver;
import org.apache.cxf.transport.servlet.ServletContextResourceResolver;
import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.core.io.InputStreamResource;


/**
 * A Servlet which supports loading of JAX-WS endpoints from an
 * XML file and handling requests for endpoints created via other means
 * such as Spring beans, or the Java API. All requests are passed on
 * to the {@link ServletController}.
 *
 */
public class CXFServlet extends AbstractCXFServlet {
    private static final long serialVersionUID = 7205539321557238521L;

    private GenericApplicationContext childCtx;
    
    public static Logger getLogger() {
        return LogUtils.getL7dLogger(CXFServlet.class);
    }
    
    @Override
    public void loadBus(ServletConfig servletConfig) throws ServletException {
        String springCls = "org.springframework.context.ApplicationContext";
        try {
            ClassLoaderUtils.loadClass(springCls, getClass());
            loadSpringBus(servletConfig);
        } catch (ClassNotFoundException e) {                
            LOG.log(Level.SEVERE, "FAILED_TO_LOAD_SPRING_BUS", new Object[]{e});
            new ServletException("Can't load bus with Spring context class", e);
        }
    }
    
    
    private void loadSpringBus(ServletConfig servletConfig) throws ServletException {
        
        // try to pull an existing ApplicationContext out of the
        // ServletContext
        ServletContext svCtx = getServletContext();

        // Spring 1.x
        ApplicationContext ctx = (ApplicationContext)svCtx
            .getAttribute("interface org.springframework.web.context.WebApplicationContext.ROOT");

        // Spring 2.0
        if (ctx == null) {
            Object ctxObject = svCtx
                .getAttribute("org.springframework.web.context.WebApplicationContext.ROOT");
            if (ctxObject instanceof ApplicationContext) {
                ctx = (ApplicationContext) ctxObject;
            } else if (ctxObject != null) {
                // it should be the runtime exception                
                Exception ex = (Exception) ctxObject;
                throw new ServletException(ex);
            }                   
        }
        
        // This constructor works whether there is a context or not
        // If the ctx is null, we just start up the default bus
        if (ctx == null) {            
            LOG.info("LOAD_BUS_WITHOUT_APPLICATION_CONTEXT");
            bus = new SpringBusFactory().createBus();
        } else {
            LOG.info("LOAD_BUS_WITH_APPLICATION_CONTEXT");
            bus = new SpringBusFactory(ctx).createBus();
        }
        ResourceManager resourceManager = bus.getExtension(ResourceManager.class);
        resourceManager.addResourceResolver(new ServletContextResourceResolver(
                                               servletConfig.getServletContext()));
        
        replaceDestinationFactory();

        // Set up the ServletController
        controller = createServletController(servletConfig);
        
        // build endpoints from the web.xml or a config file
        loadAdditionalConfig(ctx, servletConfig);
    }

    private void loadAdditionalConfig(ApplicationContext ctx, 
                                        ServletConfig servletConfig) throws ServletException {
        String location = servletConfig.getInitParameter("config-location");
        if (location == null) {
            location = "/WEB-INF/cxf-servlet.xml";
        }
        InputStream is = null;
        try {
            is = servletConfig.getServletContext().getResourceAsStream(location);
            
            if (is == null || is.available() == -1) {
                URIResolver resolver = new URIResolver(location);

                if (resolver.isResolved()) {
                    is = resolver.getInputStream();
                }
            }
        } catch (IOException e) {
            //throw new ServletException(e);
        }
        
        if (is != null) {
            LOG.log(Level.INFO, "BUILD_ENDPOINTS_FROM_CONFIG_LOCATION", new Object[]{location});
            childCtx = new GenericApplicationContext(ctx);
            
            XmlBeanDefinitionReader reader = new XmlBeanDefinitionReader(childCtx);
            reader.setValidationMode(XmlBeanDefinitionReader.VALIDATION_XSD);
            reader.loadBeanDefinitions(new InputStreamResource(is, location));
            
            childCtx.refresh();
        } 
    }

    @Override
    public void destroy() {
        if (childCtx != null) {
            childCtx.destroy();
        }
        super.destroy();        
    }

    
}
