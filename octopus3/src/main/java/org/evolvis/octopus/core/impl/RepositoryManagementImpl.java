package org.evolvis.octopus.core.impl;

import java.io.File;

import org.evolvis.octopus.management.application.RepositoryManagement;
import org.evolvis.octopus.management.modules.Module;

public class RepositoryManagementImpl implements RepositoryManagement {
	public void installModule(File path) {
		// harden the module path
		path = path.getAbsoluteFile();
	}

	public void uninstallModule(File path) {
		// harden the module path
		path = path.getAbsoluteFile();
	}
	
	public void uninstallModule(Module module) {
		
	}
}
