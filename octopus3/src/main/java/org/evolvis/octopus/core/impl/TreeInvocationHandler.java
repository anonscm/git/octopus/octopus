package org.evolvis.octopus.core.impl;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class TreeInvocationHandler {
	protected Map<String, TreeInvocationHandler> childrens;
	
	public boolean invoke(String urlPart, HttpServletRequest request, HttpServletResponse response) throws IOException {
		System.out.println(getClass().getSimpleName() + "#invoke(" + urlPart + ")");
		if (childrens != null) {
			for (Map.Entry<String, TreeInvocationHandler> entry : childrens.entrySet()) {
				if (urlPart.startsWith(entry.getKey())) {
					urlPart = urlPart.substring(entry.getKey().length());
					System.out.println(" --> " + entry.getValue().getClass().getSimpleName());
					entry.getValue().invoke(urlPart, request, response);
					return true;
				}
			}
		}
		System.out.println(" --> no children found " + (childrens != null ? childrens.keySet() : "[]"));
		return false;
	}
	
	public void addChildren(String startWith, TreeInvocationHandler child) {
		if (childrens == null)
			childrens = new LinkedHashMap<String, TreeInvocationHandler>();
		childrens.put(startWith, child);
	}
	
	public void removeChildren(String startWith) {
		if (childrens != null)
			childrens.remove(startWith);
	}
}
