package org.evolvis.octopus.core.config;

import java.util.LinkedList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import org.evolvis.octopus.management.modules.Module;
import org.evolvis.octopus.management.modules.Service;


/** Java representation of an octopus configuration file
 * 
 * @author Martin Pelzer, tarent GmbH
 *
 */
@XmlRootElement(namespace = "http://octopus.evolvis.org/")
public class Configuration {

	private List<Service> serviceList;
	private List<Module> moduleList;
	
	
	@XmlElementWrapper(name="modules")
	@XmlElement(name="module")
	public List<Module> getModules() {
		return moduleList;
	}

	
	public void setModules(List<Module> modules) {
		this.moduleList = modules;
	}
	
	
	public void addModule(Module module) {
		if (this.moduleList == null)
			this.moduleList = new LinkedList<Module>();
		this.moduleList.add(module);
	}
	

	@XmlElementWrapper(name="services")
	@XmlElement(name="service")
	public List<Service> getServices() {
		return serviceList;
	}
	
	
	public void setServices(List<Service> services) {
		this.serviceList = services;
	}
	
	
	public void addService(Service service) {
		if (this.serviceList == null)
			this.serviceList = new LinkedList<Service>();
		this.serviceList.add(service);
	}
	
}
