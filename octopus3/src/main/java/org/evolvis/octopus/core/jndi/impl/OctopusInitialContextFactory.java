package org.evolvis.octopus.core.jndi.impl;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.spi.InitialContextFactory;

public class OctopusInitialContextFactory implements InitialContextFactory {
	public Context getInitialContext(Hashtable<?, ?> environment) throws NamingException {
		return new OctopusContext();
	}
}
