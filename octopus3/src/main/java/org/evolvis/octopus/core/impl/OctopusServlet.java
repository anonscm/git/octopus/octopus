package org.evolvis.octopus.core.impl;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.evolvis.octopus.backend.BackendManager;
import org.evolvis.octopus.core.config.ConfigReader;
import org.evolvis.octopus.core.config.ConfigurationFileException;
import org.evolvis.octopus.management.modules.Service;


/** central servlet in octopus
 * 
 * For now it only initializes the services
 * 
 * @author Martin Pelzer, tarent GmbH
 *
 */
public class OctopusServlet extends HttpServlet {
    private static final long serialVersionUID = -5885867036370259945L;


	/** initializes the octopus
	 * 
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
	    super.init(config);
	    
	    // read octopus configuration file and deploy services
	    // to the backend
	    ConfigReader configuration = new ConfigReader();
		BackendManager backendManager = new BackendManager();
		backendManager.init();
		
		// get services defined in configuration file
		List<Service> services = null;
		try {
			services = configuration.getServices();
		} catch (ConfigurationFileException e) {
			e.printStackTrace();
			throw new ServletException(e);
		}
		
		// deploy services to backend
		Iterator<Service> iter = services.iterator();
		while (iter.hasNext()) {
			Service service = iter.next();
			backendManager.deployService(service, config);
		}
	}
	
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.getWriter().write("Hallo");
		// TODO
	}
	
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO
	}
	
}
