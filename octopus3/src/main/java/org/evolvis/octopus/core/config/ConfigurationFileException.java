package org.evolvis.octopus.core.config;

/** exception that is thrown if something is wrong with
 *  the octopus configuration file
 * 
 * @author Martin Pelzer, tarent GmbH
 *
 */
public class ConfigurationFileException extends Exception {
    private static final long serialVersionUID = 1194556471866348891L;

	public ConfigurationFileException(String msg) {
		super(msg);
	}
	
	
	public ConfigurationFileException(String msg, Throwable cause) {
		super(msg, cause);
	}

}
