package org.evolvis.octopus.core.impl;

import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Enumeration;

public class FilteredURLClassLoader extends URLClassLoader {
	private String[] packageFilter;

	public FilteredURLClassLoader(URL url[], String packageFilter[]) {
		super(url, null);
		if (packageFilter == null)
			packageFilter = new String[0];
		this.packageFilter = packageFilter;
	}

	public FilteredURLClassLoader(URL url[], String packageFilter[], ClassLoader parent) {
		super(url, parent);
		if (packageFilter == null)
			packageFilter = new String[0];
		this.packageFilter = packageFilter;
	}
	
	@Override
	public URL getResource(String name) {
		for (String filter : packageFilter)
			if (name.startsWith(filter.replaceAll("\\.", "/")))
				return super.getResource(name);
		return null;
	}
	
	@Override
	public Enumeration<URL> getResources(String name) throws IOException {
		for (String filter : packageFilter)
			if (name.startsWith(filter.replaceAll("\\.", "/")))
				return super.getResources(name);
		return null;
	}
	
	@Override
	public Class<?> loadClass(String name) throws ClassNotFoundException {
		for (String filter : packageFilter)
			if (name.startsWith(filter))
				return super.loadClass(name);
		return null;
	}
	
	@Override
	protected Class<?> findClass(String name) throws ClassNotFoundException {
		for (String filter : packageFilter)
			if (name.startsWith(filter))
				return super.findClass(name);
		return null;
	}
}
