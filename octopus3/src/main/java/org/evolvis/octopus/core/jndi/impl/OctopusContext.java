package org.evolvis.octopus.core.jndi.impl;

import java.util.Hashtable;

import javax.naming.Binding;
import javax.naming.Context;
import javax.naming.Name;
import javax.naming.NameClassPair;
import javax.naming.NameParser;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;

public class OctopusContext implements Context {
	public Object addToEnvironment(String propName, Object propVal)
			throws NamingException {
		throw new NamingException("Not supported.");
	}

	public void bind(Name name, Object obj) throws NamingException {
		throw new NamingException("Not supported.");
	}

	public void bind(String name, Object obj) throws NamingException {
		throw new NamingException("Not supported.");
	}

	public void close() throws NamingException {
	}

	public Name composeName(Name name, Name prefix) throws NamingException {
		throw new NamingException("Not supported.");
	}

	public String composeName(String name, String prefix)
			throws NamingException {
		throw new NamingException("Not supported.");
	}

	public Context createSubcontext(Name name) throws NamingException {
		throw new NamingException("Not supported.");
	}

	public Context createSubcontext(String name) throws NamingException {
		throw new NamingException("Not supported.");
	}

	public void destroySubcontext(Name name) throws NamingException {
		throw new NamingException("Not supported.");
	}

	public void destroySubcontext(String name) throws NamingException {
		throw new NamingException("Not supported.");
	}

	public Hashtable<?, ?> getEnvironment() throws NamingException {
		throw new NamingException("Not supported.");
	}

	public String getNameInNamespace() throws NamingException {
		throw new NamingException("Not supported.");
	}

	public NameParser getNameParser(Name name) throws NamingException {
		throw new NamingException("Not supported.");
	}

	public NameParser getNameParser(String name) throws NamingException {
		throw new NamingException("Not supported.");
	}

	public NamingEnumeration<NameClassPair> list(Name name)
			throws NamingException {
		throw new NamingException("Not supported.");
	}

	public NamingEnumeration<NameClassPair> list(String name)
			throws NamingException {
		throw new NamingException("Not supported.");
	}

	public NamingEnumeration<Binding> listBindings(Name name)
			throws NamingException {
		throw new NamingException("Not supported.");
	}

	public NamingEnumeration<Binding> listBindings(String name)
			throws NamingException {
		throw new NamingException("Not supported.");
	}

	public Object lookup(Name name) throws NamingException {
		return lookup(name.toString());
	}

	public Object lookup(String name) throws NamingException {
		if (name.contains("version"))
			return "zwei-null-irgendwas";
		else
			return null;
	}

	public Object lookupLink(Name name) throws NamingException {
		throw new NamingException("Not supported.");
	}

	public Object lookupLink(String name) throws NamingException {
		throw new NamingException("Not supported.");
	}

	public void rebind(Name name, Object obj) throws NamingException {
		throw new NamingException("Not supported.");
	}

	public void rebind(String name, Object obj) throws NamingException {
		throw new NamingException("Not supported.");
	}

	public Object removeFromEnvironment(String propName) throws NamingException {
		throw new NamingException("Not supported.");
	}

	public void rename(Name oldName, Name newName) throws NamingException {
		throw new NamingException("Not supported.");
	}

	public void rename(String oldName, String newName) throws NamingException {
		throw new NamingException("Not supported.");
	}

	public void unbind(Name name) throws NamingException {
		throw new NamingException("Not supported.");
	}

	public void unbind(String name) throws NamingException {
		throw new NamingException("Not supported.");
	}
}
