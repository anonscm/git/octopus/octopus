package org.evolvis.octopus.core.impl;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.evolvis.octopus.backend.DeploymentException;
import org.evolvis.octopus.backend.cxf2.CXFBackend;
import org.evolvis.octopus.backend.scio.ScioBackend;
import org.evolvis.octopus.backend.scio.Testservice;
import org.evolvis.octopus.management.modules.Operation;
import org.evolvis.octopus.management.modules.Parameter;
import org.evolvis.octopus.management.modules.Service;

public class TreeInvocationServlet extends HttpServlet {
	private static final long serialVersionUID = -4380123359590916658L;
	
	private TreeInvocationHandler childrens;
	
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		childrens = new TreeInvocationHandler();
		
		// For testing hardcoded:
		ScioBackend scioBackend = new ScioBackend();
		try {
		    Service service = new Service();
		    service.setId("1");
		    service.setName(Testservice.class.getSimpleName());
		    service.setSource(Testservice.class);
		    
		    Operation operation = new Operation(service);
		    operation.setId("1");
		    operation.setName("sum");
		    operation.setSource(Testservice.class.getMethod("sum", Integer.class, Integer.class));
		    operation.addInputParameter(new Parameter("a", Integer.class));
		    operation.addInputParameter(new Parameter("b", Integer.class));
		    operation.addOutputParameter(new Parameter("result", Integer.class));
		    
		    System.out.println("  service: " + service);
			System.out.println("    operation: " + operation);
			
			scioBackend.deployService(service, config);
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (DeploymentException e) {
	        e.printStackTrace();
        }
		
		addChildren("/scio", scioBackend);
		addChildren("/cxf", new CXFBackend());
	}
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		invoke(request, response);
	}
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		invoke(request, response);
	}
	
	protected void invoke(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = request.getRequestURI();
		if (request.getContextPath() != null)
			url = url.substring(request.getContextPath().length());
		childrens.invoke(url, request, response);
	}
	
	public void addChildren(String startWith, TreeInvocationHandler child) {
		childrens.addChildren(startWith, child);
	}
	
	public void removeChildren(String startWith) {
		childrens.removeChildren(startWith);
	}
}
