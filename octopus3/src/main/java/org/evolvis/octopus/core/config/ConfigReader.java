package org.evolvis.octopus.core.config;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.evolvis.octopus.management.modules.Module;
import org.evolvis.octopus.management.modules.ModuleState;
import org.evolvis.octopus.management.modules.Operation;
import org.evolvis.octopus.management.modules.Parameter;
import org.evolvis.octopus.management.modules.Service;

/** class for reading the octopus configuration file
 * 
 * @author Martin Pelzer, tarent GmbH
 *
 */
public class ConfigReader {

	private String configFileName;
	private Configuration configuration;
	
	
	/** returns a Configuration object representing the octopus configuration file
	 * 
	 * @return
	 * @throws ConfigurationFileException 
	 */
	public Configuration getConfiguration() throws ConfigurationFileException {
		// read configuration file only once and then store it in memory
		if (this.configuration != null)
			return this.configuration;
		
		try {
			if (this.configFileName == null) {
				this.configFileName = "src/main/webapp/octopus.xml";
				System.out.println("Force configFileName to " + this.configFileName);
			}
			
			JAXBContext context = JAXBContext.newInstance(Configuration.class);
			Unmarshaller um = context.createUnmarshaller();
			Configuration config = (Configuration) um.unmarshal( new FileReader(this.configFileName));
			return config;
		} catch (JAXBException e) {
			throw new ConfigurationFileException("There was a problem while unmarshalling the octopus configuration file.", e);
		} catch (FileNotFoundException e) {
			throw new ConfigurationFileException("The octopus configuration file could not be found.", e);
		} 
	}
	

	/** returns a list containing the modules defined in the configuration file
	 * 
	 * @return
	 * @throws ConfigurationFileException 
	 */
	public List<Module> getModules() throws ConfigurationFileException {
		if (this.configuration == null)
			this.configuration = this.getConfiguration();
		
		return this.configuration.getModules();
	}
	
	
	/** returns a list containing the services defined in the configuration file
	 * 
	 * @return
	 * @throws ConfigurationFileException 
	 */
	public List<Service> getServices() throws ConfigurationFileException {
		if (this.configuration == null)
			this.configuration = this.getConfiguration();
		
		return this.configuration.getServices();
	}
	
	
	
	/** sets the configuration file name
	 * 
	 * @param configurationFileName
	 */
	public void setConfigurationFileName(String configurationFileName) {
		this.configFileName = configurationFileName;
	}
	
	
	
	
	// just for testing reasons
	public static void main(String[] args) {
		Configuration config = new Configuration();
		
		Module m1 = new Module();
		m1.setCreated(new Date());
		m1.setId("ID 1");
		m1.setName("ein toller Name");
		m1.setPath(new File("/home/mpelze/tmp.tmp"));
		m1.setState(ModuleState.STARTING);
		m1.setVersion("version 1");
		
		config.addModule(m1);

		Parameter p1 = new Parameter();
		p1.setName("parameterName");
		p1.setType(Parameter.class);
		
		List<Parameter> parameters = new LinkedList<Parameter>();
		parameters.add(p1);
		
		Operation o1 = new Operation();
		o1.setId("o1id");
		o1.setName("paul");
		o1.setSource(new Configuration());
		o1.setInputParameters(parameters);
		o1.setOutputParameters(parameters);
		
		List<Operation> operations = new LinkedList<Operation>();
		operations.add(o1);
		
		Service s1 = new Service();
		s1.setDynamicService(false);
		s1.setId("eine ID");
		s1.setName("simpleDemo");
		s1.setSource("de.tarent.octopus.prototype.JAXWSSimpleDemo");
		s1.setOperations(operations);
		
		config.addService(s1);
		config.addService(s1);
		
		try {
			JAXBContext context = JAXBContext.newInstance(Configuration.class);
			Marshaller m = context.createMarshaller();
			m.setProperty( Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE );
			m.marshal( config, System.out );
		} catch (JAXBException e) {
			e.printStackTrace();
		}
	}
}
