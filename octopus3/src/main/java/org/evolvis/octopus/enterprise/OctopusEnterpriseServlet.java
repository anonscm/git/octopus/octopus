package org.evolvis.octopus.enterprise;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.URL;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.evolvis.octopus.core.impl.FilteredURLClassLoader;
import org.evolvis.octopus.management.application.Stage;

public class OctopusEnterpriseServlet extends HttpServlet {
    private static final long serialVersionUID = 1703812664032020558L;

    private OctopusEnterpriseImpl impl;

	@Override
	public void init(ServletConfig config) throws ServletException {
	    super.init(config);
	    
	    impl = new OctopusEnterpriseImpl();
	    
	    try {
			ClassLoader cl1 = new FilteredURLClassLoader(
					new URL[] { new File("target/classes").toURI().toURL() },
					new String[] { "java.", "de.tarent.application1." });
	    } catch (IOException e) {
	    	e.printStackTrace();
	    }
	    
	    Stage stage = new Stage();
	    stage.setName("default");
	}
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		System.out.println(request.getContextPath());
		System.out.println(request.getPathInfo());
		System.out.println(request.getPathTranslated());
		System.out.println(request.getRequestURI());
		System.out.println(request.getRequestURL());
		System.out.println(request.getServerName());
		System.out.println(request.getServletPath());
		
		System.out.println(InetAddress.getLocalHost());
		
		response.getWriter().write("Hallo");
		
	}
	
	@Override
	protected long getLastModified(HttpServletRequest request) {
	    return super.getLastModified(request);
	}
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}
}
