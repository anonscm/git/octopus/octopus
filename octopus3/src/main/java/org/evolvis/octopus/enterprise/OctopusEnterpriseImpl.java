package org.evolvis.octopus.enterprise;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.RandomStringUtils;
import org.evolvis.octopus.management.application.Stage;
import org.evolvis.octopus.management.application.StageManagement;
import org.evolvis.octopus.management.modules.Module;
import org.evolvis.octopus.management.modules.ModuleManagement;

public class OctopusEnterpriseImpl implements StageManagement, ModuleManagement {
	private Map<Stage, Object> stages = new LinkedHashMap<Stage, Object>();
	private Map<Stage, Set<Module>> modules = new LinkedHashMap<Stage, Set<Module>>();

	public Stage getStage(Stage stage) {
		return null;
    }

	public Set<Stage> getStageList() {
	    return null;
    }

	public Stage setStage(Stage stage) {
	    return null;
    }

	public Module getModule(Stage stage, Module module) {
	    return null;
    }

	public Set<Module> getModuleList(Stage stage) {
	    return null;
    }

	public Module setModule(Stage stage, Module module) {
	    return null;
    }
	
	public String getNewId() {
		return RandomStringUtils.randomAlphanumeric(8);
	}
}
