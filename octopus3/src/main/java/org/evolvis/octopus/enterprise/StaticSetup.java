package org.evolvis.octopus.enterprise;

import org.evolvis.octopus.management.application.Stage;
import org.evolvis.octopus.management.modules.Module;

public class StaticSetup {
	public static final Stage defaultStage = new Stage();
	public static final Stage updateStage = new Stage();
	
	public static final Module application1 = new Module();
	public static final Module application2 = new Module();
	
	static {
		defaultStage.setId("1");
		defaultStage.setName("default");
		
		updateStage.setId("2");
		updateStage.setId("update");
		
		application1.setId("3");
		application1.setName("application1");
		
		application2.setId("4");
		application2.setName("application2");
		
		
	}
}
