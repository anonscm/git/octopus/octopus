package org.evolvis.octopus.logging;

public interface Logger {
	public boolean isDebugLoggable();
	public boolean isInfoLoggable();
	public boolean isWarningLoggable();
	public boolean isErrorLoggable();
	public void debug(Object message);
	public void debug(Object message, Throwable throwable);
	public void info(Object message);
	public void info(Object message, Throwable throwable);
	public void warning(Object message);
	public void warning(Object message, Throwable throwable);
	public void error(Object message);
	public void error(Object message, Throwable throwable);
}
