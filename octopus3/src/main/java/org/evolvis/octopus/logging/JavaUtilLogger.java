package org.evolvis.octopus.logging;

import java.util.logging.Level;

public class JavaUtilLogger implements Logger {
	private final java.util.logging.Logger delegate;

	JavaUtilLogger(String name) {
		delegate = java.util.logging.Logger.getLogger(name);
	}

	public boolean isDebugLoggable() {
		return delegate.isLoggable(Level.FINE);
	}

	public boolean isInfoLoggable() {
		return delegate.isLoggable(Level.INFO);
	}

	public boolean isWarningLoggable() {
		return delegate.isLoggable(Level.WARNING);
	}

	public boolean isErrorLoggable() {
		return delegate.isLoggable(Level.SEVERE);
	}

	public void debug(Object message) {
		if (delegate.isLoggable(Level.FINE))
			delegate.log(Level.FINE, String.valueOf(message));
	}

	public void debug(Object message, Throwable throwable) {
		if (delegate.isLoggable(Level.FINE))
			delegate.log(Level.FINE, String.valueOf(message), throwable);
	}

	public void info(Object message) {
		if (delegate.isLoggable(Level.INFO))
			delegate.log(Level.INFO, String.valueOf(message));
	}

	public void info(Object message, Throwable throwable) {
		if (delegate.isLoggable(Level.INFO))
			delegate.log(Level.INFO, String.valueOf(message), throwable);
	}

	public void warning(Object message) {
		if (delegate.isLoggable(Level.WARNING))
			delegate.log(Level.WARNING, String.valueOf(message));
	}

	public void warning(Object message, Throwable throwable) {
		if (delegate.isLoggable(Level.WARNING))
			delegate.log(Level.WARNING, String.valueOf(message), throwable);
	}

	public void error(Object message) {
		if (delegate.isLoggable(Level.SEVERE))
			delegate.log(Level.SEVERE, String.valueOf(message));
	}

	public void error(Object message, Throwable throwable) {
		if (delegate.isLoggable(Level.SEVERE))
			delegate.log(Level.SEVERE, String.valueOf(message), throwable);
	}
}
