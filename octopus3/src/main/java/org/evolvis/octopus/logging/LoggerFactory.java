package org.evolvis.octopus.logging;

public class LoggerFactory {
	private LoggerFactory() {
	}
	
	public Logger getInstance(String name) {
		return new JavaUtilLogger(name);
	}
}
