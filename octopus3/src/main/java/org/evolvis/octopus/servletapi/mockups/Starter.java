package org.evolvis.octopus.servletapi.mockups;

import java.net.URL;

import javax.servlet.Servlet;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import de.tarent.octopus.prototype.OctopusServlet;

public class Starter {
	public static void main(String[] args) {
		try {
			URL serverContextURL = new URL("http://127.0.0.1:8080/cxf/");
			WebappContextMockup webappContext = new WebappContextMockup(serverContextURL);
			
			ServletContextMockup servletContext = new ServletContextMockup();
	    	ServletConfigMockup servletConfig = new ServletConfigMockup(servletContext);  
			ServletRequest servletRequest;
			ServletResponse servletResponse;
			
			Servlet servlet = new OctopusServlet();
			servlet.init(servletConfig);
			
			servletRequest = new ServletRequestMockup(new URL("http://127.0.0.1:8008/"));
			servletResponse = new ServletResponseMockup();
			servlet.service(servletRequest, servletResponse);
			
			servletRequest = new ServletRequestMockup(new URL("http://127.0.0.1:8008/services/"));
			servletResponse = new ServletResponseMockup();
			servlet.service(servletRequest, servletResponse);
			
			/*
			servletRequest = new ServletRequestMockup(new URL("http://127.0.0.1:8008/services/test-restful-provider?num=1&num=2"));
			servletResponse = new ServletResponseMockup();
			servlet.service(servletRequest, servletResponse);
			*/
			
			servletRequest = new ServletRequestMockup(new URL("http://127.0.0.1:8008/test-restful-provider?num1=1&num2=2"));
			servletResponse = new ServletResponseMockup();
			servlet.service(servletRequest, servletResponse);

			servletRequest = new ServletRequestMockup(new URL("http://127.0.0.1:8008/test-soap-provider"));
			servletResponse = new ServletResponseMockup();
			servlet.service(servletRequest, servletResponse);
			
			/*
			servletRequest = new ServletRequestMockup(new URL("http://127.0.0.1:8008/test-restful-provider?wsdl"));
			servletResponse = new ServletResponseMockup();
			servlet.service(servletRequest, servletResponse);
			
			servletRequest = new ServletRequestMockup(new URL("http://127.0.0.1:8008/octopus-reflection-test-1"));
			servletResponse = new ServletResponseMockup();
			servlet.service(servletRequest, servletResponse);
			*/
			
	    } catch (Exception e) {
	    	e.printStackTrace();
	    }
    }
}
