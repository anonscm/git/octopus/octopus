package org.evolvis.octopus.servletapi.mockups;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.security.Principal;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletInputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.collections.IteratorEnumeration;

public class ServletRequestMockup implements HttpServletRequest {
	private Map<String, String> attributes = new HashMap<String, String>();
	private Map<String, String> header = new HashMap<String, String>();
	
	private URL requestURL;
	private String contentType;
	private String characterEncoding;
	
	public ServletRequestMockup(URL requestURL) {
		System.err.println("create requestmockup with URL: " + requestURL);
		this.requestURL = requestURL;
	}
	
	/* from javax.servlet.ServletRequest */

	@Override
	public Object getAttribute(String name) {
		System.err.println("get attr " + name + " = " + attributes.get(name));
		return attributes.get(name);
	}
	
	@Override
	public Enumeration getAttributeNames() {
		return new IteratorEnumeration(attributes.keySet().iterator());
	}
	
	@Override
	public String getCharacterEncoding() {
		return characterEncoding;
	}
	
	@Override
	public int getContentLength() {
		throw new RuntimeException("not impl");
		
	}
	
	@Override
	public String getContentType() {
		return contentType;
	}
	
	@Override
	public ServletInputStream getInputStream() throws IOException {
		return new ServletInputStreamMockup(new ByteArrayInputStream(new byte[0]));
	}
	
	@Override
	public String getLocalAddr() {
		throw new RuntimeException("not impl");
		
	}
	
	@Override
	public String getLocalName() {
		throw new RuntimeException("not impl");
		
	}
	
	@Override
	public int getLocalPort() {
		throw new RuntimeException("not impl");
		
	}
	
	@Override
	public Locale getLocale() {
		throw new RuntimeException("not impl");
		
	}
	
	@Override
	public Enumeration getLocales() {
		throw new RuntimeException("not impl");
		
	}
	
	@Override
	public String getParameter(String name) {
		String query = requestURL.getQuery();
		if (query != null && query.length() != 0) {
			for (String part : query.split("&")) {
				if (part.startsWith(name + "=")) {
					part = part.substring(name.length() + 1);
					System.err.println("found parameter for " + name + " = " + part);
					return part;
				}
			}
		}
		System.err.println("no parameter found for " + name);
		return null;
	}
	
	@Override
	public Map getParameterMap() {
		throw new RuntimeException("not impl");
		
	}
	
	@Override
	public Enumeration getParameterNames() {
		throw new RuntimeException("not impl");
		
	}
	
	@Override
	public String[] getParameterValues(String arg0) {
		throw new RuntimeException("not impl");
		
	}
	
	@Override
	public String getProtocol() {
		throw new RuntimeException("not impl");
		
	}
	
	@Override
	public BufferedReader getReader() throws IOException {
		throw new RuntimeException("not impl");
		
	}
	
	@Override
	public String getRealPath(String arg0) {
		throw new RuntimeException("not impl");
		
	}
	
	@Override
	public String getRemoteAddr() {
		throw new RuntimeException("not impl");
		
	}
	
	@Override
	public String getRemoteHost() {
		throw new RuntimeException("not impl");
		
	}
	
	@Override
	public int getRemotePort() {
		throw new RuntimeException("not impl");
		
	}
	
	@Override
	public RequestDispatcher getRequestDispatcher(String arg0) {
		throw new RuntimeException("not impl");
		
	}
	
	@Override
	public String getScheme() {
		throw new RuntimeException("not impl");
		
	}
	
	@Override
	public String getServerName() {
		throw new RuntimeException("not impl");
		
	}
	
	@Override
	public int getServerPort() {
		throw new RuntimeException("not impl");
		
	}
	
	@Override
	public boolean isSecure() {
		throw new RuntimeException("not impl");
		
	}
	
	@Override
	public void removeAttribute(String arg0) {
		throw new RuntimeException("not impl");
		
	}
	
	@Override
	public void setAttribute(String arg0, Object arg1) {
		throw new RuntimeException("not impl");
		
	}
	
	@Override
	public void setCharacterEncoding(String characterEncoding) throws UnsupportedEncodingException {
		this.characterEncoding = characterEncoding;
	}
	
	/* from javax.servlet.http.HttpServletRequest */

	@Override
	public String getAuthType() {
		throw new RuntimeException("not impl");
		
	}
	
	@Override
	public String getContextPath() {
		return "/";
	}
	
	@Override
	public Cookie[] getCookies() {
		throw new RuntimeException("not impl");
		
	}
	
	@Override
	public long getDateHeader(String s) {
		throw new RuntimeException("not impl");
		
	}
	
	@Override
	public String getHeader(String name) {
		System.err.println("get header " + name + " = " + header.get(name));
		return header.get(name);
	}
	
	@Override
	public Enumeration getHeaderNames() {
		return new IteratorEnumeration(header.keySet().iterator());
	}
	
	@Override
	public Enumeration getHeaders(String s) {
		return new IteratorEnumeration(header.keySet().iterator());
	}
	
	@Override
	public int getIntHeader(String s) {
		throw new RuntimeException("not impl");
		
	}
	
	@Override
	public String getMethod() {
		// TODO must support POST for soap requests
		return "GET";
	}
	
	@Override
	public String getPathInfo() {
		return requestURL.getPath();
	}
	
	@Override
	public String getPathTranslated() {
		throw new RuntimeException("not impl");
	}
	
	@Override
	public String getQueryString() {
		return requestURL.getQuery();
	}
	
	@Override
	public String getRemoteUser() {
		throw new RuntimeException("not impl");
		
	}
	
	@Override
	public String getRequestURI() {
		System.err.println("request URI = " + requestURL.getPath());
		return requestURL.getPath();
	}
	
	@Override
	public StringBuffer getRequestURL() {
		System.err.println("request URL = " + requestURL.toString());
		return new StringBuffer(requestURL.toString());
	}
	
	@Override
	public String getRequestedSessionId() {
		throw new RuntimeException("not impl");
		
	}
	
	@Override
	public String getServletPath() {
		throw new RuntimeException("not impl");
		
	}
	
	@Override
	public HttpSession getSession() {
		throw new RuntimeException("not impl");
		
	}
	
	@Override
	public HttpSession getSession(boolean flag) {
		throw new RuntimeException("not impl");
		
	}
	
	@Override
	public Principal getUserPrincipal() {
		throw new RuntimeException("not impl");
		
	}
	
	@Override
	public boolean isRequestedSessionIdFromCookie() {
		throw new RuntimeException("not impl");
		
	}
	
	@Override
	public boolean isRequestedSessionIdFromURL() {
		throw new RuntimeException("not impl");
		
	}
	
	@Override
	public boolean isRequestedSessionIdFromUrl() {
		throw new RuntimeException("not impl");
		
	}
	
	@Override
	public boolean isRequestedSessionIdValid() {
		throw new RuntimeException("not impl");
		
	}
	
	@Override
	public boolean isUserInRole(String s) {
		throw new RuntimeException("not impl");
		
	}
}
