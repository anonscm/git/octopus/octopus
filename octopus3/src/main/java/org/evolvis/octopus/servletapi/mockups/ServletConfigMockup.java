package org.evolvis.octopus.servletapi.mockups;

import java.util.Enumeration;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;

public class ServletConfigMockup implements ServletConfig {
	private final ServletContextMockup servletContext;
	
	public ServletConfigMockup(ServletContextMockup servletContext) {
		this.servletContext = servletContext;
	}
	
	@Override
    public String getInitParameter(String name) {
	    return servletContext.getInitParameter(name);
    }

	@Override
    public Enumeration getInitParameterNames() {
	    return servletContext.getInitParameterNames();
    }

	@Override
    public ServletContext getServletContext() {
	    return servletContext;
    }

	@Override
    public String getServletName() {
	    return "asd";
    }
}
