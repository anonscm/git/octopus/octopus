package org.evolvis.octopus.servletapi.mockups;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Locale;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

public class ServletResponseMockup implements HttpServletResponse {
	private String contentType = null;
	private ServletOutputStream outputStream;
	private PrintWriter writer;
	
	private int status = 200;
	private String statusText = "OK";
	
	public ServletResponseMockup() {
		outputStream = new ServletOutputStreamMockup(System.out);
		writer = new PrintWriter(System.out);
	}
	
	/* from javax.servlet.ServletResponse */

	@Override
	public void flushBuffer() throws IOException {
		writer.flush();
		outputStream.flush();
	}
	
	@Override
	public int getBufferSize() {
		throw new RuntimeException("not impl");
		
	}
	
	@Override
	public String getCharacterEncoding() {
		throw new RuntimeException("not impl");
		
	}
	
	@Override
	public String getContentType() {
		return contentType;
	}
	
	@Override
	public Locale getLocale() {
		throw new RuntimeException("not impl");
		
	}
	
	@Override
	public ServletOutputStream getOutputStream() throws IOException {
		return outputStream;
	}
	
	@Override
	public PrintWriter getWriter() throws IOException {
		return writer;
	}
	
	@Override
	public boolean isCommitted() {
		throw new RuntimeException("not impl");
		
	}
	
	@Override
	public void reset() {
		throw new RuntimeException("not impl");
		
	}
	
	@Override
	public void resetBuffer() {
		throw new RuntimeException("not impl");
		
	}
	
	@Override
	public void setBufferSize(int arg0) {
		throw new RuntimeException("not impl");
		
	}
	
	@Override
	public void setCharacterEncoding(String arg0) {
		throw new RuntimeException("not impl");
		
	}
	
	@Override
	public void setContentLength(int arg0) {
		throw new RuntimeException("not impl");
		
	}
	
	@Override
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	
	@Override
	public void setLocale(Locale arg0) {
		throw new RuntimeException("not impl");
		
	}
	
	/* from javax.servlet.http.HttpServletResponse */

	@Override
	public void addCookie(Cookie cookie) {
		throw new RuntimeException("not impl");
		
	}
	
	@Override
	public void addDateHeader(String s, long l) {
		throw new RuntimeException("not impl");
		
	}
	
	@Override
	public void addHeader(String s, String s1) {
		throw new RuntimeException("not impl");
		
	}
	
	@Override
	public void addIntHeader(String s, int i) {
		throw new RuntimeException("not impl");
		
	}
	
	@Override
	public boolean containsHeader(String s) {
		throw new RuntimeException("not impl");
		
	}
	
	@Override
	public String encodeRedirectURL(String s) {
		throw new RuntimeException("not impl");
		
	}
	
	@Override
	public String encodeRedirectUrl(String s) {
		throw new RuntimeException("not impl");
		
	}
	
	@Override
	public String encodeURL(String s) {
		throw new RuntimeException("not impl");
		
	}
	
	@Override
	public String encodeUrl(String s) {
		throw new RuntimeException("not impl");
		
	}
	
	@Override
	public void sendError(int i) throws IOException {
		throw new RuntimeException("not impl");
		
	}
	
	@Override
	public void sendError(int i, String s) throws IOException {
		throw new RuntimeException("not impl");
		
	}
	
	@Override
	public void sendRedirect(String s) throws IOException {
		throw new RuntimeException("not impl");
		
	}
	
	@Override
	public void setDateHeader(String s, long l) {
		throw new RuntimeException("not impl");
		
	}
	
	@Override
	public void setHeader(String s, String s1) {
		throw new RuntimeException("not impl");
		
	}
	
	@Override
	public void setIntHeader(String s, int i) {
		throw new RuntimeException("not impl");
		
	}
	
	@Override
	public void setStatus(int status) {
		this.status = status;
		this.statusText = "OK";
		System.err.println("set status to " + status);
	}
	
	@Override
	public void setStatus(int status, String statusText) {
		this.status = status;
		this.statusText = statusText;
		System.err.println("set status to " + status + " - " + statusText);
	}
	
	public int getStatus() {
		return status;
	}
	
	public String getStatusString() {
		if (statusText == null || statusText.length() == 0)
			statusText = "OK";
		return status + " " + statusText;
	}
}
