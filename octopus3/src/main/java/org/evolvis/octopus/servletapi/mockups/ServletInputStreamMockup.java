package org.evolvis.octopus.servletapi.mockups;

import java.io.IOException;
import java.io.InputStream;

import javax.servlet.ServletInputStream;

public class ServletInputStreamMockup extends ServletInputStream {
	private final InputStream inputStream;

	public ServletInputStreamMockup(InputStream inputStream) {
		this.inputStream = inputStream;
	}
	
	@Override
	public int read() throws IOException {
	    return inputStream.read();
	}
	
	@Override
	public int read(byte[] b) throws IOException {
	    return inputStream.read(b);
	}
	
	@Override
	public int read(byte[] b, int off, int len) throws IOException {
	    return inputStream.read(b, off, len);
	}
}
