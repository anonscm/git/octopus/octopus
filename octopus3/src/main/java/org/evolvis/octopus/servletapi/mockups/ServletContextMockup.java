package org.evolvis.octopus.servletapi.mockups;

import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.servlet.RequestDispatcher;
import javax.servlet.Servlet;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.apache.commons.collections.IteratorEnumeration;

public class ServletContextMockup implements ServletContext {
	private Map<String, Object> attributes = new HashMap<String, Object>();
	private Map<String, String> initParamaters = new HashMap<String, String>();

	@Override
    public Object getAttribute(String name) {
		System.err.println("get attr " + name + " = " + attributes.get(name));
		return attributes.get(name);
    }

	@Override
    public Enumeration getAttributeNames() {
		System.err.println("getAttributeNames");
	    return new IteratorEnumeration(attributes.keySet().iterator());
    }

	@Override
    public ServletContext getContext(String uripath) {
		System.err.println("getcontext for " + uripath);
	    return this;
    }

	@Override
    public String getContextPath() {
	    return "/";
    }

	@Override
    public String getInitParameter(String name) {
	    return initParamaters.get(name);
    }

	@Override
    public Enumeration getInitParameterNames() {
		return new IteratorEnumeration(initParamaters.keySet().iterator());
    }

	@Override
    public int getMajorVersion() {
	    return 2;
    }

	@Override
    public String getMimeType(String file) {
	    return null;
    }

	@Override
    public int getMinorVersion() {
	    return 5;
    }

	@Override
    public RequestDispatcher getNamedDispatcher(String name) {
	    // TODO Auto-generated method stub
	    return null;
    }

	@Override
    public String getRealPath(String path) {
	    return "/tmp/test0101021";
    }

	@Override
    public RequestDispatcher getRequestDispatcher(String path) {
	    return new RequestDispatcherMockup(path);
    }

	@Override
    public URL getResource(String path) throws MalformedURLException {
		System.err.println("getresource " + path);
	    return null;
    }

	@Override
    public InputStream getResourceAsStream(String path) {
		System.err.println("getresourcesasstream " + path);
	    return null;
    }

	@Override
    public Set getResourcePaths(String path) {
		System.err.println("getresourcepaths " + path);
	    return null;
    }

	@Override
    public String getServerInfo() {
	    return "octopus/1.0";
    }

	@Override
    public Servlet getServlet(String name) throws ServletException {
		System.err.println("getservlet " + name);
	    return null;
    }

	@Override
    public String getServletContextName() {
		System.err.println("getservletcontextname");
	    return null;
    }

	@Override
    public Enumeration getServletNames() {
		System.err.println("getservletnames");
	    return null;
    }

	@Override
    public Enumeration getServlets() {
	    System.err.println("getservlets");
	    return null;
    }

	@Override
    public void log(String msg) {
		System.err.println("LOG " + msg);
    }

	@Override
    public void log(Exception exception, String msg) {
		System.err.println("LOG " + msg + " -- " + exception);
    }

	@Override
    public void log(String message, Throwable throwable) {
		System.err.println("LOG " + message + " -- " + throwable);
    }

	@Override
    public void removeAttribute(String name) {
		System.err.println("remove attr " + name);
		attributes.remove(name);
    }

	@Override
    public void setAttribute(String name, Object object) {
		System.err.println("set attr " + name + " = " + object);
	    attributes.put(name, object);
    }
	
}
