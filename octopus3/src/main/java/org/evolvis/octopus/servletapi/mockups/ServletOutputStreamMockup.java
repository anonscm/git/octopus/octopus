package org.evolvis.octopus.servletapi.mockups;

import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletOutputStream;

public class ServletOutputStreamMockup extends ServletOutputStream {
	private final OutputStream outputStream;
	
	public ServletOutputStreamMockup(OutputStream outputStream) {
	    super();
	    this.outputStream = outputStream;
    }
	
	@Override
	public void flush() throws IOException {
		outputStream.flush();
	}
	
	@Override
	public void close() throws IOException {
		// TODO change this flush to close
		outputStream.flush();
	}
	
	@Override
	public void write(int b) throws IOException {
		outputStream.write(b);
	}
	
	@Override
	public void write(byte[] b) throws IOException {
		outputStream.write(b);
	}
	
	@Override
	public void write(byte[] b, int off, int len) throws IOException {
		outputStream.write(b, off, len);
	}
}
