package org.evolvis.octopus.servletapi.mockups;

import java.io.IOException;
import java.net.URL;

public class WebappContextMockup {
	private final URL url;
	
	public WebappContextMockup(URL url) throws IOException {
		if (url.getPath() == null)
			throw new NullPointerException("Webapplication path countn't be null");
		else if (!url.getPath().startsWith("/"))
			throw new IllegalArgumentException("Webapplication path must start with a slash.");
		else if (!url.getPath().endsWith("/"))
			throw new IllegalArgumentException("Webapplication path must end with a slash.");
		this.url = url;
	}
	
	public URL getURL() {
		return url;
	}
	
	public String getPath() {
		return url.getPath();
	}
}
