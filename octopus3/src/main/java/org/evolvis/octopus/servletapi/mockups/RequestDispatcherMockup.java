package org.evolvis.octopus.servletapi.mockups;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class RequestDispatcherMockup implements RequestDispatcher {
	private final String path;
	
	public RequestDispatcherMockup(String path) {
		this.path = path;
		System.err.println("create request dispatcher with path " + path);
	}
	
	@Override
    public void forward(ServletRequest request, ServletResponse response) throws ServletException, IOException {
		System.err.println("wannabe forward request to " + path);
    }

	@Override
    public void include(ServletRequest request, ServletResponse response) throws ServletException, IOException {
		System.err.println("wannabe include request to " + path);
    }
}
