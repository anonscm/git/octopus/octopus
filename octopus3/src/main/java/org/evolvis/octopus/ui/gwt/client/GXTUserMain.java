package org.evolvis.octopus.ui.gwt.client;

import com.extjs.gxt.ui.client.GXT;
import com.extjs.gxt.ui.client.Registry;
import com.extjs.gxt.ui.client.Style.LayoutRegion;
import com.extjs.gxt.ui.client.util.Margins;
import com.extjs.gxt.ui.client.util.Theme;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.HtmlContainer;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.Viewport;
import com.extjs.gxt.ui.client.widget.layout.BorderLayout;
import com.extjs.gxt.ui.client.widget.layout.BorderLayoutData;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.ui.RootPanel;

public class GXTUserMain implements EntryPoint {
	private Viewport viewport;
	
	public void onModuleLoad() {
		GXT.setDefaultTheme(Theme.GRAY, true);
		GXT.hideLoadingPanel("loading");
		
		viewport = new Viewport();
		viewport.setLayout(new BorderLayout());
		
		createNorth();
		
		BorderLayoutData westData = new BorderLayoutData(LayoutRegion.WEST, 200, 150, 350);
		westData.setMargins(new Margins(5, 0, 5, 5));
		
		ContentPanel west = new ContentPanel();
		west.setLayoutOnChange(true);
		west.setHeading("GXT Mail Demo");
		west.setLayout(new FitLayout());
		
		viewport.add(west, westData);
		
		LayoutContainer main = new LayoutContainer();
		BorderLayout layout = new BorderLayout();
		main.setLayout(layout);
		
		ContentPanel center = new ContentPanel();
		center.setLayout(new FitLayout());
		center.add(new CalculationPanel());
		
		LayoutContainer south = new LayoutContainer();
		south.setBorders(true);
		south.setLayout(new FitLayout());
		
		BorderLayoutData centerData = new BorderLayoutData(LayoutRegion.CENTER);
		centerData.setMargins(new Margins(0, 0, 5, 0));
		
		BorderLayoutData southData = new BorderLayoutData(LayoutRegion.SOUTH, .5f, 100, 1000);
		southData.setSplit(true);
		southData.setMargins(new Margins(0, 0, 0, 0));
		
		main.add(center, centerData);
		main.add(new CalculationPanel());
		main.add(south, southData);
		
		BorderLayoutData mainCenter = new BorderLayoutData(LayoutRegion.CENTER);
		mainCenter.setMargins(new Margins(5, 5, 5, 5));
		
		viewport.add(main, mainCenter);
		
		Registry.register("viewport", viewport);
		Registry.register("west", west);
		Registry.register("center", center);
		Registry.register("south", south);
		
		RootPanel.get().add(viewport);
	}
	
	private void createNorth() {
		StringBuffer sb = new StringBuffer();
		sb.append(
				"<div id='demo-header' class='x-small-editor'>" +
				"<div id='demo-theme'></div>" +
				"<div id=demo-title>Ext GWT Mail Demo</div>" +
				"</div>");
		
		HtmlContainer northPanel = new HtmlContainer(sb.toString());
		northPanel.setEnableState(false);
		
		BorderLayoutData data = new BorderLayoutData(LayoutRegion.NORTH, 33);
		data.setMargins(new Margins());
		northPanel.setData(data);
		viewport.add(northPanel, data);
	}
}
