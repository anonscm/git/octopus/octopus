package org.evolvis.octopus.ui.gwt.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.DeferredCommand;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.WindowResizeListener;
import com.google.gwt.user.client.ui.DockPanel;
import com.google.gwt.user.client.ui.Frame;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalSplitPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

public class GWTUserMain implements EntryPoint, WindowResizeListener {
	private DockPanel dockPanel;
	
	public void onModuleLoad() {
		dockPanel = new DockPanel();
		dockPanel.setSize("100%", "100%");
		dockPanel.add(new HTML("<div id='demo-header' class='x-small-editor'>" + "<div id='demo-theme'></div>" + "<div id=demo-title>Ext GWT Mail Demo</div>" + "</div>"), DockPanel.NORTH);
		
		VerticalPanel verticalPanel = new VerticalPanel();
		verticalPanel.add(new Label("..."));
		
		Frame frame = new Frame();
		frame.setUrl("http://www.google.de/");
		
		verticalPanel.add(frame);
		
		HorizontalSplitPanel splitPanel = new HorizontalSplitPanel();
		splitPanel.setSize("100%", "100%");
		splitPanel.add(new TreeWidget());
		splitPanel.add(verticalPanel);
		splitPanel.setSplitPosition("20%");
		
		dockPanel.add(splitPanel, DockPanel.CENTER);
		dockPanel.setCellHeight(splitPanel, "100%");
		dockPanel.add(new CalculationPanel(), DockPanel.SOUTH);
		
		// Hook the window resize event, so that we can adjust the UI.
		Window.addWindowResizeListener(this);
		
		// Get rid of scrollbars, and clear out the window's built-in margin,
		// because we want to take advantage of the entire client area.
		Window.enableScrolling(false);
		Window.setMargin("0px");
		
		RootPanel.get().add(dockPanel);
		
		// Call the window resized handler to get the initial sizes setup. Doing
		// this in a deferred command causes it to occur after all widgets'
		// sizes
		// have been computed by the browser.
		DeferredCommand.addCommand(new Command() {
			public void execute() {
				onWindowResized(Window.getClientWidth(), Window.getClientHeight());
			}
		});
		
		onWindowResized(Window.getClientWidth(), Window.getClientHeight());
	}
	
	public void onWindowResized(int width, int height) {
		dockPanel.setSize(width + "px",  height + "px");
	}
}
