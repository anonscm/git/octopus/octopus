package org.evolvis.octopus.ui.gwt.client;

import com.google.gwt.user.client.ui.Tree;

public class TreeWidget extends Tree {
	public TreeWidget() {
	    super();
	    
	    addItem("Octopus Info");
	    addItem("Java VM Info");
	    addItem("System Info");
    }
}
