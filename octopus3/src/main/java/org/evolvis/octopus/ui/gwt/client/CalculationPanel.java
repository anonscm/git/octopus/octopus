package org.evolvis.octopus.ui.gwt.client;

import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

public class CalculationPanel extends SimplePanel {
	public CalculationPanel() {
		final VerticalPanel panel = new VerticalPanel();
		panel.setWidth("100%");
		
		final TextBox urlTextBox = new TextBox();
		urlTextBox.setText("http://127.0.0.1:8080/json/test-restful-provider/invoke");
		urlTextBox.setWidth("100%");
		
		final TextBox num1TextBox = new TextBox();
		num1TextBox.setText("3");
		
		final TextBox num2TextBox = new TextBox();
		num2TextBox.setText("4");
		
		final TextBox resultTextBox = new TextBox();
		resultTextBox.setReadOnly(true);
		
		final Button calcButton = new Button();
		calcButton.setText("calc...");
		calcButton.addClickListener(new ClickListener() {
			public void onClick(Widget sender) {
				try {
					String url = urlTextBox.getText();
					url += "?num1=" + num1TextBox.getText();
					url += "&num2=" + num2TextBox.getText();
					
					RequestBuilder requestBuilder = new RequestBuilder(RequestBuilder.GET, url);
					requestBuilder.sendRequest(null, new RequestCallback() {
						public void onResponseReceived(Request request, Response response) {
							if (200 == response.getStatusCode()) {
								try {
									// parse the response text into JSON
									JSONObject jsonObject = JSONParser.parse(response.getText()).isObject();
									jsonObject = jsonObject.get("addNumbersResponse").isObject();
									
									resultTextBox.setText(jsonObject.get("return").isNumber().toString());
									
								} catch (Exception e) {
									Window.alert("Could not parse JSON (" + e.getMessage() + ")");
								}
							} else {
								Window.alert("Couldn't retrieve JSON (" + response.getStatusText() + ")");
							}
						}
						
						public void onError(Request request, Throwable e) {
							Window.alert("Couldn't retrieve JSON (" + e.getMessage() + ")");
						}
					});
				} catch (RequestException e) {
					Window.alert("Couldn't retrieve JSON (" + e.getMessage() + ")");
				}
				
			}
		});
		
		panel.add(urlTextBox);
		panel.add(num1TextBox);
		panel.add(num2TextBox);
		panel.add(resultTextBox);
		panel.add(calcButton);
		
		setWidget(panel);
	}
}
