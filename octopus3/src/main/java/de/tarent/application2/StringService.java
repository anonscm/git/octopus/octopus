package de.tarent.application2;

import de.tarent.application1.MathService;

public class StringService {
	public String sum(String a, String b) throws IllegalArgumentException {
		if (a == null || b == null)
			throw new IllegalArgumentException("Null Parameters are not allowed.");
		if (a.length() == 0 || b.length() == 0)
			throw new IllegalArgumentException("Empty Parameters are not allowed.");
		
		return a + b;
	}
	
	public void testApplication1Access() throws IllegalAccessException {
		System.out.println(new MathService().getClass().getClassLoader());
		System.out.println("Application 2 access Application 1 successful.");
	}
}
