package de.tarent.application1;

import de.tarent.application2.StringService;

public class MathService {
	public Integer sum(Integer a, Integer b) throws IllegalArgumentException {
		if (a == null || b == null)
			throw new IllegalArgumentException("Null Parameters are not allowed.");
		if (a < 0 || b < 0)
			throw new IllegalArgumentException("Negative Parameters are not allowed.");
		
		return a + b;
	}
	
	public void testApplication2Access() {
		System.out.println(new StringService().getClass().getClassLoader());
		System.out.println("Application 1 access Application 2 successful.");
	}
}
