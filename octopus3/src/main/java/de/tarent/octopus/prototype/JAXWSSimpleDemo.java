/*
 * Copyright (c) tarent GmbH
 * Bahnhofstrasse 13 . 53123 Bonn
 * www.tarent.de . info@tarent.de
 *
 * Created on 20.12.2007
 */

package de.tarent.octopus.prototype;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;
import javax.jws.soap.SOAPBinding.Use;

@WebService
@SOAPBinding(style = Style.RPC, use = Use.ENCODED)
public interface JAXWSSimpleDemo {
	@WebMethod
	@WebResult(name = "sum")
	public Integer sum(
			@WebParam(name = "a") Integer a,
			@WebParam(name = "b") Integer b);
}
