package de.tarent.octopus.prototype;

import javax.xml.namespace.QName;

import org.apache.cxf.frontend.MethodDispatcher;
import org.apache.cxf.jaxws.support.JaxWsServiceFactoryBean;
import org.apache.cxf.message.Exchange;
import org.apache.cxf.service.ServiceImpl;
import org.apache.cxf.service.invoker.Invoker;
import org.apache.cxf.service.model.InterfaceInfo;
import org.apache.cxf.service.model.MessageInfo;
import org.apache.cxf.service.model.MessagePartInfo;
import org.apache.cxf.service.model.OperationInfo;
import org.apache.cxf.service.model.ServiceInfo;

public class OctopusServiceFactory extends JaxWsServiceFactoryBean /*ReflectionServiceFactoryBean*/ {
	@Override
	protected void initializeServiceModel() {
		setEndpointName(new QName("http://schema.taremt.de/octopus", "octopus"));
		
		ServiceInfo serviceInfo = new ServiceInfo();
		serviceInfo.setName(new QName("http://schemas.tarent.de/octopus", "test"));
		serviceInfo.setTargetNamespace("http://prototype.octopus.tarent.de/");
		setService(new ServiceImpl(serviceInfo));
		
		if (getProperties() != null) {
			getService().putAll(getProperties());
		}
		
		System.err.println(getService().keySet());
		
		getService().put(MethodDispatcher.class.getName(), getMethodDispatcher());
		
		System.err.println(getService().keySet());
		
		InterfaceInfo interfaceInfo = new InterfaceInfo(serviceInfo, new QName("http://schemas.tarent.de/octopus", "test"));
		
		OperationInfo operationInfo = interfaceInfo.addOperation(new QName("http://schemas.tarent.de/octopus", "c"));
		operationInfo.setProperty("action", "blub2312");
		
		// Setup the input message
		operationInfo.setProperty(METHOD, new Object());
		
		MessageInfo inMsg = operationInfo.createMessage(new QName("http://schemas.tarent.de/octopus", "cIn"), MessageInfo.Type.INPUT);
		
		operationInfo.setInput(inMsg.getName().getLocalPart(), inMsg);
		
		MessagePartInfo part1 = inMsg.addMessagePart(new QName("InPartName"));
		initializeParameter(part1, Integer.class, Integer.class.getGenericSuperclass());
		part1.setProperty(ELEMENT_NAME, new QName("InParameterName"));
		// part1.setProperty(HEADER, Boolean.TRUE);
		part1.setIndex(0);
		
		MessagePartInfo part2 = inMsg.addMessagePart(new QName("InPartName2"));
		initializeParameter(part2, Integer.class, Integer.class.getGenericSuperclass());
		part2.setProperty(ELEMENT_NAME, new QName("InParameterName2"));
		// part2.setProperty(HEADER, Boolean.TRUE);
		part2.setIndex(1);
		
		MessageInfo outMsg = operationInfo.createMessage(new QName("http://schemas.tarent.de/octopus", "cOut"), MessageInfo.Type.OUTPUT);
		
		operationInfo.setOutput(outMsg.getName().getLocalPart(), outMsg);
		
		MessagePartInfo part3 = outMsg.addMessagePart(new QName("OutPartName"));
		initializeParameter(part3, Integer.class, Integer.class.getGenericSuperclass());
		part3.setProperty(ELEMENT_NAME, new QName("OutParameterName"));
		part3.setIndex(0);
		
		// Set up the fault messages
		addFault(interfaceInfo, operationInfo, IllegalArgumentException.class);
		
		boolean isWrapped = isWrapped();
		if (isWrapped) {
			initializeWrappedSchema(serviceInfo);
		}
		
		/*
		try {
			Method m1 = OctopusServlet.class.getMethod("c1");
			Method m2 = OctopusServlet.class.getMethod("c2", Integer.class, Integer.class);
			
			getMethodDispatcher().bind(operationInfo, m1, m2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		*/
		
		if (getDataBinding() != null) {
			getDataBinding().initialize(getService());
		}
		
		getService().setInvoker(new Invoker() {
			public Object invoke(Exchange exchange, Object o) {
			    System.err.println("invoke  " + exchange + " with " + o);
			    return null;
			}
		});
	}
}
