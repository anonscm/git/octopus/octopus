
package de.tarent.octopus.prototype;

import javax.jws.WebService;

@WebService(endpointInterface = "de.tarent.octopus.prototype.JAXWSSimpleDemo", portName = "JaxWsSimplePort", targetNamespace = "http://schemas.tarent.de/octopus")
public class JAXWSSimpleDemoImpl implements JAXWSSimpleDemo {
	public Integer sum(Integer a, Integer b) {
		if (a == null || b == null)
			throw new NullPointerException("Parameters should not be null.");
		if (a < 0 || b < 0)
			throw new IllegalArgumentException("Parameters should not be negative.");
		return a + b;
	}
}
