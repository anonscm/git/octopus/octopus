package de.tarent.octopus.prototype;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.Name;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPFactory;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.Provider;
import javax.xml.ws.ServiceMode;
import javax.xml.ws.WebServiceProvider;
import javax.xml.ws.Service.Mode;

@WebServiceProvider
@ServiceMode(value = Mode.MESSAGE)
public class TestSoapProvider implements Provider<SOAPMessage> {
	public SOAPMessage invoke(SOAPMessage request) {
		try {	
			MessageFactory mf = MessageFactory.newInstance(SOAPConstants.SOAP_1_2_PROTOCOL);
			SOAPFactory sf = SOAPFactory.newInstance(SOAPConstants.SOAP_1_2_PROTOCOL);
			
			SOAPMessage response = mf.createMessage();
			SOAPBody respBody = response.getSOAPBody();
			Name bodyName = sf.createName("getStockPriceResponse");
			respBody.addBodyElement(bodyName);
			SOAPElement respContent = respBody.addChildElement("price");
			respContent.setValue("123.00");
			response.saveChanges();
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
