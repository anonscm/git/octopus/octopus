package de.tarent.octopus.prototype;

import java.io.ByteArrayInputStream;
import java.util.StringTokenizer;

import javax.annotation.Resource;
import javax.servlet.ServletRequest;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.ws.BindingType;
import javax.xml.ws.Provider;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.WebServiceProvider;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.http.HTTPBinding;
import javax.xml.ws.http.HTTPException;

@WebServiceProvider
@BindingType(value=HTTPBinding.HTTP_BINDING)
public class TestRestfulProvier implements Provider<Source> {
	@Resource
	protected WebServiceContext context;
	
	public Source invoke(Source request) {
		try {
			MessageContext mc = context.getMessageContext();
			// check for a PATH_INFO request
			String path = (String) mc.get(MessageContext.PATH_INFO);
			if (path != null && path.contains("/num1") && path.contains("/num2")) {
				return createResultSource(path);
			}
			
			String query = (String) mc.get(MessageContext.QUERY_STRING);
			System.out.println("Query String = " + query);
			ServletRequest req = (ServletRequest) mc.get(MessageContext.SERVLET_REQUEST);
			int num1 = Integer.parseInt(req.getParameter("num1"));
			int num2 = Integer.parseInt(req.getParameter("num2"));
			return createResultSource(num1 + num2);
		} catch (RuntimeException e) {
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
			throw new HTTPException(500);
		}
	}
	
	private Source createResultSource(String str) {
		StringTokenizer st = new StringTokenizer(str, "=&/");
		System.err.println(st.nextToken());
		System.err.println(st.nextToken());
		int number1 = Integer.parseInt(st.nextToken());
		System.err.println(st.nextToken());
		int number2 = Integer.parseInt(st.nextToken());
		int sum = number1 + number2;
		return createResultSource(sum);
	}
	
	private Source createResultSource(int sum) {
		String body = "<ns:addNumbersResponse xmlns:ns=\"http://java.duke.org\"><ns:return>" + sum + "</ns:return></ns:addNumbersResponse>";
		Source source = new StreamSource(new ByteArrayInputStream(body.getBytes()));
		return source;
	}
}
