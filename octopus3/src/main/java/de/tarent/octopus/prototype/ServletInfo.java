package de.tarent.octopus.prototype;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;

public class ServletInfo extends HttpServlet {
    private static final long serialVersionUID = -7643463487965261368L;

	@Override
	public void init(ServletConfig config) throws ServletException {
	    super.init(config);
	    
	    System.out.println("== config ==");
	    System.out.println("config" + config.getServletName());
	    System.out.println("config" + config.getServletContext().getContextPath());
	    System.out.println("config" + config.getServletContext().getServerInfo());
	    try {
		    for (Map.Entry<String, Object> entry : ((Map<String, Object>) BeanUtils.describe(config)).entrySet()) {
		    	System.out.println(entry + " = " + entry.getValue());
		    }
	    } catch (Exception e) {
	    	System.out.println(e);
	    }
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	    System.out.println("== request ==");
	    System.out.println("pathinfo = " + req.getPathInfo());
	    System.out.println("pathtranslated = " + req.getPathTranslated());
	    System.out.println("r uri " + req.getRequestURI());
	    System.out.println("r url " + req.getRequestURL());
	    //System.out.println("realpath " + req.getRealPath("."));
	    System.out.println("type " + req.getContentType());
	    System.out.println("contextpath " + req.getContextPath());
	    
	    try {
		    for (Map.Entry<String, Object> entry : ((Map<String, Object>) BeanUtils.describe(req)).entrySet()) {
		    	System.out.println(entry + " = " + entry.getValue());
		    }
	    } catch (Exception e) {
	    	System.out.println(e);
	    }
	    System.out.println("== response ==");
		   try {
		    for (Map.Entry<String, Object> entry : ((Map<String, Object>) BeanUtils.describe(resp)).entrySet()) {
		    	System.out.println(entry + " = " + entry.getValue());
		    }
	    } catch (Exception e) {
	    	System.out.println(e);
	    }
	}
}
