package de.tarent.octopus.prototype;

import javax.xml.transform.dom.DOMSource;
import javax.xml.ws.Provider;
import javax.xml.ws.ServiceMode;
import javax.xml.ws.WebServiceProvider;
import javax.xml.ws.Service.Mode;

@WebServiceProvider
@ServiceMode(value = Mode.PAYLOAD)
public class TestDomProvider implements Provider<DOMSource> {
	public DOMSource invoke(DOMSource request) {
		try {	
			DOMSource response = new DOMSource();
			
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
