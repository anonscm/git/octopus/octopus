package de.tarent.octopus.prototype;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.ws.Endpoint;
import javax.xml.ws.spi.Provider;

import org.apache.cxf.BusFactory;
import org.apache.cxf.frontend.ServerFactoryBean;
import org.apache.cxf.jaxws.EndpointImpl;
import org.apache.cxf.jaxws.spi.ProviderImpl;
import org.apache.cxf.transport.servlet.CXFServlet;
import org.codehaus.jettison.mapped.MappedXMLInputFactory;
import org.codehaus.jettison.mapped.MappedXMLOutputFactory;

public class OctopusServlet extends CXFServlet {
	private static final long serialVersionUID = -3965786246881138257L;
	
	@Override
	public void init(ServletConfig servletConfig) throws ServletException {
		BusFactory.setDefaultBus(getBus());
		
		super.init(servletConfig);
		
		// Create a mapping between the XML namespaces and the JSON prefixes.
		// The JSON prefix can be "" to specify that you don't want any prefix.
		Map<String, String> nstojns = new LinkedHashMap<String, String>() {
            private static final long serialVersionUID = 7783871512921748105L;

			@Override
			public String get(Object key) {
			    System.out.println("[nstojns] get " + key + " = " + super.get(key));
			    return super.get(key);
			}
			
			@Override
			public Set<Entry<String, String>> entrySet() {
			    System.out.println("[nstojns] get entry set: " + super.keySet());
			    return super.entrySet();
			}
			
			@Override
			public Set<String> keySet() {
			    System.out.println("[nstojns] get key set: " + super.keySet());
			    return super.keySet();
			}
		};
		nstojns.put("http://schemas.tarent.de/octopus", "octopus");
		MappedXMLInputFactory xif = new MappedXMLInputFactory(nstojns);
		MappedXMLOutputFactory xof = new MappedXMLOutputFactory(nstojns);
		
		Map<String, Object> properties = new LinkedHashMap<String, Object>() {
            private static final long serialVersionUID = 7783871512921748104L;

			@Override
			public Object get(Object key) {
			    System.out.println("[properties] get " + key + " = " + super.get(key));
			    return super.get(key);
			}
			
			@Override
			public Set<Entry<String, Object>> entrySet() {
			    System.out.println("[properties] get entry set: " + super.keySet());
			    return super.entrySet();
			}
			
			@Override
			public Set<String> keySet() {
			    System.out.println("[properties] get key set: " + super.keySet());
			    return super.keySet();
			}
		};
		properties.put("Content-Type", "text/plainn");
		properties.put(XMLInputFactory.class.getName(), xif);
		properties.put(XMLOutputFactory.class.getName(), xof);
		
		
		Endpoint jaxwsEndpoint = Endpoint.publish("/jaxws-simple-demo", new JAXWSSimpleDemoImpl());
		jaxwsEndpoint.setProperties(properties);
		
		Endpoint.publish("/test-restful-provider", new TestRestfulProvier()).setProperties(properties);
		Endpoint.publish("/test-soap-provider", new TestSoapProvider()).setProperties(properties);
		Endpoint.publish("/test-dom-provider", new TestDomProvider()).setProperties(properties);
		
		ProviderImpl provider = (ProviderImpl) Provider.provider();
		EndpointImpl endpoint = (EndpointImpl) provider.createEndpoint(null, new TestRestfulProvier());
		endpoint.publish("/test-restful-provider-d");
		endpoint.setProperties(properties);
		
		
		ServerFactoryBean serverFactoryBean = new ServerFactoryBean();
		serverFactoryBean.setBus(bus);
		serverFactoryBean.setServiceFactory(new OctopusServiceFactory());
		serverFactoryBean.setServiceClass(OctopusServlet.class);
		serverFactoryBean.setAddress("/octopus-reflection-test-1");
		serverFactoryBean.setProperties(properties);
		serverFactoryBean.setStart(true);
		serverFactoryBean.create();
		
		/*
		ProviderImpl provider2 = (ProviderImpl) Provider.provider();
		EndpointImpl endpoint2 = (EndpointImpl) provider2.createEndpoint(null, new TestRestfulProvier());
		System.err.println(endpoint2.getServiceFactory());
		endpoint2.setServiceFactory(new OctopusServiceFactory());
//		endpoint2.setInvoker(new Invoker() {
//			public Object invoke(Exchange exchange, Object o) {
//			    System.out.println("invoke: " + o);
//			    return null;
//			}
//		});
		endpoint2.publish("/octopus-reflection-test-2");
		*/
		
//		ApplicationContext applicationContext = null;
//		GenericApplicationContext childCtx = new GenericApplicationContext(applicationContext);
//		childCtx.refresh();
		
	}
	
	public void c1() {
		System.err.println("c1");
	}
	
	public void c2(Integer a, Integer b) {
		System.err.println("c2: " + a + ", " + b);
	}
}
