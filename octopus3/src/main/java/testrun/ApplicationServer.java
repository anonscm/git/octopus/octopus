package testrun;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;

import org.codehaus.cargo.container.Container;
import org.codehaus.cargo.container.ContainerType;
import org.codehaus.cargo.container.LocalContainer;
import org.codehaus.cargo.container.configuration.Configuration;
import org.codehaus.cargo.container.configuration.ConfigurationType;
import org.codehaus.cargo.container.deployable.DeployableType;
import org.codehaus.cargo.generic.ContainerFactory;
import org.codehaus.cargo.generic.DefaultContainerFactory;
import org.codehaus.cargo.generic.configuration.ConfigurationFactory;
import org.codehaus.cargo.generic.configuration.DefaultConfigurationFactory;
import org.codehaus.cargo.generic.deployable.DefaultDeployableFactory;
import org.codehaus.cargo.generic.deployable.DeployableFactory;

public class ApplicationServer {
	public static void main(String[] args) throws Exception {
	    
		String containerId = args[0];
		
		ConfigurationFactory configurationFactory = new DefaultConfigurationFactory();
		Configuration configuration = configurationFactory.createConfiguration(
				containerId, 
			    ContainerType.EMBEDDED,
			    ConfigurationType.STANDALONE);
		
		ContainerFactory containerFactory = new DefaultContainerFactory();
		Container container = containerFactory.createContainer(
				containerId,
				ContainerType.EMBEDDED,
				configuration);
		
		DeployableFactory deployableFactory = new DefaultDeployableFactory();
		
		System.out.println(configurationFactory);
		System.out.println(containerFactory);
		System.out.println();
		
		System.out.println(configuration.getClass());
		System.out.println(configuration);
		System.out.println();
		
		System.out.println(container);
		System.out.println(container.getName());
		
		
		LocalContainer localContainer = (LocalContainer) container;
		
		if (false && new File(System.getProperty("user.home"), "Desktop/juddi-web-2.0rc5.war").exists())
			localContainer.getConfiguration().addDeployable(deployableFactory.createDeployable(containerId,
					new File(System.getProperty("user.home"), "Desktop/juddi-web-2.0rc5.war").getAbsolutePath(), 
					DeployableType.WAR));
		
		if (false && new File(System.getProperty("user.home"), "Tools/apache-ode-war-1.1.1/ode.war").exists())
			localContainer.getConfiguration().addDeployable(deployableFactory.createDeployable(containerId,
					new File(System.getProperty("user.home"), "Tools/apache-ode-war-1.1.1/ode.war").getAbsolutePath(), 
					DeployableType.WAR));
		
		localContainer.getConfiguration().addDeployable(deployableFactory.createDeployable(containerId,
				"src/main/webapp", 
				DeployableType.WAR));
		localContainer.start();
		
		BufferedReader r = new BufferedReader(new InputStreamReader(System.in));
		while (!r.readLine().toUpperCase().contains("SHUTDOWN")) {
			// nothing
		}
		localContainer.stop();
    }
}
