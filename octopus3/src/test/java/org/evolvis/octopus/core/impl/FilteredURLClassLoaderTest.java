package org.evolvis.octopus.core.impl;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.net.URLClassLoader;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import de.tarent.application1.MathService;
import de.tarent.application2.StringService;

public class FilteredURLClassLoaderTest {
	protected URLClassLoader ucl1;
	protected URLClassLoader ucl2;
	
	@Before
	public void before() throws Exception {
		ucl1 = new FilteredURLClassLoader(
				new URL[] { new File("target/classes").toURI().toURL() },
				new String[] { "java.", "de.tarent.application1." });
		
		ucl2 = new FilteredURLClassLoader(
				new URL[] { new File("target/classes").toURI().toURL() },
				new String[] { "java.", "de.tarent.application2." });
	}
	
	@After
	public void after() {
		ucl1 = null;
		ucl2 = null;
	}
	
	@Test
	public void testAccess() throws Exception {
		Assert.assertNotNull(ucl1);
		Assert.assertNotNull(ucl2);
		
		Assert.assertNotNull(ucl1.getResource("de/tarent/application1/MathService.class"));
		Assert.assertNotNull(ucl2.getResource("de/tarent/application2/StringService.class"));

		Assert.assertNull(ucl1.getResource("de/tarent/application2/StringService.class"));
		Assert.assertNull(ucl2.getResource("de/tarent/application1/MathService.class"));
		
		Assert.assertNotNull(ucl1.loadClass("de.tarent.application1.MathService"));
		Assert.assertNotNull(ucl2.loadClass("de.tarent.application2.StringService"));
		
		Assert.assertNull(ucl1.loadClass("de.tarent.application2.StringService"));
		Assert.assertNull(ucl2.loadClass("de.tarent.application1.MathService"));
	}
	
	@Test(expected = InvocationTargetException.class)
	public void testAccessDenied() throws Exception {
		Class<?> c1 = ucl1.loadClass("de.tarent.application1.MathService");
		Class<?> c2 = ucl2.loadClass("de.tarent.application2.StringService");
		
		Object o1 = c1.newInstance();
		Object o2 = c2.newInstance();
		
		Assert.assertFalse(o1 instanceof MathService);
		Assert.assertFalse(o2 instanceof StringService);
		
		c1.getMethod("testApplication2Access").invoke(o1);
		c2.getMethod("testApplication1Access").invoke(o2);
	}
}
