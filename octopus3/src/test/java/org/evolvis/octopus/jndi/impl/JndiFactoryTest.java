package org.evolvis.octopus.jndi.impl;

import java.util.Properties;

import javax.naming.InitialContext;

import org.evolvis.octopus.core.jndi.impl.OctopusInitialContextFactory;
import org.junit.Test;
import static org.junit.Assert.*;

public class JndiFactoryTest {
	@Test
	public void simpleTest() throws Exception {
		
		Properties env = new Properties();
		env.setProperty("java.naming.factory.initial", OctopusInitialContextFactory.class.getName());
		env.setProperty("java.naming.provider.url", "/octopus/version");
		
		InitialContext ctx = new InitialContext(env);
		
		ctx.lookup("/octopus/version");
		
		assertEquals("asd", "asd");
	}
}
